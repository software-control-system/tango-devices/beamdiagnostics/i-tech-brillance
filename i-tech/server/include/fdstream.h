// $Id: fdstream.h,v 1.1 2006/03/01 10:37:48 miha Exp $

//! \file fdstream.h
//! file descriptor streambuf

#if !defined(_FDSTREAM_H)
#define _FDSTREAM_H

extern "C"
{
#include <unistd.h>
}
#include <istream>
#include <streambuf>

//--------------------------------------------------------------------------

/* fdbuf and fdiostream based on work from Nicolai M. Josuttis
 *
 * (C) Copyright Nicolai M. Josuttis 2001.
 * Permission to copy, use, modify, sell and distribute this software
 * is granted provided this copyright notice appears in all copies.
 * This software is provided "as is" without express or implied
 * warranty, and with no claim as to its suitability for any purpose.
 */

// ReadFuncType provides operator() with the same iface as ::read
// WriteFuncType provides operator() with the same iface as ::write

// input/output stream buffer class initialized with a file descriptor
template <typename ReadFuncType, typename WriteFuncType>
class fdbuf : public std::streambuf
{
protected:
	int fd;	// file descriptor

	/* data buffer:
	 * - at most, pbSize characters in putback area plus
	 * - at most, bufSize characters in ordinary read buffer
	 */
	static const int pbSize = 4;    // size of putback area
	const int bufSize;              // size of the data buffer
	char *buffer;                   // data buffer

	ReadFuncType read_func;
	WriteFuncType write_func;

public:
	/* constructor
	 * - initialize file descriptor
	 * - initialize empty data buffer
	 * - no putback area
	 * => force underflow()
	 */
	explicit fdbuf(int _fd, int size=BUFSIZ) : fd(_fd), bufSize(size) {

		buffer = new char [bufSize+pbSize];
		setg (buffer+pbSize,     // beginning of putback area
		      buffer+pbSize,     // read position
		      buffer+pbSize);    // end position
	}
	~fdbuf() {
		delete[] buffer;
	}
	// return associated file descriptor
	int descriptor() const { return this->fd; }

protected:
	// write one character
	virtual int_type overflow(int_type c) {
		if (c != EOF) {
			const char z(c);
			if (write_func(fd, &z, 1) != 1) {
				return EOF;
			}
		}
		return c;
	}

	// write multiple characters
	virtual std::streamsize xsputn(const char *s, std::streamsize num) {
		return write_func(fd, s, num);
	}

	// read characters into buffer, return one character
    virtual int_type underflow () {
		using std::memcpy;

		// is read position before end of buffer?
		if (gptr() < egptr()) {
			return traits_type::to_int_type(*gptr());
		}

		/* process size of putback area
		 * - use number of characters read
		 * - but at most size of putback area
		 */
		int numPutback = gptr() - eback();
		if (numPutback > pbSize) {
			numPutback = pbSize;
		}

		/* copy up to pbSize characters previously read into
		 * the putback area
		 */
		memcpy(buffer+(pbSize-numPutback), gptr()-numPutback,
		       numPutback);

		// read at most bufSize new characters
		const int num = read_func(fd, buffer+pbSize, bufSize);
		if (num <= 0) {
			// ERROR or EOF
			return EOF;
		}

		// reset buffer pointers
		setg (buffer+(pbSize-numPutback),    // beginning of putback area
				buffer+pbSize,               // read position
				buffer+pbSize+num);          // end of buffer

		// return next character
		return traits_type::to_int_type(*gptr());
    }
};

//--------------------------------------------------------------------------

template <typename ReadFuncType, typename WriteFuncType>
class fdiostream : public std::iostream
{
public:
	typedef fdbuf<ReadFuncType, WriteFuncType> streambuf_type;

protected:
	streambuf_type buf;

public:
	explicit fdiostream(int fd) : std::iostream(0), buf(fd) {
		rdbuf(&buf);
	}
};

//--------------------------------------------------------------------------

// stream buffer class initialized with a UDP socket descriptor
template <typename ReadFuncType, typename WriteFuncType>
class udpbuf : public fdbuf<ReadFuncType,WriteFuncType>
{
public:
	explicit udpbuf(int _fd, int size=BUFSIZ)
	: fdbuf<ReadFuncType,WriteFuncType>(_fd, size)
	{}
	~udpbuf() {}

	// return this on success, 0 otherwise
	udpbuf* attach(int _fd) {
		if (is_open()) return 0;
		this->fd = _fd;
		return this;
	}
	// return this
	udpbuf* detach() {
		this->fd = -1;
		return this;
	}
	bool is_open() const { return -1 != this->fd; }

	// return pointer to read functor
	ReadFuncType* reader() {
		return &this->read_func;
	}
	// return pointer to write functor
	WriteFuncType* writer() {
		return &this->write_func;
	}
};

#endif	// _FDSTREAM_H
