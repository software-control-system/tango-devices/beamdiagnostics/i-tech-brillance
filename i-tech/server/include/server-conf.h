// $Id: server-conf.h,v 1.1 2006/03/01 10:37:48 miha Exp $

//! \file server-conf.h
//! Default server configuration.

#if !defined(_SERVER_CONF_H)
#define _SERVER_CONF_H

// uncomment to run as a daemon
//#define DAEMON

// default TCP port to listen on
#define SERVER_DEFAULT_PORT 23271

// default multicast group address to send events to
#define SERVER_DEFAULT_MCAST_ADDR "224.0.1.240"

// default multicast TTL (1 = local subnet)
#define SERVER_DEFAULT_MCAST_TTL 1

// PID file pathname.
#define SERVER_PID_FILE "/var/run/liberad.pid"

// max. number of pending connections.
#define SERVER_MAX_BACKLOG 8

// idle connection timeout in seconds
#define SERVER_CONN_TIMEOUT 30

// max. data block size in a message in bytes
#define SERVER_MAX_DATABLOCK (250000*32)

// max. size of data-on-demand cache in samples(!)
#define SERVER_MAX_CACHESIZE 16384

#endif	// _SERVER_CONF_H
