// $Id: server-proxy.h,v 1.15 2006/05/16 06:50:55 miha Exp $

//! \file server-proxy.h
//! Header file for CSPI proxy.

#if !defined(_SERVER_PROXY_H)
#define _SERVER_PROXY_H

#include <list>

#include "cspi.h"
#include "protocol.h"

// fwd decl
class message;

//--------------------------------------------------------------------------

struct handle_adapter {
	explicit handle_adapter(CSPIHANDLE h=0, int t=0) : handle(h), type(t) {}
	~handle_adapter() {}

	operator CSPIHANDLE() { return handle; }
	int destroy() {

		int rc;
		if (CSPI_HANDLE_ENV == type) {
	
			rc = cspi_freehandle(CSPI_HANDLE_ENV, handle);
		}
		else {
			ASSERT(CSPI_HANDLE_CON == type);
			cspi_disconnect(handle);
			rc = cspi_freehandle(CSPI_HANDLE_CON, handle);
		}
		ASSERT(CSPI_OK == rc);
		return rc;
	}

protected:
	CSPIHANDLE handle;
	int type;
};

//--------------------------------------------------------------------------

inline handle_adapter mkadapter(int type, CSPIHANDLE handle)
{
	ASSERT(CSPI_HANDLE_ENV == type || CSPI_HANDLE_CON == type);
	return handle_adapter(handle, type);
}

//--------------------------------------------------------------------------

// CSPI proxy class
class proxy
{
public:
	proxy() {}
	// dtor; free CSPI resources not released by the user
	virtual ~proxy();

	// delegate to marshall(msg)
	int operator() (message& msg) { return marshall(msg); }

protected:
	/* A pointer to the marshalling member function taking
	 * a message and returning an integer (return code).
	 */
	typedef int (proxy::*marshalling_f)(message&);

	// map Libera messages to marshalling functions
	static marshalling_f table[];

	/* Top marshalling function. Calls a CSPI marshalling function.
	 * Returns a value returned by the CSPI call.
	 */
	virtual int marshall(message& msg) {

		// NOTE: assume no check is needed on message header
		const protocol::header *hdr = msg.header();

		// lookup marshalling function
		marshalling_f f = table[hdr->cmd];
		// call function on this object
		return (this->*f)(msg);
	}

private:

	// shortens declarations in code
	typedef std::list<handle_adapter> handle_list;
	// keep track of CSPI resources used by this object
	handle_list handles;

protected:
	/* Following are marshalling functions to delegate calls to the
	 * CSPI layer. A function deserializes call arguments from the
	 * message, calls CSPI function, then serializes the results back
	 * into the message.
	 *
	 * The data_size field of the message header is assigned the size
	 * of trailing data block in bytes. Returns a value returned by
	 * the CSPI call. Can throw a protocol_error or bad_alloc error.
	 *
	 * NOTE: the function should be passed message HEADER in host
	 *  byte order. Data block, on the other hand, is specific to
	 *  each message and should be left as-is, that is in network
	 *  byte order.
	 */
	int allochandle(message& msg);
	int freehandle(message& msg);
	int getlibparam(message& msg);
	int setlibparam(message& msg);
	int getenvparam(message& msg);
	int setenvparam(message& msg);
	int getenvparam_fa(message& msg);
	int setenvparam_fa(message& msg);
	int getconparam(message& msg);
	int setconparam(message& msg);
	int connect(message& msg);
	int disconnect(message& msg);
	int seek(message& msg);
	int read(message& msg);
	int read_ex(message& msg);
	int gettimestamp(message& msg);
	int get(message& msg);
	int settime(message& msg);
};

//--------------------------------------------------------------------------

/* Extends proxy class to handle server-specific functions in
 * addition to the CSPI functions.
 */
class proxy_ex : public proxy
{
public:
	proxy_ex() : proxy() {}
	~proxy_ex() {}

protected:
	/* A pointer to the marshalling member function taking
	 * a message and returning an integer (return code).
	 */
	typedef int (proxy_ex::*marshalling_f)( message& );

	// map Libera messages to server-specific marshalling functions
	static marshalling_f table_ex[];

	/* Overload base class marshalling function to handle
	 * server-specific messages. For CSPI messages, call
	 * base version instead. For server-specific messages,
	 * invokes the appropriate marshalling code.
	 */
	int marshall(message& msg) {

		// NOTE: assume no check is needed on message header
		const protocol::header *hdr = msg.header();
		if (protocol::is_valid_cspi_message(hdr->cmd)) {
			// delegate to base class
			return proxy::marshall(msg);
		}
		ASSERT(protocol::is_valid_server_message(hdr->cmd));
		_LOG_DEBUG("received server-specific message %u", hdr->cmd);

		// interpret as a server-specific message
		const size_t idx = hdr->cmd - protocol::LM_SERVER_FIRST - 1;

		// look up marshalling function
		marshalling_f f = table_ex[idx];
		// call function on this object
		return (this->*f)(msg);
	}

protected:
	/* A marshalling function for the NOOP (no operation) call.
	 * Always returns 0.
	 */
	int server_noop(message& msg);
	int server_setparam(message& msg);
	int server_getparam(message& msg);
};

#endif	// _SERVER_PROXY_H
