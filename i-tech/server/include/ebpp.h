// $Id: ebpp.h,v 1.1 2006/03/01 10:37:48 miha Exp $

//! \file ebpp.h
//! Declares EBPP specific functions and classes.

#if !defined(_SRV_EBPP_H)
#define _SRV_EBPP_H

#include "cspi.h"
#include "transform.h"

typedef CSPI_CONPARAMS_DD SUPER_CONPARAMS;

//--------------------------------------------------------------------------

// return sizeof a CSPI_CONPARAMS-derived structure
inline size_t sizeof_conparams(CSPI_BITMASK flags) {

	return flags & CSPI_CON_DEC ?
		sizeof(CSPI_CONPARAMS_DD) : sizeof(CSPI_CONPARAMS);
}

//--------------------------------------------------------------------------

inline SUPER_CONPARAMS& ntoh(SUPER_CONPARAMS& obj)
{
	typedef SUPER_CONPARAMS composite_type;
	typedef uint32_t        base_type;

	return transform<composite_type,base_type>(obj, ntoh);
}

//--------------------------------------------------------------------------

inline SUPER_CONPARAMS& hton(SUPER_CONPARAMS& obj)
{
	typedef SUPER_CONPARAMS composite_type;
	typedef uint32_t        base_type;

	return transform<composite_type,base_type>(obj, hton);
}

//--------------------------------------------------------------------------

// return data_traits object for connection hcon
const basic_traits* mktraits(int mode)
{
	if (CSPI_MODE_ADC == mode) {

		static const data_traits<CSPI_ADC_ATOM, short> adc_traits;
		return &adc_traits;
	}
	ASSERT(CSPI_MODE_DD == mode || CSPI_MODE_PM == mode);
	
	static const data_traits<CSPI_DD_ATOM, int> dd_traits;
	return &dd_traits;
}

#endif	// _SRV_EBPP_H
