// $Id: server-model.h,v 1.5 2006/06/12 07:24:17 miha Exp $

//! \file server-model.h
//! Header file for multithreaded server model with one thread per client.

#if !defined(_SERVER_MODEL_H)
#define _SERVER_MODEL_H

#include <stdexcept>
#include <vector>

#include "debug.h"
#include "error.h"
#include "lock.h"
#include "util.h"
#include "server-proxy.h"
#include "server-main.h"

// fwd decls
class worker_counter;
class pipe_of_death;
class event_transmitter;
class cache_iface;

// main pid
extern pid_t _process_id;

// declare one and only worker counter
extern worker_counter _workers;

// declare one and only pipe_of_death
extern pipe_of_death _pipe;

// declare one and only multicaster
extern event_transmitter _multicast;

// declare one and only data-on-demand cache
extern cache_iface _cache;

//--------------------------------------------------------------------------

// worker counter
class worker_counter {
public:
	worker_counter() : blocked(false), count(0) {}
	~worker_counter() {}

	// bump up worker count
	size_t operator++ () {

		auto_lock<mutex> lock(list_mutex);
		if (blocked) throw std::runtime_error("operation canceled");
		return ++count;
	}
	// decrement worker count
	size_t operator-- () {

		auto_lock<mutex> lock(list_mutex);
		return --count;
	}
	// block counter increments, return count
	int block() {

		auto_lock<mutex> lock(list_mutex);
		blocked = true;
		return count;
	}

private:
	bool blocked;	// is blocked?
	size_t count;
	mutex list_mutex;
};

//--------------------------------------------------------------------------

class server
{
public:
	explicit server(const config& cfg);
	~server() {
		_workers.block();
		// Worker threads are running in the detached state.
		// We need not wait for them to terminate.
	}

	// wait for new connection request or shutdown
	int wait() const {
		return wait_input(fd);	// delegate
	}
	// accept connection, return socket descriptor
	int accept() const;

	// create new worker thread for socket descriptor connfd
	void new_worker(int connfd) const;

private:
	int fd;	// descriptor (communication endpoint) to listen to

	// following shortens all typecasts of pointer arguments
	typedef struct sockaddr SA;
};

//--------------------------------------------------------------------------

// group together event daemon related ops
struct eventd
{
	// subscribe this process to events
	static bool subscribe() {
		/* TODO: All bits in the event mask are set to 1.
		* This should be replaced with an aggregate mask
		* for all active connections.
		*/
		return request(0xFFFF);
	}
	// unsubscribe this process, previously registered with subscibe()
	static bool unsubscribe(){
		return request(0x0000);
	}
private:
	// send request to eventd
	// return true on success, false on sys error
	static bool request(size_t mask);
};

//--------------------------------------------------------------------------

class message;	// fwd decl
class worker
{
public:
	worker() { ++_workers; }
	~worker() {
		if (0 == --_workers) eventd::unsubscribe();
	}

	message& dispatch(message& msg);

private:
	proxy_ex proxy;
};

//--------------------------------------------------------------------------

// data-on-demand cache
class data_cache
{
public:
	data_cache() : buf(0), state(unlockbit) {
		reset();
	}
	~data_cache() {}

	// cache states
	enum { unlockbit=0x0, lockbit=0x1 };
	// get cache state
	size_t rdstate() const { return state; }
	// set cache state, return old state
	size_t setstate(size_t st) {
		size_t tmp(state); state=st; return tmp;
	}

	const void* data() const { return &buf[0]; }
	void* data() { return &buf[0]; }

	// resize cache to accommodate size samples
	void resize(size_t size) {
		buf.resize(size);
		reset();
	}
	// return cache size in samples
	size_t size() const { return buf.size(); }
	// return true if cache empty
	size_t empty() const { return buf.empty(); }

	// reset timestamp and state
	void reset() {
		std::memset(&stamp, 0, sizeof(stamp));
		setstate(unlockbit);
	}
	CSPI_TIMESTAMP* timestamp() { return &stamp; }

	typedef CSPI_DD_RAWATOM element_type;

protected:
	std::vector<element_type> buf;
	// state bits
	size_t state;
	// cache timestamp
	CSPI_TIMESTAMP stamp;
};

//--------------------------------------------------------------------------

// implements thread-safe interface to data-on-demand cache
struct cache_iface
{
	cache_iface() {}
	~cache_iface() {}

	// resize to accommodate count samples, return cache size
	size_t resize(size_t count);
	// return cache size
	size_t size() {
		auto_lock<mutex> lock(cache_mutex);
		return cache.size();
	}
	// return true if cache empty
	bool empty() {
		auto_lock<mutex> lock(cache_mutex);
		return cache.empty();
	}
	// fill cache with data acquired on a trigger
	void fill();
	// get timestamp, return 0 if not avail.
	CSPI_TIMESTAMP* timestamp(CSPI_TIMESTAMP *ts) {

		auto_lock<mutex> lock(cache_mutex);
		if (!cache.empty()) {

			return (CSPI_TIMESTAMP *)
			       std::memcpy(ts, cache.timestamp(), sizeof(*ts));
		}
		return 0;	// cache disabled
	}
	// copy up to count samples to dest, return num. of samples copied
	size_t copy(data_cache::element_type *dest, size_t count);

	void lock(bool enable);
	bool locked() {
		auto_lock<mutex> lock(cache_mutex);
		return cache.rdstate() & data_cache::lockbit;
	}

protected:
	data_cache cache;
	mutex cache_mutex;
};

#endif	// _SERVER_MODEL_H
