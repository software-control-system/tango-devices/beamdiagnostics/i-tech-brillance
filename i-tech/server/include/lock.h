// $Id: lock.h,v 1.1 2006/03/01 10:37:48 miha Exp $

//! \file lock.h
//! Simple sync. and locking wrappers.

#if !defined(_LOCK_H)
#define _LOCK_H

extern "C" 
{
#include <pthread.h>
}
#include "debug.h"

template <class T>
struct auto_lock
{
	explicit auto_lock(T& obj) : sync(obj) {
		VERIFY(0 == sync.lock());
	}
	~auto_lock() {
		VERIFY(0 == sync.unlock());
	}

	private:
		T& sync;
};

//--------------------------------------------------------------------------

struct mutex
{
	mutex() { pthread_mutex_init(&m, 0); }
	~mutex() { VERIFY(0 == pthread_mutex_destroy(&m)); }

	int lock() { return pthread_mutex_lock(&m); }
	int unlock() { return pthread_mutex_unlock(&m); }

	// allow passing *this instead of pthread_mutex_t*
	operator pthread_mutex_t* () { return &m; }

	private:
		pthread_mutex_t m;
};

//--------------------------------------------------------------------------

struct cond
{
	explicit cond(pthread_mutex_t *p = 0) : m(p) {
		pthread_cond_init(&c, 0);
	}
	~cond() { VERIFY(0 == pthread_cond_destroy(&c)); }

	// mutex must be locked by the caller
	int lock() { return pthread_cond_wait(&c, m); }
	int unlock() { return 0; }

	// allow passing *this instead of pthread_cond_t*
	operator pthread_cond_t* () { return &c; }

	int signal() { return pthread_cond_signal(&c); }
	int broadcast() { return pthread_cond_broadcast(&c); }

	private:
		pthread_cond_t c;
		pthread_mutex_t *m;
};

#endif	// _LOCK_H
