// $Id: server-main.h,v 1.6 2006/03/01 10:56:41 miha Exp $

//! \file server-main.h
//! Main header file for the Libera Server.

#if !defined(_SERVER_MAIN_H)
#define _SERVER_MAIN_H

#include "server-conf.h"

// worker thread serving an active aconnection
void* worker_thread(void*);

//--------------------------------------------------------------------------

// program configuration, initialized from command-line
struct config
{
	config() :
		port(SERVER_DEFAULT_PORT),
		mcast_addr(SERVER_DEFAULT_MCAST_ADDR),
		mcast_ttl(SERVER_DEFAULT_MCAST_TTL)
		{}

	// server port to listen to
	in_port_t port;
	// multicast group address to send events to
	std::string mcast_addr;
	// multicast TTL
	u_char mcast_ttl;
};

#endif	// _SERVER_MAIN_H
