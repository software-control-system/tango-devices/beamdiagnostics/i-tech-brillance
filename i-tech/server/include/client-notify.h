// $Id: client-notify.h,v 1.3 2006/03/01 10:56:41 miha Exp $

//! \file client-notify.h
//! Header file for CSPI client library event receive and
//! dispatch mechanism.

#if !defined(_CLIENT_NOTIFY_H)
#define _CLIENT_NOTIFY_H

#include <list>
#include <algorithm>
#include <functional>

#include "cspi.h"
#include "lock.h"

// fdw decl
class subscriber_list;

// declare one and only event subscriber list
extern subscriber_list _list;

//--------------------------------------------------------------------------

/* Start event thread.
 * On success, returns 0. On error, returns error code.
 */
int multicast_connect( const char *mcast_addr, int mcast_port );

//--------------------------------------------------------------------------

/* Stop event thread.
 * Call after multicast_connect() to perform cleanup.
 */
void multicast_disconnect();

//--------------------------------------------------------------------------

// event subscriber
class subscriber {
public:
	subscriber() : handle(0) {
		std::memset(&params, 0, sizeof(params));
	}
	subscriber(CSPIHCON _handle, const CSPI_CONPARAMS *_params)
	: handle(_handle) {
		std::memcpy(&params, _params, sizeof(params));
	}
	~subscriber(){}

	CSPIHCON handle;
	CSPI_CONPARAMS params;
};

//--------------------------------------------------------------------------

class subscriber_list {
protected:
		typedef std::list<subscriber> list_type;
		
		list_type list;
		mutex list_mutex;

public:
	subscriber_list() {}
	~subscriber_list() {}
	
	// delete all subscribers
	void clear() {
		auto_lock<mutex> lock(list_mutex);
		list.clear();
	}
	// find a subscriber
	subscriber* find(CSPIHCON handle) {
		auto_lock<mutex> lock(list_mutex);
		list_type::iterator p;
		p = std::find_if(list.begin(),
						 list.end(),
						 std::bind2nd(equal_to(),handle));
		return p != list.end() ? &(*p) : 0;
	}
	// add new subscriber
	void subscribe(CSPIHCON handle, CSPI_CONPARAMS *params) {
		auto_lock<mutex> lock(list_mutex);
		list.push_back(subscriber(handle, params));
	}
	// remove existing subscriber
	void unsubscribe(CSPIHCON handle) {
		auto_lock<mutex> lock(list_mutex);
		list_type::iterator p;
		p = std::find_if(list.begin(),
		                 list.end(),
		                 std::bind2nd(equal_to(),handle));
		if (p != list.end()) {
			list.erase(p);
		}
	}
	// dispatch event to all subscribers
	void dispatch(const CSPI_EVENTHDR& hdr) {
		auto_lock<mutex> lock(list_mutex);
		if (!list.empty()) {

			CSPI_EVENT msg;
			std::memcpy(&msg, &hdr, sizeof(CSPI_EVENTHDR));
			for (list_type::iterator p=list.begin(); p!=list.end(); ++p) {

				const CSPI_CONPARAMS *q = &p->params;
				if (hdr.id & q->event_mask) {
					
					msg.user_data = q->user_data;
					if (q->handler && !q->handler(&msg)) break;
				}
			}
		}
	}

protected:
	struct equal_to
	: public std::binary_function<subscriber, CSPIHCON, bool> {

		bool operator() (const subscriber& obj, CSPIHCON handle) const {
			return obj.handle == handle;
		}
	};
};

#endif	// _CLIENT_NOTIFY_H
