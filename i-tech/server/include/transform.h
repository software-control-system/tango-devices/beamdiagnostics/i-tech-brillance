// $Id: transform.h,v 1.3 2006/12/18 08:23:27 ales Exp $

//! \file transform.h
//! Functions and classes to convert between host and network byte order.

#if !defined(_TRANSFORM_H)
#define _TRANSFORM_H

extern "C"
{
#include <netinet/in.h>
}
#include <algorithm>

#include "cspi.h"

#include "debug.h"
#include "error.h"
#include "protocol.h"

//--------------------------------------------------------------------------

/* Test byte ordering.
 * Return true on a little-endian system, false on a big-endian system.
 */
inline bool is_little_endian()
{
	const short int s = 1;
	return 1 == *((const char*) &s);
}

//--------------------------------------------------------------------------

// return as-is, no conversion needed
inline uint8_t ntoh(uint8_t val) { return val; }
inline uint8_t hton(uint8_t val) { return val; }

// return as-is, no conversion needed
inline int8_t ntoh(int8_t val) { return val; }
inline int8_t hton(int8_t val) { return val; }

// return as-is, no conversion needed
inline char ntoh(char val) { return val; }
inline char hton(char val) { return val; }

//--------------------------------------------------------------------------

// convert from network to host byte order
inline uint16_t ntoh(uint16_t val) { return ntohs(val); }
inline uint32_t ntoh(uint32_t val) { return ntohl(val); }

// convert from host to network byte order
inline uint16_t hton(uint16_t val) { return htons(val); }
inline uint32_t hton(uint32_t val) { return htonl(val); }
inline uint64_t hton(uint64_t val) {

	const uint64_t LOWORD = 0x00000000ffffffffLL;

	typedef uint64_t I64;
	return is_little_endian() ?
		// convert and swap both words
			(((I64) htonl(val & LOWORD)) << 32) | ((I64) htonl(val >> 32))
	:
		// return unchanged
			val;
}
inline uint64_t ntoh(uint64_t val) { return hton(val); }

//--------------------------------------------------------------------------

// prevent ambiguous call of overloaded ntoh
inline int16_t ntoh(int16_t val) { return ntohs(val); }
// prevent ambiguous call of overloaded ntoh
inline int32_t ntoh(int32_t val) {
	return ntoh(static_cast<uint32_t>(val));
}
// prevent ambiguous call of overloaded ntoh
inline long ntoh(long val) {
	return ntoh(static_cast<uint32_t>(val));
}
// prevent ambiguous call of overloaded ntoh
inline unsigned long ntoh(unsigned long val) {
	return ntoh(static_cast<uint32_t>(val));
}

// prevent ambiguous call of overloaded hton
inline int16_t hton(int16_t val) { return htons(val); }
// prevent ambiguous call of overloaded hton
inline int32_t hton(int32_t val) {
	return hton(static_cast<uint32_t>(val));
}
// prevent ambiguous call of overloaded hton
inline long hton(long val) {
	return hton(static_cast<uint32_t>(val));
}
// prevent ambiguous call of overloaded hton
inline unsigned long hton(unsigned long val) {
	return hton(static_cast<uint32_t>(val));
}

//--------------------------------------------------------------------------

// convert from network to host byte order
inline protocol::header& ntoh(protocol::header& hdr) {

	hdr.magic     = ntoh(hdr.magic);
	hdr.cmd       = ntoh(hdr.cmd);
	hdr.status    = ntoh(hdr.status);
	hdr.cmd_val   = ntoh(hdr.cmd_val);
	hdr.req_val   = ntoh(hdr.req_val);
	hdr.data_size = ntoh(hdr.data_size);

	return hdr;
}

// convert from host to network byte order
inline protocol::header& hton(protocol::header& hdr) {

	hdr.magic     = hton(hdr.magic);
	hdr.cmd       = hton(hdr.cmd);
	hdr.status    = hton(hdr.status);
	hdr.cmd_val   = hton(hdr.cmd_val);
	hdr.req_val   = hton(hdr.req_val);
	hdr.data_size = hton(hdr.data_size);

	return hdr;
}

//--------------------------------------------------------------------------

// prevent ambiguous call of overloaded ntoh(void*)
inline CSPIHANDLE ntoh(CSPIHANDLE val) {
	return (CSPIHANDLE) ntoh(reinterpret_cast<uint32_t>(val));
}
// prevent ambiguous call of overloaded hton(void*)
inline CSPIHANDLE hton(CSPIHANDLE val) {
	return (CSPIHANDLE) hton(reinterpret_cast<uint32_t>(val));
}

//--------------------------------------------------------------------------

// interpret DataType as a vector of BaseType elements
//  and transform it in-place with a unary operator
template <typename CompositeType, typename BaseType>
inline CompositeType&
transform(CompositeType& obj, BaseType (*op)(BaseType), size_t count=1)
{
	BaseType *begin = reinterpret_cast<BaseType*>(&obj);
	BaseType *end   = reinterpret_cast<BaseType*>(&obj + count);

	std::transform(begin, end, begin, op);
	return obj;
}

//--------------------------------------------------------------------------

inline CSPI_LIBPARAMS& ntoh(CSPI_LIBPARAMS& obj)
{
	typedef CSPI_LIBPARAMS composite_type;
	typedef uint32_t       base_type;

	return transform<composite_type,base_type>(obj, ntoh);
}

//--------------------------------------------------------------------------

inline CSPI_LIBPARAMS& hton(CSPI_LIBPARAMS& obj)
{
	typedef CSPI_LIBPARAMS composite_type;
	typedef uint32_t       base_type;

	return transform<composite_type,base_type>(obj, hton);
}

//--------------------------------------------------------------------------

inline CSPI_ENVPARAMS& ntoh(CSPI_ENVPARAMS& obj)
{
	typedef CSPI_ENVPARAMS composite_type;
	typedef uint32_t       base_type;

	return transform<composite_type,base_type>(obj, ntoh);
}

//--------------------------------------------------------------------------

inline CSPI_ENVPARAMS& hton(CSPI_ENVPARAMS& obj)
{
	typedef CSPI_ENVPARAMS composite_type;
	typedef uint32_t       base_type;

	return transform<composite_type,base_type>(obj, hton);
}

//--------------------------------------------------------------------------

inline CSPI_TIMESTAMP& hton(CSPI_TIMESTAMP& obj)
{
	// long
	obj.st.tv_sec  = hton(obj.st.tv_sec);
	obj.st.tv_nsec = hton(obj.st.tv_nsec);

	// unsigned long long
	obj.mt = hton(obj.mt);
	return obj;
}

//--------------------------------------------------------------------------

inline CSPI_TIMESTAMP& ntoh(CSPI_TIMESTAMP& obj)
{
	// long
	obj.st.tv_sec  = ntoh(obj.st.tv_sec);
	obj.st.tv_nsec = ntoh(obj.st.tv_nsec);

	// unsigned long long
	obj.mt = ntoh(obj.mt);
	return obj;
}

//--------------------------------------------------------------------------

inline CSPI_SETTIMESTAMP& hton(CSPI_SETTIMESTAMP& obj)
{
	// long
	obj.st.tv_sec  = hton(obj.st.tv_sec);
	obj.st.tv_nsec = hton(obj.st.tv_nsec);

	// unsigned long long
	obj.mt = hton(obj.mt);

	// unsigned long
	obj.phase = hton(obj.phase);
	return obj;
}

//--------------------------------------------------------------------------

inline CSPI_SETTIMESTAMP& ntoh(CSPI_SETTIMESTAMP& obj)
{
	// long
	obj.st.tv_sec  = ntoh(obj.st.tv_sec);
	obj.st.tv_nsec = ntoh(obj.st.tv_nsec);

	// unsigned long long
	obj.mt = ntoh(obj.mt);

	// unsigned long
	obj.phase = ntoh(obj.phase);
	return obj;
}

//--------------------------------------------------------------------------

inline CSPI_SA_ATOM& hton(CSPI_SA_ATOM& obj)
{
	typedef CSPI_SA_ATOM composite_type;
	typedef uint32_t     base_type;

	return transform<composite_type,base_type>(obj, hton);
}

//--------------------------------------------------------------------------

inline CSPI_SA_ATOM& ntoh(CSPI_SA_ATOM& obj)
{
	typedef CSPI_SA_ATOM composite_type;
	typedef uint32_t     base_type;

	return transform<composite_type,base_type>(obj, ntoh);
}

#endif	// _TRANSFORM_H
