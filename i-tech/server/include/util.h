// $Id: util.h,v 1.1 2006/03/01 10:34:19 miha Exp $

//! \file util.h
//! Common utility classes and functions that do not really
//! belong anywhere else.

#if !defined(_SERVER_UTIL_H)
#define _SERVER_UTIL_H

extern "C"
{
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
}
#include <cstdlib>

#include "cspi.h"

#include "debug.h"
#include "error.h"
#include "lock.h"
#include "protocol.h"
#include "fdstream.h"
#include "transform.h"
#include "socket-io.h"
#include "server-conf.h"

//--------------------------------------------------------------------------

class obytebuf
{
public:
	explicit obytebuf(uint8_t *buf) : pbase(buf), pptr(buf) {}
	~obytebuf() {}

	void* serialize(const void *p, size_t n) {
		std::memcpy(pptr, p, n);
		return pptr += n;
	}
	size_t nwritten() const { return pptr - pbase; }

protected:
	uint8_t *const pbase, *pptr;	// put base and put ptr
};

//--------------------------------------------------------------------------

class ibytebuf
{
public:
	explicit ibytebuf(const uint8_t *buf) : gbase(buf), gptr(buf) {}
	~ibytebuf() {}

	const void* deserialize(void *p, size_t n) {
		std::memcpy(p, gptr, n);
		return gptr += n;
	}
	size_t nread() const { return gptr - gbase; }

protected:
	const uint8_t *const gbase, *gptr;	// get base and get ptr
};

//--------------------------------------------------------------------------

template <typename T>
inline obytebuf& operator<< (obytebuf& obuf, const T& val) {
	T t(val);
	t = hton(t);
	obuf.serialize(&t, sizeof(t));
	return obuf;
}

//--------------------------------------------------------------------------

template <typename T>
inline ibytebuf& operator>> (ibytebuf& obuf, T& val) {
	obuf.deserialize(&val, sizeof(val));
	val = ntoh(val);
	return obuf;
}

//--------------------------------------------------------------------------

// interface to data traits for various acq. modes
struct basic_traits
{
	basic_traits() {}
	virtual ~basic_traits() {}

	virtual size_t size() const = 0;
	virtual void* xntoh(void *begin, size_t count) const = 0;
	virtual void* xhton(void *begin, size_t count) const = 0;
};

//--------------------------------------------------------------------------

// adds generic functions to convert between host and network
// byte order a vector of data elements
template <typename CompositeType, typename BaseType>
struct data_traits : public basic_traits {

	size_t size() const {
		return sizeof(CompositeType);
	}
	void* xntoh(void *first, size_t count) const {

		CompositeType *p = static_cast<CompositeType*>(first);
		transform<CompositeType,BaseType>(*p, ntoh, count);
		return first;
	}
	void* xhton(void *first, size_t count) const {

		CompositeType *p = static_cast<CompositeType*>(first);
		transform<CompositeType,BaseType>(*p, hton, count);
		return first;
	}
};

//--------------------------------------------------------------------------

/* Set timeout on all read and write operations to a socket descriptor.
 * On success, return 0. On error, return -1.
 */
int set_socket_timeout(int fd, size_t timeout, bool no_rcvtimeo=false);

//--------------------------------------------------------------------------

/* Wait for input on a file descriptor or pipe-of-death.
 * Return 0 in former, -1 in later case.
 */
int wait_input(int fd, struct timeval *timeout = 0);

//--------------------------------------------------------------------------

// used to cancel all threads from main thread
class pipe_of_death
{
public:
	static const char char_of_death = 0x1B;

	pipe_of_death() {
		if (-1 == pipe(fd)) throw SYS_ERROR("pipe");
	}
	~pipe_of_death() {
}

	enum pipe_end {read_end = 0, write_end = 1};
	int operator[] (pipe_end end) const { return fd[end]; }

private:
	int fd[2];
};

//--------------------------------------------------------------------------

class message
{
public:
	message() : msg(0), capacity(0) {

		const size_t initial_size = 64 * sizeof(uint32_t);
		reserve(initial_size);	// the memory is set to zero
	}
	~message() {
		free(msg);
	}

	// return pointer to message header
	protocol::header* header() { return &msg->hdr; }
	const protocol::header* header() const { return &msg->hdr; }

	// return pointer to message data (one past the header)
	uint8_t* data() { return msg->data; }
	const uint8_t* data() const { return msg->data; }

	/* Get message status:
	 * incoming message: 0 = no reply, else need reply
	 * outgoing message: 0 = success, else failed
	 */
	uint8_t status() const { return header()->status; }
	// delegate to status()
	operator void*() const {
		return reinterpret_cast<void *>(status());
	}

	/* Reserve internal memory for at least a header and num data
	 * bytes. If num is less than the actual capacity, the call
	 * no effect. Each reallocation invalidates all pointers.
	 */
	void reserve(size_t num);

protected:
	/* Note: msg is allocated a block of memory large enough to
	 * accomodate the header AND a variable-size data section.
	 * Thus, the message can be passed (i.e. to a C function) as
	 * a single, continuous memory block.
	 */
	protocol::message *msg;

	// number of bytes the message may contain without realloc
	size_t capacity;

private:
	// NOOP. Prevent compiler from creating a default assignment op.
	message& operator= (const message&);
};

std::ostream& operator <<(std::ostream& os, message &msg);
std::istream& operator >>(std::istream& is, message &msg);

//--------------------------------------------------------------------------

// read functor
class socket_read {
public:
	socket_read() {}
	~socket_read() {}

	// prevent system read to block
	ssize_t operator() (int fd, void *buf, size_t count) {

		struct timeval tv = {SERVER_CONN_TIMEOUT, 0};
		ssize_t rc = wait_input(fd, &tv);
		if (0 == rc) {
			do {
				rc = ::read(fd, (void *)buf, (int)count);
			} while (-1 == rc && EINTR == rc);
		}
		return rc;
	}
};

//--------------------------------------------------------------------------

// write functor
class socket_write {
public:
	socket_write() {}
	~socket_write() {}

	ssize_t operator() (int fd, const void *buf, size_t count) {
		return writen(fd, buf, count);
	}
};

//--------------------------------------------------------------------------

// noop (dummy) read/write functor
class socket_noop {
public:
	explicit socket_noop() {}
	~socket_noop() {}

	ssize_t operator() (int, void*, size_t) {
		ASSERT(false);
		return 0;
	}
	ssize_t operator() (int, const void*, size_t) {
		ASSERT(false);
		return 0;
	}
};

//--------------------------------------------------------------------------

// synonim for udp read/write operations
typedef socket_noop udp_noop;

//--------------------------------------------------------------------------

// read functor
class udp_read {
public:
	udp_read() {}
	~udp_read() {}

	// receive a message from a socket
	ssize_t operator() (int fd, void *buf, size_t count) {
		
		ssize_t rc;
		do {
			rc = recvfrom(fd, buf, count, 0, 0, 0);
		} while (-1 == rc && EINTR == rc);

		/* NOTE: We assume that count is much less than
		* the UDP max datagram size and therefore do not
		* have to deal with fragmented reads!
		*/
		ASSERT(rc < 576);

		return rc;
	}
};

//--------------------------------------------------------------------------

// write functor
class udp_write {
public:
	// following shortens all typecasts of pointer arguments
	typedef struct sockaddr SA;
	SA addr;    // destination

	explicit udp_write(const SA *sa=0) {
		set_addr(sa);
	}
	~udp_write() {}

	const SA* set_addr(const SA *sa){
		void *p = sa ? std::memcpy(&addr, sa, sizeof(addr)) : 0;
		return static_cast<const SA*>(p);
	}
	// transmit to destination address
	ssize_t operator() (int fd, const void *buf, size_t count) {
		ssize_t rc;
		do {
			rc = sendto(fd, buf, count, 0, &addr, sizeof(addr));
		} while (-1 == rc && EINTR == errno);
		return rc;
	}
};

//--------------------------------------------------------------------------

// implements thread-safe event input interface
class event_receiver
{
public:
	event_receiver() : udp(-1) {}
	~event_receiver() {}

	/* Open UDP multicast socket.
		* On success, return 0. On error, return -1.
		*/
	void open(const char* group, in_port_t port);

	// close UDP socket descriptor
	void close() {
		auto_lock<mutex> lock(udp_mutex);
		int rc;
		do {
			rc = ::close(udp.descriptor());
		}
		while (-1==rc && EINTR==errno);
		VERIFY(udp.detach());
	}

	// receive multicast event, return the number of chars read
	int recv(void *msg, size_t len) {
		auto_lock<mutex> lock(udp_mutex);
		return udp.sgetn(static_cast<char*>(msg), len);
	}

	// return streambuf
	typedef udpbuf<udp_read,udp_noop> udpbuf_type;
	const udpbuf_type* rdbuf() const {
		return &udp;
	}

protected:
	udpbuf_type udp;
	mutex udp_mutex;
};

//--------------------------------------------------------------------------

// implements thread-safe event output interface
class event_transmitter
{
public:
	event_transmitter() : udp(-1, 0) {}
	~event_transmitter() {}

	/* Open UDP multicast socket.
	 * On success, return 0. On error, return -1.
	 */
	void open(const char* group, in_port_t port, u_char ttl);

	// close UDP socket descriptor
	void close() {
		auto_lock<mutex> lock(udp_mutex);
		int rc;
		do {
			rc = ::close(udp.descriptor());
		}
		while (-1==rc && EINTR==errno);
		VERIFY(udp.detach());
	}

	// transmit a multicast message, return the number of chars written
	int send(const void *msg, size_t len) {
		auto_lock<mutex> lock(udp_mutex);
		return udp.sputn(static_cast<const char*>(msg), len);
	}
	// delegate to send
	int operator() (const CSPI_EVENTHDR *p) {
		const CSPI_EVENTHDR h = { hton(p->id), hton(p->param) };
		return send(&h, sizeof(h));
	}

	/* Allow this objects to be tested in control structures in
	 * a short and idiomatic way for its current state:
	 * if (multicaster) ...
	 */
	operator void*() const { return (void *)udp.is_open(); }

protected:
	udpbuf<udp_noop,udp_write> udp;
	mutex udp_mutex;
};

#endif	// _SERVER_UTIL_H
