// $Id: error.h,v 1.9 2006/03/06 14:21:58 miha Exp $

//! \file error.h
//! Declares error handling functions, classes and debugging macros
//! common to all application modules.

#if !defined(_ERROR_H)
#define _ERROR_H

#include <cerrno>
#include <string>
#include <exception>

#define SYS_ERROR(what) \
	sys_error(what, __FILE__, __FUNCTION__, __LINE__)

#define PTHREAD_ERROR(what,retval) \
	sys_error(what, __FILE__, __FUNCTION__, __LINE__, retval)

#define CSPI_ERROR(what) \
	cspi_error(what, __FILE__, __FUNCTION__, __LINE__)

//--------------------------------------------------------------------------

// generic server errors start at SRV_E_FIRST to avoid conflicts with CSPI
enum {
	SRV_E_FIRST 	= -128,
	SRV_E_PROTO,	// protocol mismatch
	SRV_E_INVAL,	// invalid argument
	SRV_E_LAST
};

//--------------------------------------------------------------------------

// thrown to force program to terminate
struct force_exit {
	explicit force_exit(int val) throw() : status(val) {}
	~force_exit() throw() {}

	int status;
};

//--------------------------------------------------------------------------

// thrown to terminate connection
class protocol_error : public std::exception
{
public:
	protocol_error() throw() {}
	~protocol_error() throw() {}
};

//--------------------------------------------------------------------------

// system call error, togehther with location in code
class sys_error : public std::exception
{
public:
	explicit
	sys_error(const char *what,
              const char *file,
              const char *function,
              int line,
              int retval = errno);
	~sys_error() throw() {}

	virtual const char* what() const throw() { return str.c_str(); }

private:
	std::string str;
};

//--------------------------------------------------------------------------

// cspi error, together with location in code
class cspi_error : public std::exception
{
public:
	explicit
	cspi_error(int what,
               const char *file,
               const char *function,
               int line);
	~cspi_error() throw() {}

	virtual const char* what() const throw() { return str.c_str(); }

private:
	std::string str;
};

#endif	// _ERROR_H
