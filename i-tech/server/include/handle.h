// $Id: handle.h,v 1.1 2006/03/01 10:37:48 miha Exp $

//! \file handle.h
//! Simple CSPI handle wrappers.

#if !defined(_HANDLE_H)
#define _HANDLE_H

#include "cspi.h"
#include "error.h"

// represents cspi handle
template <typename cspiH>
struct cspi_handle
{
	explicit cspi_handle(cspiH h=0) : handle(h) {}
	virtual ~cspi_handle() {}

	operator cspiH() { return handle; }
	cspiH handle;

	private:
		cspi_handle(cspi_handle<cspiH>&) {}
};

//--------------------------------------------------------------------------

// represents cspi environment handle
struct cspihenv : public cspi_handle<CSPIHENV>
{
	// uses "construction is initialization" pattern
	explicit cspihenv(bool as_su=0) : cspi_handle<CSPIHENV>() {

		CSPI_LIBPARAMS lib = {1,1};
		if (as_su) cspi_setlibparam(&lib, CSPI_LIB_SUPERUSER);
	
		int rc = cspi_allochandle(CSPI_HANDLE_ENV, 0, &handle);
		if (CSPI_OK != rc) throw CSPI_ERROR(rc);
	};

	~cspihenv() {

		int rc = cspi_freehandle(CSPI_HANDLE_ENV, handle);
		if (CSPI_OK != rc) throw CSPI_ERROR(rc);
	}
};

//--------------------------------------------------------------------------

// represents cspi connection handle
struct cspihcon : public cspi_handle<CSPIHCON>
{
	// uses "construction is initialization" pattern
	explicit cspihcon(CSPIHENV henv) : cspi_handle<CSPIHCON>() {

		int rc = cspi_allochandle(CSPI_HANDLE_CON, henv, &handle);
		if (CSPI_OK != rc) throw CSPI_ERROR(rc);
	};

	~cspihcon() {

		int rc = cspi_freehandle(CSPI_HANDLE_CON, handle);
		if (CSPI_OK != rc) throw CSPI_ERROR(rc);
	}
};

//--------------------------------------------------------------------------

struct auto_connect
{
	explicit auto_connect(CSPIHCON handle, CSPI_MODE mode) : hcon(handle) {
		CSPI_CONPARAMS p;
		p.mode = mode;

		int rc = cspi_setconparam(hcon, &p, CSPI_CON_MODE);
		if (CSPI_OK != rc) throw CSPI_ERROR(rc);

		rc = cspi_connect(hcon);
		if (CSPI_OK != rc) throw CSPI_ERROR(rc);
	}
	~auto_connect() {
		int rc = cspi_disconnect(hcon);
		if (CSPI_OK != rc) throw CSPI_ERROR(rc);
	}
protected:
	CSPIHCON hcon;
};

#endif	// _HANDLE_H
