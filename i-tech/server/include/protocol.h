// $Id: protocol.h,v 1.13 2006/03/02 10:04:32 miha Exp $

//! \file protocol.h
//! Header file for Libera Server protocol.

#if !defined(_PROTOCOL_H)
#define _PROTOCOL_H

extern "C" {
#include <stdint.h>	// uint16_t, uint32_t, ...
}
#include "cspi.h"

namespace protocol
{
	// server magic numbers
	enum {
		MAGIC = 23799,
	};

	/* List of CSPI and server messages.
	 * CSPI messages: LM_CSPI_FIRST ... LM_CSPI_LAST
	 * Server messages: LM_SERVER_FIRST ... LM_SERVER_LAST
	 */
	enum {
		LM_FIRST = -1,
		LM_CSPI_FIRST = LM_FIRST,

		LM_GETLIBPARAM,
		LM_SETLIBPARAM,

		LM_ALLOCHANDLE,
		LM_FREEHANDLE,

		LM_GETENVPARAM,
		LM_SETENVPARAM,

		LM_GETENVPARAM_FA,
		LM_SETENVPARAM_FA,

		LM_GETCONPARAM,
		LM_SETCONPARAM,

		LM_CONNECT,
		LM_DISCONNECT,

		LM_SEEK,
		LM_READ,
		LM_READ_EX,
		LM_GETTIMESTAMP,

		LM_GET,

		LM_SETTIME,

		LM_CSPI_LAST,
		/* -------------------------------- */
		LM_SERVER_FIRST = LM_CSPI_LAST,

		LM_SERVER_NOOP,
		LM_SERVER_SETPARAM,
		LM_SERVER_GETPARAM,

		LM_SERVER_LAST,
		LM_LAST = LM_SERVER_LAST
	};

	// list of available request codes to accompany a
	//  LM_SERVER_SET/GETPARAM message.
	typedef enum {
		// data-on-demand cache size
		SERVER_CACHE_SIZE = 1,
		// lock (freeze) or unlock the cache
		SERVER_CACHE_LOCK,
	}
	SERVER_PARAMS;

	// list of server specific async. events
	// (struct CSPI_EVENTHDR's 'id' member variable)
	typedef enum {
		// data-on-demand not cached error
		SERVER_EVENT_NOCACHE = CSPI_EVENT_USER,
	}
	SERVER_EVENTMASK;

	// message header
	struct header {

		// magic number
		uint16_t magic;
		// CSPI command
		uint16_t cmd;
		// request or reply status
		uint8_t  status;
		// command-specific value
		int32_t cmd_val;
		/* Request-specific value Not interpreted by the server
		 * and included as-is in the reply.
		 */
		int32_t req_val;
		// The size of the trailing data block in bytes.
		uint32_t data_size;
	};

	/* Generic server message.
	 * A message is composed of a fixed-size header, describing
	 * the request (or reply), and an optional variable-size 
	 * binary data stream.
	 */
	struct message {
		struct header hdr;
		// optional data associated with message
		u_int8_t data[1];
	};

	// return 1 if id represents a valid message, 0 otherwise
	inline bool is_valid_message(int id) {
		return LM_FIRST < id && id < LM_LAST;
	}

	// return 1 if id represents a valid CSPI message, 0 otherwise
	inline bool is_valid_cspi_message(int id) {
		return LM_CSPI_FIRST < id && id < LM_CSPI_LAST;
	}

	// returns 1 if id represents a valid server message, 0 otherwise
	inline bool is_valid_server_message(int id) {
		return LM_SERVER_FIRST < id && id < LM_SERVER_LAST;
	}

	// validate message header; returns 1 on success, 0 otherwise
	inline bool is_valid_header(const header *h) {
		return (MAGIC == h->magic) && is_valid_message(h->cmd);
	}

};	// namespace protocol

#endif	// _PROTOCOL_H
