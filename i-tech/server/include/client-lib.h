// $Id: client-lib.h,v 1.12 2006/03/01 10:56:41 miha Exp $

//! \file client-lib.h
//! Main header file for the CSPI client library.

#if !defined(_CLIENT_LIB_H)
#define _CLIENT_LIB_H

#include "cspi.h"
#include "protocol.h"		// SERVER_... request codes
#include "client-proxy.h"

#endif	// _CLIENT_LIB_H
