// $Id: bbfp.h,v 1.1 2006/03/08 07:58:42 miha Exp $

//! \file bbfp.h
//! Declares BBFP specific functions and classes.

#if !defined(_SRV_BBFP_H)
#define _SRV_BBFP_H

#include "cspi.h"
#include "transform.h"

typedef CSPI_CONPARAMS_DD SUPER_CONPARAMS;

//--------------------------------------------------------------------------

// return sizeof a CSPI_CONPARAMS-derived structure
inline size_t sizeof_conparams(CSPI_BITMASK flags) {

	return flags & CSPI_CON_STEP ?
		sizeof(CSPI_CONPARAMS_DD) : sizeof(CSPI_CONPARAMS);
}

//--------------------------------------------------------------------------

inline SUPER_CONPARAMS& ntoh(SUPER_CONPARAMS& obj)
{
	typedef SUPER_CONPARAMS composite_type;
	typedef uint32_t        base_type;

	return transform<composite_type,base_type>(obj, ntoh);
}

//--------------------------------------------------------------------------

inline SUPER_CONPARAMS& hton(SUPER_CONPARAMS& obj)
{
	typedef SUPER_CONPARAMS composite_type;
	typedef uint32_t        base_type;

	return transform<composite_type,base_type>(obj, hton);
}

//--------------------------------------------------------------------------

// return data_traits object for connection hcon
const basic_traits* mktraits(int mode)
{
	ASSERT(CSPI_MODE_DD == mode);
	static const data_traits<CSPI_DD_RAWATOM, CSPI_DD_RAWATOM> dd_traits;
	return &dd_traits;
}

#endif	// _SRV_BBFP_H
