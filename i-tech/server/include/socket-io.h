// $Id: socket-io.h,v 1.4 2006/03/01 10:56:41 miha Exp $

//! \file socket-io.h
//! Declares functions to read or write to a socket stream.

#if !defined(_SOCKET_IO_H)
#define _SOCKET_IO_H

/* Read n bytes from a socket stream.
 * On success, returns the number of bytes read. On error,
 * -1 is returned, and errno is set appropriately.
 * Used to read from a stream socket (a TCP socket). See A. Stevens'
 * UNIX Network Programming (section 3.9) for more information.
 */
ssize_t readn( int fd, void *vptr, size_t n );

/* Write n bytes to a socket stream.
 * On success, returns the number of bytes written. On error,
 * -1 is returned, and errno is set appropriately.
 * Used to write to a stream socket (a TCP socket). See A. Stevens'
 * UNIX Network Programming (section 3.9) for more information.
 */
ssize_t writen( int fd, const void *vptr, size_t n );

#endif	// _SOCKET_IO_H
