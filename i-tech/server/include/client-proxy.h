// $Id: client-proxy.h,v 1.3 2006/03/15 09:04:45 miha Exp $

//! \file client-proxy.h
//! Header file for the CSPI client library.

#if !defined(_CLIENT_PROXY_H)
#define _CLIENT_PROXY_H

extern "C"
{
#include <netinet/in.h>	// in_port_t
}

//--------------------------------------------------------------------------

/** Open connection to a Libera server. This is a
 *  server extension with no real CSPI counterpart.
 *  Return 0 on success, or -1 if an error occurred.
 *  The errno is set appropriately.
 *  If any of the port, mcast_addr or mcast_port is 0, a
 *  default value is used instead.
 *  @param addr Server IP address.
 *  @param port Server port.
 *  @param mcast_addr Multicast group address to join to.
 *  @param mcast_port Not implemented (must be 0).
 */
int server_connect(const char *addr, in_port_t port,
                   const char *mcast_addr, in_port_t mcast_port);

/** Closes open connection to Libera server. This is a
 *  server extension with no real CSPI counterpart.
 *  Returns 0 on success, or -1 if an error occurred.
 *  The errno is set appropriately.
 */
int server_disconnect();

/** No operation (NOOP). This is a server extension with no
 *  real CSPI counterpart.
 *  On success, returns 0. On error, returns -1. The errno
 *  is set appropriately.
 */
int server_noop();

/** Manipulates server-specific parameters. This is a server extension
 *  with no real CSPI counterpart.
 *  On success, returns 0. On error, returns < 0:
 *  -1 (the errno is set appropriately),
 *  SRV_E_PROTO,
 *  SRV_E_INVAL.
 *  @param code Parameter code (request code). See SERVER_PARAMS
 *              definition for a list of possible values.
 *  @param val  Untyped pointer to a variable with request code
 *              specific value. The type of the variable depends on
 *              the request code:
 *              ---------------------------------------------------------
 *              code              | type    | value description
 *              ---------------------------------------------------------
 *              SERVER_CACHE_SIZE | integer | cache size in samples
 *              SERVER_CACHE_LOCK | integer | unlock if 0, lock otherwise
 *              ---------------------------------------------------------
 */
int server_setparam(int code, const void *val);

/** Retrieves server-specific parameter value. This is a server
 *  extension with no real CSPI counterpart.
 *  See server_setparam for description of return values and
 *  function arguments.
 */
int server_getparam(int code, void *val);

#endif	// _CLIENT_PROXY_H
