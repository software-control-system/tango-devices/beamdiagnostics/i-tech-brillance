/* $Id: libera_pipe.h,v 1.4 2006/01/03 10:29:46 ales Exp $ */

//! \file libera_pipe.h 
//! Libera GNU/Linux driver: Interface to pipes, mostly used in SA

/*
LIBERA - Libera GNU/Linux device driver
Copyright (C) 2004-2006 Instrumentation Technologies

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or visit http://www.gnu.org
*/


#ifndef _LIBERA_PIPE_H_
#define _LIBERA_PIPE_H_


ssize_t
libera_pipe_read(struct file *filp, char *buf, size_t count, loff_t *ppos);

ssize_t
libera_pipe_write(struct inode *inode, const char *buf, size_t count);

int
libera_pipe_release(struct inode *inode, int decr, int decw);

int
libera_pipe_read_release(struct inode *inode, struct file *filp);

int
libera_pipe_write_release(struct inode *inode, struct file *filp);

int
libera_pipe_rdwr_release(struct inode *inode, struct file *filp);

int
libera_pipe_read_open(struct inode *inode, struct file *filp);

int
libera_pipe_write_open(struct inode *inode, struct file *filp);

int
libera_pipe_rdwr_open(struct inode *inode, struct file *filp);

struct inode* 
libera_pipe_new(struct inode* inode);



#endif 
