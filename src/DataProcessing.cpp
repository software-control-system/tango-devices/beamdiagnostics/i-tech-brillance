//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include "DataProcessing.h"
#include "TimeUtils.h"

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
# include "ArmOptimization.h"
#endif

// ============================================================================
//- select cordic algorithm implementation
// ============================================================================
#if defined(_EMBEDDED_DEVICE_) &&  defined(_USE_ARM_OPTIMIZATION_)
//- arm optimization: use M. Abbott's asm cordic algorithm implementation
#   define CORDIC_MAG ::CordicMagnitude
#else
//- no arm optimization: use the CSPI ordic algorithm implementation 
#   define CORDIC_MAG bpm::cspi_cordic_amp 
#endif

namespace bpm
{

// ============================================================================
// DataProcessing::compute_pos for Turn-by-Turn data (DD)
// ============================================================================
void DataProcessing::compute_pos (DDData & _dd_data,
                                  const DDRawBuffer & _dd_raw_buffer,
                                  size_t _actual_num_samples,
                                  const BPMConfig & _config)
{
  switch (_config.get_pos_algorithm())
  {
    //------------------------------------------------------------------------------
    //- use std algorithms
    case BPMConfig::kSTD_POS_ALGO:
      {
        switch(_config.get_block_geometry())
        {
          case BPMConfig::BLOCK_GEOMETRY_45:
            DataProcessing::compute_pos_block_45 (_dd_data,
                                                  _dd_raw_buffer,
                                                  _actual_num_samples,
                                                  _config);
            break;
          case BPMConfig::BLOCK_GEOMETRY_90:
            DataProcessing::compute_pos_block_90 (_dd_data,
                                                  _dd_raw_buffer,
                                                  _actual_num_samples,
                                                  _config);
            break;
          case BPMConfig::BLOCK_GEOMETRY_UNKNOWN:
            Tango::Except::throw_exception(_CPTC("unexpected configuration"),
                                           _CPTC("unknown BPM buttons configuration"),
                                           _CPTC("DataProcessing::compute_pos"));
            break;
        }
      }
      break;
    //------------------------------------------------------------------------------
    //- use ESRF SR algorithm
    case BPMConfig::kESRF_SR_POS_ALGO:
      {
        DataProcessing::compute_pos_esrf_sr (_dd_data,
                                             _dd_raw_buffer,
                                             _actual_num_samples,
                                             _config);
      }
      break;
    //------------------------------------------------------------------------------
    //- ohoh!
    default:
      Tango::Except::throw_exception(_CPTC("unexpected configuration"),
                                     _CPTC("unknown position computation algorithm"),
                                     _CPTC("DataProcessing::compute_pos"));
      break;
  }
}

// ============================================================================
// DataProcessing::compute_pos for slow acquisition data (SA)
// ============================================================================
void DataProcessing::compute_pos (SAData & _data, const BPMConfig & _config)
{
  //- compute pos or use FPGA data?
  if (! _config.local_sa_pos_computation_enabled())
  {
    //- just convert from nm to mm
    _data.x_ *= 1.e-6;
    _data.z_ *= 1.e-6;
    _data.q_ *= 1.e-6; 
    return;
  }

  switch (_config.get_pos_algorithm())
  {
    //------------------------------------------------------------------------------
    //- use std algorithms
    case BPMConfig::kSTD_POS_ALGO:
      {
        switch(_config.get_block_geometry())
        {
          case BPMConfig::BLOCK_GEOMETRY_45:
            DataProcessing::compute_pos_block_45 (_data, _config);
            break;
          case BPMConfig::BLOCK_GEOMETRY_90:
            DataProcessing::compute_pos_block_90 (_data, _config);
            break;
          case BPMConfig::BLOCK_GEOMETRY_UNKNOWN:
            Tango::Except::throw_exception(_CPTC("unexpected configuration"),
                                           _CPTC("unknown BPM buttons configuration"),
                                           _CPTC("DataProcessing::compute_pos"));
            break;
        }
      }
      break;
    //------------------------------------------------------------------------------
    //- use ESRF SR algorithm
    case BPMConfig::kESRF_SR_POS_ALGO:
      {
        DataProcessing::compute_pos_esrf_sr (_data, _config);
      }
      break;
    //------------------------------------------------------------------------------
    //- ohoh!
    default:
      Tango::Except::throw_exception(_CPTC("unexpected configuration"),
                                     _CPTC("unknown position computation algorithm"),
                                     _CPTC("DataProcessing::compute_pos"));
      break;
  }
}

#if ! defined(_EMBEDDED_DEVICE_) || ! defined(_USE_ARM_OPTIMIZATION_)
// ============================================================================
// cspi_cordic_amp
// ============================================================================
// we include our own version of the cspi <cordic_amp> function in order to
// to avoid cspi library compil option dependency. 
// ============================================================================
//               Table of CORDIC gain (CG) corrections:
// ----------------------------------------------------------------------------
// L             CG                      1/CG           (1/CG)<<32
// ----------------------------------------------------------------------------
// 0  : 1.41421356237310000000   0.70710678118654700    3037000500
// 1  : 1.58113883008419000000   0.63245553203367600    2716375826
// 2  : 1.62980060130066000000   0.61357199107789600    2635271635
// 3  : 1.64248406575224000000   0.60883391251775200    2614921743
// 4  : 1.64568891575726000000   0.60764825625616800    2609829388
// 5  : 1.64649227871248000000   0.60735177014129600    2608555990
// 6  : 1.64669325427364000000   0.60727764409352600    2608237621
// 7  : 1.64674350659690000000   0.60725911229889300    2608158028
// 8  : 1.64675607020488000000   0.60725447933256200    2608138129
// 9  : 1.64675921113982000000   0.60725332108987500    2608133154
// 10 : 1.64675999637562000000   0.60725303152913400    2608131911
// 11 : 1.64676019268469000000   0.60725295913894500    2608131600 (!)
// 12 : 1.64676024176197000000   0.60725294104139700    2608131522
// 13 : 1.64676025403129000000   0.60725293651701000    2608131503
// 14 : 1.64676025709862000000   0.60725293538591400    2608131498
// 15 : 1.64676025786545000000   0.60725293510313900    2608131497
// 16 : 1.64676025805716000000   0.60725293503244600    2608131496
// 17 : 1.64676025810509000000   0.60725293501477200    2608131496
// 18 : 1.64676025811707000000   0.60725293501035400    2608131496
// 19 : 1.64676025812007000000   0.60725293500925000    2608131496
// 20 : 1.64676025812082000000   0.60725293500897300    2608131496
// ----------------------------------------------------------------------------
#define CORDIC_IGNORE_GAIN
// ----------------------------------------------------------------------------
static inline int cspi_cordic_amp ( int  I, int  Q )
{
	//- The number of iterations = CORDIC_MAXLEVEL + 1
	static const int CORDIC_MAXLEVEL = 11;
  
  //- CORDIC gain correction factor associated with the CORDIC_MAXLEVEL.
  static const long long CORDIC_GAIN64 = 2608131600LL; // (1/CG)<<32
  
	int  tmp_I;
	int  L = 0;
	
	if ( I < 0 ) 
	{
		tmp_I = I;
		if ( Q > 0 ) 
		{
		  I = Q;
		  Q = -tmp_I;     // Rotate by -90 degrees
		}
		else 
		{
		  I = -Q;
		  Q = tmp_I;      // Rotate by +90 degrees
		}
	}
	for( ; L <= CORDIC_MAXLEVEL; ++L  ) 
	{
		tmp_I = I;
		if ( Q >= 0 )
		{
			// Positive phase; do negative rotation
			I += (Q >> L); 
      Q -= (tmp_I >> L);
		}
		else 
		{
			// Negative phase; do positive rotation
			I -= (Q >> L); 
      Q += (tmp_I >> L);
		}
	}
  
#if defined(CORDIC_IGNORE_GAIN)
  return I;
#else
  return (int)((CORDIC_GAIN64 * I) >> 32);
#endif
}
# endif  // ! _USE_ARM_OPTIMIZATION_

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

// ============================================================================
// bpm::delta_to_position                   ** written by M.Abbott - DIAMOND **
// ============================================================================
// Computes K * M / S without loss of precision.  We use
// our knowledge of the arguments to do this work as efficiently as possible.
// 
// Firstly we know that InvS = 2^(60-shift)/S and that S < 2^(31-shift).
// However, we also know that |M| <= S (this is a simple consequence of S
// being a sum of non-negative values and M being a sum of differences), so
// in particular also |M| < 2^(31-shift) and so we can safely get rid of the
// shift in InvS by giving it to M!
// 
// We have now transformed K * M / S to 2^(60-shift) * K * M * InvS and then
// to 2^60 * K * (2^-shift * M) * InvS.  Note finally that the scaling
// constant K can be safely bounded by 2^27 ~~ 128mm and so we can
// with excellent efficiency compute
// 
//                 K * M    -64     4        shift
//      Position = ----- = 2    * (2  K) * (2      M) * InvS
//                   S
// 
// In fact we gain slightly more head-room on K by computing K*InvS as an
// <unsigned> multiply: an upper bound on K of over 250mm seems ample!
 // ============================================================================
inline int delta_to_position (int k, int m, int invs, int shift)
{
  return ::MulSS(::MulUU(k << 4, invs), m << shift);
}

#endif //- _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

// ============================================================================
// DataProcessing::compute_pos_block_45 for Turn-by-Turn data (DD) 
// ============================================================================
void DataProcessing::compute_pos_block_45 (DDData & _dd_data,
                                           const DDRawBuffer & _dd_raw_buffer, 
                                           size_t actual_num_samples,
                                           const BPMConfig & _config)
{
  CSPI_DD_RAWATOM * _dd_raw_data = _dd_raw_buffer.base();
  
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

// Timer t;
// #define kDD_NUM_IT 200
// for (size_t it = 0; it < kDD_NUM_IT; it++)
// {

  for (size_t i = 0; i < actual_num_samples; i++)
  {
    //- optimized pure integer algorithm
    //----------------------------------------------------------------------

    // In oder to avoid overflow in the computation of the total intensity (sum),
    // we prescale each button value by 4 (i.e. >> 2). This can involve loss of 
    // bits when the intensity is extremely low, but in fact the bottom bits are 
    // pretty well pure noise and can be cheaply discarded. The button values 
    // A,B,C,D are known to lie in the range 0 to 2^31 - 1 so we similarly know 
    // that: 0 <= S < 2^31.
    
    i32 va = (CORDIC_MAG(_dd_raw_data[i].sinVa, _dd_raw_data[i].cosVa) >> 2)
                * _config.int_VaGainCorrection;

    i32 vb = (CORDIC_MAG(_dd_raw_data[i].sinVb, _dd_raw_data[i].cosVb) >> 2)
                * _config.int_VbGainCorrection;

    i32 vc = (CORDIC_MAG(_dd_raw_data[i].sinVc, _dd_raw_data[i].cosVc) >> 2)
                * _config.int_VcGainCorrection;

    i32 vd = (CORDIC_MAG(_dd_raw_data[i].sinVd, _dd_raw_data[i].cosVd) >> 2)
                * _config.int_VdGainCorrection;
 
    i32 sum = va + vb + vc + vd;

    int shift = 0;
    int inv_sum = ::Reciprocal(sum, shift);
    shift = 60 - shift;
            
    i32 x = bpm::delta_to_position(_config.int_Kx, va - vb - vc + vd, inv_sum, shift) 
               - _config.int_Xoffset_local;

    i32 z = bpm::delta_to_position(_config.int_Kz, va + vb - vc - vd, inv_sum, shift) 
              - _config.int_Zoffset_local;
   
    i32 q = bpm::delta_to_position(_config.int_Kx, va - vb + vc - vd, inv_sum, shift) 
              - _config.int_Qoffset_local;
    
    //- pos: nano to millimeters 
    NM_TO_MM(x, _dd_data.x_.base() + i); 
    NM_TO_MM(z, _dd_data.z_.base() + i); 
    NM_TO_MM(q, _dd_data.q_.base() + i); 

    if (DDData::m_optional_data_enabled)
    {
      INT_TO_FP(_dd_raw_data[i].sinVa, _dd_data.sin_va_.base() + i);
      INT_TO_FP(_dd_raw_data[i].sinVb, _dd_data.sin_vb_.base() + i);
      INT_TO_FP(_dd_raw_data[i].sinVc, _dd_data.sin_vc_.base() + i);
      INT_TO_FP(_dd_raw_data[i].sinVd, _dd_data.sin_vd_.base() + i);
      INT_TO_FP(_dd_raw_data[i].cosVa, _dd_data.cos_va_.base() + i);
      INT_TO_FP(_dd_raw_data[i].cosVb, _dd_data.cos_vb_.base() + i);
      INT_TO_FP(_dd_raw_data[i].cosVc, _dd_data.cos_vc_.base() + i);
      INT_TO_FP(_dd_raw_data[i].cosVd, _dd_data.cos_vd_.base() + i);
    }
    
    //- electrode values: int to float
    UNSCALE_BUTTON_GAIN_CORRECTION(va, _dd_data.va_.base() + i);
    UNSCALE_BUTTON_GAIN_CORRECTION(vb, _dd_data.vb_.base() + i);
    UNSCALE_BUTTON_GAIN_CORRECTION(vc, _dd_data.vc_.base() + i);
    UNSCALE_BUTTON_GAIN_CORRECTION(vd, _dd_data.vd_.base() + i);
    UNSCALE_BUTTON_GAIN_CORRECTION(sum, _dd_data.sum_.base() + i);
  } //- for each DD raw sample

// }
// double dt = t.elapsed_usec();
// double dd_perf = t.elapsed_usec() / (kDD_NUM_IT * actual_num_samples);
// std::cout << "dd_perf: " << dd_perf << " usec per DDRawAtom" << std::endl;
 
//   std::cout << "DataProcessing::compute_pos_block_45::DD::optimized::x: " 
//             << _dd_data.x_[actual_num_samples - 1] << std::endl;
//   std::cout << "DataProcessing::compute_pos_block_45::DD::optimized::z: " 
//             << _dd_data.z_[actual_num_samples - 1] << std::endl;

#else // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

  //- pure floating point algorithm 
  //----------------------------------------------------------------------
  for (size_t i = 0; i < actual_num_samples; i++)
  {
    _dd_data.va_[i] = CORDIC_MAG(_dd_raw_data[i].sinVa, _dd_raw_data[i].cosVa)
                        * _config.VaGainCorrection;

    _dd_data.vb_[i] = CORDIC_MAG(_dd_raw_data[i].sinVb, _dd_raw_data[i].cosVb)
                        * _config.VbGainCorrection;

    _dd_data.vc_[i] = CORDIC_MAG(_dd_raw_data[i].sinVc, _dd_raw_data[i].cosVc)
                        * _config.VcGainCorrection;

    _dd_data.vd_[i] = CORDIC_MAG(_dd_raw_data[i].sinVd, _dd_raw_data[i].cosVd)
                        * _config.VdGainCorrection;
    
    _dd_data.sum_[i] = _dd_data.va_[i] + _dd_data.vb_[i] + _dd_data.vc_[i] + _dd_data.vd_[i];

    _dd_data.x_[i] = 
        _config.Kx * (((_dd_data.va_[i] + _dd_data.vd_[i]) - (_dd_data.vb_[i] + _dd_data.vc_[i])) / _dd_data.sum_[i]) 
            - _config.Xoffset_local;

    _dd_data.z_[i] = 
        _config.Kz * (((_dd_data.va_[i] + _dd_data.vb_[i]) - (_dd_data.vc_[i] + _dd_data.vd_[i])) / _dd_data.sum_[i]) 
            - _config.Zoffset_local;

    _dd_data.q_[i] = 
        _config.Kx * (((_dd_data.va_[i] + _dd_data.vc_[i]) - (_dd_data.vb_[i] + _dd_data.vd_[i])) / _dd_data.sum_[i]) 
            - _config.Qoffset_local;

    if (DDData::m_optional_data_enabled)
    {
      _dd_data.sin_va_[i] = _dd_raw_data[i].sinVa;
      _dd_data.cos_va_[i] = _dd_raw_data[i].cosVa;
      _dd_data.sin_vb_[i] = _dd_raw_data[i].sinVb;
      _dd_data.cos_vb_[i] = _dd_raw_data[i].cosVb;
      _dd_data.sin_vc_[i] = _dd_raw_data[i].sinVc;
      _dd_data.cos_vc_[i] = _dd_raw_data[i].cosVc;
      _dd_data.sin_vd_[i] = _dd_raw_data[i].sinVd;
      _dd_data.cos_vd_[i] = _dd_raw_data[i].cosVd;
    }
  } //- for each DD raw sample

//   std::cout << "DataProcessing::compute_pos_block_45::DD::---std---::x: "
//             << _dd_data.x_[actual_num_samples - 1] << std::endl;
//   std::cout << "DataProcessing::compute_pos_block_45::DD::---std---::z: " 
//             << _dd_data.z_[actual_num_samples - 1] << std::endl;
  
#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_
}

// ============================================================================
// DataProcessing::compute_pos_block_90 for Turn-by-Turn data (DD) 
// ============================================================================
void DataProcessing::compute_pos_block_90 (DDData & _dd_data,
                                           const DDRawBuffer & _dd_raw_buffer, 
                                           size_t actual_num_samples,
                                           const BPMConfig & _config)
{
  CSPI_DD_RAWATOM * _dd_raw_data = _dd_raw_buffer.base();
 
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

  for (size_t i = 0; i < actual_num_samples; i++)
  {
    //- optimized pure integer algorithm
    //----------------------------------------------------------------------

    // In oder to avoid overflow in the computation of the total intensity (sum),
    // we prescale each button value by 4 (i.e. >> 2). This can involve loss of 
    // bits when the intensity is extremely low, but in fact the bottom bits are 
    // pretty well pure noise and can be cheaply discarded. The button values 
    // A,B,C,D are known to lie in the range 0 to 2^31 - 1 so we similarly know 
    // that: 0 <= S < 2^31.
    
    i32 va = (CORDIC_MAG(_dd_raw_data[i].sinVa, _dd_raw_data[i].cosVa) >> 2)
                * _config.int_VaGainCorrection;

    i32 vb = (CORDIC_MAG(_dd_raw_data[i].sinVb, _dd_raw_data[i].cosVb) >> 2)
                * _config.int_VbGainCorrection;

    i32 vc = (CORDIC_MAG(_dd_raw_data[i].sinVc, _dd_raw_data[i].cosVc) >> 2)
                * _config.int_VcGainCorrection;

    i32 vd = (CORDIC_MAG(_dd_raw_data[i].sinVd, _dd_raw_data[i].cosVd) >> 2)
                * _config.int_VdGainCorrection;

    i32 sum = va + vb + vc + vd;

    int shift = 0;
    int inv_sum = ::Reciprocal(vd + vb, shift);
    shift = 60 - shift;  
        
    i32 x = bpm::delta_to_position(_config.int_Kx, vd - vb, inv_sum, shift)
              - _config.int_Xoffset_local;

    shift = 0;
    inv_sum = ::Reciprocal(va + vc, shift);
    shift = 60 - shift;  
    
    i32 z = bpm::delta_to_position(_config.int_Kz, va - vc, inv_sum, shift)
              - _config.int_Zoffset_local;

    shift = 0;
    inv_sum = ::Reciprocal(sum, shift);
    shift = 60 - shift;  
    
    i32 q = bpm::delta_to_position(_config.int_Kx, va - vb + vc - vd, inv_sum, shift) 
              - _config.int_Qoffset_local;
        
    //- pos: nano to millimeters 
    NM_TO_MM(x, _dd_data.x_.base() + i); 
    NM_TO_MM(z, _dd_data.z_.base() + i); 
    NM_TO_MM(q, _dd_data.q_.base() + i); 

    if (DDData::m_optional_data_enabled)
    {
      INT_TO_FP(_dd_raw_data[i].sinVa, _dd_data.sin_va_.base() + i);
      INT_TO_FP(_dd_raw_data[i].sinVb, _dd_data.sin_vb_.base() + i);
      INT_TO_FP(_dd_raw_data[i].sinVc, _dd_data.sin_vc_.base() + i);
      INT_TO_FP(_dd_raw_data[i].sinVd, _dd_data.sin_vd_.base() + i);
      INT_TO_FP(_dd_raw_data[i].cosVa, _dd_data.cos_va_.base() + i);
      INT_TO_FP(_dd_raw_data[i].cosVb, _dd_data.cos_vb_.base() + i);
      INT_TO_FP(_dd_raw_data[i].cosVc, _dd_data.cos_vc_.base() + i);
      INT_TO_FP(_dd_raw_data[i].cosVd, _dd_data.cos_vd_.base() + i);
    }
    
    //- electrode values: int to float
    UNSCALE_BUTTON_GAIN_CORRECTION(va, _dd_data.va_.base() + i);
    UNSCALE_BUTTON_GAIN_CORRECTION(vb, _dd_data.vb_.base() + i);
    UNSCALE_BUTTON_GAIN_CORRECTION(vc, _dd_data.vc_.base() + i);
    UNSCALE_BUTTON_GAIN_CORRECTION(vd, _dd_data.vd_.base() + i);
    UNSCALE_BUTTON_GAIN_CORRECTION(sum, _dd_data.sum_.base() + i);

  } //- for each DD raw sample
  
//   std::cout << "DataProcessing::compute_pos_block_90::DD::optimized::x: " 
//             << _dd_data.x_[actual_num_samples - 1] << std::endl;
//   std::cout << "DataProcessing::compute_pos_block_90::DD::optimized::z: " 
//             << _dd_data.z_[actual_num_samples - 1] << std::endl;

#else // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

  //- pure floating point algorithm
  //----------------------------------------------------------------------
  for (size_t i = 0; i < actual_num_samples; i++)
  {
    _dd_data.va_[i] = CORDIC_MAG(_dd_raw_data[i].sinVa, _dd_raw_data[i].cosVa)
                        * _config.VaGainCorrection;

    _dd_data.vb_[i] = CORDIC_MAG(_dd_raw_data[i].sinVb, _dd_raw_data[i].cosVb)
                        * _config.VbGainCorrection;

    _dd_data.vc_[i] = CORDIC_MAG(_dd_raw_data[i].sinVc, _dd_raw_data[i].cosVc)
                        * _config.VcGainCorrection;

    _dd_data.vd_[i] = CORDIC_MAG(_dd_raw_data[i].sinVd, _dd_raw_data[i].cosVd)
                        * _config.VdGainCorrection;
                        
    _dd_data.sum_[i] = _dd_data.va_[i] + _dd_data.vb_[i] + _dd_data.vc_[i] + _dd_data.vd_[i];

    _dd_data.x_[i] = 
        _config.Kx * ((_dd_data.vd_[i] - _dd_data.vb_[i]) / (_dd_data.vd_[i] + _dd_data.vb_[i])) 
            - _config.Xoffset_local;

    _dd_data.z_[i] = 
        _config.Kz * ((_dd_data.va_[i] - _dd_data.vc_[i]) / (_dd_data.va_[i] + _dd_data.vc_[i])) 
            - _config.Zoffset_local;

    _dd_data.q_[i] = 
        _config.Kx * (((_dd_data.va_[i] + _dd_data.vc_[i]) - (_dd_data.vb_[i] + _dd_data.vd_[i])) / _dd_data.sum_[i]) 
            - _config.Qoffset_local;

    if (DDData::m_optional_data_enabled)
    {
      _dd_data.sin_va_[i] = _dd_raw_data[i].sinVa;
      _dd_data.cos_va_[i] = _dd_raw_data[i].cosVa;
      _dd_data.sin_vb_[i] = _dd_raw_data[i].sinVb;
      _dd_data.cos_vb_[i] = _dd_raw_data[i].cosVb;
      _dd_data.sin_vc_[i] = _dd_raw_data[i].sinVc;
      _dd_data.cos_vc_[i] = _dd_raw_data[i].cosVc;
      _dd_data.sin_vd_[i] = _dd_raw_data[i].sinVd;
      _dd_data.cos_vd_[i] = _dd_raw_data[i].cosVd;
    }
  }
  
//   std::cout << "DataProcessing::compute_pos_block_90::DD::---std---::x: "
//             << _dd_data.x_[actual_num_samples - 1] << std::endl;
//   std::cout << "DataProcessing::compute_pos_block_90::DD::---std---::z: " 
//             << _dd_data.z_[actual_num_samples - 1] << std::endl;

#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_
}

// ============================================================================
// DataProcessing::compute_pos_esrf_sr for Turn-by-Turn data (DD) 
// ============================================================================
void DataProcessing::compute_pos_esrf_sr (DDData & _dd_data,
                                          const DDRawBuffer & _dd_raw_buffer, 
                                          size_t actual_num_samples,
                                          const BPMConfig & _config)
{
//  std::cout << "DataProcessing::compute_pos_esrf_sr <-" << std::endl ;
 
  CSPI_DD_RAWATOM * _dd_raw_data = _dd_raw_buffer.base();

//#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

//#error no ESRF SR impl for ARM!
//  for (size_t i = 0; i < actual_num_samples; i++)
//  {
//    //- optimized pure integer algorithm
//    //----------------------------------------------------------------------
//    
//  }
//// std::cout << "DataProcessing::compute_pos_esrf_sr::DD::optimized::x: " 
////           << _dd_data.x_[actual_num_samples - 1] << std::endl;
//// std::cout << "DataProcessing::compute_pos_esrf_sr::DD::optimized::z: " 
////           << _dd_data.z_[actual_num_samples - 1] << std::endl;

//#else // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

  //- pure floating point algorithm
  //----------------------------------------------------------------------

  for (size_t i = 0; i < actual_num_samples; i++)
  {
    _dd_data.va_[i] = CORDIC_MAG(_dd_raw_data[i].sinVa, _dd_raw_data[i].cosVa)
                        * _config.VaGainCorrection;

    _dd_data.vb_[i] = CORDIC_MAG(_dd_raw_data[i].sinVb, _dd_raw_data[i].cosVb)
                        * _config.VbGainCorrection;

    _dd_data.vc_[i] = CORDIC_MAG(_dd_raw_data[i].sinVc, _dd_raw_data[i].cosVc)
                        * _config.VcGainCorrection;

    _dd_data.vd_[i] = CORDIC_MAG(_dd_raw_data[i].sinVd, _dd_raw_data[i].cosVd)
                        * _config.VdGainCorrection;
                        
    _dd_data.sum_[i] = _dd_data.va_[i] + _dd_data.vb_[i] + _dd_data.vc_[i] + _dd_data.vd_[i];
    
    _dd_data.q_[i] = 
        _config.Kx * (((_dd_data.va_[i] + _dd_data.vc_[i]) - (_dd_data.vb_[i] + _dd_data.vd_[i])) / _dd_data.sum_[i]) 
            - _config.Qoffset_local;
            
    //- the algorithm is given by: (c.f. Joel Chavanne Feb 2002)
    //- X = -0.0592 + 11.469 * atanh(1.3658 * Qh)
    //- In the present case, the offset -0.0592 is not applied
    //- because we suppose it is already included in the offset.
    //- which has been determided experimentally.
    //- However, we add the -0.0592 to X to calculate the Z with the
    //- following formula: Z = 14.37 * (1+0.001452 * X * X).
			 
    //- Qh = ((a - c) / (a + c) + (d - b) / (d + b))/2;
    //- Rv = ((a - d) / (a + d) + (b - c) / (b + c))/2;
			   
    //- Xb = 11.469 * atanh(1.3658 * Qh);
    //- Xb2 = Xb - 0.0592;
    //- Zb = 14.37 * (1 + 0.001452 *(Xb2) * (Xb2)) * Rv;
  
    double Qh = ((_dd_data.va_[i] - _dd_data.vc_[i]) / (_dd_data.va_[i] + _dd_data.vc_[i]) + 
                 (_dd_data.vd_[i] - _dd_data.vb_[i]) / (_dd_data.vd_[i] + _dd_data.vb_[i])) / 2.0;

    double Rv = ((_dd_data.va_[i] - _dd_data.vd_[i]) / (_dd_data.va_[i] + _dd_data.vd_[i]) +
                 (_dd_data.vb_[i] - _dd_data.vc_[i]) / (_dd_data.vb_[i] + _dd_data.vc_[i])) / 2.0;
  
    _dd_data.x_[i] = 11.469 * atanh(1.3658 * Qh);
    
    double Xb2  = _dd_data.x_[i] - 0.0592;

    _dd_data.z_[i] = 14.37 * (1 + 0.001452 * Xb2 * Xb2) * Rv; 
  }
  
// std::cout << "DataProcessing::compute_pos_esrf_sr::DD::---std---::x: " 
//           << _dd_data.x_[actual_num_samples - 1] << std::endl;
// std::cout << "DataProcessing::compute_pos_esrf_sr::DD::---std---::z: " 
//           << _dd_data.z_[actual_num_samples - 1] << std::endl;
         
//#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

// std::cout << "DataProcessing::compute_pos_esrf_sr ->" << std::endl;
}

// ============================================================================
// DataProcessing::compute_pos_block_45 for slow acquisition data (SA) 
// ============================================================================
void DataProcessing::compute_pos_block_45 (SAData & _sa_data, const BPMConfig & _config)
{
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

// Timer t;
// #define kSA_NUM_IT 100000
// for (size_t it = 0; it < kSA_NUM_IT; it++)
// {
  
  //- optimized pure integer algorithm
  //----------------------------------------------------------------------
  
  // In oder to avoid overflow in the computation of the total intensity S (sum),
  // we prescale each button value by 4 (i.e. >> 2). This can involve loss of 
  // bits when the intensity is extremely low, but in fact the bottom bits are 
  // pretty well pure noise and can be cheaply discarded. The button values 
  // A,B,C,D are known to lie in the range 0 to 2^31 - 1 so we similarly know 
  // that: 0 <= S < 2^31.
  
  i32 va = (_sa_data.int_va_ >> 2) * _config.int_VaGainCorrection;   
  i32 vb = (_sa_data.int_vb_ >> 2) * _config.int_VbGainCorrection;   
  i32 vc = (_sa_data.int_vc_ >> 2) * _config.int_VcGainCorrection;   
  i32 vd = (_sa_data.int_vd_ >> 2) * _config.int_VdGainCorrection;
  
  i32 sum = va + vb + vc + vd;

  i32 shift = 0;
  i32 inv_sum = ::Reciprocal(sum, shift);
  shift = 60 - shift;
  
  i32 x = bpm::delta_to_position(_config.int_Kx, va - vb - vc + vd, inv_sum, shift) 
            - _config.int_Xoffset_local;

  i32 z = bpm::delta_to_position(_config.int_Kz, va + vb - vc - vd, inv_sum, shift)
            - _config.int_Zoffset_local;

  i32 q = bpm::delta_to_position(_config.int_Kx, va - vb + vc - vd, inv_sum, shift) 
            - _config.int_Qoffset_local;

  //- store x & z for SA stats computation (see BPMData.cpp)
  _sa_data.int_x_ = x;  
  _sa_data.int_z_ = z;  
  
  //- pos: nano to millimeters 
  NM_TO_MM(x, &_sa_data.x_); 
  NM_TO_MM(z, &_sa_data.z_); 
  NM_TO_MM(q, &_sa_data.q_); 

  //- electrode values: int to float
  UNSCALE_BUTTON_GAIN_CORRECTION(va, &_sa_data.va_);
  UNSCALE_BUTTON_GAIN_CORRECTION(vb, &_sa_data.vb_);
  UNSCALE_BUTTON_GAIN_CORRECTION(vc, &_sa_data.vc_);
  UNSCALE_BUTTON_GAIN_CORRECTION(vd, &_sa_data.vd_);
  UNSCALE_BUTTON_GAIN_CORRECTION(sum, &_sa_data.sum_);
  
// }
// double dt = t.elapsed_usec();
// double sa_perf = t.elapsed_usec() / kSA_NUM_IT ;
// std::cout << "sa_perf: " << sa_perf << " usec per SARawAtom" << std::endl;

// std::cout << "DataProcessing::compute_pos_block_45::SA::optimized::x: " 
//           << _sa_data.x_ << std::endl;
// std::cout << "DataProcessing::compute_pos_block_45::SA::optimized::z: " 
//           << _sa_data.z_ << std::endl;

#else // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

  //- pure floating point algorithm
  //----------------------------------------------------------------------
  
  //_config.dump_offsets();
  //_config.dump_env_parameters();
  
  _sa_data.va_ *= _config.VaGainCorrection;
  _sa_data.vb_ *= _config.VbGainCorrection;
  _sa_data.vc_ *= _config.VcGainCorrection;
  _sa_data.vd_ *= _config.VdGainCorrection;
  
  _sa_data.sum_ = _sa_data.va_ + _sa_data.vb_ + _sa_data.vc_ + _sa_data.vd_;

  _sa_data.x_ = 
      _config.Kx * (((_sa_data.va_ + _sa_data.vd_) - (_sa_data.vb_ + _sa_data.vc_)) / _sa_data.sum_) 
          - _config.Xoffset_local;

  _sa_data.z_ = 
      _config.Kz * (((_sa_data.va_ + _sa_data.vb_) - (_sa_data.vc_ + _sa_data.vd_)) / _sa_data.sum_) 
          - _config.Zoffset_local;

  _sa_data.q_ = 
      _config.Kx * (((_sa_data.va_ + _sa_data.vc_) - (_sa_data.vb_ + _sa_data.vd_)) / _sa_data.sum_) 
          - _config.Qoffset_local;
   
  //- std::cout << "DataProcessing::compute_pos_block_45::SA::---std---::x: " << _sa_data.x_ << std::endl;
  //- std::cout << "DataProcessing::compute_pos_block_45::SA::---std---::z: " << _sa_data.z_ << std::endl;

#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_
}

// ============================================================================
// DataProcessing::compute_pos_block_90 for slow acquisition data (SA) 
// ============================================================================
void DataProcessing::compute_pos_block_90 (SAData & _sa_data, const BPMConfig & _config)
{
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

  //- optimized pure integer algorithm
  //----------------------------------------------------------------------

  // In oder to avoid overflow in the computation of the total intensity sum),
  // we prescale each button value by 4 (i.e. >> 2). This can involve loss of 
  // bits when the intensity is extremely low, but in fact the bottom bits are 
  // pretty well pure noise and can be cheaply discarded. The button values 
  // A,B,C,D are known to lie in the range 0 to 2^31 - 1 so we similarly know 
  // that: 0 <= S < 2^31.

  i32 va = (_sa_data.int_va_ >> 2) * _config.int_VaGainCorrection;   
  i32 vb = (_sa_data.int_vb_ >> 2) * _config.int_VbGainCorrection;   
  i32 vc = (_sa_data.int_vc_ >> 2) * _config.int_VcGainCorrection;   
  i32 vd = (_sa_data.int_vd_ >> 2) * _config.int_VdGainCorrection;

  i32 sum = va + vb + vc + vd;

  int shift = 0;
  int inv_sum = ::Reciprocal(vd + vb, shift);
  shift = 60 - shift;  
        
  i32 x = bpm::delta_to_position(_config.int_Kx, vd - vb, inv_sum, shift)
            - _config.int_Xoffset_local;

  shift = 0;
  inv_sum = ::Reciprocal(va + vc, shift);
  shift = 60 - shift;  
  
  i32 z = bpm::delta_to_position(_config.int_Kz, va - vc, inv_sum, shift)
            - _config.int_Zoffset_local;

  shift = 0;
  inv_sum = ::Reciprocal(sum, shift);
  shift = 60 - shift;  
  
  i32 q = bpm::delta_to_position(_config.int_Kx, va - vb + vc - vd, inv_sum, shift) 
            - _config.int_Qoffset_local;       
              
  //- pos: nano to millimeters 
  NM_TO_MM(x, &_sa_data.x_); 
  NM_TO_MM(z, &_sa_data.z_); 
  NM_TO_MM(q, &_sa_data.q_); 

  //- electrode values: int to float
  UNSCALE_BUTTON_GAIN_CORRECTION(va, &_sa_data.va_);
  UNSCALE_BUTTON_GAIN_CORRECTION(vb, &_sa_data.vb_);
  UNSCALE_BUTTON_GAIN_CORRECTION(vc, &_sa_data.vc_);
  UNSCALE_BUTTON_GAIN_CORRECTION(vd, &_sa_data.vd_);
  UNSCALE_BUTTON_GAIN_CORRECTION(sum, &_sa_data.sum_);

// std::cout << "DataProcessing::compute_pos_block_90::SA::optimized::x: " 
//           << _sa_data.x_ << std::endl;
// std::cout << "DataProcessing::compute_pos_block_90::SA::optimized::z: " 
//           << _sa_data.z_ << std::endl;

#else // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

  //- pure floating point algorithm
  //----------------------------------------------------------------------

  _sa_data.va_ *= _config.VaGainCorrection;
  _sa_data.vb_ *= _config.VbGainCorrection;
  _sa_data.vc_ *= _config.VcGainCorrection;
  _sa_data.vd_ *= _config.VdGainCorrection;

  _sa_data.sum_ = _sa_data.va_ + _sa_data.vb_ + _sa_data.vc_ + _sa_data.vd_;

  _sa_data.x_ = 
      _config.Kx * ((_sa_data.vd_ - _sa_data.vb_) / (_sa_data.vd_ + _sa_data.vb_)) 
          - _config.Xoffset_local;

  _sa_data.z_ = 
      _config.Kz * ((_sa_data.va_ - _sa_data.vc_) / (_sa_data.va_ + _sa_data.vc_)) 
          - _config.Zoffset_local;

  _sa_data.q_ = 
      _config.Kx * (((_sa_data.va_ + _sa_data.vc_) - (_sa_data.vb_ + _sa_data.vd_)) / _sa_data.sum_) 
          - _config.Qoffset_local;
          
// std::cout << "DataProcessing::compute_pos_block_90::SA::---std---::x: " 
//           << _sa_data.x_ << std::endl;
// std::cout << "DataProcessing::compute_pos_block_90::SA::---std---::z: " 
//           << _sa_data.z_ << std::endl;
  
#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_
}

// ============================================================================
// DataProcessing::compute_pos_esrf_sr for slow acquisition data (SA) 
// ============================================================================
void DataProcessing::compute_pos_esrf_sr (SAData & _sa_data, const BPMConfig & _config)
{
  //- the SAData has been pre-filled with a CSPI_SA_ATOM
  _sa_data.va_ *= _config.VaGainCorrection;

  _sa_data.vb_ *= _config.VbGainCorrection;

  _sa_data.vc_ *= _config.VcGainCorrection;

  _sa_data.vd_ *= _config.VdGainCorrection;

  _sa_data.sum_ = _sa_data.va_ + _sa_data.vb_ + _sa_data.vc_ + _sa_data.vd_;
  
  _sa_data.q_ = 
    _config.Kx 
        * (((_sa_data.va_ + _sa_data.vc_) - (_sa_data.vb_ + _sa_data.vd_)) / _sa_data.sum_) 
            - _config.Qoffset_local;
  
  /* 
    the algorithme is given by: (Cf Joel Chavanne Feb 2002)
	  X = -0.0592 + 11.469 * atanh(1.3658 * Qh)
	  In the present case, the offset -0.0592 is not applied
	  because we suppose it is already included in the offset.
	  which has been determided experimentally.
	  However, we add the -0.0592 to X to calculate the Z with the
	  following formula: Z = 14.37 * (1+0.001452 * X * X).
	*/
			 
  // Qh = ((a - c) / (a + c) + (d - b) / (d + b))/2;
  // Rv = ((a - d) / (a + d) + (b - c) / (b + c))/2;
			   
  // Xb  = 11.469 * atanh(1.3658*Qh);
  // Xb2 = Xb - 0.0592;
  // Zb  = 14.37 * (1+0.001452*(Xb2)*(Xb2)) * Rv;
  
  double Qh   = ((_sa_data.va_ - _sa_data.vc_) / (_sa_data.va_ + _sa_data.vc_) + 
                 (_sa_data.vd_ - _sa_data.vb_) / (_sa_data.vd_ + _sa_data.vb_)) / 2.0;

  double Rv   = ((_sa_data.va_ - _sa_data.vd_) / (_sa_data.va_ + _sa_data.vd_) +
                 (_sa_data.vb_ - _sa_data.vc_) / (_sa_data.vb_ + _sa_data.vc_)) / 2.0;
  
  _sa_data.x_ = 11.469 * atanh(1.3658 * Qh);
  
  double Xb2  = _sa_data.x_ - 0.0592;

  _sa_data.z_ = 14.37 * (1 + 0.001452 * (Xb2)*(Xb2)) * Rv; 
}

} // namespace bpm
