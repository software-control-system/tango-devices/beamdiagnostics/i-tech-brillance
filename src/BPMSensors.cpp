//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------
// = INITIAL AUTHOR
//    Dr. Michael Abbott - Diamond - EPICS driver 
//
// = ADAPTOR/THIEF/HACKER/
//    Nicolas Leclercq - SOLEIL
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fts.h>
#include <math.h>
#include "CommonHeader.h"
#include "BPMSensors.h"

namespace bpm {

// ============================================================================
// BPMSensors::BPMSensors
// ============================================================================
BPMSensors::BPMSensors ()
 : memory_free (0),
   ram_fs_usage (0),
   uptime (0),
   cpu_usage (0)
{

}

// ============================================================================
// BPMSensors::~BPMSensors
// ============================================================================
BPMSensors::~BPMSensors ()
{

}

// ============================================================================
// parse_file
// ============================================================================
bool parse_file (const char * filename, int count, const char * fmt, ...)
                 __attribute__((format(scanf, 3, 4)));
                   
bool parse_file (const char * filename, int count, const char * fmt, ...)
{
  bool ok = false;
  FILE * input = fopen(filename, "r");
  if (input != NULL)
  {
    va_list args;
    va_start(args, fmt);
    ok = vfscanf(input, fmt, args) == count;
    fclose(input);
  }
  else
  {
    //- std::cout << "parse_file error: " << filename << std::endl;
  }
  return ok;
}

// ============================================================================
// BPMSensors::update_uptime
// ============================================================================
// Total uptime and idle time can be read directly from /proc/uptime, and by
// keeping track of the cumulative idle time we can report percentage CPU
// usage over the scan period.
// ============================================================================
void BPMSensors::update_uptime ()
{
  double db_uptime;
  if (parse_file("/proc/uptime", 1, "%lf", &db_uptime))
  {
    this->uptime = int(db_uptime);
    
    //- std::cout << "BPMSensors::update_uptime:up_time..." << db_uptime << std::endl;
  }
}

// ============================================================================
// BPMSensors::update_cpu_usage
// ============================================================================
void BPMSensors::update_cpu_usage ()
{
  static long last_user = 0;
  static long last_nice = 0;
  static long last_sys  = 0;
  static long last_idle = 0;
  
  char ignored[16];
  long user, nice, sys, idle;
  if (parse_file("/proc/stat", 
                  5, 
                  "%s %ld %ld %ld %ld", 
                  ignored, 
                  &user, 
                  &nice, 
                  &sys, 
                  &idle))
  {
    long d_user = user - last_user;
    long d_nice = nice - last_nice; 
    long d_idle = idle - last_idle;
    long d_sys  = sys  - last_sys;
    
    double db_cpu_usage = 
      100. * ( double(d_user + d_sys) / double(d_user + d_sys + d_nice + d_idle));
    
    this->cpu_usage = int(::nearbyint(db_cpu_usage));
    
    if (this->cpu_usage == 0)
      ++this->cpu_usage;
    
    //- std::cout << "BPMSensors::update_cpu_usage:user........" << user << std::endl;
    //- std::cout << "BPMSensors::update_cpu_usage:nice........" << nice << std::endl;
    //- std::cout << "BPMSensors::update_cpu_usage:sys........." << sys << std::endl;
    //- std::cout << "BPMSensors::update_cpu_usage:idle........" << idle << std::endl;
    //- std::cout << "BPMSensors::update_cpu_usage:cpu_usage..." << db_cpu_usage << "%" << std::endl;
    //- std::cout << "BPMSensors::update_cpu_usage:cpu_usage..." << this->cpu_usage << "%" << std::endl << std::endl;

    last_user = user;
    last_nice = nice;
    last_sys  = sys;
    last_idle = idle;
  }
}

// ============================================================================
// BPMSensors::update_ramfs_usage
// ============================================================================
// This discovers how many bytes of space are being consumed by the ramfs:
// this needs to be subtracted from the "cached" space.
// We do this by walking all of the file syss mounted as ramfs -- the
// actual set of mount points is hard-wired here.
// ===========================================================================
void BPMSensors::update_ramfs_usage ()
{
  //- the following mount points all contain ram file sys
  //- the (char* const) cast makes gcc-4.2 happy!
  static char * const ram_fs[] =
  {
      (char* const)"/var/log",
      (char* const)"/var/lock",
      (char* const)"/var/run",
      (char* const)"/tmp",
      (char* const)NULL
  };
  
  this->ram_fs_usage = 0;
  
  FTS * fts = fts_open(ram_fs, FTS_PHYSICAL | FTS_XDEV, NULL);
  if (fts == NULL)
  {
    //- std::cout << "BPMSensors::update_ramfs_usage:fts_open error" << std::endl;
    return;
  }
  
  FTSENT *ftsent;
  while (ftsent = fts_read(fts),  ftsent != NULL)
      if (ftsent->fts_info != FTS_D)
          this->ram_fs_usage += ftsent->fts_statp->st_size;

  fts_close(fts);
}

// ============================================================================
// BPMSensors::update_ramfs_usage
// ============================================================================
// Free memory processing is a little tricky.  By reading /proc/meminfo we can 
// discover "free" and "cached" memory, but turning this into a true free memory 
// number is more difficult.
// In general, the cached memory is effectively free ... but unfortunately, 
// files in the RAM file sys also appear as "cached" and are NOT free. Even 
// more unfortunately, it appears to be particularly difficult to how much space
// is used by the RAM file sys!
// ============================================================================
void BPMSensors::update_free_memory()
{
  int free, cached;
  if (parse_file("/proc/meminfo", 2, "%*[^\n]\nMem: %*d %*d %d %*d %*d %d", &free, &cached))
  {
    this->update_ramfs_usage();
    this->memory_free = free + cached - this->ram_fs_usage;
  }
}

// ============================================================================
// BPMSensors::update_all
// ============================================================================
void BPMSensors::update_all ()
{
  this->update_cpu_usage ();
  this->update_uptime ();
  //- this->update_free_memory ();
}

} //- namespace bpm

