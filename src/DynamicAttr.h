//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2007-2008 Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _DYNAMIC_ATTR_H_
#define _DYNAMIC_ATTR_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"

//=============================================================================
// Forward declarations
//=============================================================================
namespace bpm
{
  class BPM;
  class DDData;
  class SAData;
  class ADCData;
}

namespace Libera_ns
{

//=============================================================================
// Base class for any DynnamicAttritute
//=============================================================================
class DynamicAttritute
{
  friend class Libera;
  
protected:
  //- the BPM
  static bpm::BPM * bpm;
  //- the BPM DD data (data on demand)
  static bpm::DDData * dd_data;
  //- the BPM ADC data (adc values)
  static bpm::ADCData * adc_data;
  //- the BPM SA data (slow acquisition)
  static bpm::SAData * sa_data;
  //- the BPM PM data (post mortem)
  static bpm::DDData * pm_data;

  //- the BPM
  inline static bpm::BPM * bpm_hw ()
    throw (Tango::DevFailed)
  {
    if (! DynamicAttritute::bpm)
    {
      Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                     _CPTC("no BPM instance registered"),
                                     _CPTC("DynamicAttritute::bpm_hw"));
    }
    return DynamicAttritute::bpm;
  }
  
  DynamicAttritute();
  DynamicAttritute(const DynamicAttritute&);
  const DynamicAttritute& operator= (const DynamicAttritute&);
  ~DynamicAttritute();
};

//=============================================================================
// PosAlgorithmIdAttr: read only scalar of type bpm::FPDataType
//=============================================================================
class PosAlgorithmIdAttr : public DynamicAttritute,
                           public Tango::Attr,
                           public Tango::LogAdapter
{
public:
  //- ctor
  PosAlgorithmIdAttr (const std::string& name, Tango::DeviceImpl * device);
                
  //- dtor
  virtual ~PosAlgorithmIdAttr ();

  //- read
  virtual void read (Tango::DeviceImpl* d, Tango::Attribute &ra);
  
  //- write
  virtual void write (Tango::DeviceImpl* d, Tango::WAttribute &wa);

  //- is_allowed
  virtual bool is_allowed (Tango::DeviceImpl* d, Tango::AttReqType &rt);
  
private:
  //- the currently selected pos comp algorithm 
  Tango::DevUShort pos_algorithm_id_;
};

// //=============================================================================
// // BMPSpectrum: read only spectrum of type bpm::FPDataType
// //=============================================================================
// class BMPSpectrum : public DynamicAttritute,
//                     public Tango::Attr,
//                     public Tango::LogAdapter
// {
// public:
//   //- ctor
//   BMPSpectrum (const std::string& name, Tango::DeviceImpl * device);
//   //- dtor
//   virtual ~BMPSpectrum ();
//   //- read
//   virtual void read (Tango::DeviceImpl* d, Tango::Attribute &ra);
//   //- write
//   virtual void write (Tango::DeviceImpl* d, Tango::WAttribute &wa);
//   //- is_allowed
//   virtual bool is_allowed (Tango::DeviceImpl* d, Tango::AttReqType &rt);
// };

} // namespace

//=============================================================================
// INLINED CODE
//=============================================================================
#if defined (INLINE_IMPL)
# include "DynamicAttr.i"
#endif

#endif 
