//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------
// = INITIAL AUTHOR
//    Dr. Michael Abbott - Diamond - EPICS driver 
//
// = ADAPTOR/THIEF/HACKER/
//    Nicolas Leclercq - SOLEIL
//------------------------------------------------------------------------------

#ifndef _BPM_SENSORS_H_
#define _BPM_SENSORS_H_

namespace bpm
{

// ============================================================================
//! BPM sensors abstraction class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class BPMSensors  //-: public Tango::LogAdapter
{
  friend class BPM;

private:
  //- ctor
  BPMSensors ();
  
  //-dtor
  ~BPMSensors ();
  
  //- update all sensors values
  void update_all ();
  
  //- update both uptime and idle
  void update_uptime ();
  
  //- update ramfs usage
  void update_ramfs_usage ();
  
  //- update memory usage 
  void update_free_memory ();
 
  //- update cpu usage 
  void update_cpu_usage ();

  //- nominal memory free (free + cached - ramfs)
  int memory_free;  
  
  //- number of bytes allocated in ram filesystems
  int ram_fs_usage;
  
  //- libera uptime in seconds
  int uptime; 
  
  //- % CPU usage over the last sample interval
  int cpu_usage;
};

} // namespace bpm

#endif //- _BPM_SENSORS_H_
