//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2007-2008 Julien Malik, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "DynamicAttr.h"
#include "DynamicAttrHelper.h"

#if !defined (__INLINE_IMPL__)
# include "DynamicAttrHelper.i"
#endif

namespace bpm
{

// ============================================================================
// DynamicAttrHelper::ctor
// ============================================================================
DynamicAttrHelper::DynamicAttrHelper ()
	: host_device_(0)
{
  //- noop
}

// ============================================================================
// DynamicAttrHelper::dtor
// ============================================================================
DynamicAttrHelper::~DynamicAttrHelper ()
{
  this->remove_all();
}

// ============================================================================
// DynamicAttrHelper::dtor
// ============================================================================
void DynamicAttrHelper::host_device (Tango::DeviceImpl * _host_device)
{
  this->host_device_ = _host_device;
}
  
// ============================================================================
// DynamicAttrHelper::add 
// ============================================================================
void DynamicAttrHelper::add (Tango::Attr* _attr)
	throw (Tango::DevFailed)
{
	if (! this->host_device_)
	{
	  THROW_DEVFAILED("PROGRAMMING_ERROR",
	                  "no associated Tango device (host device not set)",
                    "DynamicAttrHelper::add");
  }
                   
  //- check attribute does not already exist
  DynAttrIt it = this->rep_.find(_attr->get_name());
  if (it != this->rep_.end())
  {
	  THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
	                  "attribute already exists",
                    "DynamicAttrHelper::add");
  }

  //- add it to the device
  try
  {
    this->host_device_->add_attribute( _attr );
  }
  catch(Tango::DevFailed& ex)
  {
	  RETHROW_DEVFAILED(ex,
                      "INTERNAL_ERROR",
	                    "attribute could not be added to the device",
                      "DynamicAttrHelper::add");
  }
  catch(...)
  {
	  THROW_DEVFAILED("UNKNOWN_ERROR",
	                  "unknown caught while trying to add attribute to the device",
                    "DynamicAttrHelper::add");
  }
  
  //- ok, everything went fine :
  //- insert the attribute into the list
  std::pair<DynAttrIt, bool> insertion_result;
  insertion_result = this->rep_.insert( DynAttrEntry(_attr->get_name(), _attr) );

  if (insertion_result.second == false)
  {
	  THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
	                  "attribute could not be inserted into the attribute repostory",
                    "DynamicAttrHelper::add");
  }
}

// ============================================================================
// DynamicAttrHelper::remove 
// ============================================================================
void DynamicAttrHelper::remove (const std::string& _name)
	throw (Tango::DevFailed)
{
	if (! this->host_device_)
	{
	  THROW_DEVFAILED("PROGRAMMING_ERROR",
	                  "no associated Tango device (host device not set)",
                    "DynamicAttrHelper::remove");
  }
  
  //- check if attribute exists
  DynAttrIt it = this->rep_.find(_name);
  if (it == this->rep_.end())
  {
	  THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
	                  "attribute does not exist",
                    "DynamicAttrHelper::remove");
  }

  //- remove it from the device
  try
  {
    this->host_device_->remove_attribute( (*it).second, true );
  }
  catch(Tango::DevFailed& ex)
  {
	  RETHROW_DEVFAILED(ex,
                      "INTERNAL_ERROR",
	                    "attribute could not be removed from the device",
                      "DynamicAttrHelper::remove");
  }
  catch(...)
  {
	  THROW_DEVFAILED("UNKNOWN_ERROR",
	                  "could not remove attribute from the device",
                    "DynamicAttrHelper::remove");
  }

  //- remove from db
  Tango::DeviceData argin;
  std::vector<std::string> v(2);
  v[0] = this->host_device_->name();
  v[1] = _name;
  argin << v;

  Tango::Database * db =
      this->host_device_->get_db_device()->get_dbase();

  try
  {
    Tango::DeviceData argout =
        db->command_inout("DbDeleteDeviceAttribute", argin);
  }
  catch(Tango::DevFailed& ex)
  {
	  RETHROW_DEVFAILED(ex,
                      "INTERNAL_ERROR",
	                    "unable to delete attribute from the database",
                      "DynamicAttrHelper::remove");
  }
  catch(...)
  {
	  THROW_DEVFAILED("UNKNOWN_ERROR",
	                  "unable to delete attribute from the database",
                    "DynamicAttrHelper::remove");
  }

  //- remove from the internal map
  this->rep_.erase(it);
}

// ============================================================================
// DynamicAttrHelper::remove_all 
// ============================================================================
void DynamicAttrHelper::remove_all ()
	throw (Tango::DevFailed)
{
	if (! this->host_device_ || this->rep_.empty())
		return;
  
  DynAttrIt it;

  Tango::DeviceData argin;
  std::vector<std::string> v(2);
  v[0] = this->host_device_->name();

  for (it  = this->rep_.begin(); it != this->rep_.end(); ++it)
  {

    //- remove it from the device
    try
    {
      this->host_device_->remove_attribute( (*it).second, true );
    }
    catch(Tango::DevFailed& ex)
    {
	    RETHROW_DEVFAILED(ex,
                        "INTERNAL_ERROR",
	                      "attribute could not be removed from the device",
                        "DynamicAttrHelper::remove_all");
    }
    catch(...)
    {
	    THROW_DEVFAILED("UNKNOWN_ERROR",
	                    "could not remove attribute from the device",
                      "DynamicAttrHelper::remove_all");
    }

    //- remove from db
    v[1] = (*it).first;
    argin << v;
    Tango::Database * db = this->host_device_->get_db_device()->get_dbase();

    try
    {
      Tango::DeviceData argout = db->command_inout("DbDeleteDeviceAttribute", argin);
    }
    catch(Tango::DevFailed& ex)
    {
	    RETHROW_DEVFAILED(ex,
                        "INTERNAL_ERROR",
	                      "unable to delete attribute from the database",
                        "DynamicAttrHelper::remove_all");
    }
    catch(...)
    {
	    THROW_DEVFAILED("UNKNOWN_ERROR",
	                    "unable to delete attribute from the database",
                      "DynamicAttrHelper::remove_all");
    }
  }

  //- then clear the map
  this->rep_.clear();
}

} // namespace

