//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _COMMON_H_
#define _COMMON_H_

//-----------------------------------------------------------------------------
// ARM TARGET
//-----------------------------------------------------------------------------
#if defined(__arm__)
# define _EMBEDDED_DEVICE_
#else
# undef _EMBEDDED_DEVICE_
#endif

//-----------------------------------------------------------------------------
// DEPENDENCIES
//-----------------------------------------------------------------------------
#include <stdint.h>
#include <tango.h>
#if defined (_DEBUG)
# include <debug.h>
#endif
#if ! defined(_EMBEDDED_DEVICE_)
# include <error.h>
# include <client-lib.h>
#else
# include <cspi.h>
#endif
#include "Inline.h"
#include "TimeUtils.h"

//-----------------------------------------------------------------------------
// ASSERTION
//-----------------------------------------------------------------------------
#if defined (_DEBUG) || defined (DEBUG)
# include <assert.h>
# define DEBUG_ASSERT(BOOL_EXP) assert(BOOL_EXP)
#else
# define DEBUG_ASSERT(BOOL_EXP)
#endif

//-----------------------------------------------------------------------------
// MISC MACROS
//-----------------------------------------------------------------------------
#define _CPTC(X) static_cast<const char*>(X)

//=============================================================================
// THROW_DEVFAILED MACRO
//=============================================================================
#define THROW_DEVFAILED(p, q, r) \
  Tango::Except::throw_exception(_CPTC(p), _CPTC(q), _CPTC(r))

//=============================================================================
// RETHROW_DEVFAILED
//=============================================================================
#define RETHROW_DEVFAILED(ex, p, q, r) \
  Tango::Except::re_throw_exception(ex, _CPTC(p), _CPTC(q), _CPTC(r))
  
//-----------------------------------------------------------------------------
// TIME MACROS
//-----------------------------------------------------------------------------
#define TIME_VAL struct timeval
#define	GET_TIME(T) gettimeofday(&T, 0)
#if defined(_ELAPSED_SEC)
# undef ELAPSED_SEC
#endif
#define	ELAPSED_SEC(B, A) \
  static_cast<FPDataType>((A.tv_sec - B.tv_sec) + (1.E-6 * (A.tv_usec - B.tv_usec)))
#define	ELAPSED_MSEC(B, A) ELAPSED_SEC(B, A) * 1.E3
#if defined(_IS_VALID_TIME)
# undef IS_VALID_TIME
#endif
#define IS_VALID_TIME(T) T.tv_sec

namespace bpm {
//-----------------------------------------------------------------------------
// IMPL OPTIONS
//-----------------------------------------------------------------------------
#if defined (_USE_FLOAT_FP_DATA_)
  typedef float FPDataType;
# define DEV_DOUBLE DEV_FLOAT
#else
  typedef double FPDataType;
#endif

#if defined(__arm__) && defined(_USE_ARM_OPTIMIZATION_)
  typedef int IntOffsetDataType; 
#endif
} 

//-----------------------------------------------------------------------------
// NAN
//-----------------------------------------------------------------------------
#include <math.h>
#if defined (NAN)
# define _NAN_ NAN
#else
# define _NAN_ ::sqrt(-1)
#endif

#endif // __COMMON_H_
