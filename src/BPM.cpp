//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51 
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr 
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#if defined(_SIMULATION_)
# include <stdlib.h>
#endif
#include <sys/timeb.h>
#include "BPM.h" 
#include "ArmOptimization.h" 
#include "DataProcessing.h" 

#if !defined(__INLINE_IMPL__)
# include "BPM.i"
#endif // __INLINE_IMPL__    

// ============================================================================
// MISC MACROs & PSEUDO CONSTs
// ============================================================================
#define _CSPI_CONNECT_FAILURE_RELEASE_HANDLE_
//-----------------------------------------------------------------------------
#define kSRV_TMO 				1500
#define kWATCH_DOG_TMO	2000
//-----------------------------------------------------------------------------
#define kPOST_MORTEM_BUFFER_SIZE 16000
//-----------------------------------------------------------------------------
#define kLOCK_BUFFER   1
#define kUNLOCK_BUFFER 0
//-----------------------------------------------------------------------------
#define kCLOSE_DELAY_NSEC  750000000
//-----------------------------------------------------------------------------
#define kENV_CHANGED_MSG      static_cast<size_t>((bpm::FIRST_USER_MSG +  0))
#define kSRC_CHANGED_MSG      static_cast<size_t>((bpm::FIRST_USER_MSG +  1))
#define kDD_DECIM_CHANGED_MSG static_cast<size_t>((bpm::FIRST_USER_MSG +  2))
#define kGET_ENV_MSG          static_cast<size_t>((bpm::FIRST_USER_MSG +  3))
#define kSET_TIME_MSG         static_cast<size_t>((bpm::FIRST_USER_MSG +  4))
#define kSET_ENV_FA_MSG       static_cast<size_t>((bpm::FIRST_USER_MSG +  5))
#define kGET_ENV_FA_MSG       static_cast<size_t>((bpm::FIRST_USER_MSG +  6))    
#define kSAVE_DSC_PARAMS_MSG  static_cast<size_t>((bpm::FIRST_USER_MSG +  7))
#define kALGO_CHANGED_MSG     static_cast<size_t>((bpm::FIRST_USER_MSG +  8))
#define kRESET_PM_MSG         static_cast<size_t>((bpm::FIRST_USER_MSG +  9))
#define kRESET_INTERLOCK_MSG  static_cast<size_t>((bpm::FIRST_USER_MSG + 10))
#define kEVT_MAN_ERROR_MSG    static_cast<size_t>((bpm::FIRST_USER_MSG + 11))
//-----------------------------------------------------------------------------
#if ! defined(_EMBEDDED_DEVICE_)
# define kDD_UNLOCK_CACHE_MSG  static_cast<size_t>((bpm::FIRST_USER_MSG + 100))
# define kDD_CACHE_STATUS_MSG  static_cast<size_t>((bpm::FIRST_USER_MSG + 101))
#endif
//-----------------------------------------------------------------------------
#define kNO_SA_SAMPLE_AVAILABLE (rc == CSPI_E_SYSTEM && errno == EWOULDBLOCK)
//-----------------------------------------------------------------------------
#define kSA_STAT_PROC_THRESHOLD 10
//-----------------------------------------------------------------------------
#if ! defined(_EMBEDDED_DEVICE_)
//- TCP/IP connection timeout may be really huge (Libera down or network problem)
# define kCONNECTION_TIMEOUT (5 * 60 * 1000)
#else
# define kCONNECTION_TIMEOUT
#endif
//-----------------------------------------------------------------------------
#define kRECONFIG_MSG_PRIORITY (LOWEST_MSG_PRIORITY + 10)
//-----------------------------------------------------------------------------
#define kRSC_TEMP_UNAVAILABLE_CSPI  -5
#define kRSC_TEMP_UNAVAILABLE_ERRNO 11
//-----------------------------------------------------------------------------

// =========================================================================
// DUMP_CSPI_ERROR
// =========================================================================
#define DUMP_CSPI_ERROR(CSPI_ERR_CODE, CSPI_CMD, ORIGIN) \
  if (CSPI_ERR_CODE) \
  { \
    const char * cspi_err_txt =(CSPI_ERR_CODE != -1) \
                            ? \
                            ::cspi_strerror(CSPI_ERR_CODE) \
                            : \
                            "unknown or generic error"; \
    TangoSys_OMemStream r; \
    r << CSPI_CMD << " failed [err:" << CSPI_ERR_CODE; \
    if (errno) \
      r << " - errno:" << errno << ":" << ::strerror(errno); \
    r << "]" << std::ends; \
    TangoSys_OMemStream d; \
    d << "Libera CSPI error: " \
      << cspi_err_txt \
      << ":" \
      << ::strerror(errno) \
      << std::ends; \
    TangoSys_OMemStream o; \
    o << ORIGIN << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    try { \
      Tango::Except::throw_exception(r.str(), d.str(), o.str()); \
    } \
    catch(Tango::DevFailed& df) { \
      BPM::singleton->get_logger()->info_stream() << log4tango::LogInitiator::_begin_log << df << std::endl; \
    } \
  }
  
// ==========================================================================
// HANDLE_CSPI_ERROR: CSPI ERROR HANDLING MACRO 
// ==========================================================================
#define HANDLE_CSPI_ERROR(CSPI_ERR_CODE, CSPI_CMD, ORIGIN) \
  if (CSPI_ERR_CODE) \
  { \
    this->state_ = BPM_FAULT; \
    const char * cspi_err_txt = (CSPI_ERR_CODE != -1) \
							             ? \
							             ::cspi_strerror(CSPI_ERR_CODE) \
							             : \
							             "unknown or generic error"; \
    TangoSys_OMemStream r; \
    r << CSPI_CMD << " failed [err:" << CSPI_ERR_CODE; \
    if (errno) \
      r << " - errno:" << errno << ":" << ::strerror(errno); \
    r << "]" << std::ends; \
    TangoSys_OMemStream d; \
    d << "Libera CSPI error: " \
      << cspi_err_txt \
      << ":" \
      << ::strerror(errno) \
      << std::ends; \
    TangoSys_OMemStream o; \
    o << ORIGIN << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    try { \
      Tango::Except::throw_exception(r.str(), d.str(), o.str()); \
    } \
    catch(Tango::DevFailed& df) { \
      ERROR_STREAM << df << std::endl; \
      throw; \
    } \
  } \
  else \
  { \
    if (this->state_ != BPM_FAULT) \
        this->state_ = BPM_RUNNING; \
  } \
  
// =========================================================================
// LOGGING MACROS (for static members)
// =========================================================================  
#define BPM_ERROR_STREAM BPM::singleton->get_logger()->error_stream()
#define BPM_WARN_STREAM  BPM::singleton->get_logger()->warn_stream()
#define BPM_INFO_STREAM  BPM::singleton->get_logger()->info_stream()
#define BPM_DEBUG_STREAM BPM::singleton->get_logger()->debug_stream()

// =========================================================================
// HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER: CSPI ERROR HANDLING MACRO
// =========================================================================
#define HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(CSPI_ERR_CODE, CSPI_CMD, ORIGIN) \
  if (CSPI_ERR_CODE) \
  { \
    BPM::singleton->state_ = BPM_FAULT; \
    const char * cspi_err_txt =(CSPI_ERR_CODE != -1) \
							             ? \
							             ::cspi_strerror(CSPI_ERR_CODE) \
							             : \
							             "unknown or generic error"; \
    TangoSys_OMemStream r; \
    r << CSPI_CMD << " failed [err:" << CSPI_ERR_CODE; \
    if (errno) \
      r << " - errno:" << errno << ":" << ::strerror(errno); \
    r << "]" << std::ends; \
    TangoSys_OMemStream d; \
    d << "Libera CSPI error: " \
      << cspi_err_txt \
      << ":" \
      << ::strerror(errno) \
      << std::ends; \
    TangoSys_OMemStream o; \
    o << ORIGIN << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    try { \
      Tango::Except::throw_exception(r.str(), d.str(), o.str()); \
    } \
    catch(Tango::DevFailed& df) { \
      BPM::singleton->get_logger()->error_stream() << log4tango::LogInitiator::_begin_log << df << std::endl; \
      throw; \
    } \
  } \
  else \
  { \
    if (BPM::singleton->state_ != BPM_FAULT) \
        BPM::singleton->state_ = BPM_RUNNING; \
  } \

#if ! defined(_EMBEDDED_DEVICE_)
// =========================================================================
// HANDLE_SERVER_ERROR: GENERIC SERVER ERROR HANDLING MACRO 
// =========================================================================
#define HANDLE_SERVER_ERROR(SRV_ERR_CODE, SRV_CMD, ORIGIN) \
  if (SRV_ERR_CODE) \
  { \
    BPM::singleton->state_ = BPM_FAULT; \
    std::string err_txt("unknown generic server error"); \
    switch(errno) \
    { \
      case SRV_E_PROTO: \
        err_txt = "generic server error [protocol mismatch]"; \
        break; \
      case SRV_E_INVAL: \
        err_txt = "generic server error [invalid argument]"; \
        break; \
    } \
    TangoSys_OMemStream r; \
    r << SRV_CMD << " failed [err:" << SRV_ERR_CODE; \
    if (errno) \
      r << " - errno:" << errno << ":" << ::strerror(errno); \
    r << "]" << std::ends; \
    TangoSys_OMemStream d; \
    d << "Libera generic server error: " << err_txt << std::ends; \
    TangoSys_OMemStream o; \
    o << ORIGIN << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    try { \
      Tango::Except::throw_exception(r.str(), d.str(), o.str()); \
    } \
    catch(Tango::DevFailed& df) { \
      ERROR_STREAM << df << std::endl; \
      throw; \
    } \
  } \
  else \
  { \
		if (BPM::singleton->state_ != BPM_FAULT) \
        BPM::singleton->state_ = BPM_RUNNING; \
  }
#endif //- _EMBEDDED_DEVICE_

namespace bpm
{
  
// ============================================================================
// BPM static members
// ===========================================================================
BPM * BPM::singleton = 0;
bpm::Mutex BPM::lock;
bool BPM::closed = true;

// ============================================================================
// BPM::BPM
// ============================================================================
BPM::BPM(Tango::DeviceImpl * _host_device)
  : Tango::LogAdapter(_host_device),
    state_(BPM_UNKNOWN),
    dd_raw_buffer_(0),
    last_dd_data_(0),
    last_dd_data_updated_(false),
    dd_data_1_(0),
    dd_data_2_(0),
    adc_raw_buffer_(0),
    last_adc_data_(0),
    last_adc_data_updated_(false),
    adc_data_1_(0),
    adc_data_2_(0),
    last_sa_data_(0),
    last_sa_data_updated_(false),
    sa_data_1_(0),
    sa_data_2_(0),
    last_pm_data_(0),
    last_pm_data_updated_(false),
    trigger_counter_(0),
#if ! defined(_EMBEDDED_DEVICE_)
    connected_(false),
#endif
    dd_buffer_frozen_(false),
    srv_task_(0),
    dd_task_(0),
    sa_task_(0),
    adc_task_(0),
    host_device_(_host_device),
    pm_notified_(false),
    pm_event_counter_(0),
    dd_requires_trigger_notifications_(false),
    adc_requires_trigger_notifications_(false)
#if defined(_EMBEDDED_DEVICE_)
    , evts_man_(0)
#endif 

{
  BPM::singleton = this;
  BPM::closed = true;
}

// ============================================================================
// BPM::~BPM
// ============================================================================
BPM::~BPM (void)
{
  try
  {
    //- set BPM::closed to true before locking the mutex (see cspi_evt_handler)
    BPM::closed = true;
    //- enter critical section
    bpm::AutoMutex<bpm::Mutex> guard(BPM::lock);
    //- call actual close/release impl
    this->close_i();
  }
  catch(...)
  {
    //- ignore any error
  }
  BPM::singleton = 0;
}

// ============================================================================
// BPM::init
// ============================================================================
void BPM::init(const BPMConfig & _config)
  throw (Tango::DevFailed)
{
  //- enter critical section
  bpm::AutoMutex<bpm::Mutex> guard(BPM::lock);
  //- call actual init impl
  this->init_i(_config);
}

// ============================================================================
// BPM::init_i
// ============================================================================
void BPM::init_i(const BPMConfig & _config)
  throw (Tango::DevFailed)

{
  DEBUG_STREAM << "BPM::init <-" << std::endl;

  //- allocate the BPM thread
  try
  {
#if defined(_EMBEDDED_DEVICE_)
    //- be sure the liberad is not running on the Libera
    if (LiberaEventsManager::is_liberad_running(true))
    {
      Tango::Except::throw_exception(_CPTC("DEVICE_ERROR"),
                                    _CPTC("the Libera generic server is running on the Libera [kill it then exec init on the BPM device]"),
                                    _CPTC("BPM::init"));
    }
    //- be sure the eventd is not running on the Libera
    if (LiberaEventsManager::is_eventd_running(true))
    {
      Tango::Except::throw_exception(_CPTC("DEVICE_ERROR"),
                                    _CPTC("the Libera events daemon is running on the Libera [kill it then exec init on the BPM device]"),
                                    _CPTC("BPM::init"));
    }
#endif
 
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
    //- precompute scaling factors for int to fp conversions
    ArmOptimization::init_scaling_factors();
#endif
      
    //- store "requested" configuration localy
    this->config_ = _config;

    //- enable/disable dd optional data
    DDData::m_optional_data_enabled =
            _config.dd_optional_data_enabled();
            
    //- enable/disable sa optional data
    SAData::m_optional_data_enabled =
            _config.sa_optional_data_enabled();

    //- enable/disable adc optional data
    ADCData::m_optional_data_enabled =
            _config.adc_optional_data_enabled();
            
    //- enable/disable sa history optional data 
    SAHistory::m_optional_data_enabled =
            _config.sa_history_optional_data_enabled();
            
    //- SRV task ----------------------------------------
    DEBUG_STREAM << "BPM::instanciating SRV task" << std::endl;
    bpm::Task::Config srv_config(this->host_device_,
                                BPM::srv_message_handler,
                                static_cast<Thread::IOArg>(this));
    this->srv_task_ = new bpm::Task(srv_config);
    if (this->srv_task_ == 0)
      throw std::bad_alloc();
      
    //- DD task ----------------------------------------
    DEBUG_STREAM << "BPM::instanciating DD task" << std::endl;
    bpm::Task::Config dd_config(this->host_device_,
                                BPM::dd_message_handler,
                                static_cast<Thread::IOArg>(this));
    this->dd_task_ = new bpm::Task(dd_config);
    if (this->dd_task_ == 0)
      throw std::bad_alloc();
      
    //- SA task ----------------------------------------
    DEBUG_STREAM << "BPM::instanciating SA task" << std::endl;
    bpm::Task::Config sa_config(this->host_device_,
                                BPM::sa_message_handler,
                                static_cast<Thread::IOArg>(this));
    this->sa_task_ = new bpm::Task(sa_config);
    if (this->sa_task_ == 0)
      throw std::bad_alloc();
      
    //- ADC task ----------------------------------------
    DEBUG_STREAM << "BPM::instanciating ADC task" << std::endl;
    bpm::Task::Config adc_config(this->host_device_,
                                BPM::adc_message_handler,
                                static_cast<Thread::IOArg>(this));
    this->adc_task_ = new bpm::Task(adc_config);
    if (this->adc_task_ == 0)
      throw std::bad_alloc();

#if defined(_EMBEDDED_DEVICE_)
    //- evt manager -------------------------------------
    LiberaEventsManagerConfig cfg;
    cfg.user_data = this;
    cfg.host_device = this->host_device_;
    cfg.evt_handler = BPM::libera_event_callback;
    cfg.evts_mask = 0;
    this->evts_man_ = new LiberaEventsManager(cfg);
    if (this->evts_man_ == 0)
      throw std::bad_alloc();
#endif //- _EMBEDDED_DEVICE_

  }
  catch(const std::bad_alloc&)
  {
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("bpm::Task allocation failed"),
                                   _CPTC("BPM::init"));
  }
  catch(const Tango::DevFailed& df)
  {
    Tango::Except::re_throw_exception(const_cast<Tango::DevFailed&>(df),
                                      _CPTC("INTERNAL_ERROR"),
                                      _CPTC("bpm::Task allocation failed"),
                                      _CPTC("BPM::init"));
  }
  catch(...)
  {
    Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                   _CPTC("bpm::Task allocation failed"),
                                   _CPTC("BPM::init"));
  }

  //- start the BPM tasks
  try
  {
#if defined(_EMBEDDED_DEVICE_)
    //- start the Libera events manager
    this->evts_man_->go();
    //- give other threads some time to run
    Thread::yield();
#endif

    //- start the service task(use the huge connection tmo)
    this->srv_task_->go(kCONNECTION_TIMEOUT);
    
    //- start the dd task(use the huge connection tmo)
    this->dd_task_->go(kCONNECTION_TIMEOUT);
      
    //- start the sa task(use the huge connection tmo)
    this->sa_task_->go(kCONNECTION_TIMEOUT);
      
    //- start the adc task(use the huge connection tmo)
    this->adc_task_->go(kCONNECTION_TIMEOUT);
  }
  catch (const Tango::DevFailed& df)
  {
    //- cleanup
    try { this->close_i(); } 
    catch (const Tango::DevFailed&) { }
    catch (...) { }
    Tango::Except::re_throw_exception(const_cast<Tango::DevFailed&>(df),
                                      _CPTC("initialization error"),
                                      _CPTC("BPM intialization failed"),
                                      _CPTC("BPM::init"));
  }
  catch (...)
  {
    //- cleanup
    try { this->close_i(); } 
    catch (const Tango::DevFailed&) { }
    catch (...) { }
    Tango::Except::throw_exception(_CPTC("UNKNOWN_ERROR"),
                                   _CPTC("BPM initialization failed"),
                                   _CPTC("BPM::init"));
  }

  //- start to accept cspi notification
  BPM::closed = false;

  //- update internal state
  this->state_ = BPM_RUNNING;

  DEBUG_STREAM << "BPM::init ->" << std::endl;
}

// ============================================================================
// BPM::close_i
// ============================================================================
void BPM::close_i (void)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::close <-" << std::endl;
  
#if defined(_EMBEDDED_DEVICE_)
  //- disable all cspi events
  DEBUG_ASSERT(this->evts_man_ != 0);
  this->evts_man_->set_events_mask(0);
#endif

  //- stop SRV task -----------------
  if (this->srv_task_)
  {
    try
    {
      this->srv_task_->exit();
      this->srv_task_ = 0;
    }
    catch(...) {}
  }
  
  //- stop DD task -----------------------
  if (this->dd_task_)
  {
    try
    {
      this->dd_task_->exit();
      this->dd_task_ = 0;
    }
    catch(...) {}
  }
  
  //- stop ADC task ----------------------
  if (this->adc_task_)
  {
    try
    {
      this->adc_task_->exit();
      this->adc_task_ = 0;
    }
    catch(...) {}
  }
  
  //- stop SA task -----------------------
  if (this->sa_task_)
  {
    try
    {
      //- abort task
      this->sa_task_->exit();
      this->sa_task_ = 0;
    }
    catch(...) {}
  }

#if defined(_EMBEDDED_DEVICE_)
  //- delete the Libera events manager
  if (this->evts_man_)
  {
    try
    {
      this->evts_man_->exit();
      this->evts_man_ = 0;
    }
    catch (...) {}
  }
#endif

  //- release preallocate data structures
  if (this->dd_data_1_)
  {
    this->dd_data_1_->release();
    this->dd_data_1_ = 0;
  }
  if (this->dd_data_2_)
  {
    this->dd_data_2_->release();
    this->dd_data_2_ = 0;
  }
  if (this->adc_data_1_)
  {
    this->adc_data_1_->release();
    this->adc_data_1_ = 0;
  }
  if (this->adc_data_2_)
  {
    this->adc_data_2_->release();
    this->adc_data_2_ = 0;
  }
  if (this->sa_data_1_)
  {
    this->sa_data_1_->release();
    this->sa_data_1_ = 0;
  }
  if (this->sa_data_2_)
  {
    this->sa_data_2_->release();
    this->sa_data_2_ = 0;
  }
  
  this->last_dd_data_ = 0;
  this->last_sa_data_ = 0;
  this->last_adc_data_ = 0;
  
  //- update internal state
  this->state_ = BPM_UNKNOWN;
  
  DEBUG_STREAM << "BPM::close ->" << std::endl;
}

# if ! defined(_EMBEDDED_DEVICE_)
// ============================================================================
// BPM::connect_to_generic_server
// ============================================================================
void BPM::connect_to_generic_server (const std::string& caller)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << caller << "::connecting to Libera generic server..." << std::endl;
  
  //- max num of connection retries
  size_t retries = 4;
  
  //- retry loop
  do
  {
    //- open connection to Libera generic server
    int rc =::server_connect(this->config_.ip_addr_.c_str(), 
                             this->config_.gs_port_,
                             this->config_.mcast_ip_addr_.c_str(),
                             0);
                        
    //- connected, exit do/while loop                  
    if (! rc)
      break;
      
    //- not connected, let's retry unless we reach the "retry limit"
    if (--retries == 0)
    {
      HANDLE_SERVER_ERROR(rc, "server_connect", "BPM::connect_to_generic_server");  
    }
 
    //- about to retry, sleep for a while before retrying
    INFO_STREAM << caller << "::connection to Libera generic server failed, retrying..." << std::endl;
    Thread::sleep(250);                     
  }
  while (true);
  
  INFO_STREAM << caller << " connected to Libera generic server" << std::endl;
}

// ============================================================================
// BPM::disconnect_from_generic_server
// ============================================================================
void BPM::disconnect_from_generic_server (const std::string& caller)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << caller << "::disconnecting from Libera generic server..." << std::endl;
  
  int rc =::server_disconnect();
  
  INFO_STREAM << caller << " disconnected from Libera generic server" << std::endl;
}
# endif // ! _EMBEDDED_DEVICE_

// ============================================================================
// BPM::init_srv_task
// ============================================================================
void BPM::init_srv_task (void)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::init_srv_task <-" << std::endl;
  
  //- The bpm::Task's mutex is locked (from message handler)
  //-------------------------------------------------------------------
  try
  {
    //- error code
    int rc = 0;

    //- instanciate the fa data cache(s)
    fa_data_rd_.allocate(0, 4, 1024);
    fa_data_cp_.allocate(0, 4, 1024);

#if ! defined(_SIMULATION_)      

# if ! defined(_EMBEDDED_DEVICE_)
    //- connect to Libera generic server
    this->connect_to_generic_server("Service-task");
# endif // ! _EMBEDDED_DEVICE_
      
    CSPI_LIBPARAMS lib_params = {1, 1};
    DEBUG_STREAM << "BPM::init_srv_task::getting super-user access to the CSPI" << std::endl;
    rc = ::cspi_setlibparam(&lib_params, CSPI_LIB_SUPERUSER);
    HANDLE_CSPI_ERROR(rc, "cspi_setlibparam", "BPM::init_srv_task");

    DEBUG_STREAM << "BPM::init_srv_task::allocating env. handle" << std::endl;
    rc = ::cspi_allochandle(CSPI_HANDLE_ENV, 0, &this->srv_task_->henv_);
    HANDLE_CSPI_ERROR(rc, "cspi_allochandle", "BPM::init_srv_task");
    
    DEBUG_STREAM << "BPM::init_srv_task::allocating con. handle" << std::endl;
    rc = ::cspi_allochandle(CSPI_HANDLE_CON, this->srv_task_->henv_, &this->srv_task_->hcon_);
    HANDLE_CSPI_ERROR(rc, "cspi_allochandle", "BPM::init_srv_task");
    
#endif //- !_SIMULATION_

  //- compute actual offset(synch. with current auto switching config) 
  //- this will also update <this->config_.requested_ep_> to pass Kx, Kz and offsets to the Libera FPGA
  this->config_.compute_actual_offsets(this->config_.requested_ep_,
                                       this->config_.requested_ep_.switches == kAUTO_SWITCHING_MODE); 
    
#if ! defined(_SIMULATION_) 
  //- pass Kx, Kz, offsets, interlock thresholds & switching config to the Libera FPGA 
  int ep_flags = CSPI_ENV_KX 
               | CSPI_ENV_KY 
               | CSPI_ENV_XOFFSET 
               | CSPI_ENV_YOFFSET 
               | CSPI_ENV_QOFFSET 
               | CSPI_ENV_ILK 
               | CSPI_ENV_SWITCH;
  DEBUG_STREAM << "BPM::init_srv_task::passing Kx, Kz and offsets to the Libera [FPGA side]" << std::endl;
  rc = ::cspi_setenvparam(this->srv_task_->henv_, &this->config_.requested_ep_, ep_flags);
  HANDLE_CSPI_ERROR(rc, "cspi_setenvparam", "BPM::init_srv_task");
  
  DEBUG_STREAM << "BPM::init_srv_task::obtaining current env. params from the Libera" << std::endl;
  this->get_env_parameters(this->srv_task_->henv_, this->config_.actual_ep_);
#endif

  //- sync requested with actual env. params
  this->config_.actual_to_requested_ep();
      
//#if defined(_DEBUG)
  this->config_.dump_env_parameters();
//#endif

#if ! defined(_SIMULATION_)
    //- set mode of operation - required in order to connect to data source
    DEBUG_STREAM << "BPM::init_srv_task::applying default con. parameters" << std::endl;
    this->set_con_parameters(this->srv_task_->hcon_, &this->config_.cp_srv_, CSPI_CON_MODE);
    
    //- actual connection to data source
    DEBUG_STREAM << "BPM::init_srv_task::connecting to PM data source" << std::endl;
    rc = ::cspi_connect(this->srv_task_->hcon_);
    HANDLE_CSPI_ERROR(rc, "cspi_connect", "BPM::init_srv_task");
    
    this->srv_task_->connected_ = true;
    
    DEBUG_STREAM << "BPM::init_srv_task::enabling PM & ITLK notifications" << std::endl;
    this->enable_pm_notifications();
    this->enable_interlock_notifications();
    
    this->srv_task_->enable_timeout_msg(false);
    size_t p = this->config_.fa_cache_refresh_period_;
    if (p > kSRV_TMO)
      p = kSRV_TMO;
    this->srv_task_->set_periodic_msg_period(p);
    this->srv_task_->enable_periodic_msg(true);

#else
    this->srv_task_->connected_ = true;
#endif //- !_SIMULATION_
  }
  catch(const Tango::DevFailed& df)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    Tango::Except::re_throw_exception(const_cast<Tango::DevFailed&>(df),
                                      _CPTC("initialization error"),
                                      _CPTC("TANGO exception caught while trying to initialize the BPM"),
                                      _CPTC("BPM::init_srv_task"));
  }
  catch(const std::bad_alloc &)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                  _CPTC("memory allocation failed"), 
                                  _CPTC("BPM::init_srv_tDSC_CHANGED...................yeask"));
  }
  catch(...)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    //- propagate caught exception as Tango::DevFailed
    Tango::Except::throw_exception(_CPTC("UNKNOWN_ERROR"),
                                   _CPTC("unknown exception caught while trying to initialize the BPM"),
                                   _CPTC("BPM::init_srv_task"));
  }
  DEBUG_STREAM << "BPM::init_srv_task ->" << std::endl; 
}

// ============================================================================
// BPM::close_srv_task
// ============================================================================
void BPM::close_srv_task (void)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::close_srv_task <-" << std::endl;
  
#if ! defined(_SIMULATION_)
  //- The bpm::Task's mutex is locked
  //-------------------------------------------------------------------
  try
  {
    //- disable periodic message
    this->srv_task_->enable_periodic_msg(false);
  
    //- disable notification
    DEBUG_STREAM << "BPM::close_srv_task::disable asynch. notifications" << std::endl;
    if (this->srv_task_->connected_) 
      this->disable_notifications();
    
    //- disable buffer freezing
    DEBUG_STREAM << "BPM::close_srv_task::disable buffer freezing" << std::endl;
    this->disable_dd_buffer_freezing();
    
    //- sleep for a while in order to let incoming notification to be handle
#if ! defined(_EMBEDDED_DEVICE_)
    omni_thread::sleep(0, kCLOSE_DELAY_NSEC);
#endif
  }
  catch(Tango::DevFailed df)
  {
    ERROR_STREAM << df << std::endl;
  }
  catch(...)
  {
    /*ignore error */
  }
  
  try 
  {
    //- release con handle
    if (this->srv_task_->hcon_)
    {
      DEBUG_STREAM << "BPM::close_srv_task::disconnect from Libera data source" << std::endl;
      ::cspi_disconnect(this->srv_task_->hcon_);
      DEBUG_STREAM << "BPM::close_srv_task::release con. handle" << std::endl;
      ::cspi_freehandle(CSPI_HANDLE_CON, this->srv_task_->hcon_);
      this->srv_task_->hcon_ = 0;
    }
    
    //- release env handle
    if (this->srv_task_->henv_)
    {
      DEBUG_STREAM << "BPM::close_srv_task::release env. handle" << std::endl;
      ::cspi_freehandle(CSPI_HANDLE_ENV, this->srv_task_->henv_);
      this->srv_task_->henv_ = 0;
    }
      
#if ! defined(_EMBEDDED_DEVICE_)
    //- close TCP/IP connection to generic server
    if (this->srv_task_->connected_)
      this->disconnect_from_generic_server("Service-task");
#endif

    this->srv_task_->connected_ = false;
  }
  catch(...) 
  { 
    /* ignore error */ 
  }
  
#else

  this->srv_task_->connected_ = false;
  
#endif //- !_SIMULATION_

  DEBUG_STREAM << "BPM::close_srv_task::release last_pm_data" << std::endl;
  
  //- release any existing DD data
  if (this->last_pm_data_)
  {
    this->last_pm_data_->release();
    this->last_pm_data_ = 0;
  }
  
  DEBUG_STREAM << "BPM::close_srv_task ->" << std::endl;
}

// ============================================================================
// BPM::dd_task_connect
// ============================================================================
void BPM::dd_task_connect (void)
    throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::dd_task_connect <-" << std::endl;

  //- The bpm::Task's mutex is locked(from message handler)
  //-------------------------------------------------------------------
  try
  {
    //- error code
    int rc = 0;

    try
    {
      //- preallocate DDData 1
      this->dd_data_1_ = new DDData();
      if (! this->dd_data_1_)
        throw(std::bad_alloc());
      this->dd_data_1_->allocate(this->config_.get_dd_raw_buffer_depth(), true);
      
      //- preallocate DDData 2
      this->dd_data_2_ = new DDData();
      if (! this->dd_data_2_)
        throw(std::bad_alloc());
      this->dd_data_2_->allocate(this->config_.get_dd_raw_buffer_depth(), true);

      //- do as if last_data has been acquired in DDData 2
      this->last_dd_data_ = this->dd_data_2_;
    }
    catch (...)
    {
      ERROR_STREAM << "BPM::dd_task_connect::DDData preallocation failed" << std::endl;
      throw(std::bad_alloc());
    }

#if defined(_SIMULATION_) 
    this->dd_task_->connected_ = true;
    DEBUG_STREAM << "BPM::dd_task_connect ->" << std::endl;
    return;
#endif

#if ! defined(_EMBEDDED_DEVICE_)
    //- connect to Libera generic server
    this->connect_to_generic_server("DD-task");

    //- tbt (i.e. DD) buffer freezing configuration : cache size
    int cache_size = this->config_.dd_buffer_freezing_enabled_
                   ? this->config_.get_dd_raw_buffer_depth() 
                   : 0;
                  
    //- tbt (i.e. DD) buffer freezing configuration : cache status
    int cache_lock = kUNLOCK_BUFFER;

    //- set cache size
    DEBUG_STREAM << "BPM::dd_task_connect::setting cache size" << std::endl;
    rc =::server_setparam(protocol::SERVER_CACHE_SIZE, &cache_size);
    HANDLE_SERVER_ERROR(rc, "server_setparam", "BPM::dd_task_connect");
    
    //- unlock the cache
    DEBUG_STREAM << "BPM::dd_task_connect::setting cache lock" << std::endl;
    rc =::server_setparam(protocol::SERVER_CACHE_LOCK, &cache_lock);
    HANDLE_SERVER_ERROR(rc, "server_setparam", "BPM::dd_task_connect");
#endif // ! _EMBEDDED_DEVICE_

    //- reset buffer freezing flag
    this->dd_buffer_frozen_ = false;

    DEBUG_STREAM << "BPM::dd_task_connect::allocating env. handle" << std::endl;
		rc = ::cspi_allochandle(CSPI_HANDLE_ENV, 0, &this->dd_task_->henv_);
    HANDLE_CSPI_ERROR(rc, "cspi_allochandle", "BPM::dd_task_connect");

    DEBUG_STREAM << "BPM::dd_task_connect::allocating con. handle" << std::endl;
		rc = ::cspi_allochandle(CSPI_HANDLE_CON, this->dd_task_->henv_, &this->dd_task_->hcon_);
    HANDLE_CSPI_ERROR(rc, "cspi_allochandle", "BPM::dd_task_connect");

    //- set mode of operation - required in order to connect to data source
    DEBUG_STREAM << "BPM::dd_task_connect::applying default con. parameters" << std::endl;
    this->set_con_parameters(this->dd_task_->hcon_, &this->config_.cp_dd_, CSPI_CON_MODE);
    
    //- actual connection to data source
    DEBUG_STREAM << "BPM::dd_task_connect::connecting to DD data source" << std::endl;
    rc = ::cspi_connect(this->dd_task_->hcon_);
    HANDLE_CSPI_ERROR(rc, "cspi_connect", "BPM::dd_task_connect");
    
    this->dd_task_->connected_ = true;
    
    //- apply DD decimation factor parameters
    DEBUG_STREAM << "BPM::dd_task_connect::applying decimation factor" << std::endl;
    this->config_.cp_dd_.dec = this->config_.get_decimation_factor();
    this->set_con_parameters(this->dd_task_->hcon_, &this->config_.cp_dd_, CSPI_CON_DEC);
    
    //- enable DD?
    if (this->configuration().dd_enabled())
      this->enable_dd_notifications();
    else
      this->disable_dd_notifications();
      
    size_t tmo = this->config_.external_trigger_enabled()
              ? kWATCH_DOG_TMO
              : this->config_.get_dd_thread_activity_period();
              
    this->dd_task_->set_periodic_msg_period(tmo);
    this->dd_task_->enable_periodic_msg(true);
  }
  catch(const Tango::DevFailed& df)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    Tango::Except::re_throw_exception(const_cast<Tango::DevFailed&>(df),
                                      _CPTC("initialization error"),
                                      _CPTC("TANGO exception caught while trying to initialize the BPM"),
                                      _CPTC("BPM::dd_task_connect"));
  }
  catch(const std::bad_alloc &)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("memory allocation failed"),
                                   _CPTC("BPM::dd_task_connect"));
  }
  catch(...)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    //- propagate caught exception as Tango::DevFailed
    Tango::Except::throw_exception(_CPTC("UNKNOWN_ERROR"),
                                   _CPTC("unknown exception caught while trying to initialize the BPM"),
                                   _CPTC("BPM::dd_task_connect"));
  }
  DEBUG_STREAM << "BPM::dd_task_connect ->" << std::endl;
}
  
// ============================================================================
// BPM::dd_task_disconnect
// ============================================================================
void BPM::dd_task_disconnect (void)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::dd_task_disconnect <-" << std::endl;

  //- The bpm::Task's mutex is locked
  //-------------------------------------------------------------------
  try
  {
		this->dd_task_->enable_periodic_msg(false);

    this->disable_dd_notifications();
    
#if ! defined(_SIMULATION_) 
    //- release con handle
    if (this->dd_task_->hcon_)
    {
      DEBUG_STREAM << "BPM::dd_task_disconnect::disconnect from Libera data source" << std::endl;
      ::cspi_disconnect(this->dd_task_->hcon_);
      DEBUG_STREAM << "BPM::dd_task_disconnect::release con. handle" << std::endl;
      ::cspi_freehandle(CSPI_HANDLE_CON, this->dd_task_->hcon_);
      this->dd_task_->hcon_ = 0;
    }
    
    //- release env handle
    if (this->dd_task_->henv_)
    {
      DEBUG_STREAM << "BPM::dd_task_disconnect::release env. handle" << std::endl;
      ::cspi_freehandle(CSPI_HANDLE_ENV, this->dd_task_->henv_);
      this->dd_task_->henv_ = 0;
    }
    
 #if ! defined(_EMBEDDED_DEVICE_)
    //- close TCP/IP connection to generic server
    if (this->dd_task_->connected_)
      this->disconnect_from_generic_server("DD-task");
#endif

#endif
  
    this->dd_task_->connected_ = false;
  }
  catch(...) 
  { 
    /* ignore error */ 
  }
  
  DEBUG_STREAM << "BPM::dd_task_disconnect::release last_dd_data" << std::endl;

  //- release DDData 1
  if (this->dd_data_1_)
  {
    this->dd_data_1_->release();
    this->dd_data_1_ = 0;
  }
  //- release DDData 2
  if (this->dd_data_2_)
  {
    this->dd_data_2_->release();
    this->dd_data_2_ = 0;
  }
  
  //- no last data
  this->last_dd_data_ = 0;
  
  DEBUG_STREAM << "BPM::dd_task_disconnect ->" << std::endl;
}

// ============================================================================
// BPM::sa_task_connect
// ============================================================================
void BPM::sa_task_connect (void)
    throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::sa_task_connect <-" << std::endl;
  
  //- The bpm::Task's mutex is locked(from message handler)
  //-------------------------------------------------------------------
  try
  {
    try
    {
      //- preallocate SAData 1
      this->sa_data_1_ = new SAData();
      if (! this->sa_data_1_)
        throw(std::bad_alloc());
      
      //- preallocate SAData 2
      this->sa_data_2_ = new SAData();
      if (! this->sa_data_2_)
        throw(std::bad_alloc());

      //- do as if last_data has been acquired in SAData 2
      this->last_sa_data_ = this->sa_data_2_;
    }
    catch (...)
    {
      ERROR_STREAM << "BPM::sa_task_connect::SAData preallocation failed" << std::endl;
      throw(std::bad_alloc());
    }
    
    //- allocate SA history buffer
    DEBUG_STREAM << "BPM::sa_task_connect::allocating history buffer" << std::endl;
    size_t count = this->config_.get_sa_history_depth();
    if (count == 0)
    {
      Tango::Except::throw_exception(_CPTC("invalid parameter"), 
                                      _CPTC("invalid SA history depth specified"), 
                                      _CPTC("BPM::sa_task_connect"));
    }
    this->sa_history_.depth(count);
		this->num_sa_samples_since_last_stat_proc_ = 0;

#if ! defined(_SIMULATION_) 
    //- error code
    int rc = 0;
      
# if ! defined(_EMBEDDED_DEVICE_)
    //- connect to Libera generic server
    this->connect_to_generic_server("SA-task");
# endif //- _EMBEDDED_DEVICE_

    DEBUG_STREAM << "BPM::sa_task_connect::allocating env. handle" << std::endl;
		rc = ::cspi_allochandle(CSPI_HANDLE_ENV, 0, &this->sa_task_->henv_);
    HANDLE_CSPI_ERROR(rc, "cspi_allochandle", "BPM::sa_task_connect");

    DEBUG_STREAM << "BPM::sa_task_connect::allocating con. handle" << std::endl;
		rc = ::cspi_allochandle(CSPI_HANDLE_CON, this->sa_task_->henv_, &this->sa_task_->hcon_);
    HANDLE_CSPI_ERROR(rc, "cspi_allochandle", "BPM::sa_task_connect");

    //- set mode of operation - required in order to connect to data source
    DEBUG_STREAM << "BPM::sa_task_connect::applying default con. parameters" << std::endl;
    this->set_con_parameters(this->sa_task_->hcon_, &this->config_.cp_sa_, CSPI_CON_MODE);
    
    //- actual connection to data source
    DEBUG_STREAM << "BPM::sa_task_connect::connecting to SA data source" << std::endl;
    rc = ::cspi_connect(this->sa_task_->hcon_);
    HANDLE_CSPI_ERROR(rc, "cspi_connect", "BPM::sa_task_connect");
    
    this->sa_task_->connected_ = true;
    
    //- set non blocking reading mode
    DEBUG_STREAM << "BPM::sa_task_connect::set mode non-blocking reading mode" << std::endl;
    this->set_con_parameters(this->sa_task_->hcon_, &this->config_.cp_sa_, CSPI_CON_SANONBLOCK);
    
    //- enable SA?
    if (this->configuration().sa_enabled())
      this->enable_sa_notifications();
    else
      this->disable_sa_notifications();
            
#else

    this->sa_task_->connected_ = true;  
      
#endif // !_SIMULATION_ 

    size_t tmo = 0;
    bool enable_pmsg = false;

#if ! defined(_EMBEDDED_DEVICE_)

#	if defined(_USE_SA_EVENTS_)
    tmo = kWATCH_DOG_TMO;
#	else
    tmo = this->config_.get_sa_thread_activity_period();
#	endif
    enable_pmsg = true;

#else //- !_EMBEDDED_DEVICE_

#	if ! defined(_USE_SA_EVENTS_)
    tmo = this->config_.get_sa_thread_activity_period();
    enable_pmsg = true;
# endif 

#endif //- !_EMBEDDED_DEVICE_

    this->sa_task_->set_periodic_msg_period(tmo);
    this->sa_task_->enable_periodic_msg(enable_pmsg);
  }
  catch(const Tango::DevFailed& df)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    Tango::Except::re_throw_exception(const_cast<Tango::DevFailed&>(df),
                                      _CPTC("initialization error"),
                                      _CPTC("TANGO exception caught while trying to initialize the BPM"),
                                      _CPTC("BPM::sa_task_connect"));
  }
  catch(const std::bad_alloc &)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                    _CPTC("memory allocation failed"), 
                                    _CPTC("BPM::sa_task_connect"));
  }
  catch(...)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    //- propagate caught exception as Tango::DevFailed
    Tango::Except::throw_exception(_CPTC("UNKNOWN_ERROR"),
                                    _CPTC("unknown exception caught while trying to initialize the BPM"),
                                    _CPTC("BPM::sa_task_connect"));
  }
  
  DEBUG_STREAM << "BPM::sa_task_connect ->" << std::endl;
}
  
// ============================================================================
// BPM::sa_task_disconnect
// ============================================================================
void BPM::sa_task_disconnect (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::sa_task_disconnect <-" << std::endl;
  
  //- The bpm::Task's mutex is locked
  //-------------------------------------------------------------------
  try
  {
    this->sa_task_->enable_periodic_msg(false);

    this->disable_sa_notifications();
      
#if ! defined(_SIMULATION_) 
    //- release con handle
    if (this->sa_task_->hcon_)
    {
      DEBUG_STREAM << "BPM::sa_task_disconnect::disconnect from Libera data source" << std::endl;
      ::cspi_disconnect(this->sa_task_->hcon_);
      ::cspi_freehandle(CSPI_HANDLE_CON, this->sa_task_->hcon_);
      this->sa_task_->hcon_ = 0;
    }
      
    //- release env handle
    if (this->sa_task_->henv_)
    {
      ::cspi_freehandle(CSPI_HANDLE_ENV, this->sa_task_->henv_);
      this->sa_task_->henv_ = 0;
    }
      
# if ! defined(_EMBEDDED_DEVICE_)
    //- close connection to generic server
    if (this->sa_task_->connected_)
      this->disconnect_from_generic_server("SA-task");
# endif //- _EMBEDDED_DEVICE_

#endif // !_SIMULATION_

    this->sa_task_->connected_ = false;
  }
  catch(...) 
  { 
    /* ignore error */ 
  }
  
  //- release any existing SA data
  DEBUG_STREAM << "BPM::sa_task_disconnect::release last_sa_data" << std::endl;

  //- release SAData 1
  if (this->sa_data_1_)
  {
    this->sa_data_1_->release();
    this->sa_data_1_ = 0;
  }
  //- release DDData 2
  if (this->sa_data_2_)
  {
    this->sa_data_2_->release();
    this->sa_data_2_ = 0;
  }
  
  //- no last data
  this->last_sa_data_ = 0;
  
  DEBUG_STREAM << "BPM::sa_task_disconnect ->" << std::endl;
}

// ============================================================================
// BPM::adc_task_connect
// ============================================================================
void BPM::adc_task_connect (void)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::adc_task_connect <-" << std::endl;

  //- The bpm::Task's mutex is locked(from message handler)
  //-------------------------------------------------------------------
  try
  {
    //- error code
    int rc = 0;
    
    try
    {
      //- preallocate ADCData 1
      this->adc_data_1_ = new ADCData();
      if (! this->adc_data_1_)
        throw(std::bad_alloc());
      this->adc_data_1_->allocate(this->config_.get_adc_raw_buffer_depth(), true);
      
      //- preallocate ADCData A
      this->adc_data_2_ = new ADCData();
      if (! this->adc_data_2_)
        throw(std::bad_alloc());
      this->adc_data_2_->allocate(this->config_.get_adc_raw_buffer_depth(), true);

      //- do as if last_data has been acquired in ADCData 2
      this->last_adc_data_ = this->adc_data_2_;
    }
    catch (...)
    {
      ERROR_STREAM << "BPM::adc_task_connect::ADCData preallocation failed" << std::endl;
      throw(std::bad_alloc());
    }

#if defined(_SIMULATION_) 
    this->adc_task_->connected_ = true;
    DEBUG_STREAM << "BPM::adc_task_connect ->" << std::endl;
    return;
#endif

#if ! defined(_EMBEDDED_DEVICE_)
    //- connect to Libera generic server
    this->connect_to_generic_server("ADC-task");
#endif //- _EMBEDDED_DEVICE_

    DEBUG_STREAM << "BPM::adc_task_connect::allocating env. handle" << std::endl;
		rc = ::cspi_allochandle(CSPI_HANDLE_ENV, 0, &this->adc_task_->henv_);
    HANDLE_CSPI_ERROR(rc, "cspi_allochandle", "BPM::adc_task_connect");

    DEBUG_STREAM << "BPM::adc_task_connect::allocating con. handle" << std::endl;
		rc = ::cspi_allochandle(CSPI_HANDLE_CON, this->adc_task_->henv_, &this->adc_task_->hcon_);
    HANDLE_CSPI_ERROR(rc, "cspi_allochandle", "BPM::adc_task_connect");

    //- set mode of operation - required in order to connect to data source
    DEBUG_STREAM << "BPM::adc_task_connect::applying default con. parameters" << std::endl;
    this->set_con_parameters(this->adc_task_->hcon_, &this->config_.cp_adc_, CSPI_CON_MODE);
    
    //- actual connection to data source
    DEBUG_STREAM << "BPM::adc_task_connect::connecting to DD data source" << std::endl;
    rc = ::cspi_connect(this->adc_task_->hcon_);
    HANDLE_CSPI_ERROR(rc, "cspi_connect", "BPM::adc_task_connect");
    
    this->adc_task_->connected_ = true;

    //- enable ADC?
    if (this->configuration().adc_enabled())
      this->enable_adc_notifications();
    else
      this->disable_adc_notifications();
      
    size_t tmo = this->config_.external_trigger_enabled()
               ? kWATCH_DOG_TMO
               : this->config_.get_dd_thread_activity_period();
              
    this->adc_task_->set_periodic_msg_period(tmo);
    this->adc_task_->enable_periodic_msg(true);
  }
  catch(const Tango::DevFailed& df)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    Tango::Except::re_throw_exception(const_cast<Tango::DevFailed&>(df),
                                      _CPTC("initialization error"),
                                      _CPTC("TANGO exception caught while trying to initialize the BPM"),
                                      _CPTC("BPM::adc_task_connect"));
  }
  catch(const std::bad_alloc &)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("memory allocation failed"),
                                   _CPTC("BPM::adc_task_connect"));
  }
  catch(...)
  {
    //- update internal state
    this->state_ = BPM_FAULT;
    //- propagate caught exception as Tango::DevFailed
    Tango::Except::throw_exception(_CPTC("UNKNOWN_ERROR"),
                                   _CPTC("unknown exception caught while trying to initialize the BPM"),
                                   _CPTC("BPM::adc_task_connect"));
  }
  
  DEBUG_STREAM << "BPM::adc_task_connect ->" << std::endl;
}
  
// ============================================================================
// BPM::adc_task_disconnect
// ============================================================================
void BPM::adc_task_disconnect (void)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::adc_task_disconnect <-" << std::endl;

  //- The bpm::Task's mutex is locked
  //-------------------------------------------------------------------
  try
  {
    this->adc_task_->enable_periodic_msg(false);
    
    this->disable_adc_notifications();
    
#if ! defined(_SIMULATION_) 
    //- release con handle
    if (this->adc_task_->hcon_)
    {
      DEBUG_STREAM << "BPM::adc_task_disconnect::disconnect from Libera data source" << std::endl;
      ::cspi_disconnect(this->adc_task_->hcon_);
      DEBUG_STREAM << "BPM::adc_task_disconnect::release con. handle" << std::endl;
      ::cspi_freehandle(CSPI_HANDLE_CON, this->adc_task_->hcon_);
      this->adc_task_->hcon_ = 0;
    }
    //- release env handle
    if (this->adc_task_->henv_)
    {
      DEBUG_STREAM << "BPM::adc_task_disconnect::release env. handle" << std::endl;
      ::cspi_freehandle(CSPI_HANDLE_ENV, this->adc_task_->henv_);
      this->adc_task_->henv_ = 0;
    }
    
# if ! defined(_EMBEDDED_DEVICE_)
    //- close connection to generic server
    if (this->adc_task_->connected_)
      this->disconnect_from_generic_server("ADC-task");
# endif

#endif // ! _SIMULATION_

    this->adc_task_->connected_ = false;
  }
  catch(...) 
  { 
    //- ignore error 
  }
  
  DEBUG_STREAM << "BPM::adc_task_disconnect::release last_adc_data" << std::endl;
  
  //- release ADCData 1
  if (this->adc_data_1_)
  {
    this->adc_data_1_->release();
    this->adc_data_1_ = 0;
  }
  //- release ADCData 2
  if (this->adc_data_2_)
  {
    this->adc_data_2_->release();
    this->adc_data_2_ = 0;
  }
  //- no last data
  this->last_adc_data_ = 0;
  
  DEBUG_STREAM << "BPM::adc_task_disconnect ->" << std::endl;
}

// ============================================================================
// BPM::disable_dd_buffer_freezing
// ============================================================================
void BPM::disable_dd_buffer_freezing (void)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::disable_dd_buffer_freezing <-" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_)

  //- be sure we have something to do...
  if (! this->srv_task_ || ! this->srv_task_->connected_ || ! this->config_.dd_buffer_freezing_enabled())
    return;
    
  //- ok do the job...
  int cache_size = 0;
  int rc =::server_setparam(protocol::SERVER_CACHE_SIZE, &cache_size);
  HANDLE_SERVER_ERROR(rc, "server_setparam", "BPM::disable_dd_buffer_freezing");
  
#endif

  DEBUG_STREAM << "BPM::disable_dd_buffer_freezing ->" << std::endl;
  
  this->config_.disable_dd_buffer_freezing();
}

// ============================================================================
// BPM::enable_notifications
// ============================================================================
void BPM::enable_notifications (int cspi_events_mask)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::enable_notification <-" << std::endl; 
                           
#if ! defined(_EMBEDDED_DEVICE_)

  if (!this->srv_task_ || !this->srv_task_->connected_)
    return;

  CSPI_CONPARAMS * cp = reinterpret_cast<CSPI_CONPARAMS*>(&this->config_.cp_srv_);
                  
  cp->handler = BPM::libera_event_callback;
  cp->user_data = this;
  cp->event_mask = cspi_events_mask;
  
  size_t con_flags = CSPI_CON_HANDLER 
                   | CSPI_CON_USERDATA 
                   | CSPI_CON_EVENTMASK;
                   
  this->set_con_parameters(this->srv_task_->hcon_, cp, con_flags);
  
#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  this->evts_man_->enable_events(cspi_events_mask);
  
#endif

  DEBUG_STREAM << "BPM::enable_notification ->" << std::endl;
}

// ============================================================================
// BPM::disable_notifications
// ============================================================================
void BPM::disable_notifications (void)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::disable_notifications <-" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_)

  if (!this->srv_task_ || !this->srv_task_->connected_)
    return;

  CSPI_CONPARAMS * cp = reinterpret_cast<CSPI_CONPARAMS*>(&this->config_.cp_srv_);
                  
  cp->handler = 0;
  cp->user_data = 0;
  cp->event_mask = 0;
  
  size_t con_flags = CSPI_CON_HANDLER 
                   | CSPI_CON_USERDATA 
                   | CSPI_CON_EVENTMASK;
                   
  this->set_con_parameters(this->srv_task_->hcon_, cp, con_flags);
  
#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  this->evts_man_->enable_events(0);

#endif

  DEBUG_STREAM << "BPM::disable_notifications ->" << std::endl;
}

// ============================================================================
// BPM::enable_pm_notifications
// ============================================================================
void BPM::enable_pm_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::enable_pm_notifications <-" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_)
 
  int mask = this->current_cspi_events_mask() | CSPI_EVENT_PM;
      
  this->enable_notifications(mask);
                             
#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask() | CSPI_EVENT_PM;
    
  this->evts_man_->enable_events(mask); 
  
#endif

  DEBUG_STREAM << "BPM::enable_pm_notifications ->" << std::endl;
}

// ============================================================================
// BPM::disable_pm_notifications
// ============================================================================
void BPM::disable_pm_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::disable_pm_notifications <-" << std::endl;

#if ! defined(_EMBEDDED_DEVICE_)
 
  int mask = this->current_cspi_events_mask() & ~CSPI_EVENT_PM;
      
  this->enable_notifications(mask);
                             
#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask() & ~CSPI_EVENT_PM;
    
  this->evts_man_->enable_events(mask); 
  
#endif

  DEBUG_STREAM << "BPM::disable_pm_notifications ->" << std::endl; 
}

// ============================================================================
// BPM::enable_interlock_notifications
// ============================================================================
void BPM::enable_interlock_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::enable_interlock_notifications <-" << std::endl;
     
#if ! defined(_EMBEDDED_DEVICE_)
 
  int mask = this->current_cspi_events_mask() | CSPI_EVENT_INTERLOCK;
      
  this->enable_notifications(mask);
                             
#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask() | CSPI_EVENT_INTERLOCK;
    
  this->evts_man_->enable_events(mask); 
  
#endif

  DEBUG_STREAM << "BPM::enable_interlock_notifications ->" << std::endl;
}
  
// ============================================================================
// BPM::disable_interlock_notifications
// ============================================================================
void BPM::disable_interlock_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::disable_interlock_notifications <-" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_)
 
  int mask = this->current_cspi_events_mask() & ~CSPI_EVENT_INTERLOCK;
      
  this->enable_notifications(mask);
                             
#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask() & ~CSPI_EVENT_INTERLOCK;
    
  this->evts_man_->enable_events(mask); 
  
#endif

  DEBUG_STREAM << "BPM::disable_interlock_notifications ->" << std::endl; 
}

// ============================================================================
// BPM::enable_dd_notifications
// ============================================================================
void BPM::enable_dd_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::enable_dd_notifications <-" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_)

  int mask = this->current_cspi_events_mask();

  if (this->configuration().external_trigger_enabled())
  {
    this->dd_requires_trigger_notifications_ = true;
      
    mask |= CSPI_EVENT_TRIGGET;
  }
  
  this->enable_notifications(mask);
  
#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask() ;
             
  if (this->configuration().external_trigger_enabled())
  {
    this->dd_requires_trigger_notifications_ = true;
    
    mask |= CSPI_EVENT_TRIGGET;    
  }
  
  this->evts_man_->enable_events(mask);
    
#endif

  DEBUG_STREAM << "BPM::enable_dd_notifications ->" << std::endl;
}

// ============================================================================
// BPM::disable_dd_notifications
// ============================================================================
void BPM::disable_dd_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::disable_dd_notifications <-" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_)

  int mask = this->current_cspi_events_mask();
  
  //- this works because we are under critical section
  if (! this->adc_requires_trigger_notifications_)
    mask &= ~CSPI_EVENT_TRIGGET;
     
  this->enable_notifications(mask);

#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask();
  
  //- this works because we are under critical section           
  if (! this->adc_requires_trigger_notifications_)
    mask &= ~CSPI_EVENT_TRIGGET; 
    
  this->evts_man_->enable_events(mask);
    
#endif

  DEBUG_STREAM << "BPM::disable_dd_notifications ->" << std::endl;
}

// ============================================================================
// BPM::enable_adc_notifications
// ============================================================================
void BPM::enable_adc_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::enable_adc_notifications <-" << std::endl;

#if ! defined(_EMBEDDED_DEVICE_)

  int mask = this->current_cspi_events_mask();

  if (this->configuration().external_trigger_enabled())
  {
    this->adc_requires_trigger_notifications_ = true;
      
    mask |= CSPI_EVENT_TRIGGET;
  }
  
  this->enable_notifications(mask);
  
#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask(); 
             
  if (this->configuration().external_trigger_enabled())
  {
    this->adc_requires_trigger_notifications_ = true;
    
    mask |= CSPI_EVENT_TRIGGET;    
  }
  
  this->evts_man_->enable_events(mask);
    
#endif

  //- whatever are the external trigger settings, we don't need the TRIGEVENT anymore
  this->adc_requires_trigger_notifications_ =  false ;

  DEBUG_STREAM << "BPM::enable_adc_notifications ->" << std::endl;
}

// ============================================================================
// BPM::disable_adc_notifications
// ============================================================================
void BPM::disable_adc_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::disable_adc_notifications <-" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_)

  int mask = this->current_cspi_events_mask();
  
  //- this works because we are under critical section
  if (! this->dd_requires_trigger_notifications_)
    mask &= ~CSPI_EVENT_TRIGGET;
     
  this->enable_notifications(mask);

#else

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask();
             
  //- this works because we are under critical section
  if (! this->dd_requires_trigger_notifications_)
    mask &= ~CSPI_EVENT_TRIGGET; 
    
  this->evts_man_->enable_events(mask);
    
#endif

  //- 
  this->adc_requires_trigger_notifications_ = false;

  DEBUG_STREAM << "BPM::disable_adc_notifications ->" << std::endl;
}
  
// ============================================================================
// BPM::enable_sa_notifications
// ============================================================================
void BPM::enable_sa_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::enable_sa_notifications <-" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_)

  int mask = this->current_cspi_events_mask() | CSPI_EVENT_OVERFLOW;
# if defined(_USE_SA_EVENTS_)
  mask |= CSPI_EVENT_SA;
# endif  
    
  this->enable_notifications(mask);
  
#else //- _EMBEDDED_DEVICE_

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask() | CSPI_EVENT_OVERFLOW;
# if defined(_USE_SA_EVENTS_)
  mask |= CSPI_EVENT_SA;
# endif 

  this->evts_man_->enable_events(mask);
    
#endif //- _EMBEDDED_DEVICE_

  DEBUG_STREAM << "BPM::enable_sa_notifications ->" << std::endl;
}

// ============================================================================
// BPM::disable_sa_notifications
// ============================================================================
void BPM::disable_sa_notifications (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::disable_sa_notifications <-" << std::endl;
    
#if ! defined(_EMBEDDED_DEVICE_)

  int mask = this->current_cspi_events_mask() & ~CSPI_EVENT_OVERFLOW;
# if defined(_USE_SA_EVENTS_)
  mask &= ~CSPI_EVENT_SA;
# endif  
    
  this->enable_notifications(mask);
  
#else //- _EMBEDDED_DEVICE_

  DEBUG_ASSERT(this->evts_man_ != 0);
  
  int mask = this->evts_man_->get_events_mask() & ~CSPI_EVENT_OVERFLOW;
# if defined(_USE_SA_EVENTS_)
  mask &= ~CSPI_EVENT_SA;
# endif 

  this->evts_man_->enable_events(mask);
    
#endif //- _EMBEDDED_DEVICE_

  DEBUG_STREAM << "BPM::disable_sa_notifications ->" << std::endl;
}

// ============================================================================
// BPM::reset_pm_notification
// ============================================================================
void BPM::reset_pm_notification (void) 
{
  if (this->srv_task_)
    this->srv_task_->post(new Message(kRESET_PM_MSG, kRECONFIG_MSG_PRIORITY));
}

// ============================================================================
// BPM::reset_interlock_notification
// ============================================================================
void BPM::reset_interlock_notification (void) 
{
  if (this->srv_task_)
    this->srv_task_->post(new Message(kRESET_INTERLOCK_MSG, kRECONFIG_MSG_PRIORITY));
}

// ============================================================================
// BPM::get_env_parameters
// ============================================================================
void BPM::get_env_parameters (CSPIHENV he, CSPI_ENVPARAMS & ep, size_t mask, size_t retries)
  throw (Tango::DevFailed)
{
  do
  {
    int rc = ::cspi_getenvparam(he, &ep, mask);
    if (rc)
    {
      //- might encounter a "kRSC_TEMP_UNAVAILABLE" problem 
      //- this pb seems to be linked to a timeout that might occurs when trying 
      //- to read the Libera health info (hw temp & fans speed) !
      if (rc == kRSC_TEMP_UNAVAILABLE_CSPI && errno == kRSC_TEMP_UNAVAILABLE_ERRNO)
      {
        //- let's retry...
        retries--;
        if (! retries)
        {
          HANDLE_CSPI_ERROR(rc, "cspi_getenvparam", "BPM::get_env_parameters");
        }
      }
      else
      {
        //- any other cspi error is considered critical...
        HANDLE_CSPI_ERROR(rc, "cspi_getenvparam", "BPM::get_env_parameters");
      }
    }
    else
    {
      break;
    }
  }
  while (retries);
}

// ============================================================================
// BPM::set_con_parameters
// ============================================================================
void BPM::set_con_parameters (CSPIHCON hc, CSPI_CONPARAMS * cp, int con_flags)
  throw (Tango::DevFailed)
{
  int rc = ::cspi_setconparam(hc, cp, con_flags);
  
  HANDLE_CSPI_ERROR(rc, "cspi_setconparam", "set_con_parameters");
}

// ============================================================================
// BPM::set_con_parameters
// ============================================================================
void BPM::set_con_parameters (CSPIHCON hc, CSPI_CONPARAMS_EBPP * cp, int con_flags)
  throw (Tango::DevFailed)
{
  int rc = ::cspi_setconparam(hc, reinterpret_cast < CSPI_CONPARAMS * >(cp), con_flags);
  
  HANDLE_CSPI_ERROR(rc, "cspi_setconparam", "set_con_parameters");
}

// ============================================================================
// BPM::get_con_parameters
// ============================================================================
void BPM::get_con_parameters (CSPIHCON hc, CSPI_CONPARAMS * cp)
  throw (Tango::DevFailed)
{
  size_t con_flags = CSPI_CON_MODE 
                   | CSPI_CON_HANDLER 
                   | CSPI_CON_USERDATA 
                   | CSPI_CON_EVENTMASK;
                   
  int rc = ::cspi_getconparam(hc, cp, con_flags);
  
  HANDLE_CSPI_ERROR(rc, "cspi_getconparam", "get_con_parameters");
}

// ============================================================================
// BPM::set_con_parameters
// ============================================================================
void BPM::get_con_parameters (CSPIHCON hc, CSPI_CONPARAMS_EBPP * cp)
  throw (Tango::DevFailed)
{
  size_t con_flags = CSPI_CON_MODE 
                   | CSPI_CON_HANDLER 
                   | CSPI_CON_USERDATA 
                   | CSPI_CON_EVENTMASK 
                   | CSPI_CON_DEC 
                   | CSPI_CON_SANONBLOCK;
                  
  int rc = ::cspi_getconparam(hc, reinterpret_cast < CSPI_CONPARAMS * >(cp), con_flags);
  
  HANDLE_CSPI_ERROR(rc, "cspi_getconparam", "get_con_parameters");
}

// ============================================================================
// BPM::set_interlock_configuration
// ============================================================================
void BPM::set_interlock_configuration(const BPMConfig & _config)  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::set_interlock_configuration" << std::endl;

  //- check passed config
  this->config_.check_env_parameters(_config.requested_ep_);

  //- copy requested interlock config
  this->config_.requested_ep_.ilk.mode  = _config.requested_ep_.ilk.mode;
  this->config_.requested_ep_.ilk.Xlow  = _config.requested_ep_.ilk.Xlow;
  this->config_.requested_ep_.ilk.Xhigh = _config.requested_ep_.ilk.Xhigh;
  this->config_.requested_ep_.ilk.Ylow  = _config.requested_ep_.ilk.Ylow;
  this->config_.requested_ep_.ilk.Yhigh = _config.requested_ep_.ilk.Yhigh;
  this->config_.requested_ep_.ilk.overflow_limit = _config.requested_ep_.ilk.overflow_limit;
  this->config_.requested_ep_.ilk.overflow_dur = _config.requested_ep_.ilk.overflow_dur;
  this->config_.requested_ep_.ilk.gain_limit = _config.requested_ep_.ilk.gain_limit;
  
  //- only change the interlock part of the configuration
  int env_flags = 0;
  env_flags |= CSPI_ENV_ILK;
  
  //- tell what to change to the srv task 
  BPM::EnvParamsSpecifier * eps = new BPM::EnvParamsSpecifier(this->config_.requested_ep_, env_flags);
  
  //- apply the new interlock config
  Message * msg = new Message(kENV_CHANGED_MSG, kRECONFIG_MSG_PRIORITY, true);
  
  //- attach EnvParamsSpecifier to the msg
  msg->attach_data(eps);
  
  //- notify the srv task
  this->srv_task_->wait_msg_handled(msg, 5000);
}
  
// ============================================================================
// BPM::reconfigure
// ============================================================================
void BPM::reconfigure(const BPMConfig& _config)  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::reconfigure <-" << std::endl;

  //- nature of the reconfiguration (optimization)
  int cfg_modifs = this->config_.compare(_config);
      
  //- store "requested" configuration localy(see BPMConfig::operator=)
  this->config_ = _config;
      
#if defined(_DEBUG)
  this->config_.dump_env_parameters();
#endif
    
  //- do nothing in case config modifs do not require any change on Libera side
  if (! cfg_modifs)
  {
    DEBUG_STREAM << "BPM::reconfigure [cfg not changed] ->" << std::endl;
    return;
  }

  //- any change in data sources?
  if (cfg_modifs & BPMConfig::DD_SOURCE_CHANGED)
  {
    //- post an env. change msg to the task
    if (this->dd_task_)
      this->dd_task_->post(new Message(kSRC_CHANGED_MSG, kRECONFIG_MSG_PRIORITY));
  }

  if (cfg_modifs & BPMConfig::ADC_SOURCE_CHANGED)
  {
    //- post an env. change msg to the task
    if (this->adc_task_)
      this->adc_task_->post(new Message(kSRC_CHANGED_MSG, kRECONFIG_MSG_PRIORITY));
  }

  if (cfg_modifs & BPMConfig::SA_SOURCE_CHANGED)
  {
    //- post an env. change msg to the task
    if (this->sa_task_)
      this->sa_task_->post(new Message(kSRC_CHANGED_MSG, kRECONFIG_MSG_PRIORITY));
  }

  //- any change in env. parameters?
  bool env_changed = false;
  
  //- setup env. params flags
  int env_flags = 0;
  
  if (cfg_modifs & BPMConfig::SWITCHES_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_SWITCH;   
  }
  
  if (cfg_modifs & BPMConfig::AGC_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_AGC;   
  }
  
  if (cfg_modifs & BPMConfig::DSC_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_DSC;    
  }
  
  if (cfg_modifs & BPMConfig::GAIN_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_GAIN;   
  }

  if (cfg_modifs & BPMConfig::GAINS_OR_OFFSETS_CHANGED || cfg_modifs & BPMConfig::SWITCHES_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_KX;
    env_flags |= CSPI_ENV_KY;
    env_flags |= CSPI_ENV_XOFFSET;
    env_flags |= CSPI_ENV_YOFFSET;
    env_flags |= CSPI_ENV_QOFFSET;
  }

  if (cfg_modifs & BPMConfig::EXT_SWITCHING_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_EXTSWITCH;
  }

  if (cfg_modifs & BPMConfig::SWITCHING_DELAY_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_SWDELAY;
  }
  
  if (cfg_modifs & BPMConfig::TUNE_COMPENSATION_CHANGED)
  {
    env_changed = true;
		env_flags |= CSPI_ENV_MTNCOSHFT; 
  }

  if (cfg_modifs & BPMConfig::TUNE_OFFSET_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_MTVCXOFFS; 
  }

  if (cfg_modifs & BPMConfig::EXT_TRIGGER_DELAY_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_TRIGDELAY; 
  }

  if (cfg_modifs & BPMConfig::PM_OFFSET_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_PMOFFSET; 
  }

  if (cfg_modifs & BPMConfig::MAF_LENGTH_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_DDC_MAFLENGTH;
  }
  
  if (cfg_modifs & BPMConfig::MAF_DELAY_CHANGED)
  {
    env_changed = true;
    env_flags |= CSPI_ENV_DDC_MAFDELAY;
  }               

  //- always apply interlock thresholds
  env_flags |= CSPI_ENV_ILK;

  //- any change in env. parameters?
  if (env_changed && this->srv_task_)
  {
    //- tell what to change to the srv task 
    BPM::EnvParamsSpecifier * eps = new BPM::EnvParamsSpecifier(this->config_.requested_ep_, env_flags);
    //- post an env. change msg to the service task
    Message * msg = new Message(kENV_CHANGED_MSG, kRECONFIG_MSG_PRIORITY);
    //- attach EnvParamsSpecifier to the msg 
    msg->attach_data(eps);
    //- post msg
    this->srv_task_->post(msg);
  }

  //- any change in DD cache status?
  if ((cfg_modifs & BPMConfig::DD_DECIMATION_CHANGED) && this->dd_task_)
  {
    this->dd_task_->post(new Message(kDD_DECIM_CHANGED_MSG, kRECONFIG_MSG_PRIORITY));
  }
    
#if ! defined(_EMBEDDED_DEVICE_) && ! defined (_SIMULATION_)
  //- any change in DD cache status?
  if (this->dd_task_ && (cfg_modifs & BPMConfig::DD_CACHE_STATUS_CHANGED))
  {
    this->dd_task_->post(new Message(kDD_CACHE_STATUS_MSG, kRECONFIG_MSG_PRIORITY));
  }
#else
  if (this->dd_task_)
  {
    bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock()); 
    if (! this->config_.dd_buffer_freezing_enabled_)
      this->dd_buffer_frozen_ = false;
  }
#endif

  //- did user select a new pos. comp. algo?
  if ((cfg_modifs & BPMConfig::POS_COMP_ALGO_CHANGED) && this->sa_task_)
  {
    this->sa_task_->post(new Message(kALGO_CHANGED_MSG, kRECONFIG_MSG_PRIORITY));
  }


  DEBUG_STREAM << "BPM::reconfigure ->" << std::endl;
}

// ============================================================================
// BPM::set_time_on_next_trigger
// ============================================================================
void BPM::set_time_on_next_trigger(const TimeSettings& _ts)  
    throw (Tango::DevFailed)
{
  if (! this->srv_task_) return;
  
  Message * msg = Message::allocate(kSET_TIME_MSG, kRECONFIG_MSG_PRIORITY);
  msg->attach_data(_ts);  
  this->srv_task_->post(msg);
} 

// ============================================================================
// BPM::unfreeze_dd_buffer
// ============================================================================
void BPM::unfreeze_dd_buffer (void) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "BPM::unfreeze_dd_buffer" << std::endl;
  
#if ! defined(_EMBEDDED_DEVICE_) && ! defined(_SIMULATION_)

  //- be sure service task exists
  if (! this->srv_task_) return;
  
  //- post an env. change msg to the task
  this->srv_task_->post(new Message(kDD_UNLOCK_CACHE_MSG, kRECONFIG_MSG_PRIORITY));
  
#else

  //- enter critical section
  bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock());
  this->dd_buffer_frozen_ = false;
  
#endif
}

// ============================================================================
// BPM::is_dd_buffer_freezing_enabled
// ============================================================================
bool BPM::is_dd_buffer_freezing_enabled (void) throw (Tango::DevFailed)
{
  return this->config_.dd_buffer_freezing_enabled_;
}

// ============================================================================
// BPM::is_dd_buffer_frozen
// ============================================================================
bool BPM::is_dd_buffer_frozen (void) throw (Tango::DevFailed)
{
  if (! this->dd_task_) 
    return false; 

  bool frozen;
  {
    bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock());
    frozen = this->dd_buffer_frozen_;
  }
  
  return frozen;
}

// ============================================================================
// BPM::read_fa_data
// ============================================================================
void BPM::read_fa_data(FAData & fad_)  throw (Tango::DevFailed)
{
  //- be sure service task exists
  if (! this->srv_task_) return;

  if (this->fa_data_cp_.pbuf)
  {
    bpm::AutoMutex<bpm::Mutex> guard(this->fa_data_lock_);
    fad_.copy_from_full_fa_data_set(this->fa_data_cp_);
    return;
  }

  try
  {
    delete[] fad_.pbuf;
    fad_.pbuf = new char[fad_.size * fad_.count];
    if (! fad_.pbuf) 
      throw std::bad_alloc();
  }
  catch(const std::bad_alloc&)
  { 
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("memory allocation failed"),
                                   _CPTC("BPM::read_fa_data"));
  }
  
  Message * msg = new Message(kGET_ENV_FA_MSG, kRECONFIG_MSG_PRIORITY, true);
  msg->attach_data(&fad_, false);
  this->srv_task_->wait_msg_handled(msg, 5000); 
}

// ============================================================================
// BPM::write_fa_data
// ============================================================================
void BPM::write_fa_data(const FAData & _fa_data)  throw (Tango::DevFailed)
{
  //- be sure service task exists
  if (! this->srv_task_) return;
  
  Message * msg = Message::allocate(kSET_ENV_FA_MSG, kRECONFIG_MSG_PRIORITY);
  msg->attach_data(_fa_data);
  this->srv_task_->post(msg);
}
  
// ============================================================================
// BPM::save_dsc_parameters
// ============================================================================
void BPM::save_dsc_parameters (void) throw (Tango::DevFailed)
{
  //- be sure service task exists
  if (! this->srv_task_) return;
  
  //- post an env. change msg to the task
  this->srv_task_->post(new Message(kSAVE_DSC_PARAMS_MSG, kRECONFIG_MSG_PRIORITY));
}
  
// ============================================================================
// BPM::read_dd
// ============================================================================
void BPM::read_dd (void) throw (Tango::DevFailed)
{
  // DEBUG_STREAM << "BPM::read_dd <-" << std::endl;

#if ! defined(_SIMULATION_) 
  //- be sure dd source is properly initialized
  if (! this->dd_task_->hcon_) return;
#endif
    
#if defined(_EMBEDDED_DEVICE_) || defined (_SIMULATION_)
  { //- critical section : begin
    bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock()); 
    if (this->config_.dd_buffer_freezing_enabled_ && this->dd_buffer_frozen_)
    {
      DEBUG_STREAM << "BPM::read_dd (DD buffer frozen) ->" << std::endl;
      return;
    }
  }
#endif

#if ! defined(_SIMULATION_) 
  int rc = 0;
  //- set data retrieval position
  unsigned long long offset = 0;
  if (! this->config_.external_trigger_enabled())
  {
    rc =::cspi_seek(this->dd_task_->hcon_, &offset, CSPI_SEEK_MT);
    HANDLE_CSPI_ERROR(rc, "cspi_seek", "BPM::read_dd");
  }
  else 
  {
    offset = this->config_.get_dd_trigger_offset();
    rc =::cspi_seek(this->dd_task_->hcon_, &offset, CSPI_SEEK_TR);
    HANDLE_CSPI_ERROR(rc, "cspi_seek", "BPM::read_dd");
  }
#endif // !_SIMULATION_

  //- realloc buffer if needed
  try
  {
    size_t required_depth = this->config_.get_dd_raw_buffer_depth();
    if (this->dd_raw_buffer_.depth() != required_depth)
    {
      //- change buffers size under critical section
      bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock()); 
      this->dd_raw_buffer_.depth(required_depth);
      this->dd_data_1_->allocate(required_depth, false);
      this->dd_data_2_->allocate(required_depth, false);
    }
  }
  catch(const std::bad_alloc &)
  {
    { //- critical section : begin
      bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock()); 
      this->last_dd_data_ = 0;
    } //- critical section : end
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("memory allocation failed"), 
                                   _CPTC("BPM::read_dd"));
  }
  catch(...)
  {
    { //- critical section : begin
      bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock());
      this->last_dd_data_ = 0;
    } //- critical section : end
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("memory allocation failed"), 
                                   _CPTC("BPM::read_dd")); 
  }
  
  size_t actual_nsamples = 0; 
    
#if ! defined(_SIMULATION_) 

  //- read the DD (i.e. turn by turn) data
  rc =::cspi_read_ex(this->dd_task_->hcon_,
                    this->dd_raw_buffer_.base(),
                    this->dd_raw_buffer_.depth(),
                    &actual_nsamples, 
                    0);     
                               
  //- <cspi_read_ex> special error case: err:-5 and errno:61: no data available 
  //----------------------------------------------------------------------------
  //- this might occur when usign a huge DD buffer size. in this case reading 
  //- DD data on Libera side is VERY slow and DD data might be overwritten in 
  //- the circular DD data buffer on Libera side (or something similar). let's 
  //- ignore this error...   
  if (rc == -5 && errno == 61)
  {
    WARN_STREAM << "BPM::read_dd::cspi_read_ex failed for DD data source "
                << "[err: -5 - no data available]"
                << std::endl;
    return;
  }
  else
  {
    HANDLE_CSPI_ERROR(rc, "cspi_read_ex", "BPM::read_dd");
  }
  
  //- request timestamp to the Libera
  CSPI_TIMESTAMP ts;
  rc =::cspi_gettimestamp(this->dd_task_->hcon_, &ts);
  
  //- <cspi_gettimestamp> special error case: err:-3 function call sequence error 
  //-----------------------------------------------------------------------------
  //- this might occur when changing time (using the SetTimeOnNextTrigger) on 
  //- Libera while DD data source is enabled - the trigger timestamp seems to be 
  //- reset on Libera side. anyway...     
  if (rc == -3)
  {
    //- reset <ts> 
    ::memset(&ts, 0, sizeof(CSPI_TIMESTAMP));
    //- log the problem
    WARN_STREAM << "<cspi_gettimestamp> failed for DD data "
                << "[might be due to a recent <SetTimeOnNextTrigger> call]"
                << std::endl;
  }
  else
  {
    //- no error -3 - handle error normally (might throw an exception)
    HANDLE_CSPI_ERROR(rc, "cspi_gettimestamp", "BPM::read_dd");
  }
    
#else // !_SIMULATION_

  //- inject dummy values into the buffer
  actual_nsamples = this->dd_raw_buffer_.depth();
  CSPI_DD_RAWATOM * dd_raw_atoms = this->dd_raw_buffer_.base();
  for (size_t a = 0; a < actual_nsamples; a++)
  {
    dd_raw_atoms[a].cosVa = static_cast<int>(::random());
    dd_raw_atoms[a].sinVa = static_cast<int>(::random());
    dd_raw_atoms[a].cosVb = static_cast<int>(::random());     
    dd_raw_atoms[a].sinVb = static_cast<int>(::random());
    dd_raw_atoms[a].cosVc = static_cast<int>(::random());      
    dd_raw_atoms[a].sinVc = static_cast<int>(::random());
    dd_raw_atoms[a].cosVd = static_cast<int>(::random());  
    dd_raw_atoms[a].sinVd = static_cast<int>(::random()); 
  } 
    
#endif // !_SIMULATION_

  //- select preallocated data structure
  DDData * data = (this->last_dd_data_ == this->dd_data_1_)
                ? this->dd_data_2_
                : this->dd_data_1_;
      
  //- adapt length to actual number of DD raw atoms
  data->actual_num_samples(actual_nsamples);
                
  //- compute beam pos
  try
  {
    DataProcessing::compute_pos(*data,
                                this->dd_raw_buffer_,
                                actual_nsamples,
                                this->config_);
  }
  catch(...)
  {
    { //- critical section : begin
      bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock()); 
      this->last_dd_data_ = 0;
    } //- critical section : end
    throw;
  }
  
#if ! defined(_SIMULATION_) 
  //- copy the obtained timestamp
  ::memcpy(&data->timestamp_, &ts.st, sizeof(struct timespec));
#endif
    
  { //- critical section : begin
    bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock());
    
    //- make <last_dd_data_> point to the most recently acquired data
    this->last_dd_data_ = data;
    this->last_dd_data_updated_ = true;
      
#if defined(_EMBEDDED_DEVICE_) || defined (_SIMULATION_)
    //- mark DD buffer as frozen
    if (this->config_.dd_buffer_freezing_enabled_)
      this->dd_buffer_frozen_ = true;
#endif
  } //- critical section : end
  
  // DEBUG_STREAM << "BPM::read_dd ->" << std::endl;
}

// ============================================================================
// BPM::get_dd_data
// ============================================================================
void BPM::get_dd_data (DDData & dest, bool force_update)  throw (Tango::DevFailed)
{
  if (! this->dd_task_)
  {
    Tango::Except::throw_exception(_CPTC(kNO_DATA_MSG), 
                                   _CPTC("no data available"), 
                                   _CPTC("BPM::get_dd_data"));
  }
  
  //- enter critical section for remaining code
  bpm::AutoMutex<bpm::Mutex> guard(this->dd_task_->lock());
  
  if (this->last_dd_data_ == 0)
  {
    Tango::Except::throw_exception(_CPTC(kNO_DATA_MSG), 
                                   _CPTC("no data available"), 
                                   _CPTC("BPM::get_dd_data"));
  }

  if (this->last_dd_data_updated_ || force_update)
  {
    dest = *(this->last_dd_data_);
    this->last_dd_data_updated_ = false;
  }
}
  
// ============================================================================
// BPM::read_sa
// ============================================================================
void BPM::read_sa (void) throw (Tango::DevFailed)
{ 
  // DEBUG_STREAM << "BPM::read_sa <-" << std::endl; 

  //- be sure sa source is properly initialized
#if ! defined(_SIMULATION_)
  if (! this->sa_task_->hcon_) return;
#endif

  //- cspi error code
  int rc = 0;
  
  //- CSPI SA data struct for read op
  CSPI_SA_ATOM sa_atom;
  std::vector<CSPI_SA_ATOM> sa_atoms;
    
#if ! defined(_SIMULATION_)

  size_t sa_atom_cnt = 0;
  //- enter SA reading loop (read till no more samples in the SA buffer)
  do 
  {
    rc =::cspi_get(this->sa_task_->hcon_, &sa_atom);
    if (rc)
    {
      if (kNO_SA_SAMPLE_AVAILABLE)
      {
        if (! sa_atom_cnt) 
          return;
        else
          break;
      }
      else
        HANDLE_CSPI_ERROR(rc, "cspi_get", "BPM::read_sa");
    }
    sa_atoms.push_back(sa_atom);
    sa_atom_cnt++;
  }
  while(1);

#else // !_SIMULATION_

  ::memset(&sa_atom, 0, sizeof(CSPI_SA_ATOM));
  //- simulate 16-bits ADC data
  sa_atom.Va = static_cast<short>(::random());
  sa_atom.Vb = static_cast<short>(::random());
  sa_atom.Vc = static_cast<short>(::random());
  sa_atom.Vd = static_cast<short>(::random());
  sa_atoms.push_back(sa_atom);
  
#endif // !_SIMULATION_

  if (! sa_atoms.size())
    return;

  //- select preallocated data structure
  SAData * data = (this->last_sa_data_ == this->sa_data_1_)
                ? this->sa_data_2_
                : this->sa_data_1_;
                
  //- no timestamp for SA - reset timestamp (should be removed from SAData class)
  ::memset(&data->timestamp_, 0, sizeof(struct timespec));
  
  for (size_t i = 0; i < sa_atoms.size(); i++)
  {  
    //- populate our SAData from the CSPI_SA_ATOM
    *data = sa_atoms[i];  
    //- compute SA position
    DataProcessing::compute_pos(*data, this->config_);
    //- push SA data into the SA history
    this->sa_history_.push(*data);
    this->num_sa_samples_since_last_stat_proc_++; 
  } 
  
  { //- critical section : begin
    bpm::AutoMutex<bpm::Mutex> guard(this->sa_task_->lock()); 
    //- point to last acquired data
    this->last_sa_data_ = data;
    this->last_sa_data_updated_ = true;
  } //- critical section : end

  //- update SA statistics
  if (this->num_sa_samples_since_last_stat_proc_ > kSA_STAT_PROC_THRESHOLD)
  {
    this->sa_history_.compute_statistics(this->config_.sa_stats_num_samples_);
    this->num_sa_samples_since_last_stat_proc_ = 0;
  }

  // DEBUG_STREAM << "BPM::read_sa ->" << std::endl;
}

// ============================================================================
// BPM::get_sa_data
// ============================================================================
void BPM::get_sa_data (SAData & dest, bool force_update)  throw (Tango::DevFailed)
{
  if (! this->sa_task_)
  {
    Tango::Except::throw_exception(_CPTC(kNO_DATA_MSG), 
                                   _CPTC("no data available"), 
                                   _CPTC("BPM::get_sa_data"));
  }
  
  //- enter critical section for remaining code
  bpm::AutoMutex<bpm::Mutex> guard(this->sa_task_->lock());
  
  if (this->last_sa_data_ == 0)
  {
    Tango::Except::throw_exception(_CPTC(kNO_DATA_MSG), 
                                   _CPTC("no data available"), 
                                   _CPTC("BPM::get_sa_data")); 
  }
  
  if (this->last_sa_data_updated_ || force_update)
  {
    dest = *(this->last_sa_data_);
    this->last_sa_data_updated_ = false;
  }
}
  
// ============================================================================
// BPM::get_incoherence
// ============================================================================
bpm::FPDataType BPM::get_incoherence (void) throw (Tango::DevFailed)
{
  if  (! this->sa_task_)
  {
    Tango::Except::throw_exception (_CPTC (kNO_DATA_MSG), 
                                    _CPTC ("no data available"), 
                                    _CPTC ("BPM::get_incoherence"));
  }

  //- enter critical section for remaining code
  bpm::AutoMutex<bpm::Mutex> guard(this->sa_task_->lock());

  //- check for available SA data
  if (this->last_sa_data_ == 0)
  {
    Tango::Except::throw_exception (_CPTC (kNO_DATA_MSG), 
                                    _CPTC ("no data available"), 
                                    _CPTC ("BPM::get_incoherence")); 
  }

  //- compute the incoherence value
  bpm::FPDataType dx = 
    (last_sa_data_->va_ - last_sa_data_->vb_) / (last_sa_data_->va_ + last_sa_data_->vb_)
        - (last_sa_data_->vd_ - last_sa_data_->vc_) / (last_sa_data_->vd_ + last_sa_data_->vc_);
  
  bpm::FPDataType dz = 
    (last_sa_data_->va_ - last_sa_data_->vd_) / (last_sa_data_->va_ + last_sa_data_->vd_)
        - (last_sa_data_->vb_ - last_sa_data_->vc_) / (last_sa_data_->vb_ + last_sa_data_->vc_);

  bpm::FPDataType incoherence = 0.36 * this->config_.Kz * ::hypot(dx,dz);
  
  return incoherence;
} 

// ============================================================================
// BPM::read_adc
// ============================================================================
void BPM::read_adc (void) throw (Tango::DevFailed)
{ 
  // DEBUG_STREAM << "BPM::read_adc <-" << std::endl;

#if ! defined(_SIMULATION_)
  //- be sure adc source is properly initialized
  if (! this->adc_task_->hcon_) return;
#endif

  //- realloc buffer if needed
  try
  {
    size_t required_depth = this->config_.get_adc_raw_buffer_depth();
    if (this->adc_raw_buffer_.depth() != required_depth)
    {
      this->adc_raw_buffer_.depth(required_depth);
      this->adc_data_1_->allocate(required_depth, false);
      this->adc_data_2_->allocate(required_depth, false);
    }
  }
  catch(const std::bad_alloc &)
  {
    { //- critical section : begin
      bpm::AutoMutex<bpm::Mutex> guard(this->adc_task_->lock()); 
      this->last_adc_data_ = 0;
    } //- critical section : end
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("memory allocation failed"), 
                                   _CPTC("BPM::read_adc"));
  }
  catch(...)
  {
    { //- critical section : begin
      bpm::AutoMutex<bpm::Mutex> guard(this->adc_task_->lock()); 
      this->last_adc_data_ = 0;
    } //- critical section : end
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("memory allocation failed"), 
                                   _CPTC("BPM::read_adc"));
  }
  
  size_t actual_nsamples;
    
#if ! defined(_SIMULATION_)

  int rc = ::cspi_read_ex(this->adc_task_->hcon_,
                          this->adc_raw_buffer_.base(),
                          this->adc_raw_buffer_.depth(),
                          &actual_nsamples,
                          0);
  HANDLE_CSPI_ERROR(rc, "cspi_read_ex", "BPM::read_adc");
    
#else // ! _SIMULATION_

  //- inject dummy values into the buffer
  actual_nsamples = this->adc_raw_buffer_.depth();
    
#endif // ! _SIMULATION_

  //- select preallocated data structure
  ADCData * data = (this->last_adc_data_ == this->adc_data_1_)
                 ? this->adc_data_2_
                 : this->adc_data_1_;
                
  //- adapt length to actual number of ADC raw atoms
  data->actual_num_samples(actual_nsamples);
    
  //- fill the buffers
  for (size_t i = 0; i < actual_nsamples; i++)
  {
    CSPI_ADC_ATOM & adc_raw_atom = this->adc_raw_buffer_[i];
#if ! defined(_SIMULATION_)
    *(data->a_.base() + i) = adc_raw_atom.chA;
    *(data->b_.base() + i) = adc_raw_atom.chB;
    *(data->c_.base() + i) = adc_raw_atom.chC;
    *(data->d_.base() + i) = adc_raw_atom.chD;
#else
    *(data->a_.base() + i) = static_cast<int>(::random());
    *(data->b_.base() + i) = static_cast<int>(::random());
    *(data->c_.base() + i) = static_cast<int>(::random());
    *(data->d_.base() + i) = static_cast<int>(::random());
#endif 
  }    

  //- copy timestamp
  //- ::memcpy(&last_adc_data_->timestamp_, &ts.st, sizeof(struct timespec));

  {
    //- critical section : begin
    bpm::AutoMutex<bpm::Mutex> guard(this->adc_task_->lock()); 
    //- make <last_dd_data_> point to the most recently acquired data
    this->last_adc_data_ = data;
    this->last_adc_data_updated_ = true;
  }
  
  // DEBUG_STREAM << "BPM::read_adc ->" << std::endl;
}

// ============================================================================
// BPM::get_adc_data
// ============================================================================
void BPM::get_adc_data (ADCData & dest, bool force_update)  throw (Tango::DevFailed)
{
  if (! this->adc_task_)
  {
    Tango::Except::throw_exception(_CPTC(kNO_DATA_MSG), 
                                    _CPTC("no data available"), 
                                    _CPTC("BPM::get_adc_data"));
  }
  
  //- enter critical section for remaining code
  bpm::AutoMutex<bpm::Mutex> guard(this->adc_task_->lock());
  
  if (this->last_adc_data_ == 0)
  {
    Tango::Except::throw_exception(_CPTC(kNO_DATA_MSG), 
                                    _CPTC("no data available"), 
                                    _CPTC("BPM::get_adc_data"));
  }
  
  if (this->last_adc_data_updated_ || force_update)
  {
    dest = *(this->last_adc_data_);
    this->last_adc_data_updated_ = false;
  }
}
  
// ============================================================================
// BPM::read_pm
// ============================================================================
void BPM::read_pm (void) throw (Tango::DevFailed)
{
  // DEBUG_STREAM << "BPM::read_pm <-" << std::endl;

  //- critical section
  bpm::AutoMutex<bpm::Mutex> guard(this->srv_task_->lock()); 
    
  //- leave if frozen
	if (this->pm_notified_)
		return;

  //- read the data
  DEBUG_STREAM << "BPM::read_pm::reading PM data" << std::endl;

  size_t actual_nsamples;
  DDRawBuffer pm_raw_buffer(kPOST_MORTEM_BUFFER_SIZE);
  int rc =::cspi_read_ex(this->srv_task_->hcon_, 
                         pm_raw_buffer.base(), 
                         pm_raw_buffer.depth(), 
                         &actual_nsamples, 
                         0);
  HANDLE_CSPI_ERROR(rc, "cspi_read_ex", "BPM::read_pm");
  
  DEBUG_STREAM << "BPM::read_pm::got " << actual_nsamples << " PM samples" << std::endl;
  
  //- try to allocate the returned DDData
  DEBUG_STREAM << "BPM::read_pm::computing data" << std::endl;
  DDData * data = 0;
  try
  {
    //- creat an instance of DDData...
    data = new DDData();
    if (data == 0)
      throw std::bad_alloc();
    //- ...then allocate its buffers
    data->allocate(actual_nsamples);
    //- compute beam pos
    DataProcessing::compute_pos(*data, pm_raw_buffer, actual_nsamples, this->config_);
  }
  catch(const std::bad_alloc &)
  {
    if (data) data->release();
    if (this->last_pm_data_)
      this->last_pm_data_->release();
    this->last_pm_data_ = 0;
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"), 
                                   _CPTC("memory allocation failed"), 
                                   _CPTC("BPM::read_pm"));
  }
  catch(...)
  {
    if (data) data->release(); 
    if (this->last_pm_data_)
      this->last_pm_data_->release();
    this->last_pm_data_ = 0;
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"), 
                                   _CPTC("memory allocation failed"), 
                                   _CPTC("BPM::read_pm"));
  }
  
  //- reset timestamp(no timestamp here)
  ::memset(&data->timestamp_, 0, sizeof(struct timespec));
  
  if (this->last_pm_data_)
    this->last_pm_data_->release();
    
  //- point to acquired data
  this->last_pm_data_ = data;
  this->last_pm_data_updated_ = true;
  
  //- mark PM notification has received
  this->pm_notified_ = true;
  
  //- inc. our pm notification counter
  this->pm_event_counter_++;
  
  DEBUG_STREAM << "BPM::read_pm::disconnect from PM data source" << std::endl;
        
  INFO_STREAM << "<POST MORTEM> notification handled [PM data bufferized]" << std::endl;
        
  // DEBUG_STREAM << "BPM::read_pm ->" << std::endl;
}

// ============================================================================
// BPM::get_pm_data
// ============================================================================
void BPM::get_pm_data (DDData & dest, bool force_update)  throw (Tango::DevFailed)
{
  if (! this->srv_task_)
  {
    Tango::Except::throw_exception(_CPTC(kNO_DATA_MSG), 
                                    _CPTC("no data available"), 
                                    _CPTC("BPM::get_pm_data"));
  }
  
  //- enter critical section for remaining code
  bpm::AutoMutex<bpm::Mutex> guard(this->srv_task_->lock());
  
  if (! this->pm_notified_ || this->last_pm_data_ == 0)
  {
    Tango::Except::throw_exception(_CPTC(kNO_DATA_MSG), 
                                    _CPTC("no data available"), 
                                    _CPTC("BPM::get_pm_data"));
  }
  
  if (this->last_pm_data_updated_ || force_update)
  {
    dest = *(this->last_pm_data_);
    this->last_pm_data_updated_ = false;
  }
}
 
// ============================================================================
// BPM::watch_dog
// ============================================================================
void BPM::watch_dog (void) throw (Tango::DevFailed)
{
#if ! defined(_EMBEDDED_DEVICE_)
  //- maintain connection
  int rc =::server_noop();
  HANDLE_SERVER_ERROR(rc, "server_noop", "BPM::watch_dog");
#endif
}

// ============================================================================
// BPM::libera_event_callback
// ============================================================================
int BPM::libera_event_callback (CSPI_EVENT * evt)
{
  //- be sure evt is valid (parano impl)
  if (! evt)
    return 0;

  //- try to acquire the BPM::lock (enter critical section)
  bpm::MutexState status = BPM::lock.try_lock();
  if (status == bpm::MUTEX_BUSY)
  {
    //- if lock already owned and BPM closed it means that an other thread 
    //- is executing BPM::close_i - we avoid deadlock by leaving the place...
    if (BPM::closed)
      return 0;
  }
  //- if lock acquired but BPM closed then...
  else if (BPM::closed)
  {
    //- leave critical section
    if (status == bpm::MUTEX_LOCKED)
      BPM::lock.unlock();
    return 0;
  }
  
  //- retry to enter critical section (BPM was just busy)
  if (status != bpm::MUTEX_LOCKED)
    BPM::lock.lock();

  //- get ref. to our BPM instance (reinterpret user data)
  BPM * instance = reinterpret_cast<BPM*>(evt->user_data);
  if (! instance || instance != BPM::singleton)
  {
    BPM::lock.unlock();
    return 0;
  }
  
  //- handle event
  try 
  {
    switch(evt->hdr.id)
    {
      //--------------------------------------
      case CSPI_EVENT_TRIGGET:
      {
        //- increment our trigger counter
        instance->trigger_counter_++;
        //- notify the DD task 
        if (instance->dd_task_ && instance->configuration().dd_enabled()) 
          instance->dd_task_->post(evt);
        //- notify the ADC task
        if (instance->adc_task_ && instance->configuration().adc_enabled()) 
          instance->adc_task_->post(evt);
      }
      break;
      //--------------------------------------
      case CSPI_EVENT_SA:
      {
        //- notify the SA task 
        if (instance->sa_task_ && instance->configuration().sa_enabled()) 
          instance->sa_task_->post(evt);
      }
      break;
      //--------------------------------------
      case CSPI_EVENT_PM:
      case CSPI_EVENT_INTERLOCK:
      {
        //- notify the SRV task
        if (instance->srv_task_)
          instance->srv_task_->post(new Message(evt, kRECONFIG_MSG_PRIORITY));
      }
      break;
#if defined(_EMBEDDED_DEVICE_)
      case CSPI_EVENTS_MANAGER_ERROR:
      {
        //- notify the SRV task
        instance->srv_task_->post(evt);
      }
      break;
#endif
      //--------------------------------------
      default:
      {
        //- for any other CSPI event we notify the service task
        if (instance->srv_task_)
          instance->srv_task_->post(evt);
      }
      break;
    }
  }
  catch (Tango::DevFailed& df)
  {
    BPM_ERROR_STREAM << df.errors[0].desc
                     << " [lost message type: " 
                     << bpm::Message(evt).to_string() 
                     << "]" 
                     << std::endl;
  }
  
  //- leave critical section
  BPM::lock.unlock();
  
  return 0;
}
  
// ============================================================================
// BPM::srv_message_handler
// ============================================================================
void BPM::srv_message_handler (const bpm::Message& _msg)
{     
  //- reinterpret user data
  BPM * instance = reinterpret_cast<BPM*>(_msg.user_data());
  if (instance == 0 || instance != BPM::singleton)
    return;
    
  //- handle msg
  switch(_msg.type())
  {
    //- TASK_INIT ----------------------
    case TASK_INIT:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        instance->init_srv_task();
      }
      break;
    //- TASK_EXIT ----------------------
    case TASK_EXIT:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        instance->close_srv_task();
      }
      break;
    //- TASK_PERIODIC -------------------
    case TASK_PERIODIC:
      {
        instance->srv_periodic_job();
      }
      break;  
#if ! defined(_SIMULATION_) 
    //- kSET_ENV_FA_MSG ------------------
    case kSET_ENV_FA_MSG:
      {
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        FAData & fad = _msg.get_data<FAData>();
        int rc = ::cspi_setenvparam_fa(instance->srv_task_->henv_, 
                                      fad.offset, 
                                      fad.pbuf, 
                                      fad.size, 
                                      fad.count);                          
        HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(rc, "cspi_setenvparam_fa", "BPM::srv_message_handler");
      }
      break; 
    //- kGET_ENV_FA_MSG ------------------
    case kGET_ENV_FA_MSG:
      {
        FAData & fad = _msg.get_data<FAData>();
        int rc = ::cspi_getenvparam_fa(instance->srv_task_->henv_, 
                                      fad.offset, 
                                      fad.pbuf, 
                                      fad.size, 
                                      fad.count);
        HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(rc, "cspi_getenvparam_fa", "BPM::srv_message_handler");
      }
      break; 
    //- kSAVE_DSC_PARAMS_MSG ------------------
    case kSAVE_DSC_PARAMS_MSG:
      {
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        CSPI_ENVPARAMS ep;
        ep.dsc = CSPI_DSC_SAVE_LASTGOOD;
        int rc = ::cspi_setenvparam(instance->srv_task_->henv_, &ep, CSPI_ENV_DSC);
        HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(rc, "cspi_setenvparam", "BPM::srv_message_handler");
      }
      break; 
    //- kENV_CHANGED_MSG -----------------
    case kENV_CHANGED_MSG:
      {
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        //- check requested env. params
        instance->config_.check_env_parameters(instance->config_.requested_ep_);
        //- get env. parameters specifier (what to change on Libera side?)
        BPM::EnvParamsSpecifier & eps = _msg.get_data<BPM::EnvParamsSpecifier>();
        //- do not try to change the input gain in case the AGC mode is changed (cspi_setenvprams will complain otherwise)
        if (eps.ep_flags & CSPI_ENV_AGC) 
          eps.ep_flags &= ~CSPI_ENV_GAIN;   
        //- compute actual offsets
        instance->config_.compute_actual_offsets(eps.ep, eps.ep.switches == kAUTO_SWITCHING_MODE);
        //- apply requested params
        int rc = ::cspi_setenvparam(instance->srv_task_->henv_, &eps.ep, eps.ep_flags);
        HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(rc, "cspi_setenvparam", "BPM::srv_message_handler");
        //- sync actual with requested params 
        bpm::Thread::sleep(200);
        instance->get_env_parameters(instance->srv_task_->henv_, instance->config_.actual_ep_);
        //- update requested env. params
        instance->config_.actual_to_requested_ep();
        instance->config_.dump_env_parameters();
      }
      break;
#if ! defined(_EMBEDDED_DEVICE_) 
    //- kDD_UNLOCK_CACHE_MSG -----------
    case kDD_UNLOCK_CACHE_MSG:
      {
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        int val = kUNLOCK_BUFFER;
        int rc =::server_setparam(protocol::SERVER_CACHE_LOCK, &val);
        HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(rc, "server_setparam", "BPM::srv_message_handler");
      }
      break;
#endif //- _EMBEDDED_DEVICE_
    //- POST MORTEM EVENT --------------
    case CSPI_PM:
      {       
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        BPM_INFO_STREAM << "received a <POST MORTEM> notification from Libera" << std::endl;
        instance->disable_pm_notifications();
        instance->read_pm();
      }
      break;  
    //- kRESET_PM_MSG ------------------
    case kRESET_PM_MSG:
      {
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        instance->pm_notified_ = false;
        instance->enable_pm_notifications();
        BPM_INFO_STREAM << "<POST MORTEM> notification acknowlegded by operator" << std::endl;
      }
      break;  
    //- CSPI_INTERLOCK -----------------
    case CSPI_INTERLOCK:
      { 
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        if (! instance->hw_status_.intl_flags)
        {
          instance->disable_interlock_notifications();
          InterlockFlags ilk = _msg.cspi_data();
          instance->hw_status_.intl_flags = ilk;
          BPM_INFO_STREAM << "received an <INTERLOCK> notification from Libera [ " 
                          << ((ilk & INTERLOCK_X)    ? "X "    : "")   
                          << ((ilk & INTERLOCK_Z)    ? "Z "    : "")  
                          << ((ilk & INTERLOCK_ATTN) ? "ATTN " : "")  
                          << ((ilk & INTERLOCK_ADC)  ? "ADC "  : "")  
                          << ((ilk & INTERLOCK_ADCF) ? "ADCF " : "")  
                          << "]"
                          << std::endl;
        }
		  }
		  break;
    //- kRESET_INTERLOCK_MSG -----------
    case kRESET_INTERLOCK_MSG:
      {
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        instance->hw_status_.intl_flags = 0;
        instance->enable_interlock_notifications();
        BPM_INFO_STREAM << "<INTERLOCK> notification acknowlegded by operator" << std::endl;
      }
      break;
    //- kSET_TIME_MSG ------------------
    case kSET_TIME_MSG:
      {
        bpm::AutoMutex<bpm::Mutex> guard(instance->srv_task_->lock());
        TimeSettings & _ts = _msg.get_data<TimeSettings>();
        CSPI_SETTIMESTAMP ts;
        CSPI_BITMASK mask = 0;
        //- machine time
        mask |= CSPI_TIME_MT;
        ts.mt = static_cast<unsigned long long>(_ts.mt);
        //- system time
        mask |= CSPI_TIME_ST;
        ts.st.tv_sec = static_cast<size_t>(_ts.st);
        //- phase 
        ts.phase = static_cast<unsigned long>(_ts.tp);
        int rc = ::cspi_settime(instance->srv_task_->henv_, &ts, mask);
        HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(rc, " cspi_settime", "BPM::srv_message_handler");
        CSPI_ENVPARAMS ep; 
        ep.trig_mode = CSPI_TRIGMODE_SET;
        rc = ::cspi_setenvparam(instance->srv_task_->henv_, &ep, CSPI_ENV_TRIGMODE);
        HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(rc, "cspi_setenvparam", "BPM::srv_message_handler");
      }
      break;  
#endif // ! _SIMULATION_
#if defined(_EMBEDDED_DEVICE_)
      //- kEVT_MAN_ERROR_MSG -------------
      case kEVT_MAN_ERROR_MSG:
      {
        //TODO: change dev state to FAULT and update status
      }
      break;
#endif
    //- REMAINING EVENTS ---------------
    default:      
      {
        //- noop
      }
      break;
  }
}

// ============================================================================
// BPM::srv_periodic_job
// ============================================================================
void BPM::srv_periodic_job () 
    throw (Tango::DevFailed)
{     
  static bpm::Timer t;
  double dt_ms = 0.;
  static bpm::Timer srv_timer;

  //- cspi bug: crash if try to call a cspi function without a valid connection so.
  //- ... be sure we are properly connected 
  if (! this->srv_task_->connected_)
    return;

#if ! defined(_SIMULATION_)

  double fa_dt_msec = this->fa_data_timer_.elapsed_msec() + 0.1 * this->config_.fa_cache_refresh_period_;
  if (fa_dt_msec >= this->config_.fa_cache_refresh_period_)
  {
    try
    {
      BPM_DEBUG_STREAM << "BPM::srv_periodic_job::updating FA data cache" << std::endl;
      t.restart();
      this->update_fa_data_cache();
      dt_ms = t.elapsed_msec();
      BPM_DEBUG_STREAM << "BPM::srv_periodic_job::FA data cache update took " << dt_ms << " ms" << std::endl;
    }
    catch (Tango::DevFailed& df)
    {
      BPM_ERROR_STREAM << "BPM::srv_periodic_job::FA data cache update gave error" << std::endl;
      //- BPM_ERROR_STREAM << df << std::endl;
    }
    catch (...)
    {
      BPM_ERROR_STREAM << "BPM::srv_periodic_job::FA data cache update gave error" << std::endl;
    }
    this->fa_data_timer_.restart();
  }

  double srv_dt_msec = srv_timer.elapsed_msec() + 0.1 * kSRV_TMO;
  if (srv_dt_msec < kSRV_TMO)
    return;
  srv_timer.restart();

  BPM_DEBUG_STREAM << "BPM::srv_periodic_job::it's time to read the env. parameters... " << std::endl;
  t.restart();

  static int cnt = 0;
  size_t flags = kALL_PARAMS_EXCEPT_HEALTH;
  if (! (cnt % 20))
  { 
    flags = kALL_PARAMS;
    cnt = 0;
  }
  CSPI_ENVPARAMS ep;
  ::memcpy(&ep, &this->config_.actual_ep_, sizeof(CSPI_ENVPARAMS));
  int rc = ::cspi_getenvparam(this->srv_task_->henv_, &ep, flags);
  //- error handling
  if (rc)
  {
    //- might encounter a "kRSC_TEMP_UNAVAILABLE" problem
    if (rc == kRSC_TEMP_UNAVAILABLE_CSPI && errno == kRSC_TEMP_UNAVAILABLE_ERRNO)
    {
      //- not really a problem - just log then retry later (on next TASK_PERIODIC notification) 
      DUMP_CSPI_ERROR(rc, "cspi_getenvparam", "BPM::srv_message_handler");
      return;
    }
    else
    {
      //- any other cspi error is considered critical...
      HANDLE_CSPI_ERROR(rc, "cspi_getenvparam", "BPM::srv_message_handler");
    }
  }
  cnt++;
  {
    //- get env. params under crtical section to avoid race condition on set/get env. params
    bpm::AutoMutex<bpm::Mutex> guard(this->srv_task_->lock());
    ::memcpy(&this->config_.actual_ep_, &ep, sizeof(CSPI_ENVPARAMS));      
  }    
  //- update hw status: cspi health info
  this->update_libera_health_info();
#endif // ! _SIMULATION_

  //- update hw status: system info
#if defined(_EMBEDDED_DEVICE_)
  //- we update the system sensors if running embedded - no system sensors while running externally 
  //- in order to avoid to have tens of instances (running on the same host) accessing /proc !!!!
  this->update_libera_sensors_info();     
#endif // _EMBEDDED_DEVICE_       

#if ! defined(_SIMULATION_) && ! defined(_EMBEDDED_DEVICE_)
  //- get DD cache status
  int val;
  rc =::server_getparam(protocol::SERVER_CACHE_LOCK, &val);
  HANDLE_CSPI_ERROR(rc, "server_getparam", "BPM::srv_message_handler");
  this->dd_buffer_frozen_ = val ? true : false;
#endif // !_SIMULATION_ && ! _EMBEDDED_DEVICE_

#if ! defined(_SIMULATION_)
  dt_ms = t.elapsed_msec();
  BPM_DEBUG_STREAM << "BPM::srv_periodic_job::env. data update took " << dt_ms << " ms"<< std::endl;
#endif  // ! _SIMULATION_
}

// ============================================================================
// BPM::update_fa_data_cache
// ============================================================================
void BPM::update_fa_data_cache () 
    throw (Tango::DevFailed)
{     
  if (!this->fa_data_rd_.pbuf || !this->fa_data_rd_.size || !this->fa_data_rd_.count)
    return;

  int rc = ::cspi_getenvparam_fa(this->srv_task_->henv_, 
                                 this->fa_data_rd_.offset, 
                                 this->fa_data_rd_.pbuf, 
                                 this->fa_data_rd_.size, 
                                 this->fa_data_rd_.count);
  HANDLE_CSPI_ERROR_FROM_STATIC_MEMBER(rc, "cspi_getenvparam_fa", "BPM::srv_message_handler");
        
  {
    bpm::AutoMutex<bpm::Mutex> guard(this->fa_data_lock_); 
    this->fa_data_cp_ = this->fa_data_rd_;
  }
}

// ============================================================================
// BPM::dd_message_handler
// ============================================================================
void BPM::dd_message_handler (const bpm::Message& _msg)
{
  //- reinterpret user data
  BPM *instance = reinterpret_cast<BPM*>(_msg.user_data());
  if (instance == 0 || instance != BPM::singleton)
    return;

  //- handle msg
  switch(_msg.type())
  {
    //- TASK_INIT ----------------------
    case TASK_INIT:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->dd_task_->lock());
        instance->dd_task_->enable_timeout_msg(false);
        if (instance->configuration().dd_enabled()) 
          instance->dd_task_connect();
      }
      break;
    //- TASK_EXIT ----------------------
    case TASK_EXIT:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->dd_task_->lock());
        if (instance->configuration().dd_enabled()) 
          instance->dd_task_disconnect();
      }
      break;
    //- TASK_PERIODIC -------------------
    case TASK_PERIODIC:
      {
        //- cspi bug: crash if try to call a cspi function without a valid connection so...
        //- ... be sure we are properly connected 
        if (! instance->dd_task_->connected_)
          return;
        if (instance->configuration().dd_enabled())
        {
          if (instance->configuration().external_trigger_enabled())
            instance->watch_dog();
          else             
            instance->read_dd();
        }
      }
      break;
    //- CSPI_TRIGGET --------------------- 
    case CSPI_TRIGGET:
      {
        instance->read_dd();
      }
      break;
    //- DD_DECIMATION_CHANGED_MSG -------- 
    case kDD_DECIM_CHANGED_MSG:
      {
        int flags = CSPI_CON_DEC;
        if (::cspi_setconparam(instance->dd_task_->hcon_, 
                               reinterpret_cast<CSPI_CONPARAMS*>(&instance->config_.cp_dd_), flags))
        {
          Tango::Except::throw_exception(_CPTC("BPM configuration failed"),
                                        _CPTC("cspi_setconparam failed"),
                                        _CPTC("BPM::dd_message_handler"));
        }
      }
      break;
    //- SRC_CHANGED_MSG ------------------  
    case kSRC_CHANGED_MSG:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->dd_task_->lock());
        if (instance->configuration().dd_enabled())
        {
          instance->dd_task_connect();
        }
        else
        {
          instance->dd_task_disconnect();
          if (instance->last_dd_data_)
          {
            instance->last_dd_data_->release();
            instance->last_dd_data_ = 0;
          }
        }
      }
      break;
#if ! defined(_EMBEDDED_DEVICE_)
    //- DD_CACHE_STATUS_MSG --------------
    case kDD_CACHE_STATUS_MSG:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->dd_task_->lock());
        //- this could be written a better way but... 
        instance->dd_task_disconnect();
        instance->dd_task_connect();
      }
      break;
#endif
    //- REMAINING CPSI EVENTS ------------
    default:
      break;
  }
}

// ============================================================================
// BPM::sa_message_handler
// ============================================================================
void BPM::sa_message_handler (const bpm::Message& _msg)
{
  static int local_sa_evt_counter;

  //- reinterpret user data
  BPM *instance = reinterpret_cast<BPM*>(_msg.user_data());
  if (instance == 0 || instance != BPM::singleton)
    return;

  //- handle msg
  switch(_msg.type())
  {
    //- TASK_INIT ----------------------
    case TASK_INIT:
      { 
        //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->sa_task_->lock());
        instance->sa_task_->enable_timeout_msg(false);
        if (instance->configuration().sa_enabled()) 
          instance->sa_task_connect();
        //- init our local SA event/reading counter
        local_sa_evt_counter = 0;
      }
      break;
    //- TASK_EXIT ----------------------
    case TASK_EXIT:
      { 
        //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->sa_task_->lock());
        if (instance->configuration().sa_enabled()) 
          instance->sa_task_disconnect();
      }
      break;
    //- TASK_PERIODIC -------------------
    case TASK_PERIODIC:
      {
        //- cspi bug: crash if try to call a cspi function without a valid connection so...
        //- ... be sure we are properly connected 
        if (! instance->sa_task_->connected_)
          return;
#if defined(_USE_SA_EVENTS_)
# if ! defined(_EMBEDDED_DEVICE_) &&  ! defined(_SIMULATION_)
        instance->watch_dog();
# endif
#else
        if (instance->configuration().sa_enabled())
          instance->read_sa();
#endif
      }
      break;
#if defined(_USE_SA_EVENTS_)
    //- SA EVENT -------------------------
    case CSPI_SA:
      {
        if (instance->configuration().sa_enabled())
            instance->read_sa(); 
        //- inc. SA event counter
        local_sa_evt_counter++;
      }
      break;
#endif 
    //- kSRC_CHANGED_MSG -----------------
    case kSRC_CHANGED_MSG:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->sa_task_->lock());
        if (instance->configuration().sa_enabled())
        {
          instance->sa_task_connect();
        }
        else
        {
          instance->sa_task_disconnect();
          if (instance->last_sa_data_)
          {
            instance->last_sa_data_->release();
            instance->last_sa_data_ = 0;
          }
        }
      }
      break;
    case kALGO_CHANGED_MSG:
      {
        instance->get_sa_history().reset();
      }
      break;
    //- REMAINING CPSI EVENTS ------------
    default:
      break;  
  } 
}

// ============================================================================
// BPM::adc_message_handler
// ============================================================================
void BPM::adc_message_handler (const bpm::Message& _msg)
{
  //- reinterpret user data
  BPM *instance = reinterpret_cast<BPM*>(_msg.user_data());
  if (instance == 0 || instance != BPM::singleton)
    return;

  //- handle msg
  switch(_msg.type())
  {
    //- TASK_INIT ----------------------
    case TASK_INIT:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->adc_task_->lock());
        instance->adc_task_->enable_timeout_msg(false);
        if (instance->configuration().adc_enabled()) 
          instance->adc_task_connect();
      }
      break;
    //- TASK_EXIT ----------------------
    case TASK_EXIT:
      { //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->adc_task_->lock());
        if (instance->configuration().adc_enabled()) 
          instance->adc_task_disconnect();
      }
      break;
    //- TASK_PERIODIC -------------------
    case TASK_PERIODIC:
      {
        //- cspi bug: crash if try to call a cspi function without a valid connection so...
        //- ... be sure we are properly connected 
        if (! instance->adc_task_->connected_)
          return;
        //- read data or watch dog activity     
        if (instance->configuration().adc_enabled())
        {
          if (instance->configuration().external_trigger_enabled())
            instance->watch_dog();
          else             
            instance->read_adc();
        }
      }  
      break;
    //- kSRC_CHANGED_MSG -----------------
    case kSRC_CHANGED_MSG:  
      { 
        //- critical section
        bpm::AutoMutex<bpm::Mutex> guard(instance->adc_task_->lock());
        if (instance->configuration().adc_enabled())
          instance->adc_task_connect();
        else
          instance->adc_task_disconnect();
      }
      break;
    //- CSPI_TRIGGET --------------------- 
    case CSPI_TRIGGET:
      {
        if (instance->configuration().adc_enabled())
          instance->read_adc();
      }
      break;
    //- REMAINING CPSI EVENTS ------------
    default:
      break;
  }
}

// ============================================================================
// BPM::update_libera_health_info
// ============================================================================
void BPM::update_libera_health_info ()
{
  this->hw_status_.temp = static_cast<short>(this->config_.actual_ep_.health.temp);
  this->hw_status_.fan_1_speed = static_cast<short>(this->config_.actual_ep_.health.fan[0]);
  this->hw_status_.fan_2_speed = static_cast<short>(this->config_.actual_ep_.health.fan[1]);
  this->hw_status_.sc_ppl_lock_status = this->config_.actual_ep_.pll.sc ? true : false;
  this->hw_status_.mc_ppl_lock_status = this->config_.actual_ep_.pll.mc ? true : false;
} 

// ============================================================================
// BPM::update_libera_sensors_info
// ============================================================================
#if defined(_EMBEDDED_DEVICE_)
void BPM::update_libera_sensors_info ()
{
  this->sensors_.update_all();
  this->hw_status_.memory_free = this->sensors_.memory_free;
  this->hw_status_.ram_fs_usage = this->sensors_.ram_fs_usage;
  this->hw_status_.uptime = this->sensors_.uptime;
  this->hw_status_.cpu_usage = this->sensors_.cpu_usage; 
}
#endif      
} // namespace bpm
