//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_CONFIG_H_
#define _BPM_CONFIG_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"
#include "BPMLocation.h"
#include "Offsets.h"

// ============================================================================
// CONSTs
// ============================================================================
//- libera default port (generic server and multicast notifications)
#define GS_DEFAULT_PORT       23271
#define GS_DEFAULT_MCAST_PORT 0
//-----------------------------------------------------------------------------
//- BPM block geometry
#define kBLOCK_GEOMETRY_45 45.0
#define kBLOCK_GEOMETRY_90 90.0
//-----------------------------------------------------------------------------
//- BPM tasks activity period
#define kMIN_THREAD_ACTIVITY_PERIOD_MS     75
#define kDEFAULT_THREAD_ACTIVITY_PERIOD_MS 1000
#define kMAX_THREAD_ACTIVITY_PERIOD_MS     5000
//-----------------------------------------------------------------------------
//- FA cache refresh period
#define kMIN_FA_CACHE_REFRESH_PERIOD_MS     100
#define kDEFAULT_FA_CACHE_REFRESH_PERIOD_MS 500
#define kMAX_FA_CACHE_REFRESH_PERIOD_MS     750
//-----------------------------------------------------------------------------
//- max number of DD samples that can be read with/without DD cache enabled
#define DD_NSAMPLES_MAX_VALUE_CACHE_ENABLED  16384
#define DD_NSAMPLES_MAX_VALUE_CACHE_DISABLED 250000
#define DD_NSAMPLES_MAX_VALUE_DECIM_ENABLED  10000
//-----------------------------------------------------------------------------
//- min/max number of ADC samples that can be read
#define kMIN_ADC_RAW_BUFFER_SIZE    8
#define kMAX_ADC_RAW_BUFFER_SIZE 1024
//-----------------------------------------------------------------------------
//- params flag for cspi_[get/set]_env_params function  
#define kALL_PARAMS ~0
//-----------------------------------------------------------------------------
#define kALL_PARAMS_EXCEPT_HEALTH kALL_PARAMS & ~CSPI_ENV_HEALTH
//-----------------------------------------------------------------------------
//- number of possible values for "Nb" in booster normal mode
#define MAX_NB_VALUES 4
//-----------------------------------------------------------------------------
//- minimum number of samples that can be read from "data on demand buffer"
#define NSAMPLES_MIN_VALUE 32
//-----------------------------------------------------------------------------
//- default number of samples that can be read from "data on demand buffer"
#define NSAMPLES_DEFAULT_VALUE 1024
//-----------------------------------------------------------------------------
//- default SA history buffer depth
#define kDEFAULT_SA_HISTORY_DEPTH 1024
//-----------------------------------------------------------------------------
//- default num of samples to use for SA pos statistics computation
#define kDEFAULT_SA_STAT_SAMPLES  256
//-----------------------------------------------------------------------------
//- valid range for switches 
#define kMIN_SWITCHES_MODE   static_cast<int>(0)
#define kMAX_SWITCHES_MODE   static_cast<int>(15) 
#define kAUTO_SWITCHING_MODE static_cast<int>(255) 
//-----------------------------------------------------------------------------
//- valid range for gain 
#define kMIN_GAIN static_cast<int>(-100)
#define kMAX_GAIN static_cast<int>(0)
//-----------------------------------------------------------------------------
//- valid range for agc
#define kMIN_AGC static_cast<int>(0)
#define kMAX_AGC static_cast<int>(1)
//-----------------------------------------------------------------------------
//- valid range for dsc
#define kMIN_DSC static_cast<int>(0)
#define kMAX_DSC static_cast<int>(2)
//-----------------------------------------------------------------------------
//- valid range for trigger mode
#define kMIN_TRG_MODE static_cast<int>(1)
#define kMAX_TRG_MODE static_cast<int>(2)
//-----------------------------------------------------------------------------
//- valid range for external source switching
#define kMIN_SWITCHING_EXT static_cast<int>(0)
#define kMAX_SWITCHING_EXT static_cast<int>(1)
//-----------------------------------------------------------------------------
//- valid range switching delay
#define kMIN_SWITCHING_DELAY static_cast<int>(0)
//-----------------------------------------------------------------------------
//- valid range for compensation tune
#define kMIN_TUNE_COMP static_cast<int>(0)
#define kMAX_TUNE_COMP static_cast<int>(1)
//-----------------------------------------------------------------------------
//- valid range for offset tune
#define kMIN_TUNE_OFFSET static_cast<int>(-500)
#define kMAX_TUNE_OFFSET static_cast<int>(500)
//-----------------------------------------------------------------------------
//- valid range for the external trigger delay
#define kMIN_EXT_TRIGGER_DELAY static_cast<int>(0)
//-----------------------------------------------------------------------------
//- valid range for post mortem offset
#define kMIN_PM_OFFSET static_cast<int>(-512)
#define kMAX_PM_OFFSET static_cast<int>(512)
//-----------------------------------------------------------------------------
#define kLIBERA_STARTUP_CONFIG_FIELDS   18 
#define kUSER_DEFINED_ENV_PARAMS_FIELDS 4   
#define kINTERLOCK_CONFIG_FIELDS        8 
//-----------------------------------------------------------------------------
//- trunc the gain correction factors to 4 digits: a.bcdefghij -> abcdef 
#define kGAIN_TRUNC_FACTOR     1000.
#define kGAIN_TRUNC_FACTOR_INV (1. / kGAIN_TRUNC_FACTOR)
//-----------------------------------------------------------------------------
#define kMILLI_TO_NANO_METERS 1000000
#define kNANO_TO_MILLI_METERS 1.e-6
//-----------------------------------------------------------------------------
#define kMIN_MAF_LENGTH static_cast<int>(1)
#define kMAX_MAF_LENGTH static_cast<int>(129)
#define kMIN_MAF_DELAY  static_cast<int>(0)
#define kMAX_MAF_DELAY  static_cast<int>(128)
//-----------------------------------------------------------------------------
  
namespace bpm
{

// ============================================================================
//! BPM configuration abstraction class.
// ============================================================================
//!
//! detailed description to be defined
//!
// ============================================================================
class BPMConfig
{
  friend class BPM;
  friend class DataProcessing;

public:
  // Institute
  typedef enum
  {
    kTANGO_INSTITUTE = 0,
    kALBA,
    kESRF,
    kELETTRA,
    kSOLEIL,
  } Institute;

  // Position computation algorithm
  typedef enum
  {
    //- standard algorithm (depends on block geometry)
    kSTD_POS_ALGO = 0,
    //- ESRF (very) specific atanh algorithm for its SR
    kESRF_SR_POS_ALGO,
    //- invalid identitifer (!!must stay the last in enum!!)
    kINVALID_POS_ALGO_ID
  } Algorithm;
  
  // BPM block geometry
  typedef enum
  {
    BLOCK_GEOMETRY_45,
    BLOCK_GEOMETRY_90,
    BLOCK_GEOMETRY_UNKNOWN
  } BlockGeometry;
  
  //-  possible values for "Nb" in booster normal mode
  typedef enum
  {
    NB_VALUE_00 = 158,
    NB_VALUE_01 = 632,
    NB_VALUE_02 = 2528,
    NB_VALUE_03 = 10112
  } NbValues;

  // BPMConfig default ctor 
  BPMConfig (Tango::DeviceImpl * host_device = 0);

  // BPMConfig copy ctor
  // \param src The source configuration.
  BPMConfig (const BPMConfig & src);

  // BPMConfig operator=
  // \param src The source configuration.
  BPMConfig & operator= (const BPMConfig & src);

  // Dtor
  virtual ~BPMConfig (void);

#if ! defined(_EMBEDDED_DEVICE_)
  // Set the Libera IP configuration
  // \param ip_addr The Libera IP address
  // \param mcast_ip_addr The client multicast IP address for asynch notification
  // \param gs_port The generic server port
  // \param mcast_port The client multicast port for asynch notification
  void set_ip_config (const std::string & ip_addr, 
                      int port, 
                      const std::string & mcast_ip_addr, 
                      int mcast_port = GS_DEFAULT_MCAST_PORT);
#endif //- _EMBEDDED_DEVICE_

  // Enable/Disable buffer freezing
  void enable_dd_buffer_freezing (void);
  void disable_dd_buffer_freezing (void);
  bool dd_buffer_freezing_enabled (void) const;

  // Set/Get the BPM offsets and parameters 
  void set_offsets (const BPMOffsets & _src) throw (Tango::DevFailed);
  const BPMOffsets & get_offsets (void) const;

  //- Controls wether or not the BBA offsets are taken into account when 
  //- computing the offsets passed to the FPGA process
  void pass_bba_offsets_to_fpga (bool yes_or_no);
  
  // Set/Get BPM location 
  void set_location (BPMLocation bpm_loc) throw (Tango::DevFailed);
  BPMLocation get_location (void) const;

  // Set/Get block geometry
  void set_block_geometry (BlockGeometry bg);
  BlockGeometry get_block_geometry (void) const;

  // Set/get switches mode P[0..15]
  void set_switches_mode (short mode) throw (Tango::DevFailed);
  short get_switches_mode (void) const;
  bool auto_switching_enabled (void) const;
  
  // Set interlock configuration
  void set_interlock_configuration (const std::vector<bpm::FPDataType>& vd) 
    throw (Tango::DevFailed);
    
  // Dump offsets, env. & con. parameters
  void dump_offsets(void) const;
  void dump_env_parameters (void) const;
  void dump_env_parameters (const CSPI_ENVPARAMS &ep) const;
  void dump_con_parameters (void) const;

  // Set/Get the dd-data buffer depth
  void set_dd_raw_buffer_depth (size_t depth);
  size_t get_dd_raw_buffer_depth (void);

  // Set/Get the adc-data buffer depth
  void set_adc_raw_buffer_depth (size_t depth);
  size_t get_adc_raw_buffer_depth (void) const;
  
  // Enable/Disable external trigger 
  // Influences the TANGO device behaviour not the Libera itself
  void enable_external_trigger (void);
  void disable_external_trigger (void);
  bool external_trigger_enabled (void) const;

  // Set/Get DD thread activity period 
  void set_dd_thread_activity_period (size_t period);
  size_t get_dd_thread_activity_period (void) const;

  // Set/Get the SA thread activity period 
  void set_sa_thread_activity_period (size_t period);
  size_t get_sa_thread_activity_period (void) const;
  
  // Set/Get the ADC thread activity period 
  void set_adc_thread_activity_period (size_t period);
  size_t get_adc_thread_activity_period (void) const;
  
  // Set/Get the FA cache refresh period in msecs 
  void set_fa_cache_refresh_period (size_t period);
  size_t get_fa_cache_refresh_period (void) const;

  // Enables/disables SA 
  void enable_sa (void);
  void disable_sa (void);
  bool sa_enabled (void) const;

  // Enables/disables PM
  void enable_pm (void);
  void disable_pm (void);
  bool pm_enabled (void) const;

  // Enables/disables ADC
  void enable_adc (void);
  void disable_adc (void);
  bool adc_enabled (void) const;

  // Enables/disables DD
  void enable_dd (void);
  void disable_dd (void);
  bool dd_enabled (void) const;

  // Set/get X low limit (for beam position interlock)
  void set_x_low_limit (bpm::FPDataType xlo);
  bpm::FPDataType get_x_low_limit (void) const;

  // Set/get X hi limit (for beam position interlock)
  void set_x_high_limit (bpm::FPDataType xlo);
  bpm::FPDataType get_x_high_limit (void) const;

  // Set/Get Z low limit (for beam position interlock)
  void set_z_low_limit (bpm::FPDataType xlo);
  bpm::FPDataType get_z_low_limit (void) const;

  // Set/Get Z hi limit (for beam position interlock)
  void set_z_high_limit (bpm::FPDataType xlo);
  bpm::FPDataType get_z_high_limit (void) const;

  // Set/Get the DD decimation factor
  void set_decimation_factor (int df);
  int get_decimation_factor (void) const;

  // Set/Get the max DD buffer size for "decimation on"
  void set_max_dd_buffer_depth_for_dec_on (size_t depth);
  size_t get_max_dd_buffer_depth_for_dec_on  (void) const;
  
  // Set/Get the SA history depth (in num of samples)
  void set_sa_history_depth (size_t depth);
  size_t get_sa_history_depth (void) const;

  // Set/Get the SA RMS num samples
  void set_sa_stats_num_samples (size_t nsamples);
  size_t get_sa_rms_num_samples (void) const;
 
  // Set/Get the DD trigger offset (in num of samples (i.e. turns))
  void set_dd_trigger_offset (unsigned long);
  unsigned long get_dd_trigger_offset (void) const;
    
  // Enable auto switching on sa activation ?
  void enable_auto_switching_on_sa_activation (bool behaviour);
  bool enable_auto_switching_on_sa_activation (void) const;
  
  // Enable DSC activation vs SA activation ?
  void enable_dsc_on_auto_switching_activation (bool behaviour);
  bool enable_dsc_on_auto_switching_activation (void) const;
   
  // Set/Get the Libera input gain
  void set_gain (int g);
  int get_gain (void) const;
 
  // Enables/disables AGC
  void enable_agc (void);
  void disable_agc (void);
  bool agc_enabled (void) const;

  // DSC mode
  void set_dsc_mode (const short mode);
  short get_dsc_mode (void) const;
  bool dsc_enabled (void) const;

  // Enable/disable external switching
  void enable_external_switching (void);
  void disable_external_switching (void);
  bool external_switching_enabled (void) const;   
  
  // Set/Get the switching delay
  void set_switching_delay (int sd);
  int get_switching_delay (void) const;   
  
  // Enable/disable tune compensation
  void enable_tune_compensation (void);
  void disable_tune_compensation (void);
  bool tune_compensation_enabled (void) const;   
  
  /**
   * get/Set the offset tune (mtncoshft, mtcxoffs)
   */
  void set_tune_offset (int t);
  int  get_tune_offset (void) const;   

  // Set/Get the external trigger delay
  void set_external_trigger_delay (int etd);
  int get_external_trigger_delay (void) const;  

  // Set/Get post mortem offset
  void set_pm_offset (int pmo);
  int get_pm_offset (void) const;  

  // Returns the Libera model (0:e-, 1:br, 2:ph)
  unsigned short libera_model () const;

  // Does the Libera have MAF support (on FPGA side)
  bool has_maf_support () const;
  
  // Set/Get moving average filter length
  void set_maf_length (int mafl);
  int get_maf_length (void) const; 

  // Set/Get moving average filterdelay
  void set_maf_delay (int mafd);
  int  get_maf_delay (void) const;    

  // Enable/Disable optional data
  void enable_dd_optional_data ();
  void disable_dd_optional_data ();
  bool dd_optional_data_enabled () const;

  void enable_sa_optional_data ();
  void disable_sa_optional_data ();
  bool sa_optional_data_enabled () const;

  void enable_adc_optional_data ();
  void disable_adc_optional_data ();
  bool adc_optional_data_enabled () const;
  
  void enable_sa_history_optional_data ();
  void disable_sa_history_optional_data ();
  bool sa_history_optional_data_enabled () const;

  //- set/get institute
  void set_institute (int i);
  int get_institute (void) const;
  
  //- set/get position computation algorithm
  void set_pos_algorithm (int i);
  int get_pos_algorithm (void) const;

  
  //- enable/diable local SA pos computation
  void enable_local_sa_pos_computation ();
  void disable_local_sa_pos_computation ();
  bool local_sa_pos_computation_enabled () const;
  
  // returns the current Libera env. params
  const CSPI_ENVPARAMS& get_current_env_parameters (void) const;
    
  //- tricky copy of actual env. params into requested params
  //- why such an ugly method? ask ITech please! 
  void actual_to_requested_ep (void);
   
private:

  // Modifiactions on the configuration
  typedef enum
  {
    DD_RAW_BUFFER_SIZE_CHANGED  = BIT(0),
    DD_TRIGGER_OFFSET_CHANGED   = BIT(1),
    DD_DECIMATION_CHANGED       = BIT(2),
    DD_SOURCE_CHANGED           = BIT(3),
    SA_SOURCE_CHANGED           = BIT(4),
    SWITCHES_CHANGED            = BIT(5),
    AGC_CHANGED                 = BIT(6),
    DSC_CHANGED                 = BIT(7),
    GAIN_CHANGED                = BIT(8),
    ADC_RAW_BUFFER_SIZE_CHANGED = BIT(9),
    ADC_SOURCE_CHANGED          = BIT(10),
    GAINS_OR_OFFSETS_CHANGED    = BIT(11),
    EXT_TRIGGER_DELAY_CHANGED   = BIT(12),
	  PM_OFFSET_CHANGED           = BIT(13),
	  MAF_LENGTH_CHANGED				  = BIT(14),
    MAF_DELAY_CHANGED           = BIT(15),
	  EXT_SWITCHING_CHANGED       = BIT(16),
    SWITCHING_DELAY_CHANGED     = BIT(17),
	  TUNE_COMPENSATION_CHANGED   = BIT(18),
	  TUNE_OFFSET_CHANGED         = BIT(19),
    POS_COMP_ALGO_CHANGED       = BIT(20)
#if ! defined(_EMBEDDED_DEVICE_)
    , DD_CACHE_STATUS_CHANGED   = BIT(31)
#endif
  } Modifications;

  // Check any modifiaction on this configuration
  int compare (const BPMConfig& _cfg);

  // Computes actual offsets from both BLOCK and HW offsets
  void compute_actual_offsets (CSPI_ENVPARAMS &ep, bool auto_switching_enabled)
    throw (Tango::DevFailed); 

  // Validate the env. params value. Throws an exception in case of problem!
  void check_env_parameters (const CSPI_ENVPARAMS& ep) const 
    throw (Tango::DevFailed);
  
  // The BPM offsets and parameters
  BPMOffsets offsets_;
  
  // Should the BBA offsets be passed to the FPGA process
  bool pass_bba_offsets_to_fpga_;
 
  // The requested CPSI environment parameters
  CSPI_ENVPARAMS requested_ep_;

  // The actual CPSI environment parameters
  CSPI_ENVPARAMS actual_ep_;

  // The CPSI connection parameters for SA mode
  CSPI_CONPARAMS_EBPP cp_sa_;

  // The CPSI connection parameters for the service task
  CSPI_CONPARAMS cp_srv_;

  // The CPSI connection parameters for ADC mode
  CSPI_CONPARAMS cp_adc_;

  // The CPSI connection parameters for DD mode
  CSPI_CONPARAMS_EBPP cp_dd_;

  // The DD data buffer depth
  size_t dd_raw_buffer_depth_;
  size_t max_dd_buffer_depth_for_dec_on_;

  // The ADC data buffer depth
  size_t adc_raw_buffer_depth_; 

  // The SA data history buffer depth
  size_t sa_data_history_buffer_depth_;
 
  // The num of SA samples use for RMS pos computation
  size_t sa_stats_num_samples_;

#if ! defined(_EMBEDDED_DEVICE_)
  // The Libera IP addr
  std::string ip_addr_;

  // The generic server port
  int gs_port_;

  // The Libera Multicast IP addr
  std::string mcast_ip_addr_;

  // The multicast port
  int mcast_port_;
#endif

  // Buffer freezing 
  bool dd_buffer_freezing_enabled_;

  // External trigger   
  bool external_trigger_enabled_;

  // DD trigger offset
  unsigned long dd_trigger_offset_;

  // "DD" thread activity period in ms   
  size_t dd_thread_activity_period_;

  // "SA" thread activity period in ms   
  size_t sa_thread_activity_period_;

  // "ADC" thread activity period in ms   
  size_t adc_thread_activity_period_;

  // "FA" cache refresh period  
  size_t fa_cache_refresh_period_;

  // Pre-computed offsets values
  bpm::FPDataType Kx;
  bpm::FPDataType Kz;

  bpm::FPDataType Xoffset_local;
  bpm::FPDataType Xoffset_fpga;

  bpm::FPDataType Zoffset_local;
  bpm::FPDataType Zoffset_fpga;

  bpm::FPDataType Qoffset_local;
  bpm::FPDataType Qoffset_fpga;

  bpm::FPDataType VaGainCorrection;
  bpm::FPDataType VbGainCorrection;
  bpm::FPDataType VcGainCorrection;
  bpm::FPDataType VdGainCorrection;

#if defined(__arm__) && defined(_USE_ARM_OPTIMIZATION_)
  IntOffsetDataType int_Kx;
  IntOffsetDataType int_Kz;
  
  IntOffsetDataType int_Xoffset_local;
  IntOffsetDataType int_Zoffset_local;
  IntOffsetDataType int_Qoffset_local;
  
  IntOffsetDataType int_VaGainCorrection;
  IntOffsetDataType int_VbGainCorrection;
  IntOffsetDataType int_VcGainCorrection;
  IntOffsetDataType int_VdGainCorrection;
#endif

  // BPM location
  BPMLocation location_;

  // BPM's block geometry
  BlockGeometry block_geometry_;

  // Data sources flag
  bool sa_enabled_;
  bool pm_enabled_;
  bool adc_enabled_;
  bool dd_enabled_;

  // Optional data
  bool dd_optional_data_enabled_;
  bool sa_optional_data_enabled_;
  bool adc_optional_data_enabled_;
  bool sa_history_optional_data_enabled_;
  
  // Auto-switching activation vs SA activation 
  bool enable_auto_switching_on_sa_activation_;

  // DSC activation vs SA activation 
  bool enable_dsc_on_auto_switching_activation_;
  
  //- enable/disable local SA computation
  bool local_sa_pos_computation_enabled_;
  
  // where are we running?
  int institute_;

  // which pos. computation algorithm should we use?
  int pos_algorithm_;

  //- host device for logging
  Tango::DeviceImpl * host_device_;
};

// Standalone operator!= - compares two env. parameters structs
// \param env_params_1 The CSPI_ENVPARAMS to be compared to this->env_params_2
// \param env_params_2 The CSPI_ENVPARAMS to be compared to this->env_params_1
bool operator!= (const CSPI_ENVPARAMS &env_params_1, const CSPI_ENVPARAMS &env_params_2);

// Standalone operator== - compares two env. parameters structs
// \param env_params_1 The CSPI_ENVPARAMS to be compared to this->env_params_2
// \param env_params_2 The CSPI_ENVPARAMS to be compared to this->env_params_1
bool operator== (const CSPI_ENVPARAMS &env_params_1, const CSPI_ENVPARAMS &env_params_2);
  
} // namespace bpm

#if defined (__INLINE_IMPL__)
# include "BPMConfig.i"
#endif // __INLINE_IMPL__

#endif // _BPM_CONFIG_H_
