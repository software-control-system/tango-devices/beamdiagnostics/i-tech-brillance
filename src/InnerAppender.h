// ============================================================================
//
// = CONTEXT
//   FOFBManager: fast orbit feedback manager
//
// = File
//   InnerAppender.h
//
// = AUTHOR
//   Julien Malik - SOLEIL
//
// ============================================================================

#ifndef _INNER_APPENDER_H_
#define _INNER_APPENDER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <deque>
#include <threading/Mutex.h>
#include <tango.h>

namespace Libera_ns
{

// ============================================================================
// class: InnerAppender
// ============================================================================
class InnerAppender : public log4tango::Appender
{
public:
  
  //- the LogList type
  typedef std::deque<std::string> LogList;

public:
  /**
    *
    **/
  InnerAppender (const std::string& name, bool open_connection = true);
  
  /**
    *
    **/
  virtual ~InnerAppender ();

  /**
    *
    **/
  virtual bool requires_layout () const;
      
  /**
    *
    **/
  virtual void set_layout (log4tango::Layout* layout);

  /**
    *
    **/
  virtual void close ();
                  
  /**
    *
    **/
  virtual bool reopen ();

  /**
    *
    **/
  virtual bool is_valid () const;
  
  /**
    *
    **/
  void get_logs (InnerAppender::LogList& log_list);

protected:

  /**
    *
    **/
  virtual int _append (const log4tango::LoggingEvent& event); 

private:
  bpm::Mutex m_mutex;
  LogList m_logs_list;
};

} // namespace Libera_ns

#endif // _INNER_APPENDER_H_
