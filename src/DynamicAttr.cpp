//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2007-2008 Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include "BPM.h"
#include "DynamicAttr.h"

#if !defined (INLINE_IMPL)
# include "DynamicAttr.i"
#endif

namespace Libera_ns
{

//=============================================================================
// Base class for any DynamicAttritute
//=============================================================================
//- the BPM
bpm::BPM * DynamicAttritute::bpm = 0;
//- the BPM DD data (data on demand)
bpm::DDData * DynamicAttritute::dd_data = 0;
//- the BPM ADC data (adc values)
bpm::ADCData * DynamicAttritute::adc_data = 0;
//- the BPM SA data (slow acquisition)
bpm::SAData * DynamicAttritute::sa_data = 0;
//- the BPM PM data (post mortem)
bpm::DDData * DynamicAttritute::pm_data = 0;
                                        
//=============================================================================
// DynamicAttritute::DynamicAttritute
//=============================================================================
DynamicAttritute::DynamicAttritute ()
{
  //- noop
}

//=============================================================================
// DynamicAttritute::DynamicAttritute
//=============================================================================
DynamicAttritute::~DynamicAttritute ()
{
  //- noop
}

//=============================================================================
// PosAlgorithmIdAttr::PosAlgorithmIdAttr
//=============================================================================
PosAlgorithmIdAttr::PosAlgorithmIdAttr (const std::string& _name,
                                        Tango::DeviceImpl * _device)
  : DynamicAttritute (),
    Tango::Attr (_name.c_str(), Tango::DEV_USHORT, Tango::READ_WRITE),
    Tango::LogAdapter (_device),
    pos_algorithm_id_ (bpm::BPMConfig::kINVALID_POS_ALGO_ID)
{
  Tango::UserDefaultAttrProp  prop;
  prop.set_label("Pos.Comp.Algorithm");
  prop.set_unit(" a.u.");
  prop.set_format("%2d");
  prop.set_description("Pos. Computation Algorithm (0:STD-ALGO, 1:ESRF-SR");
  this->set_disp_level(Tango::EXPERT);
  this->set_default_properties(prop);
}
                
//=============================================================================
// PosAlgorithmIdAttr::~PosAlgorithmIdAttr
//=============================================================================
PosAlgorithmIdAttr::~PosAlgorithmIdAttr ()
{
  //- noop
}
    
//=============================================================================
// PosAlgorithmIdAttr::read
//=============================================================================
void PosAlgorithmIdAttr::read (Tango::DeviceImpl* d, Tango::Attribute &a)
{
  // DEBUG_STREAM << "PosAlgorithmIdAttr::read (Tango::DeviceImpl* d, Tango::Attribute &a) entering... "<< endl;
  this->pos_algorithm_id_ =
    static_cast<Tango::DevUShort>(DynamicAttritute::bpm_hw()->configuration().get_pos_algorithm());
  
  a.set_value(&this->pos_algorithm_id_);
}

//=============================================================================
// PosAlgorithmIdAttr::write
//=============================================================================
void PosAlgorithmIdAttr::write (Tango::DeviceImpl* d, Tango::WAttribute &wa)
{
  // DEBUG_STREAM << "PosAlgorithmIdAttr::write(Tango::DeviceImpl* d, Tango::WAttribute &wa) entering... "<< endl;

  Tango::DevUShort tdus;
  wa.get_write_value(tdus);

  int id = static_cast<int>(tdus);
  
  if (id < bpm::BPMConfig::kSTD_POS_ALGO || id >= bpm::BPMConfig::kINVALID_POS_ALGO_ID)
    Tango::Except::throw_exception(_CPTC("WRONG_PARAMETER"),
                                   _CPTC("invalid pos. comp. algorithm specify"),
                                   _CPTC("PosAlgorithmIdAttr::write"));
                                   
  bpm::BPMConfig cfg = DynamicAttritute::bpm_hw()->configuration();
  cfg.set_pos_algorithm(id);
  DynamicAttritute::bpm_hw()->reconfigure(cfg);
}

//=============================================================================
// PosAlgorithmIdAttr::is_allowed
//=============================================================================
bool PosAlgorithmIdAttr::is_allowed (Tango::DeviceImpl* d, Tango::AttReqType &rt)
{
  Tango::DevState s = d->get_state();
  return s != Tango::UNKNOWN && s != Tango::FAULT;
}

// //=============================================================================
// // BMPSpectrum::BMPSpectrum
// //=============================================================================
// BMPSpectrum::BMPSpectrum (const std::string& _name,
//                           Tango::DeviceImpl * TANG)
//   : DynamicAttritute (),
//     Tango::Attr (ATTR_NAME, TANGO_TYPE, TANGO_RW),
//     Tango::LogAdapter (TANGO_DEVICE)
// {
//   Tango::UserDefaultAttrProp  prop;
//   prop.set_label(DISP_LABEL);
//   prop.set_unit(std::sting(" ") + DISP_UNIT);
//   prop.set_format(DISP_FORMAT);
//   prop.set_description(DESC);
//   this->set_disp_level(DISP_LEVEL);
//   this->set_default_properties(prop);
// }
//                 
// //=============================================================================
// // BMPSpectrum::~BMPSpectrum
// //=============================================================================
// BMPSpectrum::~BMPSpectrum ()
// {
//   //- noop
// }
//     

} // namespace

