//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_BUFFER_CPP_
#define _BPM_BUFFER_CPP_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BPMBuffer.h"

#if !defined (__INLINE_IMPL__)
# include "BPMBuffer.i"
#endif // __INLINE_IMPL__

namespace bpm {

// ============================================================================
// Class : Buffer
// ============================================================================

// ============================================================================
// Buffer::Buffer
// ============================================================================
template <typename T>
Buffer<T>::Buffer (size_t _depth) throw (Tango::DevFailed) 
 : base_(0), depth_(0)
{
  //- allocate the buffer 
  if (_depth) 
  { 
    this->depth(_depth);
    this->clear();
  }
}

// ============================================================================
// Buffer::Buffer
// ============================================================================
template <typename T>
Buffer<T>::Buffer(size_t _depth, T* _base) throw (Tango::DevFailed) 
 : base_(0), depth_(0)
{
  //- allocate the buffer 
  this->depth(_depth);
  //- copy from source to destination using <Buffer::operator=>.
  *this = _base;
}

// ============================================================================
// Buffer::Buffer
// ============================================================================
template <typename T> 
Buffer<T>::Buffer(const Buffer<T>& _src) throw (Tango::DevFailed)
  : base_(0), depth_(0), length_(0)
{
  //- allocate the buffer 
  this->depth(_src.depth());
  //- copy from source to destination using <Buffer::operator=>.
  *this = _src;
}

// ============================================================================
// Buffer::~Buffer
// ============================================================================
template <typename T> 
Buffer<T>::~Buffer(void)
{
  if (this->base_)
  { 
    delete[] this->base_;
    this->base_ = 0;
  }
}

// ============================================================================
// Buffer::depth
// ============================================================================
template <typename T>
void Buffer<T>::depth(size_t _depth)
  throw (Tango::DevFailed)
{
  //- might have nothing to do
  if (_depth == this->depth_)
    return;
    
  //- release existing buffer
  if (this->base_)
  {
    delete[] this->base_;
    this->base_ = 0;
    this->depth_ = 0;
  }
  
  //- allocate the buffer 
  this->base_ = new T[_depth];
  if (this->base_ == 0)
  {
    Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"), 
                                    _CPTC ("memory allocation failed"), 
                                    _CPTC ("Buffer<T>::Buffer"));
  }
  
  //- set buffer depth.
  this->depth_ = _depth;
  
  //- no element
  this->length_ = 0;
}

// ============================================================================
// Buffer::depth
// ============================================================================
template <typename T>
INLINE_IMPL void Buffer<T>::force_length (size_t _len)
{
  this->length_ = (_len > this->depth_) ? this->depth_ : _len;
}

// ============================================================================
// Buffer::clear
// ============================================================================
template <typename T>
INLINE_IMPL void Buffer<T>::clear (void)
{
  ::memset(this->base_, 0,  this->depth_ * sizeof(T));

  this->length_ = 0;
}

// ============================================================================
// Buffer::elem_size
// ============================================================================
template <typename T>
INLINE_IMPL size_t Buffer<T>::elem_size(void) const
{
  return sizeof(T);
}

// ============================================================================
// Buffer::depth
// ============================================================================
template <typename T>
INLINE_IMPL size_t Buffer<T>::depth(void) const
{
  return this->depth_;
}

// Buffer::depth
// ============================================================================
template <typename T>
INLINE_IMPL size_t Buffer<T>::length(void) const
{
  return this->length_;
}

// ============================================================================
// Buffer::base
// ============================================================================
template <typename T>
INLINE_IMPL T* Buffer<T>::base(void) const
{
  return this->base_;
}

// ============================================================================
// Buffer::operator[]
// ============================================================================
template <typename T>
INLINE_IMPL T& Buffer<T>::operator[] (size_t _indx)
{
  /* !! no bound error check !!*/
  return this->base_[_indx];
}

// ============================================================================
// Buffer::operator[]
// ============================================================================
template <typename T>
INLINE_IMPL const T& Buffer<T>::operator[] (size_t _indx) const
{
  /* !! no bound error check !!*/
  return this->base_[_indx];
}

// ============================================================================
// Buffer::size
// ============================================================================
template <typename T>
INLINE_IMPL size_t Buffer<T>::size(void) const
{
	return this->depth_ * sizeof(T);
}

// ============================================================================
// Buffer::fill
// ============================================================================
template <typename T>
INLINE_IMPL void Buffer<T>::fill(const T& _val)
{
  *this = _val;
}

// ============================================================================
// Buffer::operator=
// ============================================================================
template <typename T>
INLINE_IMPL Buffer<T>& Buffer<T>::operator=(const Buffer<T>& src)
{
  if (&src == this)
    return *this;
  size_t cpy_depth = (src.depth_ < this->depth_) ? src.depth_ : this->depth_; 
  ::memcpy(this->base_, src.base_, cpy_depth * sizeof(T));
  this->length_ = cpy_depth;
  return *this;
}

// ============================================================================
// Buffer::operator=
// ============================================================================
template <typename T>
INLINE_IMPL Buffer<T>& Buffer<T>::operator=(const T* _src)
{
  if (_src == this->base_)
    return *this;
  ::memcpy(this->base_, _src, this->depth_ * sizeof(T));
  this->length_ = this->depth_;
  return *this;
}

// ============================================================================
// Buffer::operator=
// ============================================================================
template <typename T>
INLINE_IMPL Buffer<T>& Buffer<T>::operator=(const T& _val)
{
  for (size_t i = 0; i < this->depth_; i++)
     *(this->base_ + i) = _val;
  this->length_ = this->depth_;
  return *this;
}

// ============================================================================
// Class : CircularBuffer
// ============================================================================
// ============================================================================
// Buffer::CircularBuffer
// ============================================================================
template <typename T>
CircularBuffer<T>::CircularBuffer(void) 
  : wp_(0), 
    frozen_(false),
    data_ (0),
    ordered_data_ (0),
    num_cycles_ (0)
{
  //- noop
}

// ============================================================================
// Buffer::CircularBuffer
// ============================================================================
template <typename T>
CircularBuffer<T>::CircularBuffer(size_t _depth) 
  : wp_(0), 
    frozen_(false),
    data_ (0),
    ordered_data_ (0),
    num_cycles_ (0)
{
  this->depth(_depth);
}

// ============================================================================
// CircularBuffer::~CircularBuffer
// ============================================================================
template <typename T> 
CircularBuffer<T>::~CircularBuffer(void)
{
  //- noop dtor
}

// ============================================================================
// Buffer::depth
// ============================================================================
template <typename T>
INLINE_IMPL size_t CircularBuffer<T>::depth(void) const
{
  return this->data_.depth();
}

// ============================================================================
// CircularBuffer::~CircularBuffer
// ============================================================================
template <typename T> 
CBPastIterator<T> CircularBuffer<T>::past_iterator (size_t n) const
{
  size_t pi_depth = this->data_.depth();
  size_t pi_length = this->data_.length();
  
  if (! pi_length)
    return CBPastIterator<T>(0, 0, 0, 0, 0);
    
  size_t adapted_n = (n > pi_length) ? pi_length : n;

  T * pi_base = this->data_.base();
  
  size_t pi_pos = 0;
  
  if (this->num_cycles_)
  {
    pi_pos = ((this->wp_ - pi_base) + (pi_length - adapted_n)) % pi_depth;
  }
  else
  {
    int tmp = this->wp_ - pi_base - adapted_n;
    if (tmp < 0)
      pi_pos = 0;
    else
      pi_pos = (size_t)tmp;
  }
  
  int wp_index = this->wp_ - pi_base;  
  
  size_t pi_max_pos = (wp_index > 0) ? wp_index : 0;

/*
  std::cout << "n............" << n << std::endl;
  std::cout << "adapted_n...." << adapted_n << std::endl;
  std::cout << "wp..........." << std::hex << this->wp_ << std::dec << std::endl;
  std::cout << "base........." << std::hex << pi_base << std::dec << std::endl;
  std::cout << "wp - base...." << this->wp_ - pi_base << std::endl;
  std::cout << "pi_depth....." << pi_depth << std::endl;
  std::cout << "pi_length...." << pi_length << std::endl;
  std::cout << "pi_pos......." << pi_pos << std::endl;
  std::cout << "pi_max_pos..." << pi_max_pos << std::endl;
*/

  DEBUG_ASSERT(pi_max_pos <=  pi_depth);
  
  return CBPastIterator<T>(pi_pos, pi_max_pos, pi_base, adapted_n, pi_length);
}
  
// ============================================================================
// CircularBuffer::freeze
// ============================================================================
template <typename T> 
INLINE_IMPL void CircularBuffer<T>::freeze (void)
{
  this->frozen_ = true;
}

// ============================================================================
// CircularBuffer::unfreeze
// ============================================================================
template <typename T> 
INLINE_IMPL void CircularBuffer<T>::unfreeze (void)
{
  this->frozen_ = false;
}

// ============================================================================
// CircularBuffer::clear
// ============================================================================
template <typename T> 
void CircularBuffer<T>::clear (void)
{
  this->wp_ = this->data_.base();
  this->data_.clear();
  this->num_cycles_ = 0;
  this->ordered_data_.clear();
}

// ============================================================================
// Buffer::depth
// ============================================================================
template <typename T> 
void CircularBuffer<T>::depth (size_t _depth) throw (Tango::DevFailed)
{
  //- set buffer depth.
  this->data_.depth(_depth);
  //- update write pointer
  this->wp_ = this->data_.base();
  //- (re)allocate ordered data buffer
  this->ordered_data_.depth(_depth);
  this->ordered_data_.clear();
  //- reset num of cycles
  this->num_cycles_ = 0;
}

// ============================================================================
// CircularBuffer::fill
// ============================================================================
template <typename T>
INLINE_IMPL  void CircularBuffer<T>::fill(const T& _val)
{
  this->data_.fill(_val);
}

// ============================================================================
// CircularBuffer::push
// ============================================================================
template <typename T> 
INLINE_IMPL void CircularBuffer<T>::push(T _data)
  throw (Tango::DevFailed)
{
  //- check preallocation
  if (! this->data_.depth()) 
  {
    Tango::Except::throw_exception (_CPTC ("programming error"), 
                                    _CPTC ("circular buffer was not initialized properly"),
                                    _CPTC ("CircularBuffer::push"));
  } 

  //- store value
  *this->wp_ = _data;
  
  //- update num of elements in the underlying buffer
  this->data_.force_length(this->data_.length() + 1);

  //- update write pointer
  this->wp_++;
  
  //- modulo 
  if (static_cast<size_t>(this->wp_ - this->data_.base()) >= this->data_.depth())
  {
  	this->num_cycles_++;
  	this->wp_ =  this->data_.base();
  }

}

// ============================================================================
// CircularBuffer::ordered_data
// ============================================================================
template <typename T> 
const Buffer<T> & CircularBuffer<T>::ordered_data (void) const 
  throw (Tango::DevFailed)
{
  //- std::cout << "CircularBuffer::ordered_data <-" << std::endl;  
 
  static T * last_wp = 0;

  //- optimization: do nothing if write pointer pos did not change since last data ordering
  if (! this->frozen_ && this->wp_ != last_wp && last_wp != 0)
  {
    //- std::cout << "CircularBuffer::ordered_data::wrt ptr pos changed - reordering data..." << std::endl;  
    
    //- check preallocation
    if (this->ordered_data_.depth() != this->data_.depth())
    {
      Tango::Except::throw_exception (_CPTC ("INTERNAL_ERROR"),
                                      _CPTC ("unexpected buffer size"),
                                      _CPTC ("CircularBuffer::ordered_data"));
    }
    
    //- clear tmp buffer
    static_cast< bpm::Buffer<T> >(this->ordered_data_).clear();
    
    size_t newer_data_count = this->wp_ - this->data_.base(); 
    size_t older_data_count = this->data_.depth() - newer_data_count;
  
    //- reorder the data
    if (newer_data_count > 0 && this->num_cycles_)
    {
    	//- reorder the data: copy older data first
      ::memcpy(this->ordered_data_.base(), this->wp_, older_data_count * sizeof(T));
      //- reorder the data: copy newer data
      ::memcpy(this->ordered_data_.base() + older_data_count,  this->data_.base(),  newer_data_count * sizeof(T));
    }
    else
    {
      ::memcpy(this->ordered_data_.base(),  this->data_.base(), this->data_.size());
    }
  }
  else if (! last_wp) 
  {
    ::memcpy(this->ordered_data_.base(),  this->data_.base(), this->data_.size());
  }

  //- store current write pointer pos
  last_wp = this->wp_;

  //- std::cout << "CircularBuffer::ordered_data ->" << std::endl;  

  //- return the reordered data
  return this->ordered_data_;
}

// ============================================================================
// CircularBuffer::data
// ============================================================================
template <typename T>
INLINE_IMPL const Buffer<T> & CircularBuffer<T>::data (void) const
{
  return this->data_;
}

} // namespace bpm 

#endif // _BPM_BUFFER_CPP_

