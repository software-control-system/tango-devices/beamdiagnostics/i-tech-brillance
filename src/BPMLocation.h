//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_LOCATION_H_
#define _BPM_LOCATION_H_

// ============================================================================
// LOCATION STRINGS
// ============================================================================
#define LOCATION_UNKNOWN          "UNKNOWN"
#define LOCATION_TL1              "TL1"
#define LOCATION_BOOSTER          "BOOSTER"
#define LOCATION_TL2              "TL2"
#define LOCATION_STORAGE_RING     "STORAGE_RING"

namespace bpm
{

// ============================================================================
//- BPMLocation
// ============================================================================
typedef enum _BPMLocation
{
  BPM_LOC_UNKNOWN,
  BPM_LOC_TL1,
  BPM_LOC_BOOSTER,
  BPM_LOC_TL2,
  BPM_LOC_STORAGE_RING
}
BPMLocation;

}                               // namespace bpm

#endif                          // _BPM_LOCATION_H_
