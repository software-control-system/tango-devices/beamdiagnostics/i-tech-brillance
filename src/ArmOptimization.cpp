//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"

// ============================================================================
//- check configuration
// ============================================================================
#if ! defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
# error ARM optimized code can only runs on ARM target - please undef _USE_ARM_OPTIMIZATION_  
#endif

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ArmOptimization.h"

#if ! defined (__INLINE_IMPL__)
# include "ArmOptimization.i"
#endif // __INLINE_IMPL___

namespace bpm
{

// ============================================================================
// SCALING FACTORS (pre-computed once at startup)
// ============================================================================
ArmOptimization::ScalingFactor ArmOptimization::no_scaling_scale;
ArmOptimization::ScalingShift  ArmOptimization::no_scaling_shift;

ArmOptimization::ScalingFactor ArmOptimization::nm_to_mm_scale;
ArmOptimization::ScalingShift  ArmOptimization::nm_to_mm_shift;

ArmOptimization::ScalingFactor ArmOptimization::nm_to_um_scale;
ArmOptimization::ScalingShift  ArmOptimization::nm_to_um_shift;

ArmOptimization::ScalingFactor ArmOptimization::gain_scaling_scale;
ArmOptimization::ScalingShift  ArmOptimization::gain_scaling_shift;

# if defined(_USE_FLOAT_FP_DATA_) 

// ============================================================================
// compute_scaling_factor                   ** written by M.Abbott - DIAMOND **
// ============================================================================
// Compute optional scaling factor for conversion.  Need to compute
// i32_scaling and scaling_shift so that:
//      2**31 <= i32_scaling < 2**32
//      scaling = i32_scaling * 2**scaling_shift 
// ============================================================================
void ArmOptimization::compute_scaling_factor (float scaling, u32& i32_scaling, i32& scaling_shift)   
{ 
  float ln2 = logf(scaling) / logf(2.0);
  i32 bits = (i32)floorf(ln2) + 1;
  i32_scaling = (u32)(scaling * powf(2.0, 32.0 - bits));
  scaling_shift = bits - 32;
}  

#else // _USE_FLOAT_FP_DATA_ 

// ============================================================================
// compute_scaling_factor                   ** written by M.Abbott - DIAMOND **
// ============================================================================
// Compute optional scaling factor for conversion.  Need to compute
// i32_scaling and scaling_shift so that:
//      2**31 <= i32_scaling < 2**32
//      scaling = i32_scaling * 2**scaling_shift 
// ============================================================================
void ArmOptimization::compute_scaling_factor (double scaling, u64& i64_scaling, i32& scaling_shift)
{ 
  double ln2 = log(scaling) / log(2.0);
  i32 bits = (i32)floor(ln2) + 1;
  i64_scaling = (u64)(scaling * pow(2.0, 32.0 - bits));
  scaling_shift = bits - 32;
}  

#endif // _USE_FLOAT_FP_DATA_ 

// ============================================================================
// ArmOptimization::init_scaling_factors
// ============================================================================
void ArmOptimization::init_scaling_factors ()
{
  compute_scaling_factor(1.0, 
                         ArmOptimization::no_scaling_scale, 
                         ArmOptimization::no_scaling_shift);

  compute_scaling_factor(1.e-6, 
                         ArmOptimization::nm_to_mm_scale,
                         ArmOptimization::nm_to_mm_shift);

  compute_scaling_factor(1.e-3, 
                         ArmOptimization::nm_to_um_scale,
                         ArmOptimization::nm_to_um_shift);

  compute_scaling_factor(1.e-4, 
                         ArmOptimization::gain_scaling_scale,
                         ArmOptimization::gain_scaling_shift); 
}

} // namespace bpm

#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_
