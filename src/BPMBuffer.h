//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_BUFFER_H_
#define _BPM_BUFFER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"

namespace bpm {

// ============================================================================
//! A buffer abstraction class.  
// ============================================================================
//!  
//! This template class provides a buffer abstraction. 
//! <operator=> must be defined for template parameter T.
//! 
// ============================================================================
template <typename T> class Buffer 
{
public:
  
  /**
   * Constructor. 
   * @param  depth the maximum number of element of type T 
   *         that can be stored into the buffer 
   */
  Buffer (size_t depth = 0)
    throw (Tango::DevFailed);
 
  /**
   * Memory copy constructor. Memory is copied from _base to _base + _depth.
   * @param  depth the maximum number of element of type T 
   *         that can be stored into the buffer. 
   * @param  base address of the block to copy.
   */
  Buffer (size_t depth, T *base)
    throw (Tango::DevFailed);

  /**
   * Copy constructor. Use allocator associated with the source buffer.
   * @param  buf the source buffer.
   */
  Buffer (const Buffer<T> &buf)
    throw (Tango::DevFailed);

  /**
   * Destructor. Release resources.
   */
  virtual ~Buffer (void);

  /**
   * operator= 
   */
  Buffer<T>& operator= (const Buffer<T> &src);

  /**
   * operator=. Memory is copied from base to base + Buffer::depth_. 
   * @param base address of the block to copy.
   */
  Buffer<T>& operator= (const T *base);

  /**
   * operator=. Fill the buffer with a specified value.
   * @param val the value.
   */
  Buffer<T>& operator= (const T &val);
   
  /**
   * Fills the buffer with a specified value.
   * @param val the value.
   */
  void fill (const T& val);
  
  /**
   * Clears buffer's content. This is a low level clear: set memory
   * from Buffer::base_ to Buffer::base_ + Buffer::depth_ to 0.
   */
  void clear (void);

  /**
   * Returns a reference to the _ith element. No bound error checking.
   * @param i index of the element to return.
   * @return a reference to the ith element.
   */
  T& operator[] (size_t i);

  /**
   * Returns a reference to the _ith element. No bound error checking.
   * @param i index of the element to return.
   * @return a const reference to the ith element.
   */
  const T& operator[] (size_t i) const;
  
  /**
   * Returns the size of each element in bytes.
   * @return sizeof(T).
   */
  size_t elem_size (void) const;

  /**
   * Returns the actual size of the buffer in bytes. 
   * @return the buffer size in bytes.
   */
  size_t size (void) const;

  /**
   * Returns the maximum number of element that can be stored into 
   * the buffer. 
   * @return the buffer depth. 
   */
  size_t depth (void) const;
  
  /**
   * Set the buffer depth to _depth
   */
  virtual void depth (size_t _depth)
    throw (Tango::DevFailed);

  /**
   * Set current number of element
   */
  void force_length (size_t len);
  
  /**
   * Returns current number of element in the buffer
   */
  size_t length (void) const;
  
  /**
   * Returns the buffer base address. 
   * @return the buffer base address. 
   */
  T * base (void) const;

protected:

  /**
   * the buffer base address. 
   */
  T * base_;

  /**
   * maximum number of element of type T.
   */
  size_t depth_;

  /**
   * current number of element of type T.
   */
  size_t length_;
};

// ============================================================================
//! A past iterator for the circular buffer class.  
// ============================================================================
//!  
//! Iterates on the last n points of a circular buffer
//! 
// ============================================================================
template <typename T> class CBPastIterator 
{
 public:
    //- ctor
    CBPastIterator (size_t pos, size_t max_pos, T * base, size_t length, size_t depth)
      : pos_(pos), 
        max_pos_(max_pos), 
        base_(base), 
        length_(length), 
        depth_(depth), 
        pos_has_been_inc_(false)
    {
      //- noop     
    }
    
    //- dtor
    ~CBPastIterator ()
    {
      //- noop     
    }
    
    //- operator++
    inline void operator++ (int)
    {
      if (! length_ || (pos_has_been_inc_ && (pos_ == max_pos_)))
        return;
			pos_++;
      pos_has_been_inc_ = true;
      if (pos_ != max_pos_)
        pos_ %= depth_; 
    }
    
    //- operator*
    inline const T& operator* () const
    {
      return *(this->base_ + this->pos_);
    }

    //- end of past
    inline bool end_of_past () const
    {
      return (! length_) ? true : (pos_has_been_inc_ && (pos_ == max_pos_));
    }
    
private:
    size_t pos_;
    size_t max_pos_;
    T * base_;
    size_t length_;
    size_t depth_;
    bool pos_has_been_inc_;
};
        
        
// ============================================================================
//! A ciruclar buffer abstraction class.  
// ============================================================================
//!  
//! This template class provides a  (write only) circular buffer abstraction. 
//! <operator=> must be defined for template parameter T.
//! 
// ============================================================================
template <typename T> class CircularBuffer
{
public:

   typedef CBPastIterator<T> PastIterator;

  /**
   * Constructor. 
   */
  CircularBuffer (void);

  /**
   * Constructor. 
   */
  CircularBuffer (size_t depth);
  
  /**
   * Destructor. Release resources.
   */
  virtual ~CircularBuffer (void);

  /**
   * Clears buffer's content.
   */
  virtual void clear (void);

  /**
   * Fills the buffer with a specified value.
   * @param val the value.
   */
  void fill (const T& val);
  
  /**
   * Pushes the specified data into the circular buffer.
   * The data buffer must have 
   */
  void push (T new_element)
    throw (Tango::DevFailed);

  /**
   * Freezes the buffer. 
   * Any data pushed into a frozen circular buffer is silently ignored.  
   */
  void freeze (void);

  /**
   * Unfreeze 
   * Data pushed into a frozen circular buffer is silently ignored (see CircularBuffer::freeze).  
   */
  void unfreeze (void);

  /**
   * Returns the "chronologically ordered" circular buffer's content
   */
  const bpm::Buffer<T> & ordered_data (void) const
    throw (Tango::DevFailed);

  /**
   * Returns the circular buffer's content
   */
  const bpm::Buffer<T> & data (void) const;
    
  /**
   * Set the buffer depth to _depth
   */
  virtual void depth (size_t _depth)
    throw (Tango::DevFailed);
    
  /**
   * Returns buffer depth 
   */
  size_t depth () const;
    
  /**
   * Iterate on last n points in the buffer
   */
  CBPastIterator<T> past_iterator (size_t n) const;
    
private:
  /**
   * The write pointer
   */
  T * wp_; 

  /**
   * Frozen flag
   */
  bool frozen_;

  /**
   * The main buffer
   */
  bpm::Buffer<T> data_;
  
  /**
   * The ordered buffer
   */
  bpm::Buffer<T> ordered_data_;

  /**
   * Num of cycles
   */
  unsigned long num_cycles_;

  // = Disallow these operations.
  //--------------------------------------------
  CircularBuffer& operator= (const CircularBuffer&);
  CircularBuffer(const CircularBuffer&);
};

} // namespace bpm

#if defined (__INLINE_IMPL__)
# include "BPMBuffer.i"
#endif // __INLINE_IMPL__

#include "BPMBuffer.cpp"

#endif // _BUFFER_H_



