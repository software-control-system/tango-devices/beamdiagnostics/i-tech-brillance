//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// BPMConfig::enable_dd_buffer_freezing
// ============================================================================
INLINE_IMPL void BPMConfig::enable_dd_buffer_freezing (void)
{
  this->dd_buffer_freezing_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_dd_buffer_freezing
// ============================================================================
INLINE_IMPL void BPMConfig::disable_dd_buffer_freezing (void)
{
  this->dd_buffer_freezing_enabled_ = false;
}

// ============================================================================
// BPMConfig::dd_buffer_freezing_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::dd_buffer_freezing_enabled (void) const
{
  return this->dd_buffer_freezing_enabled_;
}


#if ! defined(_EMBEDDED_DEVICE_)
// ============================================================================
// BPMConfig::set_ip_config
// ============================================================================
INLINE_IMPL void BPMConfig::set_ip_config (const std::string & _ip_addr, 
                                           int _gs_port, 
                                           const std::string & _mcast_ip_addr, 
                                           int _mcast_port)
{
  this->ip_addr_ = _ip_addr;
  this->mcast_ip_addr_ = _mcast_ip_addr;
  this->gs_port_ = _gs_port;
  this->mcast_port_ = _mcast_port;
}
#endif //- _EMBEDDED_DEVICE_

// ============================================================================
// BPMConfig::enable_external_trigger
// ============================================================================
INLINE_IMPL void BPMConfig::enable_external_trigger (void)
{
  this->external_trigger_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_external_trigger
// ============================================================================
INLINE_IMPL void BPMConfig::disable_external_trigger (void)
{
  this->external_trigger_enabled_ = false;
}

// ============================================================================
// BPMConfig::external_trigger_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::external_trigger_enabled (void) const
{
  return this->external_trigger_enabled_;
}

// ============================================================================
// BPMConfig::set_dd_thread_activity_period
// ============================================================================
INLINE_IMPL void BPMConfig::set_dd_thread_activity_period (size_t _period)
{
  if (_period < kMIN_THREAD_ACTIVITY_PERIOD_MS)
    this->dd_thread_activity_period_ = kMIN_THREAD_ACTIVITY_PERIOD_MS;
  else if (_period > kMAX_THREAD_ACTIVITY_PERIOD_MS)
    this->dd_thread_activity_period_ = kMAX_THREAD_ACTIVITY_PERIOD_MS;
  else
    this->dd_thread_activity_period_ = _period;
}

// ============================================================================
// BPMConfig::get_dd_thread_activity_period
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_dd_thread_activity_period (void) const
{
  return this->dd_thread_activity_period_;
}

// ============================================================================
// BPMConfig::set_sa_thread_activity_period
// ============================================================================
INLINE_IMPL void BPMConfig::set_sa_thread_activity_period (size_t _period)
{
  if (_period < kMIN_THREAD_ACTIVITY_PERIOD_MS)
    this->sa_thread_activity_period_ = kMIN_THREAD_ACTIVITY_PERIOD_MS;
  else if (_period > kMAX_THREAD_ACTIVITY_PERIOD_MS)
    this->sa_thread_activity_period_ = kMAX_THREAD_ACTIVITY_PERIOD_MS;
  else
    this->sa_thread_activity_period_ = _period;
}

// ============================================================================
// BPMConfig::get_sa_thread_activity_period
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_sa_thread_activity_period (void) const
{
  return this->sa_thread_activity_period_;
}

// ============================================================================
// BPMConfig::set_adc_thread_activity_period
// ============================================================================
INLINE_IMPL void BPMConfig::set_adc_thread_activity_period (size_t _period)
{
  if (_period < kMIN_THREAD_ACTIVITY_PERIOD_MS)
    this->adc_thread_activity_period_ = kMIN_THREAD_ACTIVITY_PERIOD_MS;
  else if (_period > kMAX_THREAD_ACTIVITY_PERIOD_MS)
    this->adc_thread_activity_period_ = kMAX_THREAD_ACTIVITY_PERIOD_MS;
  else
    this->adc_thread_activity_period_ = _period;
}

// ============================================================================
// BPMConfig::get_adc_thread_activity_period
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_adc_thread_activity_period (void) const
{
  return this->adc_thread_activity_period_;
}

// ============================================================================
// BPMConfig::set_fa_cache_refresh_period
// ============================================================================
INLINE_IMPL void BPMConfig::set_fa_cache_refresh_period (size_t _period)
{
  if (_period < kMIN_FA_CACHE_REFRESH_PERIOD_MS)
    this->fa_cache_refresh_period_ = kMIN_FA_CACHE_REFRESH_PERIOD_MS;
  else if (_period > kMAX_FA_CACHE_REFRESH_PERIOD_MS)
    this->fa_cache_refresh_period_ = kMAX_FA_CACHE_REFRESH_PERIOD_MS;
  else
    this->fa_cache_refresh_period_ = _period;
}

// ============================================================================
// BPMConfig::get_fa_cache_refresh_period
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_fa_cache_refresh_period (void) const
{
  return this->fa_cache_refresh_period_;
}

// ============================================================================
// BPMConfig::get_offsets
// ============================================================================
INLINE_IMPL const BPMOffsets & BPMConfig::get_offsets (void) const
{
  return this->offsets_;
}

// ============================================================================
// BPMConfig::pass_bba_offsets_to_fpga
// ============================================================================
INLINE_IMPL void BPMConfig::pass_bba_offsets_to_fpga (bool yes_or_no)
{
  this->pass_bba_offsets_to_fpga_ = yes_or_no;
}
  
// ============================================================================
// BPMConfig::get_location
// ============================================================================
INLINE_IMPL BPMLocation BPMConfig::get_location (void) const
{
  return this->location_;
}

// ============================================================================
// BPMConfig::set_dd_raw_buffer_depth
// ============================================================================
INLINE_IMPL void BPMConfig::set_dd_raw_buffer_depth (size_t _depth)
{
  size_t max_dd_samples = DD_NSAMPLES_MAX_VALUE_CACHE_DISABLED;
  
#if ! defined(_EMBEDDED_DEVICE_)
  if (this->dd_buffer_freezing_enabled_)
    max_dd_samples = DD_NSAMPLES_MAX_VALUE_CACHE_ENABLED;
#endif

  if (this->get_decimation_factor() == 64)
    max_dd_samples = this->get_max_dd_buffer_depth_for_dec_on();  
      
  this->dd_raw_buffer_depth_ = (_depth <= max_dd_samples) 
                             ? _depth 
                             : max_dd_samples;
}

// ============================================================================
// BPMConfig::get_dd_raw_buffer_depth
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_dd_raw_buffer_depth (void)
{
  if (   
        this->get_decimation_factor() == 64 
      && 
        this->dd_raw_buffer_depth_ > this->get_max_dd_buffer_depth_for_dec_on()
     )
  {
     this->dd_raw_buffer_depth_ = this->get_max_dd_buffer_depth_for_dec_on();
  }
  
  return this->dd_raw_buffer_depth_;
}

// ============================================================================
// BPMConfig::set_dd_raw_buffer_depth
// ============================================================================
INLINE_IMPL void BPMConfig::set_max_dd_buffer_depth_for_dec_on (size_t _depth)
{
  this->max_dd_buffer_depth_for_dec_on_ = _depth;
}

// ============================================================================
// BPMConfig::get_dd_raw_buffer_depth
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_max_dd_buffer_depth_for_dec_on (void) const
{
  return this->max_dd_buffer_depth_for_dec_on_;
}

// ============================================================================
// BPMConfig::set_adc_raw_buffer_depth
// ============================================================================
INLINE_IMPL void BPMConfig::set_adc_raw_buffer_depth (size_t _depth)
{
  if (_depth < kMIN_ADC_RAW_BUFFER_SIZE)
     _depth = kMIN_ADC_RAW_BUFFER_SIZE;
  else if (_depth > kMAX_ADC_RAW_BUFFER_SIZE)
     _depth = kMAX_ADC_RAW_BUFFER_SIZE;
  this->adc_raw_buffer_depth_ = _depth;
}

// ============================================================================
// BPMConfig::get_adc_raw_buffer_depth
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_adc_raw_buffer_depth (void) const
{
  return this->adc_raw_buffer_depth_;
}

// ============================================================================
// BPMConfig::set_sa_history_depth
// ============================================================================
INLINE_IMPL void BPMConfig::set_sa_history_depth (size_t _depth)
{
  this->sa_data_history_buffer_depth_ = _depth;
}

// ============================================================================
// BPMConfig::get_sa_history_depth
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_sa_history_depth (void) const
{
  return this->sa_data_history_buffer_depth_;
}

// ============================================================================
// BPMConfig::set_sa_stats_num_samples
// ============================================================================
INLINE_IMPL void BPMConfig::set_sa_stats_num_samples (size_t _n)
{
  if (_n > this->sa_data_history_buffer_depth_)
    this->sa_stats_num_samples_ = this->sa_data_history_buffer_depth_;
  else
    this->sa_stats_num_samples_ = _n;
}

// ============================================================================
// BPMConfig::get_sa_rms_num_samples
// ============================================================================
INLINE_IMPL size_t BPMConfig::get_sa_rms_num_samples (void) const
{
  return this->sa_stats_num_samples_; 
}

// ============================================================================
// BPMConfig::get_block_geometry
// ============================================================================
INLINE_IMPL BPMConfig::BlockGeometry BPMConfig::get_block_geometry (void) const
{
  return this->block_geometry_;
}

// ============================================================================
// BPMConfig::set_block_geometry
// ============================================================================
INLINE_IMPL void BPMConfig::set_block_geometry (BPMConfig::BlockGeometry _bg)
{
  this->block_geometry_ = _bg;
}

// ============================================================================
// BPMConfig::enable_sa
// ============================================================================
INLINE_IMPL void BPMConfig::enable_sa (void)
{
  this->sa_enabled_ = true;
  if (this->enable_auto_switching_on_sa_activation_)
    this->set_switches_mode(kAUTO_SWITCHING_MODE);
}

// ============================================================================
// BPMConfig::disable_sa
// ============================================================================
INLINE_IMPL void BPMConfig::disable_sa (void)
{
  this->sa_enabled_ = false;
}

// ============================================================================
// BPMConfig::sa_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::sa_enabled (void) const
{
  return this->sa_enabled_;
}

// ============================================================================
// BPMConfig::enable_pm
// ============================================================================
INLINE_IMPL void BPMConfig::enable_pm (void)
{
  this->pm_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_pm
// ============================================================================
INLINE_IMPL void BPMConfig::disable_pm (void)
{
  this->pm_enabled_ = false;
}

// ============================================================================
// BPMConfig::pm_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::pm_enabled (void) const
{
  return this->pm_enabled_;
}

// ============================================================================
// BPMConfig::enable_adc
// ============================================================================
INLINE_IMPL void BPMConfig::enable_adc (void)
{
  this->adc_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_adc
// ============================================================================
INLINE_IMPL void BPMConfig::disable_adc (void)
{
  this->adc_enabled_ = false;
}

// ============================================================================
// BPMConfig::adc_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::adc_enabled (void) const
{
  return this->adc_enabled_;
}

// ============================================================================
// BPMConfig::enable_dd
// ============================================================================
INLINE_IMPL void BPMConfig::enable_dd (void)
{
  this->dd_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_dd
// ============================================================================
INLINE_IMPL void BPMConfig::disable_dd (void)
{
  this->dd_enabled_ = false;
}

// ============================================================================
// BPMConfig::dd_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::dd_enabled (void) const
{
  return this->dd_enabled_;
}

// ============================================================================
// BPMConfig::set_gain
// ============================================================================
INLINE_IMPL void BPMConfig::set_gain (int g)
{
  this->requested_ep_.gain = g;
}

// ============================================================================
// BPMConfig::get_gain
// ============================================================================
INLINE_IMPL int BPMConfig::get_gain (void) const
{
  return this->actual_ep_.gain;
}

// ============================================================================
// BPMConfig::set_x_low_limit
// ============================================================================
INLINE_IMPL void BPMConfig::set_x_low_limit (bpm::FPDataType _v)
{
  this->requested_ep_.ilk.Xlow = static_cast < int >(_v);
}

// ============================================================================
// BPMConfig::get_x_low_limit
// ============================================================================
INLINE_IMPL bpm::FPDataType BPMConfig::get_x_low_limit (void) const
{
  return static_cast < bpm::FPDataType >(this->actual_ep_.ilk.Xlow);
}

// ============================================================================
// BPMConfig::set_x_high_limit
// ============================================================================
INLINE_IMPL void BPMConfig::set_x_high_limit (bpm::FPDataType _v)
{
  this->requested_ep_.ilk.Xhigh = static_cast < int >(_v);
}

// ============================================================================
// BPMConfig::get_x_high_limit
// ============================================================================
INLINE_IMPL bpm::FPDataType BPMConfig::get_x_high_limit (void) const
{
  return static_cast < bpm::FPDataType >(this->actual_ep_.ilk.Xhigh);
}

// ============================================================================
// BPMConfig::set_z_low_limit
// ============================================================================
INLINE_IMPL void BPMConfig::set_z_low_limit (bpm::FPDataType _v)
{
  this->requested_ep_.ilk.Ylow = static_cast < int >(_v);
}

// ============================================================================
// BPMConfig::get_z_low_limit
// ============================================================================
INLINE_IMPL bpm::FPDataType BPMConfig::get_z_low_limit (void) const
{
  return static_cast < bpm::FPDataType >(this->actual_ep_.ilk.Ylow);
}

// ============================================================================
// BPMConfig::set_z_high_limit
// ============================================================================
INLINE_IMPL void BPMConfig::set_z_high_limit (bpm::FPDataType _v)
{
  this->requested_ep_.ilk.Yhigh = static_cast < int >(_v);
}

// ============================================================================
// BPMConfig::get_z_high_limit
// ============================================================================
INLINE_IMPL bpm::FPDataType BPMConfig::get_z_high_limit (void) const
{
  return static_cast < bpm::FPDataType >(this->actual_ep_.ilk.Yhigh);
}

// ============================================================================
// BPMConfig::set_decimation_factor
// ============================================================================
INLINE_IMPL void BPMConfig::set_decimation_factor (int _df)
{
  this->cp_dd_.dec = (_df <= 1) ? 1 : 64;
}

// ============================================================================
// BPMConfig::get_decimation_factor
// ============================================================================
INLINE_IMPL int BPMConfig::get_decimation_factor (void) const
{
  return this->cp_dd_.dec;
}

// ============================================================================
// BPMConfig::set_dd_trigger_offset
// ============================================================================
INLINE_IMPL void BPMConfig::set_dd_trigger_offset (unsigned long _toff)
{
  this->dd_trigger_offset_ = _toff;
}

// ============================================================================
// BPMConfig::get_dd_trigger_offset
// ============================================================================
INLINE_IMPL unsigned long BPMConfig::get_dd_trigger_offset (void) const
{
  return this->dd_trigger_offset_;
}

// ============================================================================
// BPMConfig::get_switches_mode
// ============================================================================
INLINE_IMPL short BPMConfig::get_switches_mode (void) const
{
  return static_cast<short>(this->actual_ep_.switches);
}

// ============================================================================
// BPMConfig::auto_switching_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::auto_switching_enabled (void) const
{
  return this->actual_ep_.switches == kAUTO_SWITCHING_MODE;
}

// ============================================================================
// BPMConfig::enable_auto_switching_on_sa_activation
// ============================================================================
INLINE_IMPL void BPMConfig::enable_auto_switching_on_sa_activation (bool behaviour)
{
  this->enable_auto_switching_on_sa_activation_ = behaviour;
}

// ============================================================================
// BPMConfig::enable_auto_switching_on_sa_activation
// ============================================================================
INLINE_IMPL bool BPMConfig::enable_auto_switching_on_sa_activation (void) const
{
  return this->enable_auto_switching_on_sa_activation_;
}
   
// ============================================================================
// BPMConfig::enable_dsc_on_auto_switching_activation
// ============================================================================
INLINE_IMPL void BPMConfig::enable_dsc_on_auto_switching_activation (bool behaviour)
{
  this->enable_dsc_on_auto_switching_activation_ = behaviour;
}

// ============================================================================
// BPMConfig::enable_dsc_on_auto_switching_activation
// ============================================================================
INLINE_IMPL bool BPMConfig::enable_dsc_on_auto_switching_activation (void) const
{
  return this->enable_dsc_on_auto_switching_activation_;
}

// ============================================================================
// BPMConfig::get_current_env_parameters
// ============================================================================
INLINE_IMPL const CSPI_ENVPARAMS& BPMConfig::get_current_env_parameters (void) const
{
  return this->actual_ep_;
}

// ============================================================================
// BPMConfig::enable_agc
// ============================================================================
INLINE_IMPL void BPMConfig::enable_agc (void)
{
  this->requested_ep_.agc = 1;
}

// ============================================================================
// BPMConfig::disable_agc
// ============================================================================
INLINE_IMPL void BPMConfig::disable_agc (void)
{
   this->requested_ep_.agc = 0;
}

// ============================================================================
// BPMConfig::agc_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::agc_enabled (void) const
{
  return this->actual_ep_.agc;
}

// ============================================================================
// BPMConfig::set_dsc_mode
// ============================================================================
INLINE_IMPL void BPMConfig::set_dsc_mode (const short _mode)
{
  switch (_mode)
  {
    case CSPI_DSC_OFF:
    case CSPI_DSC_UNITY:
    case CSPI_DSC_AUTO:
    case CSPI_DSC_SAVE_LASTGOOD:
      this->requested_ep_.dsc = static_cast<int>(_mode);
      break;
    default:
      break;
  }     
}

// ============================================================================
// BPMConfig::get_dsc_mode
// ============================================================================
INLINE_IMPL short BPMConfig::get_dsc_mode (void) const
{
  return static_cast<short>(this->actual_ep_.dsc);
}

// ============================================================================
// BPMConfig::dsc_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::dsc_enabled (void) const
{
  return this->actual_ep_.dsc != static_cast<short>(CSPI_DSC_OFF);
}

// ============================================================================
// BPMConfig::enable_external_switching
// ============================================================================
INLINE_IMPL void BPMConfig::enable_external_switching (void)
{
  this->requested_ep_.external_switching = 1;
}

// ============================================================================
// BPMConfig::disable_external_switching
// ============================================================================
INLINE_IMPL void BPMConfig::disable_external_switching (void)
{
   this->requested_ep_.external_switching = 0;
}

// ============================================================================
// BPMConfig::external_switching
// ============================================================================
INLINE_IMPL bool BPMConfig::external_switching_enabled (void) const
{
  return this->actual_ep_.external_switching;
}

// ============================================================================
// BPMConfig::enable_dd_optional_data
// ============================================================================
INLINE_IMPL void BPMConfig::enable_dd_optional_data (void)
{
  this->dd_optional_data_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_dd_optional_data
// ============================================================================
INLINE_IMPL void BPMConfig::disable_dd_optional_data (void)
{
  this->dd_optional_data_enabled_ = false;
}

// ============================================================================
// BPMConfig::dd_optional_data_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::dd_optional_data_enabled (void) const
{
  return this->dd_optional_data_enabled_;
}

// ============================================================================
// BPMConfig::enable_sa_optional_data
// ============================================================================
INLINE_IMPL void BPMConfig::enable_sa_optional_data (void)
{
  this->sa_optional_data_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_sa_optional_data
// ============================================================================
INLINE_IMPL void BPMConfig::disable_sa_optional_data (void)
{
  this->sa_optional_data_enabled_ = false;
}

// ============================================================================
// BPMConfig::sa_optional_data_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::sa_optional_data_enabled (void) const
{
  return this->sa_optional_data_enabled_;
}

// ============================================================================
// BPMConfig::enable_adc_optional_data
// ============================================================================
INLINE_IMPL void BPMConfig::enable_adc_optional_data (void)
{
  this->adc_optional_data_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_adc_optional_data
// ============================================================================
INLINE_IMPL void BPMConfig::disable_adc_optional_data (void)
{
  this->adc_optional_data_enabled_ = false;
}

// ============================================================================
// BPMConfig::adc_optional_data_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::adc_optional_data_enabled (void) const
{
  return this->adc_optional_data_enabled_;
}

// ============================================================================
// BPMConfig::enable_sa_history_optional_data
// ============================================================================
INLINE_IMPL void BPMConfig::enable_sa_history_optional_data (void)
{
  this->sa_history_optional_data_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_sa_history_optional_data
// ============================================================================
INLINE_IMPL void BPMConfig::disable_sa_history_optional_data (void)
{
  this->sa_history_optional_data_enabled_ = false;
}

// ============================================================================
// BPMConfig::sa_history_optional_data_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::sa_history_optional_data_enabled (void) const
{
  return this->sa_history_optional_data_enabled_;
}
 
// ============================================================================
// BPMConfig::enable_local_sa_pos_computation
// ============================================================================
INLINE_IMPL void BPMConfig::enable_local_sa_pos_computation (void)
{
  this->local_sa_pos_computation_enabled_ = true;
}

// ============================================================================
// BPMConfig::disable_local_sa_pos_computation
// ============================================================================
INLINE_IMPL void BPMConfig::disable_local_sa_pos_computation (void)
{
  this->local_sa_pos_computation_enabled_ = false;
}

// ============================================================================
// BPMConfig::local_sa_pos_computation_enabled
// ============================================================================
INLINE_IMPL bool BPMConfig::local_sa_pos_computation_enabled (void) const
{
  return this->local_sa_pos_computation_enabled_;
}

// ============================================================================
// BPMConfig::set_switching_delay
// ============================================================================
INLINE_IMPL void BPMConfig::set_switching_delay (int t)
{
  this->requested_ep_.switching_delay = t;
}

// ============================================================================
// BPMConfig::get_switching_delay
// ============================================================================
INLINE_IMPL int BPMConfig::get_switching_delay (void) const
{
  return this->actual_ep_.switching_delay;
}

// ============================================================================
// BPMConfig::enable_tune_compensation
// ============================================================================
INLINE_IMPL void BPMConfig::enable_tune_compensation (void)
{
  // mtncoshft is write only! To read nco_shift has to be used.
  this->requested_ep_.mtncoshft = 1;
}

// ============================================================================
// BPMConfig::disable_tune_compensation
// ============================================================================
INLINE_IMPL void BPMConfig::disable_tune_compensation (void)
{
	// mtncoshft is write only! To read nco_shift has to be used.
  this->requested_ep_.mtncoshft = 0;
}

// ============================================================================
// BPMConfig::compensation_tune
// ============================================================================
INLINE_IMPL bool BPMConfig::tune_compensation_enabled (void) const
{
  return this->actual_ep_.pll_status.mt_stat.nco_shift ? true : false;
}

// ============================================================================
// BPMConfig::set_tune_offset
// ============================================================================
INLINE_IMPL void BPMConfig::set_tune_offset (int t)
{
  //- mtvcxoffs is write only! 
  //- to read pll_status.mt_stat.vcxo_offset has to be used.
  this->requested_ep_.mtvcxoffs = t;
}

// ============================================================================
// BPMConfig::get_tune_offset
// ============================================================================
INLINE_IMPL int BPMConfig::get_tune_offset (void) const
{
  return this->actual_ep_.pll_status.mt_stat.vcxo_offset;
}

// ============================================================================
// BPMConfig::set_external_trigger_delay
// ============================================================================
INLINE_IMPL void BPMConfig::set_external_trigger_delay (int t)
{
  this->requested_ep_.trig_delay = t;
}

// ============================================================================
// BPMConfig::get_external_trigger_delay
// ============================================================================
INLINE_IMPL int BPMConfig::get_external_trigger_delay (void) const
{
  return this->actual_ep_.trig_delay;
}

// ============================================================================
// BPMConfig::set_pm_offset
// ============================================================================
INLINE_IMPL void BPMConfig::set_pm_offset (int t)
{
  this->requested_ep_.PMoffset = t;
}

// ============================================================================
// BPMConfig::get_pm_offset
// ============================================================================
INLINE_IMPL int BPMConfig::get_pm_offset (void) const
{
  return this->actual_ep_.PMoffset;
}

// ============================================================================
// BPMConfig::libera_model
// ============================================================================
INLINE_IMPL unsigned short BPMConfig::libera_model () const
{
  return LIBERA_IS_BRILLIANCE(this->actual_ep_.feature.itech) ? 1 : 0;
}

// ============================================================================
// BPMConfig::has_maf_support
// ============================================================================
INLINE_IMPL bool BPMConfig::has_maf_support () const
{
  return LIBERA_IS_MAF(this->actual_ep_.feature.itech);
}
  
// ============================================================================
// BPMConfig::set_maf_length
// ============================================================================
INLINE_IMPL void BPMConfig::set_maf_length (int ml)
{
  if (ml < kMIN_MAF_LENGTH || ml> kMAX_MAF_LENGTH)
    Tango::Except::throw_exception(_CPTC("INVALID_PARAMETER"), 
                                   _CPTC("MAF delay must be in [1, 129]"), 
                                   _CPTC("Libera::set_maf_length"));
                                   
  this->requested_ep_.ddc_maflength = ml;
}

// ============================================================================
// BPMConfig::get_maf_length
// ============================================================================
INLINE_IMPL int BPMConfig::get_maf_length (void) const
{
  return this->actual_ep_.ddc_maflength; 
}

// ============================================================================
// BPMConfig::set_maf_delay
// ============================================================================
INLINE_IMPL void BPMConfig::set_maf_delay (int md)
{
  if (md < kMIN_MAF_DELAY || md > kMAX_MAF_DELAY)
    Tango::Except::throw_exception(_CPTC("INVALID_PARAMETER"), 
                                   _CPTC("MAF delay must be in [0, 128]"), 
                                   _CPTC("Libera::set_maf_delay"));
                                   
  this->requested_ep_.ddc_mafdelay = md; 
}

// ============================================================================
// BPMConfig::get_maf_delay
// ============================================================================
INLINE_IMPL int BPMConfig::get_maf_delay (void) const
{
  return this->actual_ep_.ddc_mafdelay;
}

// ============================================================================
// BPMConfig::set_institute
// ============================================================================
INLINE_IMPL void BPMConfig::set_institute (int i)
{
  this->institute_ = i;
}

// ============================================================================
// BPMConfig::get_institute
// ============================================================================
INLINE_IMPL int BPMConfig::get_institute (void) const
{
  return this->institute_;
}

// ============================================================================
// BPMConfig::set_pos_algorithm
// ============================================================================
INLINE_IMPL void BPMConfig::set_pos_algorithm (int a)
{
  this->pos_algorithm_ = a;
}

// ============================================================================
// BPMConfig::get_pos_algorithm
// ============================================================================
INLINE_IMPL int BPMConfig::get_pos_algorithm (void) const
{
  return this->pos_algorithm_;
}

} //- namespace bpm
