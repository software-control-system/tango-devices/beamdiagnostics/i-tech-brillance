//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/SharedObject.h"

#if !defined (__INLINE_IMPL__)
# include "threading/SharedObject.i"
#endif

namespace bpm
{
// ============================================================================
// SharedObject::SharedObject
// ============================================================================
SharedObject::SharedObject (void) : reference_count_ (1)
{
  //- noop
}

// ============================================================================
// SharedObject::~SharedObject
// ============================================================================
SharedObject::~SharedObject (void)
{
  //- check reference_count_ value
  DEBUG_ASSERT(this->reference_count_ == 0);
}

// ============================================================================
// SharedObject::duplicate
// ============================================================================
SharedObject *SharedObject::duplicate (void)
{
  AutoMutex<Mutex> guard (this->lock_);

  this->reference_count_++;

  return this;
}

// ============================================================================
// SharedObject::release
// ============================================================================
void SharedObject::release (void)
{
  //- the try/catch block is a trick for embedded version...
  try
  {
    if (this->release_i () == 0)
      delete this;
  }
  catch (...)
  {
    //- ignore exception
  }
}

// ============================================================================
// SharedObject::release_i
// ============================================================================
SharedObject *SharedObject::release_i (void)
{
  AutoMutex<Mutex> guard (this->lock_);

  DEBUG_ASSERT(this->reference_count_ > 0);

  this->reference_count_--;

  return (this->reference_count_ == 0) ? 0 : this;
}

} //- namespace bpm
