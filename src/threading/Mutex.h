//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_MUTEX_H_
#define _BPM_MUTEX_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/Utilities.h"
#include "threading/Implementation.h"

// ============================================================================
// Implementation-specific header file.
// ============================================================================
#if ! defined(__MUTEX_IMPLEMENTATION)
# error "implementation header file incomplete [no mutex implementation]"
#endif

namespace bpm {

// ============================================================================
//! <BPM_MUTEX>::try_lock may return one of the following MutexState
// ============================================================================
typedef enum
{
  MUTEX_LOCKED,
  MUTEX_BUSY,
} MutexState;

// ============================================================================
//! The BPM NullMutex class
// ============================================================================
class NullMutex
{
  //! This is the yat NullMutex class.
  //!
  //! Provides a "do nothing" Mutex impl. May be used as template argument
  //! in order to control the template instanciation and avoiding locking
  //! overhead where thread safety is not required.
  //!
  //! template <typename LOCK> class OptionalThreadSafetyImpl
  //! {
  //! public:
  //!   inline void do_something (void)
  //!   {
  //!      yat::AutoMutex<LOCK>(this->m_mutex);
  //!      ...
  //!   }
  //! private:
  //    LOCK m_mutex;
  //! }
  //!
  //! OptionalThreadSafetyImpl<yat::Mutex> will be thread safe while...
  //! OptionalThreadSafetyImpl<yat::NullMutex> will not be!
  //!
  //! This class is not supposed to be derived.

public:
  //! Constructor.
  NullMutex (void);

  //! Destructor.
  ~NullMutex (void);

  //! Locks (i.e. acquires) the mutex.
  void lock (void);

  //! Locks (i.e. acquires) the mutex.
  void acquire (void);

  //! Locks (i.e. acquires) the mutex. Always returns MUTEX_LOCKED.
  MutexState try_lock (void);

  //! Locks (i.e. acquires) the mutex. Always returns MUTEX_LOCKED.
  MutexState try_acquire (void);

  //! Unlocks (i.e. releases) the mutex.
  void unlock (void);

  //! Unlocks (i.e. releases) the mutex.
  void release (void);

private:
  //! Not implemented private member
  NullMutex (const NullMutex&);
  //! Not implemented private member
  NullMutex & operator= (const NullMutex&);
};

// ============================================================================
//! The BPM Mutex class
// ============================================================================
class Mutex
{
  //! This is the yat Mutex implementation.
  //!
  //! This class is not supposed to be derived (no virtual destructor).

public:
  //! Constructor.
  Mutex (void);

  //! Destructor.
  ~Mutex (void);

  //! Locks (i.e. acquires) the mutex.
  void lock (void);

  //! Locks (i.e. acquires) the mutex.
  void acquire (void);

  //! Locks (i.e. acquires) the mutex.
  //! Returns MUTEX_LOCKED in case the mutex was successfully locked.
  //! Returns MUTEX_BUSY if it is already owned by another thread.
  MutexState try_lock (void);
  
  //! Locks (i.e. acquires) the mutex.
  //! Returns MUTEX_LOCKED in case the mutex was successfully locked.
  //! Returns MUTEX_BUSY if it is already owned by another thread.
  MutexState try_acquire (void);

  //! Unlocks (i.e. releases) the mutex.
  void unlock (void);
  
  //! Unlocks (i.e. releases) the mutex.
  void release (void);

private:
  //! Not implemented private member
  Mutex (const Mutex&);
  //! Not implemented private member
  Mutex & operator= (const Mutex&);

  //- platform specific implementation
  __MUTEX_IMPLEMENTATION;
};

// ============================================================================
//! The BPM "auto mutex" class
// ============================================================================
template <typename LOCK_TYPE = bpm::Mutex> class AutoMutex
{
  //! An "auto mutex" providing an auto lock/unlock mechanism.
  //!
  //! The AutoMutex is ideal in context where some exceptions may be thrown.
  //! Whatever is the exit path of your code, the <AutoMutex> will garantee
  //! that the associated <Mutex> is properly unlock.
  //!
  //! This class is template since it may be used in contexts in which the
  //! thread safety is optionnal (see yat::NullMutex for an example).
  //!
  //! AutoMutex provides an efficient and safe alternative to:
  //!
  //! { //- enter critical section
  //!   my_mutex.lock();
  //!   ...your critical section code goes here (may throw an exception)...
  //!   my_mutex.unlock();
  //! } //- leave critical section
  //!
  //! In such a context, you can use a instance AutoMutex as follows:
  //!
  //! { //- enter critical section
  //!   yat::AutoMutex<> guard(my_mutex);
  //!   ...your critical section code goes here (may throw an exception)...
  //! } //- leave critical section
  //!
  //! This has the advantage that my_mutex.unlock() will be called automatically
  //! even if an exception is thrown. Since the AutoMutex is created on the stack
  //! its destructor will be called whatever is the exit path of critical section.
  //!
  //! Note that AutoMutex can be used with any "LOCK_TYPE" which interface contains
  //! both a lock(void) and a unlock(void) method. The yat::SharedObject class of
  //! such a compatible "LOCK_TYPE". 
  //!
public:
  //! Constructor (locks the associated Mutex)
  AutoMutex (LOCK_TYPE & _lock)
    : m_lock (_lock)
  {
    m_lock.lock();
  }

  //! Destructor (unlocks the associated Mutex)
  ~AutoMutex (void)
  {
    m_lock.unlock();
  }

private:
  //! The associated Mutex
  LOCK_TYPE & m_lock;

  //! Not implemented private member
  AutoMutex (const AutoMutex&);
  //! Not implemented private member
  AutoMutex & operator= (const AutoMutex&);
};

// ============================================================================
//! MutexLock: an AutoMutex specialisation (for backforward compatibility)
// ============================================================================
typedef AutoMutex<Mutex> MutexLock;

} // namespace bpm

#if defined (__INLINE_IMPL__)
#  include "threading/impl/PosixMutexImpl.i"
#endif

#endif //- _BPM_MUTEX_H_
