//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------


#ifndef _BPM_THREADING_UTILS_H_
#define _BPM_THREADING_UTILS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/Implementation.h"

// ============================================================================
// CONSTs
// ============================================================================
#define BPM_INVALID_THREAD_UID 0xffffffff

namespace bpm {

// ============================================================================
//! A dedicated type for thread identifier
// ============================================================================
typedef unsigned long ThreadUID;

// ============================================================================
//! The BPM threading utilities
// ============================================================================
class ThreadingUtilities
{
public:
 	//! Returns the calling thread identifier.
  static ThreadUID self (void);

	//! Causes the caller to sleep for the given time.
  static void sleep (unsigned long secs, unsigned long nanosecs = 0);

	//! Calculates an absolute time in seconds and nanoseconds, suitable for
	//! use in timed_waits, which is the current time plus the given relative 
  //! offset.
  static void get_time (unsigned long & abs_sec,
                        unsigned long & abs_nsec,
			                  unsigned long offset_sec = 0,
                        unsigned long offset_nsec = 0);
  
	//! Calculates an absolute time in seconds and nanoseconds, suitable for
	//! use in timed_waits, which is the current time plus the given relative 
  //! offset. WORKS ONLY FOR <delay_msecs> < 1 SECS. 
  static void get_time (unsigned long delay_msecs, Timespec& abs_time);
};

} // namespace bpm 

#endif //- _BPM_THREADING_UTILS_H_
