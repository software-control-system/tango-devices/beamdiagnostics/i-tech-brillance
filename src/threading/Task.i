//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// Task::enable_timeout_msg
// ============================================================================
INLINE_IMPL void Task::enable_timeout_msg (bool b)
{
  this->msg_q_.enable_timeout_msg(b);
}

// ============================================================================
// Task::timeout_msg_enabled
// ============================================================================
INLINE_IMPL bool Task::timeout_msg_enabled (void) const
{
  return this->msg_q_.timeout_msg_enabled();
}

// ============================================================================
// Task::set_timeout_msg_period
// ============================================================================
INLINE_IMPL void Task::set_timeout_msg_period (size_t _tmo)
{
  this->timeout_msg_period_ms_ = _tmo;
}

// ============================================================================
// Task::get_timeout_msg_period
// ============================================================================
INLINE_IMPL size_t Task::get_timeout_msg_period (void) const
{
  return this->timeout_msg_period_ms_;
}

// ============================================================================
// Task::enable_periodic_msg
// ============================================================================
INLINE_IMPL void Task::enable_periodic_msg (bool b)
{
  this->msg_q_.enable_periodic_msg(b);
}

// ============================================================================
// Task::periodic_msg_enabled
// ============================================================================
INLINE_IMPL bool Task::periodic_msg_enabled (void) const
{
  return this->msg_q_.periodic_msg_enabled();
}

// ============================================================================
// Task::set_periodic_timeout
// ============================================================================
INLINE_IMPL void Task::set_periodic_msg_period (size_t _tmo)
{
  this->periodic_msg_period_ms_ = _tmo;
}

// ============================================================================
// Task::get_timeout_msg_period
// ============================================================================
INLINE_IMPL size_t Task::get_periodic_msg_period (void) const
{
  return this->periodic_msg_period_ms_;
}

// ============================================================================
// Task::actual_timeout_msg_period
// ============================================================================
INLINE_IMPL size_t Task::actual_timeout (void) const
{
  if (this->msg_q_.enable_periodic_msg_ && this->periodic_msg_period_ms_)
    return this->periodic_msg_period_ms_;
  if (this->msg_q_.enable_timeout_msg_ && this->timeout_msg_period_ms_)
    return this->timeout_msg_period_ms_;
  return kDEFAULT_TASK_TMO_MSECS;
}

// ============================================================================
// Task::post
// ============================================================================
INLINE_IMPL void Task::post (bpm::Message * _msg, size_t _tmo_msecs) 
	throw (Tango::DevFailed)
{
  this->msg_q_.post (_msg, _tmo_msecs);
}

// ============================================================================
// Thread::post
// ============================================================================
INLINE_IMPL void Task::post (CSPI_EVENT * evt, size_t _tmo_msecs)  
  throw (Tango::DevFailed)
{
  this->msg_q_.post(evt, _tmo_msecs);
}

} //- namespace bpm
