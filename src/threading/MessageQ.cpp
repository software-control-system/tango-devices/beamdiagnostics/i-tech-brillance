//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"
#include "threading/MessageQ.h"

#if !defined (__INLINE_IMPL__)
# include "threading/MessageQ.i"
#endif // __INLINE_IMPL__

namespace bpm
{

// ============================================================================
// MessageQ::MessageQ
// ============================================================================
MessageQ::MessageQ (size_t _lo_wm, size_t _hi_wm, bool _throw_on_post_tmo)
  : msg_q_ (0),
    msg_producer_sync_ (lock_),
    msg_consumer_sync_ (lock_),
		state_(MessageQ::OPEN),
		enable_timeout_msg_ (false),
    enable_periodic_msg_ (false),
    lo_wm_ (_lo_wm),
    hi_wm_ (_hi_wm),
    saturated_ (false),
    throw_on_post_msg_timeout_ (_throw_on_post_tmo),
    last_returned_msg_periodic_ (true)
{
  this->last_periodic_msg_ts_.tv_sec = 0;
  this->last_periodic_msg_ts_.tv_usec = 0;
}

// ============================================================================
// MessageQ::~MessageQ
// ============================================================================
MessageQ::~MessageQ (void)
{
  bpm::AutoMutex<bpm::Mutex> (this->lock_);

  this->state_ = MessageQ::CLOSED;

  this->clear_i();
}

// ============================================================================
// MessageQ::clear_i
// ============================================================================
size_t MessageQ::clear_i (void)
{
  size_t num_msg_in_q = this->msg_q_.size();

  while (! this->msg_q_.empty())
  {
	  Message * m = this->msg_q_.front ();
    if (m) m->release();
	  this->msg_q_.pop_front();
  }

  return num_msg_in_q;
}

// ============================================================================
// MessageQ::close
// ============================================================================
void MessageQ::close (void)
{
  bpm::AutoMutex<bpm::Mutex> (this->lock_);
  
	this->state_ = MessageQ::CLOSED;
}

// ============================================================================
// MessageQ::post
// ============================================================================
void MessageQ::post (bpm::Message * msg, size_t _tmo_msecs)
  throw (Tango::DevFailed)
{
  //- check input
  if (! msg) return;
  
  //- caller can't post any TIMEOUT or PERIODIC msg
  if (msg->type() == TASK_TIMEOUT || msg->type() == TASK_PERIODIC)
  {
	  //- silently trash the message
	  msg->release();
	  return;
  }
  
  //- enter critical section (required for cond.var. to work properly)
  bpm::AutoMutex<bpm::Mutex> guard(this->lock_);

  //- can only post a msg on an opened MsgQ
  if (this->state_ != MessageQ::OPEN)
  {
	  //- silently trash the message (should we throw an exception instead?)
	  msg->release();
	  return;
  }

  //- we force post of ctrl message even if the msQ is saturated
  if (msg->is_ctrl_message()) 
  {
    //- insert msg according to its priority
    try
    {
      this->insert_i(msg);
    }
    catch (...)
    {
      //- insert_i released the message (no memory leak)
      Tango::Except::throw_exception (_CPTC ("INTERNAL_ERROR"),
                                      _CPTC ("could not post ctrl message [msgQ insertion error]"),
                                      _CPTC ("MessageQ::post"));
    }
    //- wakeup msg consumers (tell them there is a message to handle)
    //- this will work since we are under critical section 
    msg_consumer_sync_.signal();
    //- done (skip remaining code)
    return;
  }

  //- is the messageQ saturated?
  if (! this->saturated_ && (this->msg_q_.size() == this->hi_wm_))
  {
    //- mark msgQ as saturated
    this->saturated_ = true;
  }

  //- msg is not a ctrl message...
  //- wait for the messageQ to have room for new messages
  if (! this->wait_not_full_i(_tmo_msecs))
  {
    //- can't post msg, destroy it in order to avoid memory leak
    msg->release(); 
    //- throw exception if the messageQ is configured to do so
    if (this->throw_on_post_msg_timeout_)
    {
      Tango::Except::throw_exception (_CPTC ("TIMEOUT_EXPIRED"),
                                      _CPTC ("could not post message [timeout expired]"),
                                      _CPTC ("MessageQ::post"));
    }
    //- return if we didn't throw an exception
    return;
  }
  
  DEBUG_ASSERT(this->msg_q_.size() <= this->hi_wm_);

  //- insert the message according to its priority
  try
  {
    this->insert_i(msg);
  }
  catch (...)
  {
    //- insert_i released the message (no memory leak)
    Tango::Except::throw_exception (_CPTC ("INTERNAL_ERROR"),
                                    _CPTC ("could not post message [msgQ insertion error]"),
                                    _CPTC ("MessageQ::post"));
  }

  //- wakeup msg consumers (tell them there is a new message to handle)
  //- this will work since we are still under critical section
  //--------------------------------------------------
  //-TODO: avoid using this under sys signal callback!
  //--------------------------------------------------
  msg_consumer_sync_.signal ();
}

// ============================================================================
// MessageQ::post
// ============================================================================
void MessageQ::post (CSPI_EVENT * _evt, size_t _tmo_msecs)
  throw (Tango::DevFailed)
{
  //- check input
  if (! _evt) return;

  bpm::Message * msg = 0;

  try
  {
    msg = new bpm::Message (_evt);
    if (msg == 0)
      throw std::bad_alloc ();
  }
  catch (const std::bad_alloc &)
  {
    Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                    _CPTC ("memory allocation failed"),
                                    _CPTC ("MessageQ::post"));
  }
  catch (...)
  {
    Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                    _CPTC ("memory allocation failed"),
                                    _CPTC ("MessageQ::post"));
  }
  
  this->post(msg, _tmo_msecs);
}

// ============================================================================
// MessageQ::next_message
// ============================================================================ 
bpm::Message * MessageQ::next_message (size_t _tmo_msecs)
{
  //- enter critical section (required for cond.var. to work properly)
	bpm::AutoMutex<bpm::Mutex> guard(this->lock_);

  //- the msg
  bpm::Message * msg = 0;
  
  //- wait for the messageQ to contain at least one message
  //- bpm::Timer t;
  //- std:cout << "[this:" << (long)this << "]" << " - wait_not_empty_i waiting for msg [tmo is " << _tmo_msecs << "]" << std::endl;
  if (! this->wait_not_empty_i(_tmo_msecs))
  {
    //- std::cout << "[this:" << (long)this << "]" << " - wait_not_empty_i returned false after " << t.elapsed_msec() << "ms [tmo was: " << _tmo_msecs << "]" << std::endl;
    //- <wait_not_empty_i> returned <false> : means no msg in msg queue after <_tmo_msecs>
    //- it may be time to return a periodic message
    if (this->enable_periodic_msg_ && this->periodic_tmo_expired(_tmo_msecs))
    {
      this->last_returned_msg_periodic_ = true; 
      GET_TIME(this->last_periodic_msg_ts_);
      //- std::cout << "MessageQ::next_message::returning TASK_PERIODIC" << std::endl; 
      return new Message(TASK_PERIODIC); 
    }
    //- else return a timeout msg 
    if (this->enable_timeout_msg_)  
    {
      this->last_returned_msg_periodic_ = false;
      //- std::cout << "MessageQ::next_message::returning TASK_TIMEOUT" << std::endl; 
      return new Message(TASK_TIMEOUT);
    }
    //- no msg 
    //- std::cout << "MessageQ::next_message::returning NULL msg" << std::endl; 
    this->last_returned_msg_periodic_ = false; 
    return 0;
  }
    
  //- std::cout << "[this:" << (long)this << "]" << " - wait_not_empty_i returned true after " << t.elapsed_msec() << "ms [tmo was: " << _tmo_msecs << "]" << std::endl;
  
  //- ok, there should be at least one message in the messageQ
  DEBUG_ASSERT(this->msg_q_.empty() == false);

  //- we are still under critical section since the "Condition::timed_wait" 
  //- located in "wait_not_empty_i" garantee that the associated mutex (i.e. 
  //- this->lock_ in the present case) is acquired when the function returns

	//- get msg from Q
  msg = this->msg_q_.front();

  //- parano. debugging
  DEBUG_ASSERT(msg != 0);

  //- if the msg is a ctrl msg...
  if (msg->is_ctrl_message())
  {
    //... then extract it from the Q and return it
	  this->msg_q_.pop_front();
    //- if we reach the low water mark, then wakeup msg producer(s) 
    if (this->saturated_ && this->msg_q_.size() <= this->lo_wm_)
    {
      //- no longer saturated
      this->saturated_ = false;
      //- this will work since we are still under critical section
      this->msg_producer_sync_.broadcast();
    }
    //- we are about to return a ctrl msg so...
    this->last_returned_msg_periodic_ = false;
    //- return ctrl message
    //- std::cout << "MessageQ::next_message::returning a CTRL msg" << std::endl; 
	  return msg;
  }

  //- avoid PERIODIC msg starvation (see note above)
  if (
       this->enable_periodic_msg_ 
         &&
       this->periodic_tmo_expired(_tmo_msecs) 
         && 
       this->last_returned_msg_periodic_ == false
     )
  {
      //- we didn't actually extract the <msg> from the Q.
      //- we just "accessed it using "pop_front" so no need to reinject it into the Q
      this->last_returned_msg_periodic_ = true;
      GET_TIME(this->last_periodic_msg_ts_);
      //- std::cout << "MessageQ::next_message::returning TASK_PERIODIC msg" << std::endl; 
      return new Message(TASK_PERIODIC); 
  }
  
  //- we are about to return a msg from the Q so...
  this->last_returned_msg_periodic_ = false;
  
  //- then extract it from the Q and return it
  this->msg_q_.pop_front();
  
  //- if we reach the low water mark, then wakeup msg producer(s) 
  if (this->saturated_ && this->msg_q_.size() <= this->lo_wm_)
  {
    //- no longer saturated
    this->saturated_ = false;
    //- this will work since we are still under critical section
    this->msg_producer_sync_.broadcast(); 
  }
	
	//- std::cout << "MessageQ::next_message::returning USER msg" << std::endl; 
	
	return msg;
}

// ============================================================================
// MessageQ::wait_not_empty_i
// ============================================================================
bool MessageQ::wait_not_empty_i (size_t _tmo_msecs)
{
	//- <this->lock_> MUST be locked by the calling thread
  //----------------------------------------------------

  //- while the messageQ is empty...
  while (this->msg_q_.empty())
  {
 	  //- wait for a msg or tmo expiration 
    if (! this->msg_consumer_sync_.timed_wait(_tmo_msecs))
      return false; 
  }

  //- at least one message available in the MsgQ
  return true;
} 

// ============================================================================
// MessageQ::wait_not_full_i
// ============================================================================
bool MessageQ::wait_not_full_i (size_t _tmo_msecs)
{
	//- <this->lock_> MUST be locked by the calling thread
  //----------------------------------------------------

  //- while the messageQ is full...
  while (this->saturated_)
  {
 	  //- wait for some msgs to be consumed or tmo expiration 
    if (! this->msg_producer_sync_.timed_wait(_tmo_msecs))
      return false; 
  }

  //- at least one message available in the MsgQ
  return true;
}

// ============================================================================
// Binary predicate
// ============================================================================
static bool insert_msg_criterion (Message * const m1, Message * const m2)
{
  return m2->priority() < m1->priority();  
}
 
// ============================================================================
// MessageQ::insert_i
// ============================================================================
void MessageQ::insert_i (Message * _msg)
  throw (Tango::DevFailed)
{
  try
  {
    if (this->msg_q_.empty())
    {
      //- optimization: no need to take count of the msg priority
      this->msg_q_.push_front (_msg);
    }
    else
    { 
      //- insert msg according to its priority
      MessageQImpl::iterator pos = std::upper_bound(this->msg_q_.begin(),
                                                    this->msg_q_.end(),
                                                    _msg,
                                                    insert_msg_criterion);
      this->msg_q_.insert(pos, _msg);
    }
  }
  catch (...)
  {
    _msg->processed();
    _msg->release();
    Tango::Except::throw_exception (_CPTC ("INTERNAL_ERROR"),
                                    _CPTC ("could insert message into the message queue"),
                                    _CPTC ("MessageQ::insert_i"));
 
  }
}

} // namespace bpm
