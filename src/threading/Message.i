//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// Message::duplicate
// ============================================================================
INLINE_IMPL Message * Message::duplicate (void)
{
  return reinterpret_cast<Message*>(this->SharedObject::duplicate ());
}

// ============================================================================
// Message::release
// ============================================================================
INLINE_IMPL void Message::release (void)
{
  this->SharedObject::release ();
}
  
// ============================================================================
// Message::is_ctrl_message
// ============================================================================
INLINE_IMPL bool Message::is_ctrl_message (void)
{
  return this->type_ == TASK_INIT || this->type_ == TASK_EXIT;
}

// ============================================================================
// Message::to_string
// ============================================================================
INLINE_IMPL const char * Message::to_string (void) const
{
  switch (this->type_)
  {
    case CSPI_USER:
      return "CSPI_USER";
      break;
    case CSPI_DD_FPGA_OVERFLOW:
      return "CSPI_DD_FPGA_OVERFLOW";
      break;
    case CSPI_SA_FPGA_OVERFLOW:
      return "CSPI_SA_FPGA_OVERFLOW";
      break;
    case CSPI_SA_DRVR_OVERFLOW:
      return "CSPI_SA_DRVR_OVERFLOW";
      break;
    case CSPI_CONFIG_CHANGE:
      return "CSPI_CONFIG_CHANGE";
      break;
    case CSPI_SA:
      return "CSPI_SA";
      break;
    case CSPI_INTERLOCK:
      return "CSPI_INTERLOCK";
      break;
    case CSPI_PM:
      return "CSPI_PM";
      break;
    case CSPI_FA:
      return "CSPI_FA";
      break;
    case CSPI_TRIGGET:
      return "CSPI_TRIGGET";
      break;
    case CSPI_TRIGSET:
      return "CSPI_TRIGSET";
      break;
    case TASK_INIT:
      return "TASK_INIT";
      break;
    case TASK_TIMEOUT:
      return "TASK_TIMEOUT";
      break;
    case TASK_EXIT:
      return "TASK_EXIT";
      break;
  }
  return "UNKNOWN OR USER DEFINED MSG";
}
    
// ============================================================================
// Message::type
// ============================================================================
INLINE_IMPL size_t Message::type (void) const
{
  return this->type_;
}

// ============================================================================
// Message::priority
// ============================================================================
INLINE_IMPL size_t Message::priority (void) const
{
  return this->priority_;
}

// ============================================================================
// Message::user_data
// ============================================================================
INLINE_IMPL void * Message::user_data (void) const
{
  return this->user_data_;
}

// ============================================================================
// Message::user_data
// ============================================================================
INLINE_IMPL void Message::user_data (void* _ud)
{
  this->user_data_ = _ud;
}

// ============================================================================
// Message::cspi_data
// ============================================================================
INLINE_IMPL int Message::cspi_data (void) const
{
  return this->cspi_data_;
}

// ============================================================================
// Message::cspi_data
// ============================================================================
INLINE_IMPL void Message::cspi_data (int _cspi_data)
{
  this->cspi_data_ = _cspi_data;
}

// ============================================================================
// Message::waitable
// ============================================================================
INLINE_IMPL bool Message::waitable (void) const
{
  return this->cond_  ? true : false;
}

// ============================================================================
// Message::wait_processed
// ============================================================================
INLINE_IMPL bool Message::wait_processed (unsigned long _tmo_ms)
{
  bpm::AutoMutex<bpm::Mutex> guard (this->lock_);
  
  if (! this->waitable())
  {
	  Tango::Except::throw_exception(_CPTC("PROGRAMMING_ERROR"),
                                   _CPTC("Message::wait_processed called on a none waitable message [check code]"),
                                   _CPTC("Message::wait_processed"));
  }
  
  if (this->processed_)
    return true;
 
  return this->cond_->timed_wait(_tmo_ms);
}

// ============================================================================
// Message::processed
// ============================================================================
INLINE_IMPL void Message::processed (void)
{
  bpm::AutoMutex<bpm::Mutex> guard(this->lock_);

  this->processed_ = true;

  if (this->waitable())
    this->cond_->broadcast();
}

// ============================================================================
// Message::has_error
// ============================================================================
INLINE_IMPL bool Message::has_error (void) const
{
  return this->has_error_;
}

// ============================================================================
// Message::set_error
// ============================================================================
INLINE_IMPL void Message::set_error (const Tango::DevFailed & e)
{
  this->has_error_ = true;
  this->exception_ = e;
}

// ============================================================================
// Message::get_error
// ============================================================================
INLINE_IMPL const Tango::DevFailed & Message::get_error (void) const
{
  return this->exception_;
}

} //- namespace bpm
