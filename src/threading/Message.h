//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_MSG_H_
#define _BPM_MSG_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/Mutex.h"
#include "threading/Condition.h"
#include "threading/Semaphore.h"
#include "threading/SharedObject.h"
#include "threading/GenericContainer.h"

// ============================================================================
// CONSTs
// ============================================================================
#define HIGHEST_MSG_PRIORITY  0xFFFF
#define LOWEST_MSG_PRIORITY   0
#define INIT_MSG_PRIORITY     HIGHEST_MSG_PRIORITY
#define EXIT_MSG_PRIORITY     HIGHEST_MSG_PRIORITY
#define MAX_USER_PRIORITY     (HIGHEST_MSG_PRIORITY - 20)
#define DEFAULT_MSG_PRIORITY  LOWEST_MSG_PRIORITY
//-----------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// enum: MessageType
// ============================================================================
typedef enum
{
  //--------------------
  CSPI_USER = 0,
  CSPI_DD_FPGA_OVERFLOW,
  CSPI_SA_FPGA_OVERFLOW,
  CSPI_SA_DRVR_OVERFLOW,
  CSPI_CONFIG_CHANGE,
  CSPI_SA,
  CSPI_INTERLOCK,
  CSPI_PM,
  CSPI_FA,
  CSPI_TRIGGET,
  CSPI_TRIGSET,
  //--------------------
  TASK_INIT,
  TASK_TIMEOUT,
  TASK_PERIODIC,
  TASK_EXIT,
  //--------------------
  FIRST_USER_MSG = 1000
} MessageType;

// ============================================================================
//  struct: Message
// ============================================================================
class Message : private SharedObject
{
  friend class Task;
  friend class Thread;
  
public:

	//---------------------------------------------
	// Message::factory
	//---------------------------------------------
  static Message * allocate (size_t msg_type, 
                             size_t msg_priority = DEFAULT_MSG_PRIORITY,
                             bool waitable = false)
    throw (Tango::DevFailed);


  //---------------------------------------------
  // Message::ctor
  //---------------------------------------------
  explicit Message (CSPI_EVENT * evt, 
                    size_t msg_priority = DEFAULT_MSG_PRIORITY,
                    bool waitable = false);

  //---------------------------------------------
  // Message::ctor
  //---------------------------------------------
  explicit Message (size_t msg_type, 
                    size_t msg_priority = DEFAULT_MSG_PRIORITY,
                    bool waitable = false);

  //---------------------------------------------
  // Message::dtor
  //---------------------------------------------
  virtual ~Message ();

  //---------------------------------------------
  // Message::to_string
  //---------------------------------------------
  const char * to_string (void) const;

  //---------------------------------------------
  // Message::is_ctrl_message
  //---------------------------------------------
  bool is_ctrl_message (void);
  
  //---------------------------------------------
  // Message::duplicate
  //---------------------------------------------
  Message * duplicate (void);
  
  //---------------------------------------------
  // Message::release
  //---------------------------------------------
  void release (void);
  
	//---------------------------------------------
	// Message::type
	//---------------------------------------------
	size_t type (void) const;

	//---------------------------------------------
	// Message::type
	//---------------------------------------------
  size_t priority (void) const;
  
	//---------------------------------------------
	// Message::user_data
	//---------------------------------------------
  void * user_data (void) const;
  
	//---------------------------------------------
	// Message::cspi_data
	//---------------------------------------------
  void user_data (void * user_data);
  
	//---------------------------------------------
	// Message::cspi_data
	//---------------------------------------------
  int cspi_data (void) const;
  
	//---------------------------------------------
	// Message::cspi_data
	//---------------------------------------------
  void cspi_data (int _cspi_data);

  //---------------------------------------------
	// Message::attach_data - gets ownership
	//---------------------------------------------
  template <typename T> void attach_data (T * _data, bool _ownership = true)
    throw (Tango::DevFailed)
  {
    Container * md = new GenericContainer<T>(_data, _ownership);
    if (md == 0)
    {
     Tango::Except::throw_exception (_CPTC("OUT_OF_MEMORY"),
                                     _CPTC("MessageData allocation failed"),
                                     _CPTC("Message::attach_data"));
    }
    this->msg_data_ = md;
  }

  //---------------------------------------------
	// Message::attach_data - makes a copy
	//---------------------------------------------
  template <typename T> void attach_data (const T & _data)
    throw (Tango::DevFailed)
  {
    Container * md = new GenericContainer<T>(_data);
    if (md == 0)
    {
     Tango::Except::throw_exception (_CPTC("OUT_OF_MEMORY"),
                                     _CPTC("MessageData allocation failed"),
                                     _CPTC("Message::attach_data"));
    }
    this->msg_data_ = md;
  }
  
  //---------------------------------------------
  // Message::get_data
  //---------------------------------------------
  template <typename T> T& get_data () const
    throw (Tango::DevFailed)
  {
    GenericContainer<T> * c = 0;
    try
    {
      c = dynamic_cast<GenericContainer<T>*>(this->msg_data_);
      if (c == 0)
      {
        Tango::Except::throw_exception (_CPTC("RUNTIME_ERROR"),
                                        _CPTC("could not extract data from message [unexpected content]"),
                                        _CPTC("Message::msg_data"));
      }
    }
    catch(const std::bad_cast&)
    {
      Tango::Except::throw_exception (_CPTC("RUNTIME_ERROR"),
                                      _CPTC("could not extract data from message [unexpected content]"),
                                      _CPTC("Message::msg_data"));
    }
    return c->content();
  }
  
	//---------------------------------------------
	// Message::detach_data
	//---------------------------------------------
  template <typename T> void detach_data (T*& _data) const
    throw (Tango::DevFailed)
  {
    try
    {
    	GenericContainer<T> * c = dynamic_cast<GenericContainer<T>*>(this->msg_data_);
      if (c == 0)
  	  {
        Tango::Except::throw_exception (_CPTC("RUNTIME_ERROR"),
                                        _CPTC("could not extract data from message [unexpected content]"),
                                        _CPTC("Message::msg_data"));
  	  }
  	  _data = c->content(true);
    }
    catch(const std::bad_cast&)
    {
      Tango::Except::throw_exception (_CPTC("RUNTIME_ERROR"),
                                      _CPTC("could not extract data from message [unexpected content]"),
                                      _CPTC("Message::msg_data"));
    }
  }

	//---------------------------------------------
	// Message::make_waitable
	//--------------------------------------------
  void make_waitable (void)
    throw (Tango::DevFailed);
    
	//---------------------------------------------
	// Message::waitable
	//--------------------------------------------
  bool waitable (void) const;
  
  //---------------------------------------------
  // Message::wait_processed
  //---------------------------------------------
  bool wait_processed (unsigned long tmo_ms);
    
 	//---------------------------------------------
	// Message::processed
	//---------------------------------------------
  void processed (void); 
  
	//---------------------------------------------
	// Message::has_error
	//---------------------------------------------
  bool has_error (void) const;

	//---------------------------------------------
	// Message::set_error
	//---------------------------------------------
  void set_error (const Tango::DevFailed & e);

	//---------------------------------------------
	// Message::get_error
	//---------------------------------------------
  const Tango::DevFailed & get_error (void) const;
  
private:
  //- the notification type
  int type_;
  
	//- the msg priority
	size_t priority_;
	
  //- the associated user data (common to all messages)
  void * user_data_;
  
	//- the msg data (specific to a given message)
	Container * msg_data_;
	
	//- the cspi data (from cspi evt)
  int cspi_data_;
  
  //- condition variable (for waitable msgs)
	Condition * cond_;
	
  //- true if a thread is "waiting" for the message to be handled
  bool has_waiter_;
  
  //- true if an error occured during message handling
  bool has_error_;
  
  //- true if msg has been processed
  bool processed_;
  
  //- TANGO exception local storage
  Tango::DevFailed exception_;

  // = Disallow these operations.
  //--------------------------------------------
  Message & operator= (const Message &);
  Message (const Message &);
};

} // namespace bpm

#if defined (__INLINE_IMPL__)
# include "threading/Message.i"
#endif 

#endif // _BPM_MSG_H_
