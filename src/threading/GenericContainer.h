//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _GENERIC_CONTAINER_H_
#define _GENERIC_CONTAINER_H_

#include "CommonHeader.h"

namespace bpm 
{

// ============================================================================
//	class: Container
// ============================================================================
class Container
{
public:
  Container ()
  {
    //- noop
  };

  virtual ~Container () 
  {
    //- noop
  };
};

// ============================================================================
//	template class: GenericContainer - class T must have a copy ctor
// ============================================================================
template <typename T> 
class GenericContainer : public Container
{
public:

  //- ctor
  GenericContainer (T* _msg_data, bool _ownership = true) 
    : ptr_(_msg_data), own_(_ownership) 
  {
    //- noop
  }

  //- ctor
  GenericContainer (const T& _data) 
    : ptr_(0), own_(true)
  {
    try
    {
      this->ptr_ = new T(_data);
      if (this->ptr_ == 0)
        throw std::bad_alloc();
    }
    catch (const std::bad_alloc&)
    {
      this->ptr_ = 0;
      Tango::Except::throw_exception (_CPTC("OUT_OF_MEMORY"),
                                      _CPTC("memory allocation failed"),
                                      _CPTC("GenericContainer:GenericContainer"));
    }
  }

  //- dtor
  virtual ~GenericContainer ()
  {
    if (own_)
      delete this->ptr_;
  }

  //- returns content and optionaly transfers ownership to caller
  T * content (bool transfer_ownership)
  {
    T * tmp = this->ptr_;
    if (transfer_ownership)
    {
      this->own_ = false;
      this->ptr_ = 0;
    }
    return tmp;
  }

  //- returns content
  T & content ()
  {
    return  *(this->ptr_);
  }
  
private:
  //- actual container content
  T * ptr_;
  bool own_;
};


// ============================================================================
//	template function
// ============================================================================
template <typename T> T * extract_data (Container * _c, 
                                        bool _transfer_ownership = true)
    throw (Tango::DevFailed)
{
  GenericContainer<T> * gc = 0;

  try
  {
    gc = dynamic_cast<GenericContainer<T>*>(_c);
    if (gc == 0)
  	{
      Tango::Except::throw_exception (_CPTC("RUNTIME_ERROR"),
                                      _CPTC("could not extract data from Container [unexpected content]"),
                                      _CPTC("dettach_data"));
  	}
  }
  catch (const std::bad_cast&)
  {
    Tango::Except::throw_exception (_CPTC("RUNTIME_ERROR"),
                                    _CPTC("could not extract data from Container [unexpected content]"),
                                    _CPTC("dettach_data"));
  }

  return gc->content(_transfer_ownership);
}

} // namespace bpm

#endif // _GENERIC_CONTAINER_H_



