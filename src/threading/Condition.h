//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_CONDITION_H_
#define _BPM_CONDITION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/Implementation.h"

// ----------------------------------------------------------------------------
// Implementation-specific header file.
// ----------------------------------------------------------------------------
#if ! defined(__CONDITION_IMPLEMENTATION)
# error "implementation header file incomplete [no condition implementation]"
#endif

namespace bpm {

// ----------------------------------------------------------------------------
// FORWARD DECL
// ----------------------------------------------------------------------------
class Mutex;

// ----------------------------------------------------------------------------
//! \class Condition
//! \brief The BPM Condition variable class
//!
//! The Windows implementation is based on D.C.Schmidt & Al solution describes
//! in the following article: http://www.cs.wustl.edu/~schmidt/win32-cv-1.html
//!
//! Under Linux (and any other \c POSIX platforms), the code relies on the local
//! \c pthread implementation.
//!
//! \remarks
//! While its destructor is virtual, this class is not supposed to be derived.\n
//! Be sure to clearly understand the internal behaviour before trying to do so.
// ----------------------------------------------------------------------------
class Condition
{
public:
  //! Constructor.
  //!
  //! Each condition must be associated to a mutex that must be hold while
  //! evaluating the condition. It means that \a external_mutex must be locked
  //! prior to any to call to the Condition interface. See \link
  //! http://www.cs.wustl.edu/~schmidt/win32-cv-1.html D.C.Schmidt and I.Pyarali
  //! \endlink article for details.
  Condition (bpm::Mutex& external_mutex);

  //! Destructor.
  //!
  //! While this destructor is virtual, this class is not supposed to be derived.
  //! Be sure to understand the internal behaviour before trying to do so.
  virtual ~Condition (void);

  //! Wait until the condition is either \link Condition::signal signaled\endlink
  //! or \link Condition::broadcast broadcasted\endlink by another thread.
  //!
  //! The associated \a external_mutex <b>must be locked</b> by the calling thread.
  void wait (void);

  //! Wait for the condition to be \link Condition::signal signaled\endlink
  //! or \link Condition::broadcast broadcasted\endlink by another thread.
  //! Returns \c false in case the specified timeout expired before the condition 
  //! was notified. Returns \c true otherwise.
  //!
  //! The associated \a external_mutex <b>must be locked</b> by the calling thread.
  //!
  //! \param tmo_msecs The timeout in milliseconds
  //! \return \c false [timeout expired] or \c true [condition notified]
  bool timed_wait (unsigned long tmo_msecs);

  //! Signals the condition by notifying \b one of the waiting threads.
  //!
  //! The associated \a external_mutex <b>must be locked</b> by the calling thread.
  void signal (void);

  //! Broadcasts the condition by notifying \b all waiting threads.
  //!
  //! The associated \a external_mutex <b>must be locked</b> by the calling thread.
  void broadcast (void);

private:
  //! The so called "external mutex" (see D.Schmidt's article)
  Mutex & m_external_lock;
  
  //! Not implemented private member
  Condition (const Condition&);
  //! Not implemented private member
  Condition & operator= (const Condition&);
  
  //! hidden/abstract platform specific implementation
  __CONDITION_IMPLEMENTATION;
};

} // namespace bpm

#if defined (__INLINE_IMPL__)
#  include <threading/impl/PosixConditionImpl.i>
#endif

#endif //- _BPM_CONDITION_H_
