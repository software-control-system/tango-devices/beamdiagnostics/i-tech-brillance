//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// MessageQ::periodic_tmo_expired
// ============================================================================
INLINE_IMPL bool MessageQ::periodic_tmo_expired (double _tmo_msecs) const
{
  TIME_VAL now; 
  GET_TIME(now);
  return ELAPSED_MSEC(this->last_periodic_msg_ts_, now) >= (0.95 * _tmo_msecs);
}

// ============================================================================
// MessageQ::lo_wm
// ============================================================================
INLINE_IMPL void MessageQ::lo_wm (size_t _lo_wm)
{
  this->lo_wm_ = _lo_wm;  

  if (this->lo_wm_ < kMIN_LO_WATER_MARK)
    this->lo_wm_ = kMIN_LO_WATER_MARK;

  if ((this->hi_wm_ - this->lo_wm_) < kMIN_WATER_MARKS_DIFF)
    this->hi_wm_ = this->lo_wm_ + kMIN_WATER_MARKS_DIFF;
}

// ============================================================================
// MessageQ::lo_wm
// ============================================================================
INLINE_IMPL size_t MessageQ::lo_wm ()  const
{
  return this->lo_wm_;
}

// ============================================================================
// MessageQ::hi_wm
// ============================================================================
INLINE_IMPL void MessageQ::hi_wm (size_t _hi_wm)
{
  this->hi_wm_ = _hi_wm;

  if ((this->hi_wm_ - this->lo_wm_) < kMIN_WATER_MARKS_DIFF)
    this->hi_wm_ = this->lo_wm_ + kMIN_WATER_MARKS_DIFF;
}

// ============================================================================
// MessageQ::hi_wm
// ============================================================================
INLINE_IMPL size_t MessageQ::hi_wm ()  const
{
  return this->hi_wm_;
}

// ============================================================================
// MessageQ::throw_on_post_msg_timeout
// ============================================================================
INLINE_IMPL void MessageQ::throw_on_post_msg_timeout (bool _strategy)
{
  this->throw_on_post_msg_timeout_ = _strategy;
}

// ============================================================================
// MessageQ::clear
// ============================================================================
INLINE_IMPL void MessageQ::clear (void)
{
  bpm::AutoMutex<bpm::Mutex> guard(this->lock_);
  this->clear_i();
}

// ============================================================================
// MessageQ::enable_timeout_msg
// ============================================================================
INLINE_IMPL void MessageQ::enable_timeout_msg (bool b)
{
  this->enable_timeout_msg_ = b;
}

// ============================================================================
// MessageQ::timeout_msg_enabled
// ============================================================================
INLINE_IMPL bool MessageQ::timeout_msg_enabled (void) const
{
  return this->enable_timeout_msg_;
}

// ============================================================================
// MessageQ::enable_periodic_msg
// ============================================================================
INLINE_IMPL void MessageQ::enable_periodic_msg (bool b)
{
  this->enable_periodic_msg_ = b;
}

// ============================================================================
// MessageQ::periodic_msg_enabled
// ============================================================================
INLINE_IMPL bool MessageQ::periodic_msg_enabled (void) const
{
  return this->enable_periodic_msg_;
}

} //- namespace bpm
