//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_SEMAPHORE_H_
#define _BPM_SEMAPHORE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/Implementation.h"
#include "threading/Mutex.h"
#include "threading/Condition.h"

// ============================================================================
// Implementation-specific header file.
// ============================================================================
#if ! defined(__SEMAPHORE_IMPLEMENTATION)
# error "implementation header file incomplete [no semaphore implementation]"
#endif

namespace bpm {

// ============================================================================
//! <BPM_SEMAPHORE>::try_wait may return one of the following SemaphoreState
// ============================================================================
typedef enum
{
  //! semaphore is currently "decrementable"
  SEMAPHORE_DEC,
  //! no resource available (semaphore value is 0)
  SEMAPHORE_NO_RSC,
} SemaphoreState;

// ============================================================================
//! The BPM Semaphore class
// ============================================================================
class Semaphore
{
  //! This is the BPM Semaphore class.
  //!
  //! This class is not supposed to be derived.

public:
  //! Constructor (may throw an Exception)
  Semaphore (unsigned int initial = 1);

  //! Destructor.
  ~Semaphore (void);

  //! If semaphore value is > 0 then decrement it and carry on. 
  //! If it's already 0 then block untill the semaphore is either "signaled" 
  //! or "broascasted" (see post, signal and broadcast members below).
  void wait (void);

  //! If semaphore value is > 0 then decrements it and returns true. Returns 
  //! "false" in case the specified timeout expired before the semaphore
  //! has been "signaled" or "broascasted" by another thread.
  bool timed_wait (unsigned long tmo_msecs);

  //! If the current semaphore value is > 0, then crements it and returns 
  //! SEMAPHORE_DEC. In case the semaphore has reached its maximum value,
  //! this method does not block and "immediately" returns SEMAPHORE_NO_RSC.
  SemaphoreState try_wait (void);

  //! If any threads are blocked in wait(), wake one of them up. 
  //! Otherwise increments the value of the semaphore. 
  void post (void);

private:
  //! Not implemented private member
  Semaphore (const Semaphore&);
  //! Not implemented private member
  Semaphore & operator= (const Semaphore&);
  
  //- platform specific implementation
  __SEMAPHORE_IMPLEMENTATION;
};

// ============================================================================
//! The BPM "auto semaphore" class
// ============================================================================
class AutoSemaphore
{
  //! An "auto semaphore" providing an auto wait/post mechanism.

public:
  //! Constructor (wait on the associated Semaphore)
  AutoSemaphore (Semaphore & _sem)
    : m_sem (_sem)
  {
    m_sem.wait();
  }

  //! Destructor (post the associated Semaphore)
  ~AutoSemaphore (void)
  {
    m_sem.post();
  }

private:
  //! The associated Mutex
  Semaphore & m_sem;

  //! Not implemented private member
  AutoSemaphore (const AutoSemaphore&);
  //! Not implemented private member
  AutoSemaphore & operator= (const AutoSemaphore&);
};

} // namespace bpm

#if defined (__INLINE_IMPL__)
# include "threading/impl/PosixSemaphoreImpl.i"
#endif

#endif //- _BPM_SEMAPHORE_H_
