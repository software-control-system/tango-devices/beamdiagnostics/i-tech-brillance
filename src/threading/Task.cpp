//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr 
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"
#include "threading/Task.h"

#if !defined (__INLINE_IMPL__)
# include "threading/Task.i"
#endif 

namespace bpm
{

// ======================================================================
// Task::Config::Config
// ======================================================================
 Task::Config::Config ()
		: enable_timeout_msg (false),
      timeout_msg_period_ms (0),
      enable_periodic_msg (false),
      periodic_msg_period_ms (0),
      lock_msg_handling (false),
      lo_wm (kDEFAULT_LO_WATER_MARK),
      hi_wm (kDEFAULT_HI_WATER_MARK),
      throw_on_post_tmo (false),
      msg_handler (0),
      user_data (0),
      host_device (0)
{
  /* noop ctor */
}

// ======================================================================
// Task::Config::Config
// ======================================================================
 Task::Config::Config (Tango::DeviceImpl * _hd,
                       MessageHandler _msg_handler,
		                   Thread::IOArg _user_data,
                       bool   _enable_periodic_msg,
                       size_t _periodic_msg_period_ms,
		                   bool   _enable_timeout_msg,
                       size_t _timeout_msg_period_ms,
                       bool   _lock_msg_handling,
                       size_t _lo_wm,
                       size_t _hi_wm,
                       bool   _throw_on_post_tmo)
		: enable_timeout_msg (_enable_timeout_msg),
      timeout_msg_period_ms (_timeout_msg_period_ms),
      enable_periodic_msg (_enable_periodic_msg),
      periodic_msg_period_ms (_periodic_msg_period_ms),
      lock_msg_handling (_lock_msg_handling),
      lo_wm (_lo_wm),
      hi_wm (_hi_wm),
      throw_on_post_tmo (_throw_on_post_tmo),
      msg_handler (_msg_handler),
      user_data (_user_data),
      host_device (_hd)
{
  /* noop ctor */
}

// ======================================================================
// Task::Task
// ======================================================================
Task::Task (const Task::Config& _cfg)
  : Thread (_cfg.host_device),
    msg_q_ (_cfg.lo_wm, _cfg.hi_wm, _cfg.throw_on_post_tmo),
    timeout_msg_period_ms_ (_cfg.timeout_msg_period_ms),
    periodic_msg_period_ms_ (_cfg.periodic_msg_period_ms),
    user_data_ (_cfg.user_data),
    lock_msg_handling_ (_cfg.lock_msg_handling),
    henv_ (0),
    hcon_ (0),
    msg_handler_ (_cfg.msg_handler),
    connected_ (false)  
{
  this->msg_q_.enable_timeout_msg(_cfg.enable_timeout_msg);
  this->msg_q_.enable_periodic_msg(_cfg.periodic_msg_period_ms);
}

// ======================================================================
// Task::~Task
// ======================================================================
Task::~Task (void)
{
  //- noop
}
  
// ============================================================================
// Task::go
// ============================================================================
void Task::go (size_t _tmo_ms) 
  throw (Tango::DevFailed)
{
  this->start_undetached();

  Message * msg = 0;
  try
  {
    msg = bpm::Message::allocate(TASK_INIT, INIT_MSG_PRIORITY, true);
  }
  catch (Tango::DevFailed & ex)
  {
    Tango::Except::throw_exception (_CPTC("OUT_OF_MEMORY"),
                                    _CPTC("Message allocation failed"),
                                    _CPTC("Task::go"));
  }

  this->wait_msg_handled (msg, _tmo_ms);
}

// ============================================================================
// Task::go
// ============================================================================
void Task::go (Message * _msg, size_t _tmo_ms) 
  throw (Tango::DevFailed)
{
  this->start_undetached();

  if ( 
         (_msg == 0)
      || 
         (_msg->type() != TASK_INIT)
      ||
         (_msg->waitable() == false)
     )
     Tango::Except::throw_exception (_CPTC("PROGRAMMING_ERROR"),
                                     _CPTC("invalid INIT message [null, wrong type or not waitable]"),
                                     _CPTC("Task::go"));
                                      
  this->wait_msg_handled (_msg, _tmo_ms);
}

// ============================================================================
// Task::run_undetached
// ============================================================================
void * Task::run_undetached (void *)
{
	//- init flag - set to true when TASK_INIT received
	bool received_init_msg = false;

	//- exit flag - set to true when TASK_EXIT received
	bool received_exit_msg = false;

	size_t msg_type;
	Message * msg = 0;

  //- actual tmo on msg waiting
  size_t tmo = this->actual_timeout(); 

	//- trick: avoid handling TIMEOUT messages before INIT is itself handled
	//- may be the case for very short Task timeout
	do
	{
		//- release any previously obtained msg
		if (msg) msg->release();

    //- get/wait next message from the msgQ
		msg = this->msg_q_.next_message (tmo);
	}
	while (! msg || msg->type() != TASK_INIT);

  //- TASK_INIT received
  received_init_msg = true;
    
	//- enter thread's main loop 
	while (! received_exit_msg)
	{
    //- actual tmo on msg waiting
    tmo = this->actual_timeout();

		//- get/wait next message from the msgQ
		if (! msg)
		{ 
			do
			{
        //- get next message
				msg = this->msg_q_.next_message (tmo);
        //- do not handle TASK_INIT twice
        if (msg && msg->type() == TASK_INIT && received_init_msg)
        {
          msg->release();
          msg = 0;
        }
      }
			while (! msg);
		}

		//- set msg user data
		msg->user_data(this->user_data_);

		//- we may need msg type after msg release
		msg_type = msg->type();
	        
    //- got a valid message from message Q
		try
    {
      //      std::cout << "Task::handling msg::"
      //                << std::hex
      //			  				<< long(msg)
      //			  				<< std::dec
      //			  				<< "::"
      //			  				<< msg->to_string()
      //			  				<< std::endl;
			//- call message handler
      if (this->lock_msg_handling_)
		  { 
        //- enter critical section 
			  MutexLock guard (this->m_lock);
        (this->msg_handler_)(*msg);
			}
      else
      {
        this->msg_handler_(*msg);
      }
    }
		catch (const Tango::DevFailed& e)
		{
			//- store exception into the message
      msg->set_error(e);
		}
		catch (...)
		{
      Tango::DevErrorList errors;
		  errors.length(1);
      errors[0].severity = Tango::ERR;
      errors[0].reason = CORBA::string_dup("UNKNOWN_ERROR");
      errors[0].origin = CORBA::string_dup("unknown error caught while handling msg");
      errors[0].desc = CORBA::string_dup("Task::run_undetached");
      msg->set_error(Tango::DevFailed(errors));
		}
    //	  std::cout << "Task::run_undetached::msg [" 
    //    	        << std::hex 
    //    	        << (void*)msg 
    //    	        << std::dec 
    //    	        << "] handled - notifying waiters"
    //    	        << std::endl;
    //- mark message as "processed" (this will signal waiters if any)
    msg->processed();
		//- release our msg ref 
		msg->release();
    //- abort requested?
    if (msg_type == TASK_EXIT)
    {
	    //- close the msgQ
	    this->msg_q_.close();
	    //- mark TASK_EXIT as received (exit thread's main loop)
	    received_exit_msg = true;
    }
		//- reset msg in order to get next msg from msgQ
		msg = 0;
  } //- thread's while loop

	return 0;
}

// ======================================================================
// Task::exit
// ======================================================================
void Task::exit (void)
  throw (Tango::DevFailed)
{
  //- we may have to implicitly delete the thread
  bool delete_self = false;

  //- enter critical section
  this->m_lock.lock();

  //- get underlying thread state
  Thread::State ts = this->state_i();

	//- if the thread is running, then ask it to exit
  if (ts == bpm::Thread::STATE_RUNNING)
	{
    bpm::Message * msg = 0;
    try
    {
      msg = new Message(bpm::TASK_EXIT, EXIT_MSG_PRIORITY, true);
    }
    catch (Tango::DevFailed &ex)
    {
      this->m_lock.unlock();
      Tango::Except::re_throw_exception (ex,
                                         _CPTC("SOFTWARE_ERROR"),
                                         _CPTC("Could not stop task [bpm::Message allocation failed]"),
                                         _CPTC("Task::exit"));
	  }
    catch (...)
    {
      this->m_lock.unlock();
      Tango::Except::throw_exception (_CPTC("UNKNOWN_ERROR"),
                                      _CPTC("Could not stop task [bpm::Message allocation failed]"),
                                      _CPTC("Task::exit"));
	  }
    //- unlock the thread lock (avoid deadlock during message handling)
    this->m_lock.unlock();
    try
    {
		  //- ... then wait for TASK_EXIT msg to be handled
		  //- TODO: change kINFINITE_WAIT to a more flexible TIMEOUT
		  // std::cout << "Task::exit - waiting for the TASK_EXIT msg to be handled" << std::endl;
		  this->wait_msg_handled (msg, kINFINITE_WAIT);
    }
    catch (...)
    {
      //- ignore any error
    }
		//- wait for the thread to actually quit
		try
    {
      Thread::IOArg dummy = 0;
			// std::cout << "Task::exit - about to join with the underlying thread" << std::endl;
		  this->join (&dummy);
    }
		catch (...)
    {
		 //- ignore any error
    }
  }
  else if (ts == bpm::Thread::STATE_NEW) 
  {
    //- delete the thread (instanciated but never been started)
	  // std::cout << "Task::exit - about to delete the thread [has never been started]" << std::endl;
		delete_self = true;
    //- leave critical section
    this->m_lock.unlock();
  }
	else
  {
    //- nothing to do...
	  // std::cout << "Task::exit - do nothing" << std::endl;
    //- leave critical section
    this->m_lock.unlock();
  }

  //- delete (if required)
  if (delete_self)
  {
    // std::cout << "Task::exit - deleting <this> Task instance" << std::endl;
    delete this;
  }
}

// ======================================================================
// Task::wait_msg_handled
// ======================================================================
void Task::wait_msg_handled (Message * _msg, size_t _tmo_ms)
		 throw (Tango::DevFailed)
{
	//- check input
	if (! _msg || ! _msg->waitable())	
  {
    Tango::Except::throw_exception (_CPTC("INVALID_ARGUMENT"),
                                    _CPTC("invalid message [either null or not waitable]"),
                                    _CPTC("Task::wait_msg_handled"));
	}

	try
	{
		//- post a shallow copy of the msg 
		this->msg_q_.post(_msg->duplicate());
	}
	catch (...)
	{
		_msg->release();
    Tango::Except::throw_exception (_CPTC("INTERNAL_ERROR"),
                                    _CPTC("message could not be posted"),
                                    _CPTC("Task::wait_msg_handled"));
	}
	
  //	std::cout << "Task::wait_msg_handled::waiting for msg [" 
  //	        << std::hex 
  //	        << (void*)_msg 
  //	        << std::dec 
  //	        << "] to be handled"
  //	        << std::endl;
	
	//- wait for the msg to be handled or tmo expiration
	if (_msg->wait_processed(_tmo_ms))
	{
    //	  std::cout << "Task::wait_msg_handled::msg [" 
    //	          << std::hex 
    //	          << (void*)_msg 
    //	          << std::dec 
    //	          << "] handled [gave error::" 
    //	          << (_msg->has_error() ? "yes" : "no")
    //	          << "]"
    //	          << std::endl;
    Tango::DevFailed msg_exception;
    bool msg_gave_error = _msg->has_error();
    //- to store error localy before releasing msg
	  if (msg_gave_error)
      msg_exception = _msg->get_error();
	  //- release msg
		_msg->release();
		//- throw an exception if msg gave error
		if (msg_gave_error)
	    throw msg_exception;
		//- msg did not gave error, just return
		return;
	}

	//- too bad, timeout expired...
  //  std::cout << "Task::wait_msg_handled::timeout expired while waiting for msg [" 
  //	        << std::hex 
  //	        << (void*)_msg 
  //	        << std::dec 
  //	        << "] to be handled"
  //	        << std::endl;
	          
	//- release msg
	_msg->release();

	//- throw timeout exception
  Tango::Except::throw_exception (_CPTC ("TIMEOUT_EXPIRED"),
                                  _CPTC ("timeout expired while waiting for message to be handled"),
                                  _CPTC ("Task::wait_msg_handled"));
}

} // namespace
