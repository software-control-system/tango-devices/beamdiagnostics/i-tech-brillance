//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_TASK_H_
#define _BPM_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/Thread.h"
#include "threading/MessageQ.h"

// ============================================================================
// CONSTs
// ============================================================================
#define kDEFAULT_TASK_TMO_MSECS         5000
#define kDEFAULT_THD_PERIODIC_TMO_MSECS 1000
//-----------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// class: Task
// ============================================================================
class Task : public bpm::Thread
{
  friend class BPM;
  
public:

  //-define what a msg handler is
  typedef void (*MessageHandler) (const bpm::Message&);
  
  //! yat::Task configuration class
	class Config
	{
	public:
    //- enable TIMEOUT messages
		bool enable_timeout_msg;
		//- timeout msg period in msec
		size_t timeout_msg_period_ms;
    //- enable PERIODIC messages
		bool enable_periodic_msg;
		//- periodic msg period in msec
		size_t periodic_msg_period_ms;
		//- should we process msg under critical section?
		bool lock_msg_handling;
    //- msgQ low water mark
    size_t lo_wm;
    //- msgQ high water mark
    size_t hi_wm;
    //- throw exception on post message timeout
    bool throw_on_post_tmo;
    //- msg handler
    MessageHandler msg_handler;
		//- user data (passed back in all msg)
    Thread::IOArg user_data;
    //- host TANGO device
    Tango::DeviceImpl * host_device;
    
    //- default ctor
		Config ();
		
    //- ctor
		Config (Tango::DeviceImpl * hd, 
		        MessageHandler msg_handler,
		        Thread::IOArg user_data = 0,
            bool   enable_periodic_msg = false,
            size_t periodic_msg_period_ms = 0,
		        bool   enable_timeout_msg = false,
            size_t timeout_msg_period_ms = 0,
            bool   lock_msg_handling = false,
            size_t lo_wm = kDEFAULT_LO_WATER_MARK,
            size_t hi_wm = kDEFAULT_HI_WATER_MARK,
            bool   throw_on_post_tmo = true);
	};

  //- ctor
	Task (const Config& cfg);

	//- dtor
	virtual ~Task (void);

	//- starts the task
  void go (size_t _tmo_ms = kDEFAULT_MSG_TMO_MSECS)
    throw (Tango::DevFailed);

 	//- starts the task 
  //- an exception is thrown otherwise in case the specified message:
  //-   * is not of type TASK_INIT
  //-   * is not "waitable"
  void go (Message * msg, size_t _tmo_ms = kDEFAULT_MSG_TMO_MSECS)
    throw (Tango::DevFailed);

	//! Abort the task (join with the underlying thread before returning).
  //! Provides an implementation to the Thread::exit pure virtual method.
  virtual void exit (void)
    throw (Tango::DevFailed);

  //- posts a message to the task
	void post (Message * msg, size_t tmo_msecs = kDEFAULT_POST_MSG_TMO)
		throw (Tango::DevFailed);

  //- post a CSPI event to the task
  void post (CSPI_EVENT * evt, size_t tmo_msecs = kDEFAULT_POST_MSG_TMO)
    throw (Tango::DevFailed);
    
	//- wait for a msg to be handled
	void wait_msg_handled (Message * msg, size_t tmo_ms = kDEFAULT_MSG_TMO_MSECS)
		throw (Tango::DevFailed);

	//- timeout msg period mutator
	void set_timeout_msg_period (size_t p_msecs);
	
	//- periodic msg period accessor
	size_t get_timeout_msg_period (void) const;
	
	//- enable/disable timeout messages
	void enable_timeout_msg (bool enable);

	//- returns timeout messages handling status
	bool timeout_msg_enabled (void) const;

	//- periodic msg period mutator
	void set_periodic_msg_period (size_t p_msecs);
	
	//- periodic msg period accessor
	size_t get_periodic_msg_period (void) const;

  //- enable/disable periodic messages
	void enable_periodic_msg (bool enable);

	//- returns period messages handling status
	bool periodic_msg_enabled (void) const;

protected:
	//- run_undetached
  virtual Thread::IOArg run_undetached (Thread::IOArg);

private:
	//- actual_timeout
  size_t actual_timeout (void) const;

	//- the associated messageQ
	MessageQ msg_q_;

	//- timeout msg period
	size_t timeout_msg_period_ms_;

	//- periodic msg period 
  size_t periodic_msg_period_ms_;

	//- user data 
	Thread::IOArg user_data_;

  //- should we process msg under critical section?
  bool lock_msg_handling_;

  //- the CSPI env. handle
  CSPIHENV henv_;

  //- the CSPI con. handle
  CSPIHCON hcon_;
  
  //- the message handler entry point
  MessageHandler msg_handler_;
  
  //- connection flag: true if connected to the Libera, false ortherwise 
  bool connected_;
};

} // namespace 

#if defined (__INLINE_IMPL__)
# include <threading/Task.i>
#endif

#endif // _BPM_TASK_H_
