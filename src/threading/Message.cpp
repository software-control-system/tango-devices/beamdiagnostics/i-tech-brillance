//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include "CommonHeader.h"
#include "threading/Message.h"

#if !defined (__INLINE_IMPL__)
# include "threading/Message.i"
#endif // __INLINE_IMPL__

namespace bpm
{

// ============================================================================
// Message::allocate 
// ============================================================================
Message * Message::allocate (size_t _msg_type, size_t _msg_priority, bool _waitable)
  throw (Tango::DevFailed)
{
  bpm::Message * msg = 0;
     
  try
  {
    msg = new bpm::Message (_msg_type, _msg_priority, _waitable);
  	if (msg == 0)
      throw std::bad_alloc();
  }
  catch (const std::bad_alloc&)
  {
    Tango::Except::throw_exception(_CPTC("OUT_OF_MEMORY"),
                                   _CPTC("Message allocation failed"),
                                   _CPTC("Message::allocate"));
	}
  catch (...)
  {
    Tango::Except::throw_exception(_CPTC("UNKNOWN_ERROR"),
                                   _CPTC("Message allocation failed [unknown exception caught]"),
                                   _CPTC("Message::allocate"));
	}

  return msg;
}

// ============================================================================
// Message::Message
// ============================================================================
Message::Message (CSPI_EVENT * e, size_t p, bool w)
  : SharedObject (),
    type_ (CSPI_USER),
    priority_ (p),
    user_data_ (e->user_data),
    msg_data_ (0),
    cspi_data_ (e->hdr.param),
    cond_ (0),
    has_waiter_ (false),
    has_error_ (false)
{
  switch (e->hdr.id)
  {
    case CSPI_EVENT_USER:
      this->type_ = CSPI_USER;
      break;
    case CSPI_EVENT_OVERFLOW:
      switch (e->hdr.param)
      {
      case CSPI_OVERFLOW_DD_FPGA:
        this->type_ = CSPI_DD_FPGA_OVERFLOW;
        break;
      case CSPI_OVERFLOW_SA_FPGA:
        this->type_ = CSPI_SA_FPGA_OVERFLOW;
        break;
      case CSPI_OVERFLOW_SA_DRV:
        this->type_ = CSPI_SA_DRVR_OVERFLOW;
        break;
      }
      break;
    case CSPI_EVENT_CFG:
      this->type_ = CSPI_CONFIG_CHANGE;
      break;
    case CSPI_EVENT_SA:
      this->type_ = CSPI_SA;
      break;
    case CSPI_EVENT_INTERLOCK:
      this->type_ = CSPI_INTERLOCK;
      break;
    case CSPI_EVENT_PM:
      this->type_ = CSPI_PM;
      break;
    case CSPI_EVENT_FA:
      this->type_ = CSPI_FA;
      break;
    case CSPI_EVENT_TRIGGET:
      this->type_ = CSPI_TRIGGET;
      break;
    case CSPI_EVENT_TRIGSET:
      this->type_ = CSPI_TRIGSET;
      break;
  }
  
  if (w)
    this->make_waitable();
}

// ============================================================================
// Message::Message
// ============================================================================
Message::Message (size_t t, size_t p, bool _w)
  : SharedObject (),
    type_ (t),
    priority_ (p),
    user_data_ (0), 
    msg_data_ (0),
    cspi_data_ (0),
    cond_ (0),
    has_waiter_ (false),
    has_error_ (false),
    processed_ (false)
{
  if (_w)
    this->make_waitable();
}

// ============================================================================
// Message::~Message
// ============================================================================
Message::~Message ()
{
  //-note: exception_ contains some Tango::DevError which itself 
  //-note: contains CORBA::string_member which releases the 
  //-note: associated memory (i.e. char*)

	if (this->msg_data_)
  {
		delete this->msg_data_;
    this->msg_data_ = 0;
  }
  
  DEBUG_ASSERT(this->processed_ ? true : (this->has_waiter_ ? false : true));
  
	if (this->cond_)
  {
    if (! this->processed_)
    {
      AutoMutex<Mutex> guard(this->lock_);
      this->cond_->broadcast();
    }
		delete this->cond_;
  }
}

// ============================================================================
// Message::make_waitable 
// ============================================================================
void Message::make_waitable (void)
  throw (Tango::DevFailed)
{ 
  if (this->cond_)
    return;

  try
  {
    this->cond_ = new Condition(this->lock_);
    if (this->cond_ == 0)
      throw std::bad_alloc();
  }
  catch (const std::bad_alloc&)
  {
    Tango::Except::throw_exception(_CPTC("MEMORY_ERROR"),
                                   _CPTC("memory allocation failed"),
                                   _CPTC("Message::make_waitable"));
  }
  catch (...)
  {
    Tango::Except::throw_exception(_CPTC("UNKNOWN_ERROR"),
                                   _CPTC("memory allocation failed"),
                                   _CPTC("Message::make_waitable"));
  }
}

} // namespace bpm
