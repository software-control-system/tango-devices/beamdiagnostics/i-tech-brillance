//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_MSGQ_H_
#define _BPM_MSGQ_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <list>
#include "CommonHeader.h"
#include "threading/Message.h"

// ============================================================================
// CONSTs
// ============================================================================
#define kDEFAULT_THD_TMO_MSECS 1000
#if defined(kDEFAULT_MSG_TMO_MSECS)
# undef kDEFAULT_MSG_TMO_MSECS
#endif
#define kDEFAULT_MSG_TMO_MSECS 2500
//-----------------------------------------------------------------------------
#define kDEFAULT_POST_MSG_TMO   1000
#define kDEFAULT_LO_WATER_MARK  256
#define kDEFAULT_HI_WATER_MARK  512
#define kMIN_LO_WATER_MARK      kDEFAULT_LO_WATER_MARK
#define kMIN_WATER_MARKS_DIFF   kDEFAULT_LO_WATER_MARK
//-----------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// class: MessageQ
// ============================================================================
class MessageQ
{
  friend class Task;

  typedef std::list<bpm::Message *> MessageQImpl;

public:

  //- MessageQ has a state
  typedef enum
  {
    OPEN,
    CLOSED
  } State;

  //- post a bpm::Message into the msgQ
  void post (bpm::Message * msg, size_t tmo_msecs = kDEFAULT_POST_MSG_TMO) 
    throw (Tango::DevFailed);

  //- post a cspi event into the msgQ
  void post (CSPI_EVENT * evt, size_t tmo_msecs = kDEFAULT_POST_MSG_TMO) 
    throw (Tango::DevFailed);

  //- extract next message from the msgQ
  bpm::Message * next_message (size_t tmo_msecs);
  
  //- low water mark mutator
  void lo_wm (size_t _lo_wm);

  //- low water mark accessor
  size_t lo_wm () const;

  //- high water mark mutator
  void hi_wm (size_t _hi_wm);

  //- high water mark accessor
  size_t hi_wm () const;

  //- should the msgQ throw an exception on post msg tmo expiration?
  void throw_on_post_msg_timeout (bool _strategy);
  
  //- clear msgQ content 
	void clear();
	
  //- close the msqQ
  void close (void);

  //- enable/disable timeout msg 
  void enable_timeout_msg (bool b);
  bool timeout_msg_enabled () const;
  
  //- enable/disable handling flag
  void enable_periodic_msg (bool b);
  bool periodic_msg_enabled () const;
  
private:
  //- private ctor
  MessageQ (size_t lo_wm = kDEFAULT_LO_WATER_MARK,
            size_t hi_wm = kDEFAULT_HI_WATER_MARK,
            bool throw_on_post_tmo = false);
            
  //- private ctor
  virtual ~MessageQ ();
            
  //- check the periodic msg timeout
  bool periodic_tmo_expired (double tmo_msecs) const;

  //- clears msgQ content (returns num of trashed messages)
	size_t clear_i();

  //- waits for the msQ to contain at least one msg
  //- returns false if tmo expired, true otherwise.
  bool wait_not_empty_i (size_t tmo_msecs);

  //- waits for the msQ to have room for new messages
  //- returns false if tmo expired, true otherwise.
  bool wait_not_full_i (size_t tmo_msecs);

  //- insert a msg according to its priority
  void insert_i (Message * msg)
    throw (Tango::DevFailed);

	//- use a std::deque to implement msgQ
	MessageQImpl msg_q_;

	//- sync. object in order to make the msgQ thread safe
	bpm::Mutex lock_;

	//- Producer(s) synch object
	bpm::Condition msg_producer_sync_;

	//- Consumer synch object
	bpm::Condition msg_consumer_sync_;
	
	//- state
	MessageQ::State state_;

  //- timeout msg handling flag
  bool enable_timeout_msg_;

  //- periodic msg handling flag
  bool enable_periodic_msg_;

  //- last returned PERIODIC msg timestamp
  TIME_VAL last_periodic_msg_ts_;

  //- low water marks
  size_t lo_wm_;

  //- high water marks
  size_t hi_wm_;

  //- msqQ saturation flag
  bool saturated_;

  //- expection activation flag
  bool throw_on_post_msg_timeout_;
  
  //- flag indicating whether or not the last returned msg was a periodoc msg
  //- we use this flag in order to avoid PERIODIC event flooding in case
  //- the PERIODIC event frequency is really high - which could prevent other
  //- messages from being handled. reciprocally, a very high msg posting freq.
  //- could prevent the PERIODIC msg from being handled. the following tries
  //- to ensure that any msg is "finally" handled.
  bool last_returned_msg_periodic_;
  
  // = Disallow these operations.
  //--------------------------------------------
  MessageQ & operator= (const MessageQ &);
  MessageQ (const MessageQ &);
};

} // namespace bpm

#if defined (__INLINE_IMPL__)
# include "threading/MessageQ.i"
#endif // __INLINE_IMPL__

#endif // _BPM_MSGQ_H_
