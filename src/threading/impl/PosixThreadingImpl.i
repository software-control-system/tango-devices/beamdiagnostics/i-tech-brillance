//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm {

// ****************************************************************************
// YAT DUMMY_MUTEX IMPL
// ****************************************************************************
// ----------------------------------------------------------------------------
// DummyMutex::lock
// ----------------------------------------------------------------------------
INLINE_IMPL void DummyMutex::lock (void)
{
 //- noop
}
// ----------------------------------------------------------------------------
// DummyMutex::acquire
// ----------------------------------------------------------------------------
INLINE_IMPL void DummyMutex::acquire (void)
{
 //- noop
}
// ----------------------------------------------------------------------------
// DummyMutex::unlock
// ----------------------------------------------------------------------------
INLINE_IMPL void DummyMutex::unlock (void)
{
 //- noop
}
// ----------------------------------------------------------------------------
// DummyMutex::release
// ----------------------------------------------------------------------------
INLINE_IMPL void DummyMutex::release (void)
{
 //- noop
}
// ----------------------------------------------------------------------------
// DummyMutex::try_lock
// ----------------------------------------------------------------------------
INLINE_IMPL MutexState DummyMutex::try_lock (void)
{
  return bpm::MUTEX_LOCKED;
}
// ----------------------------------------------------------------------------
// DummyMutex::try_acquire
// ----------------------------------------------------------------------------
INLINE_IMPL MutexState DummyMutex::try_acquire (void)
{
  return bpm::MUTEX_LOCKED;
}

// ****************************************************************************
// YAT MUTEX IMPL
// ****************************************************************************
// ----------------------------------------------------------------------------
// Mutex::lock
// ----------------------------------------------------------------------------
INLINE_IMPL void Mutex::lock (void)
{
#error no impl
}
// ----------------------------------------------------------------------------
// Mutex::acquire
// ----------------------------------------------------------------------------
INLINE_IMPL void Mutex::acquire (void)
{
  this->lock();
}
// ----------------------------------------------------------------------------
// Mutex::try_acquire
// ----------------------------------------------------------------------------
MutexState Mutex::try_acquire (void)
{
  return this->try_lock();
}
// ----------------------------------------------------------------------------
// Mutex::unlock
// ----------------------------------------------------------------------------
INLINE_IMPL void Mutex::unlock (void)
{
#error no impl
}
// ----------------------------------------------------------------------------
// Mutex::acquire
// ----------------------------------------------------------------------------
INLINE_IMPL void Mutex::release (void)
{
  this->unlock();
}

// ****************************************************************************
// YAT THREAD IMPL
// ****************************************************************************
// ----------------------------------------------------------------------------
// Thread::priority
// ----------------------------------------------------------------------------
INLINE_IMPL Priority Thread::priority (void) const
{
  //- enter critical section
  bpm::SmartMutex guard(this->m_lock);

  return this->m_priority;
}
// ----------------------------------------------------------------------------
// Thread::state
// ----------------------------------------------------------------------------
INLINE_IMPL State Thread::state (void) const
{
  //- enter critical section
  bpm::SmartMutex guard(this->m_lock);

  return this->m_state;
}
// ----------------------------------------------------------------------------
// Thread::yield
// ----------------------------------------------------------------------------
INLINE_IMPL void Thread::yield (void)
{
#error no impl
}
// ----------------------------------------------------------------------------
// Thread::sleep
// ----------------------------------------------------------------------------
INLINE_IMPL void Thread::sleep (unsigned long _tmo_msecs)
{
  ThreadingUtilities::sleep(0, 1000000 * _tmo_msecs);
}
// ----------------------------------------------------------------------------
// Thread::self
// ----------------------------------------------------------------------------
INLINE_IMPL ThreadUID Thread::self (void) const
{
  return this->m_uid;
}
// ----------------------------------------------------------------------------
// Thread::lock
// ----------------------------------------------------------------------------
INLINE_IMPL Thread::Mutex & lock (void) const
{
  return this->m_lock;
}

} // namespace bpm
