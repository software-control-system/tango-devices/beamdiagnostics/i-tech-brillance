//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _POSIX_THREADING_IMPL_
#define _POSIX_THREADING_IMPL_

//- no nano sleep!
//- #define HAS_NANO_SLEEP

//- no thread priority!
//- #define HAS_THREAD_PRIORITY

// ----------------------------------------------------------------------------
// MUTEX - MUTEX - MUTEX - MUTEX - MUTEX - MUTEX - MUTEX - MUTEX - MUTEX
// ----------------------------------------------------------------------------
#define __MUTEX_IMPLEMENTATION \
  pthread_mutex_t m_posix_mux; \
  friend class Condition;

// ----------------------------------------------------------------------------
// CONDITION - CONDITION - CONDITION - CONDITION - CONDITION - CONDITION
// ----------------------------------------------------------------------------
#define __CONDITION_IMPLEMENTATION \
  pthread_cond_t m_posix_cond;

// ----------------------------------------------------------------------------
// SEMAPHORE - SEMAPHORE - SEMAPHORE - SEMAPHORE - SEMAPHORE - SEMAPHORE
// ----------------------------------------------------------------------------
#define __SEMAPHORE_IMPLEMENTATION \
  Mutex m_mux; \
  Condition m_cond; \
  int m_value;

// ----------------------------------------------------------------------------
// THREAD - THREAD - THREAD - THREAD - THREAD - THREAD - THREAD - THREAD
// ----------------------------------------------------------------------------
//- common thread entry point (non-OO OS interface to interface)
#define THREAD_COMMON_ENTRY_POINT \
  void * bpm_thread_common_entry_point (void *)

extern "C" THREAD_COMMON_ENTRY_POINT;

#define __THREAD_IMPLEMENTATION \
  pthread_t m_posix_thread; \
  void spawn (void) throw (Tango::DevFailed); \
  static int bpm_to_posix_priority (Priority); \
  friend THREAD_COMMON_ENTRY_POINT;

#endif //- _POSIX_THREADING_IMPL_
