//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm {

// ----------------------------------------------------------------------------
// Condition::wait
// ----------------------------------------------------------------------------
INLINE_IMPL void Condition::wait (void)
{
  this->timed_wait(0);
}

// ----------------------------------------------------------------------------
// Condition::signal
// ----------------------------------------------------------------------------
INLINE_IMPL void Condition::signal (void)
{
  ::pthread_cond_signal(&m_posix_cond);
}

// ----------------------------------------------------------------------------
// Condition::broadcast
// ----------------------------------------------------------------------------
INLINE_IMPL void Condition::broadcast (void)
{
  ::pthread_cond_broadcast(&m_posix_cond);
}

} // namespace bpm
