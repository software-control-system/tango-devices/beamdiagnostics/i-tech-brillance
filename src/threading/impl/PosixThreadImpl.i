//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm {

// ----------------------------------------------------------------------------
// Thread::priority
// ----------------------------------------------------------------------------
INLINE_IMPL Thread::Priority Thread::priority (void)
{
  //- enter critical section
  bpm::MutexLock guard(this->m_lock);

  return this->m_priority;
}
// ----------------------------------------------------------------------------
// Thread::state
// ----------------------------------------------------------------------------
INLINE_IMPL Thread::State Thread::state (void)
{
  //- enter critical section
  bpm::MutexLock guard(this->m_lock);

  return this->m_state;
}
// ----------------------------------------------------------------------------
// Thread::state
// ----------------------------------------------------------------------------
INLINE_IMPL Thread::State Thread::state_i (void) const
{
  return this->m_state;
}
// ----------------------------------------------------------------------------
// Thread::yield
// ----------------------------------------------------------------------------
INLINE_IMPL void Thread::yield (void)
{
  ::sched_yield();
}
// ----------------------------------------------------------------------------
// Thread::sleep
// ----------------------------------------------------------------------------
INLINE_IMPL void Thread::sleep (unsigned long _msecs)
{
#define kNSECS_PER_SEC  1000000000
#define kNSECS_PER_MSEC 1000000

  unsigned long secs = 0;
  unsigned long nanosecs = kNSECS_PER_MSEC * _msecs;

	while (nanosecs >= kNSECS_PER_SEC)
	{
		secs += 1;
		nanosecs -= kNSECS_PER_SEC;
	}

  ThreadingUtilities::sleep(secs, nanosecs);

#undef kNSECS_PER_MSEC
#undef kNSECS_PER_SEC
}
// ----------------------------------------------------------------------------
// Thread::self
// ----------------------------------------------------------------------------
INLINE_IMPL ThreadUID Thread::self (void) const
{
  return this->m_uid;
}

// ----------------------------------------------------------------------------
// Thread::lock
// ----------------------------------------------------------------------------
INLINE_IMPL Mutex & Thread::lock (void)
{
  return this->m_lock;
}
  
} // namespace bpm
