//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <errno.h>
#include <sys/time.h>
#include "CommonHeader.h"
#include "threading/Utilities.h"
#include "threading/Mutex.h"
#include "threading/Condition.h"
#include "threading/Semaphore.h"
#include "threading/Thread.h"

#if ! defined (__INLINE_IMPL__)
# include "threading/impl/PosixMutexImpl.i"
# include "threading/impl/PosixConditionImpl.i"
# include "threading/impl/PosixSemaphoreImpl.i"
# include "threading/impl/PosixThreadImpl.i"
#endif

// ============================================================================
// SOME PSEUDO CONSTs
// ============================================================================
#define MAX_SLEEP_SECONDS (long)4294966	//- this is (2^32 - 2) / 1000 
#define NANOSECS_PER_SEC  1000000000L
#define ONE_SEC_IN_MSECS  1000L

// ============================================================================
// PLATFORM SPECIFIC THREAD PRIORITIES
// ============================================================================
#if defined(HAS_THREAD_PRIORITY)
 static int lowest_priority;
 static int normal_priority;
 static int highest_priority;
#endif

namespace bpm {

// ****************************************************************************
// BPM DUMMY_MUTEX IMPL
// ****************************************************************************
// ============================================================================
// NullMutex::NullMutex
// ============================================================================
NullMutex::NullMutex (void)
{
 //- noop
}
// ============================================================================
// NullMutex::~NullMutex
// ============================================================================
NullMutex::~NullMutex (void)
{
 //- noop
}

// ****************************************************************************
// BPM MUTEX IMPL
// ****************************************************************************
// ============================================================================
// Mutex::Mutex
// ============================================================================
Mutex::Mutex (void)
  : m_posix_mux ()
{
  pthread_mutexattr_t ma;
  ::pthread_mutexattr_init(&ma);
  ::pthread_mutexattr_settype(&ma, PTHREAD_MUTEX_RECURSIVE);
  ::pthread_mutex_init(&m_posix_mux, &ma);
  ::pthread_mutexattr_destroy(&ma);
}

// ============================================================================
// Mutex::~Mutex
// ============================================================================
Mutex::~Mutex(void)
{
  ::pthread_mutex_destroy(&m_posix_mux);
}

// ****************************************************************************
// BPM SEMAPHORE IMPL
// ****************************************************************************
#define SEMAPHORE_MAX_COUNT 0x7fffffff

// ============================================================================
// Semaphore::Semaphore
// ============================================================================
Semaphore::Semaphore (unsigned int _initial_value)
  : m_mux () , m_cond (m_mux), m_value (_initial_value)
{
  //- noop
}

// ============================================================================
// Semaphore::~Semaphore
// ============================================================================
Semaphore::~Semaphore(void)
{
  //- noop
}

// ****************************************************************************
// BPM CONDITION IMPL
// ****************************************************************************
// ============================================================================
// Condition::Condition
// ============================================================================
Condition::Condition (Mutex & external_lock)
 : m_external_lock (external_lock),
   m_posix_cond ()
{
  ::pthread_cond_init(&m_posix_cond, 0);
}

// ============================================================================
// Condition::~Condition
// ============================================================================
Condition::~Condition (void)
{
  ::pthread_cond_destroy(&m_posix_cond);
}

// ============================================================================
// Condition::timed_wait
// ============================================================================
bool Condition::timed_wait (unsigned long _tmo_msecs)
{
  //- null tmo means infinite wait

  bool signaled = true;
 
	if (_tmo_msecs <= 0) 
  {
    ::pthread_cond_wait(&m_posix_cond, &m_external_lock.m_posix_mux);
  }
	else 
  {
		//- get absoulte time
		struct timespec ts;
		ThreadingUtilities::get_time(_tmo_msecs, ts);
		
    //- wait for the condition to be signaled or tmo expiration
		int result = ::pthread_cond_timedwait(&m_posix_cond, 
                                          &m_external_lock.m_posix_mux, 
                                          &ts);
                                          
    if (result == ETIMEDOUT || result == EINTR)
      signaled = false;
      
    /*
    switch (result)
    { 
      case 0:
        std::cout << "Condition::pthread_cond_timedwait returned: SUCCESS" << std::endl;
        break;
      case ETIMEDOUT:
        std::cout << "Condition::pthread_cond_timedwait returned: ETIMEDOUT" << std::endl;
        break;      
      case EINTR:
        std::cout << "Condition::pthread_cond_timedwait returned: EINTR" << std::endl;
        break;
    }
    */
	}

  return signaled;
}

// ****************************************************************************
// BPM THREAD IMPL
// ****************************************************************************
// ============================================================================
// BPM common thread entry point (non-OO OS intertace to OO BPM interface)
// ============================================================================
Thread::IOArg bpm_thread_common_entry_point (Thread::IOArg _p)
{
  //- check input (parano. impl.)
  if (! _p) return 0;

  //- reinterpret input
  Thread * me = reinterpret_cast<Thread*>(_p);

  //- store the thread identifier
  me->m_uid = ThreadingUtilities::self();

  //- select detached or undetached mode
  if (me->m_detached)
  {
    //- just protect bpm impl. against user code using a try/catch statement
    try
    {
	    me->run(me->m_iarg);
    }
    catch (...)
    {
      //- ignore any exception
    }
  }
  else 
  {
    //- just protect bpm impl. against user code using a try/catch statement
    try
    {
	    me->m_oarg = me->run_undetached(me->m_iarg);
    }
    catch (...)
    {
      //- ignore any exception

    }
  }

  //- set state to terminated
  {
    //- must lock the mutex even in the case of a detached thread. This is because
    //- a thread may run to completion before the thread that created it has had a
    //- chance to get out of start().  By locking the mutex we ensure that the
    //- creating thread must have reached the end of start() before we delete the
    //- thread object.  Of course, once the call to start() returns, the user can
    //- still incorrectly refer to the thread object, but that's his problem!
	  AutoMutex<Mutex> guard(me->m_lock);
    //- set state to TERMINATED
    me->m_state = bpm::Thread::STATE_TERMINATED;
  }

  //- commit suicide in case the thread ran detached
	if (me->m_detached)
    delete me;

  return 0;
}

// ============================================================================
// Thread::Thread
// ============================================================================
Thread::Thread (Tango::DeviceImpl * hd, Thread::IOArg _iarg, Thread::Priority _p)
 : Tango::LogAdapter (hd),
   m_state (bpm::Thread::STATE_NEW),
   m_priority (_p),
   m_iarg (_iarg),
   m_oarg (0),
   m_detached (true),
   m_uid (BPM_INVALID_THREAD_UID),
   //- platform specific members
   m_posix_thread (0)
{
#if defined(HAS_THREAD_PRIORITY)
 static bool init_done = false;
 if (! init_done)
 {
   lowest_priority = ::sched_get_priority_min(SCHED_FIFO);
   highest_priority = ::sched_get_priority_max(SCHED_FIFO);
   switch (highest_priority - lowest_priority) 
   {
     case 0:
     case 1:
       normal_priority = lowest_priority;
       break;
     default:
       normal_priority = lowest_priority + 1;
       break;
   }
   init_done = true;
 }
#endif
}

// ============================================================================
// Thread::~Thread
// ============================================================================
Thread::~Thread (void)
{
  //- noop
}

// ============================================================================
// Thread::start [detatched thread]
// ============================================================================
void Thread::start (void) 
  throw (Tango::DevFailed)
{
  //- mark the thread as detached
  this->m_detached = true;
  //- then spawn it
  this->spawn();
}

// ============================================================================
// Thread::start_undetached [undetatched thread]
// ============================================================================
void Thread::start_undetached (void)
  throw (Tango::DevFailed)
{
  //- mark the thread as undetached
  this->m_detached = false;
  //- then spawn it
  this->spawn();
}

// ============================================================================
// Thread::spawn (common to detatched & undetached threads)
// ============================================================================
void Thread::spawn (void) 
  throw (Tango::DevFailed)
{
  //- enter critical section
  AutoMutex<Mutex> guard(this->m_lock);

  //- be sure the thread is not already running or terminated
  if (this->m_state != bpm::Thread::STATE_NEW)
	  return;

  //- intialize thread attributes
  pthread_attr_t thread_attrs;

  ::pthread_attr_init(&thread_attrs);

  //- set detach attribute
  int ds = this->m_detached 
         ? PTHREAD_CREATE_DETACHED 
         : PTHREAD_CREATE_JOINABLE;
  ::pthread_attr_setdetachstate(&thread_attrs, ds);

  //- set thread priority
#if defined(HAS_THREAD_PRIORITY)
  struct sched_param sp;
  sp.sched_priority = bpm_to_posix_priority(m_priority);
  ::pthread_attr_setschedparam(&thread_attrs, &sp));
#endif

  //- spawn the thread
  int result = 0;
  result = ::pthread_create(&m_posix_thread, 
                            &thread_attrs, 
                            bpm_thread_common_entry_point, 
                            static_cast<void*>(this));
  ::pthread_attr_destroy(&thread_attrs);

  //- check result
  if (result)
    Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                   _CPTC("system call <pthread_create> failed"),
                                   _CPTC("Thread::spawn"));

  //- mark the thread as running (before leaving the critical section)
  this->m_state = bpm::Thread::STATE_RUNNING;
}

// ============================================================================
// Thread::join
// ============================================================================
void Thread::join (Thread::IOArg * oarg_)
  throw (Tango::DevFailed)
{
  {
    //- enter critical section
    AutoMutex<Mutex> guard(this->m_lock);

    //- check thread state
    if (   
           (this->m_state != bpm::Thread::STATE_RUNNING) 
        && 
           (this->m_state != bpm::Thread::STATE_TERMINATED)
       )
      Tango::Except::throw_exception(_CPTC("PROGRAMMING_ERROR"),
                                     _CPTC("can't join [thread never started or already terminated]"),
                                     _CPTC("Thread::join"));
  }

  //- be sure the thread is not detached
  if (this->m_detached)
      Tango::Except::throw_exception(_CPTC("PROGRAMMING_ERROR"),
                                     _CPTC("can't join with a detached thread"),
                                     _CPTC("Thread::join"));

  if (::pthread_join(m_posix_thread, 0))
      Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                     _CPTC("system call <pthread_join> failed"),
                                     _CPTC("Thread::join"));

  //- return the "thread result"
  if (oarg_)
    *oarg_ = this->m_oarg;

  //- commit suicide
  delete this;
}

// ============================================================================
// Thread::priority
// ============================================================================
void Thread::priority (Priority _p)
  throw (Tango::DevFailed)
{
  //- enter critical section
  AutoMutex<Mutex> guard(this->m_lock);
  
  //- check thread state
  if (this->m_state != bpm::Thread::STATE_RUNNING)
    return; //- throw exception 

#if defined(HAS_THREAD_PRIORITY)
    struct sched_param sp;
    sp.sched_priority = this->bpm_to_posix_priority(_p);
    if (::pthread_setschedparam(m_posix_thread, SCHED_OTHER, &sp))
      Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                     _CPTC("system call <pthread_setschedparam> failed"),
                                     _CPTC("Thread::priority"));
#endif

  //- store new priority
  this->m_priority = _p;
}

// ============================================================================
// Thread::bpm_to_posix_priority
// ============================================================================
int Thread::bpm_to_posix_priority (Priority _p)
{
#if defined(HAS_THREAD_PRIORITY)
  switch (_p) 
  {
    case bpm::Thread::PRIORITY_LOW:
	    return lowest_priority;
      break;
    case bpm::Thread::PRIORITY_HIGH:
      return highest_priority;
      break;
    case bpm::Thread::PRIORITY_RT:
      return highest_priority;
      break;
    default:
	    return normal_priority;
  }
#else
  switch (_p) 
  {
    default:
			return 0;
  } 
#endif
}


// ============================================================================
// ThreadingUtilities::self
// ============================================================================
ThreadUID ThreadingUtilities::self (void)
{
  return static_cast<bpm::ThreadUID>(::pthread_self());
}

// ============================================================================
// ThreadingUtilities::sleep
// ============================================================================
void ThreadingUtilities::sleep (unsigned long _secs, unsigned long _nano_secs)
{
#if defined(HAS_NANO_SLEEP)

  timespec ts2;
  timespec ts1 = {_secs, _nano_secs};

  while (::nanosleep(&ts1, &ts2)) 
  {
    if (errno == EINTR) 
    {
	    ts1.tv_sec  = ts2.tv_sec;
	    ts1.tv_nsec = ts2.tv_nsec;
	    continue;
    }
  }

#else

  if (_secs > 2000) 
    while ((_secs = ::sleep(_secs))) ;
  else 
 	  ::usleep(_secs * 1000000 + (_nano_secs / 1000));

#endif
}

// ============================================================================
// ThreadingUtilities::get_time
// ============================================================================
void ThreadingUtilities::get_time (unsigned long & abs_sec_,
                                   unsigned long & abs_nano_sec_,
                  		             unsigned long _rel_sec,
                                   unsigned long _rel_nano_sec)
{
  timespec abs;

  struct timeval tv;
  ::gettimeofday(&tv, NULL); 

  abs.tv_sec = tv.tv_sec;
  abs.tv_nsec = tv.tv_usec * 1000;

  abs.tv_sec += _rel_sec + abs.tv_nsec / NANOSECS_PER_SEC;
  abs.tv_nsec += _rel_nano_sec;
  abs.tv_nsec %= NANOSECS_PER_SEC;

  abs_sec_ = abs.tv_sec;
  abs_nano_sec_ = abs.tv_nsec;
}

// ============================================================================
// ThreadingUtilities::get_time
// ============================================================================
void ThreadingUtilities::get_time (unsigned long delay_msecs, Timespec& abs_time)
{
  struct timeval now;
  ::gettimeofday(&now, NULL); 
  
  //- std::cout << "delay_msecs........" << delay_msecs << std::endl;
  
  abs_time.tv_sec  = now.tv_sec + delay_msecs / 1000;
  delay_msecs -=  delay_msecs / 1000 * 1000;
  abs_time.tv_nsec = now.tv_usec * 1000;
  abs_time.tv_nsec += delay_msecs * 1000000;
  abs_time.tv_sec  += abs_time.tv_nsec / NANOSECS_PER_SEC;
  abs_time.tv_nsec %= NANOSECS_PER_SEC;
  
  //- std::cout << "now.tv_sec........." << now.tv_sec << std::endl;
  //- std::cout << "now.tv_nsec........" << now.tv_usec * 1000 << std::endl;
  //- std::cout << "abs_time.tv_sec...." << abs_time.tv_sec << std::endl;
  //- std::cout << "abs_time.tv_nsec..." << abs_time.tv_nsec << std::endl;
  //- std::cout << "dt.sec............." << abs_time.tv_sec - now.tv_sec << std::endl;
  //- std::cout << "dt.nsec............" << abs_time.tv_nsec - (now.tv_usec * 1000) << std::endl;
}

} // namespace bpm
