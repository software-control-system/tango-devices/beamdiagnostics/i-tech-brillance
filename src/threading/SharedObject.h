//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _SHARED_OBJECT_H_
#define _SHARED_OBJECT_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"
#include "threading/Mutex.h"

namespace bpm
{

// ============================================================================
//! A reference counted object abstraction.
// ============================================================================
//!
//! Base class for any reference counted object (i.e. shared) object.
//!
// ============================================================================
class SharedObject
{
public:

  SharedObject (void);
  // Constructor.

  virtual ~SharedObject (void);
  // Destructor.

  SharedObject *duplicate (void);
  // Return a "shallow" copy. Increment the reference count by 1
  // to avoid deep copies.

  void release (void);
  // Decrease the shared reference count by 1.  If the reference count
  // equals 0, then delete <this> and return 0. Behavior is undefined
  // if reference count < 0.

  int reference_count (void) const;
  // Returns the current reference count.

  void lock (void);
  // Gets exclusive access to the data.

  void unlock (void);
  // Release exclusive access to the data.

protected:
  // a mutex to protect the data against race conditions
  bpm::Mutex lock_;

private:
  // internal release implementation
  SharedObject * release_i (void);

  //- reference count for (used to avoid deep copies)
  int reference_count_;

  // = Disallow these operations.
  //--------------------------------------------
  SharedObject & operator= (const SharedObject &);
  SharedObject (const SharedObject &);
};

}  // namespace bpm

#if defined (__INLINE_IMPL__)
# include "threading/SharedObject.i"
#endif 

#endif // _SHARED_OBJECT_H_
