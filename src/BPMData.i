//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// DDData::duplicate
// ============================================================================
INLINE_IMPL DDData *DDData::duplicate (void)
{
  return reinterpret_cast < DDData * >(SharedObject::duplicate ());
}

// ============================================================================
// DDData::release
// ============================================================================
INLINE_IMPL void DDData::release (void)
{
  SharedObject::release ();
}

// ============================================================================
// DDData::current_buffers_depth
// ============================================================================
INLINE_IMPL size_t DDData::current_buffers_depth (void) const
{
  return this->x_.depth();
}

// ============================================================================
// DDData::x
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::x (void) const
{
  return this->x_;
}

// ============================================================================
// DDData::z
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::z (void) const
{
  return this->z_;
}

// ============================================================================
// DDData::q
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::q (void) const
{
  return this->q_;
}

// ============================================================================
// DDData::sum
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::sum (void) const
{
  return this->sum_;
}

// ============================================================================
// DDData::va
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::va (void) const
{
  return this->va_;
}

// ============================================================================
// DDData::vb
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::vb (void) const
{
  return this->vb_;
}

// ============================================================================
// DDData::vc
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::vc (void) const
{
  return this->vc_;
}

// ============================================================================
// DDData::vd
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::vd (void) const
{
  return this->vd_;
}

// ============================================================================
// DDData::cos_va
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::cos_va (void) const 
  throw (Tango::DevFailed)  
{
  if (! DDData::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "DDData::cos_va");
  return this->cos_va_;
}

// ============================================================================
// DDData::cos_vb
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::cos_vb (void) const
  throw (Tango::DevFailed)  
{
  if (! DDData::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "DDData::cos_vb");
  return this->cos_vb_;
}

// ============================================================================
// DDData::cos_vc
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::cos_vc (void) const
  throw (Tango::DevFailed)  
{
  if (! DDData::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "DDData::cos_vc");
  return this->cos_vc_;
}

// ============================================================================
// DDData::cos_vd
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::cos_vd (void) const
  throw (Tango::DevFailed)  
{
  if (! DDData::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "DDData::cos_vd");
  return this->cos_vd_;
}

// ============================================================================
// DDData::sin_va
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::sin_va (void) const
  throw (Tango::DevFailed)  
{
  if (! DDData::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "DDData::sin_va");
  return this->sin_va_;
}

// ============================================================================
// DDData::sin_vb
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::sin_vb (void) const
  throw (Tango::DevFailed)  
{
  if (! DDData::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "DDData::sin_vb");
  return this->sin_vb_;
}

// ============================================================================
// DDData::sin_vc
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::sin_vc (void) const
  throw (Tango::DevFailed)  
{
  if (! DDData::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "DDData::sin_vc");
  return this->sin_vc_;
}

// ============================================================================
// DDData::sin_vd
// ============================================================================
INLINE_IMPL const DDData::Buffer& DDData::sin_vd (void) const
  throw (Tango::DevFailed)  
{
  if (! DDData::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "DDData::sin_vd");
  return this->sin_vd_;
}

// ============================================================================
// DDData::timestamp
// ============================================================================
INLINE_IMPL const struct timespec &DDData::timestamp (void) const
{
  return this->timestamp_;
}

// ============================================================================
// DDData::actual_num_samples
// ============================================================================
INLINE_IMPL void DDData::actual_num_samples (size_t _num_samples)
{
  this->x_.force_length(_num_samples);
  this->z_.force_length(_num_samples);
  this->q_.force_length(_num_samples);

  this->sum_.force_length(_num_samples);

  this->va_.force_length(_num_samples);
  this->vb_.force_length(_num_samples);
  this->vc_.force_length(_num_samples);
  this->vd_.force_length(_num_samples);

  if (DDData::m_optional_data_enabled)
  {
    this->sin_va_.force_length(_num_samples);
    this->cos_va_.force_length(_num_samples);
    this->sin_vb_.force_length(_num_samples);
    this->cos_vb_.force_length(_num_samples);
    this->sin_vc_.force_length(_num_samples);
    this->cos_vc_.force_length(_num_samples);
    this->sin_vd_.force_length(_num_samples);
    this->cos_vd_.force_length(_num_samples);
  }
}

// ============================================================================
// SAData::duplicate
// ============================================================================
INLINE_IMPL SAData *SAData::duplicate (void)
{
  return reinterpret_cast < SAData * >(SharedObject::duplicate ());
}

// ============================================================================
// SAData::release
// ============================================================================
INLINE_IMPL void SAData::release (void)
{
  SharedObject::release ();
}

// ============================================================================
// SAData::operator=
// ============================================================================
INLINE_IMPL SAData & SAData::operator= (const SAData & _sad)
{
  if (this == &_sad)
    return *this;
    
  ::memcpy(this, &_sad, sizeof(SAData));
  
  return *this;
}

// ============================================================================
// SAData::operator=
// ============================================================================
INLINE_IMPL SAData & SAData::operator= (const CSPI_SA_ATOM & _saa)
{
  this->x_ = _saa.X;
  this->z_ = _saa.Y;
  this->q_ = _saa.Q;
  this->sum_ = _saa.Sum; 
  
  this->va_ = _saa.Va; 
  this->vb_ = _saa.Vb; 
  this->vc_ = _saa.Vc;
  this->vd_ = _saa.Vd; 
       
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  this->int_va_ = _saa.Va; 
  this->int_vb_ = _saa.Vb; 
  this->int_vc_ = _saa.Vc;
  this->int_vd_ = _saa.Vd; 
  this->int_sum_ = _saa.Sum; 
#endif

  this->cx_ = _saa.Cx;
  this->cz_ = _saa.Cy; 
  
  for (size_t i = 0, j = 0; i < 6; i++)
  {
    this->user_data_[j++] = 
      static_cast<short>(_saa.reserved[i] & 0x0000FFFF);
    
    this->user_data_[j++] = 
      static_cast<short>((_saa.reserved[i] & 0xFFFF0000) / 0x10000);
  }
  
  return *this;
}

// ============================================================================
// SAData::x
// ============================================================================
INLINE_IMPL const FPDataType & SAData::x (void) const
{
  return this->x_;
}

// ============================================================================
// SAData::z
// ============================================================================
INLINE_IMPL const FPDataType & SAData::z (void) const
{
  return this->z_;
}

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
// ============================================================================
// SAData::int_x
// ============================================================================
INLINE_IMPL i32 SAData::int_x (void) const
{
  return this->int_x_;
}

// ============================================================================
// SAData::int_z
// ============================================================================
INLINE_IMPL i32 SAData::int_z (void) const
{
  return this->int_z_;
}

// ============================================================================
// SAData::int_sum
// ============================================================================
INLINE_IMPL i32 SAData::int_sum (void) const
{
  return this->int_sum_;
}
#endif

// ============================================================================
// SAData::q
// ============================================================================
INLINE_IMPL const FPDataType & SAData::q (void) const
{
  return this->q_;
}

// ============================================================================
// SAData::sum
// ============================================================================
INLINE_IMPL const FPDataType & SAData::sum (void) const
{
  return this->sum_;
}

// ============================================================================
// SAData::va
// ============================================================================
INLINE_IMPL const FPDataType & SAData::va (void) const
{
  return this->va_;
}

// ============================================================================
// SAData::vb
// ============================================================================
INLINE_IMPL const FPDataType & SAData::vb (void) const
{
  return this->vb_;
}

// ============================================================================
// SAData::vc
// ============================================================================
INLINE_IMPL const FPDataType & SAData::vc (void) const
{
  return this->vc_;
}

// ============================================================================
// SAData::vd
// ============================================================================
INLINE_IMPL const FPDataType & SAData::vd (void) const
{
  return this->vd_;
}

// ============================================================================
// SAData::cx
// ============================================================================
INLINE_IMPL const long & SAData::cx (void) const
{
  return this->cx_;
}

// ============================================================================
// SAData::cz
// ============================================================================
INLINE_IMPL const long & SAData::cz (void) const
{
  return this->cz_;
}

// ============================================================================
// SAData::user_data
// ============================================================================
INLINE_IMPL const SAData::UserData & SAData::user_data (void) const
{
  return this->user_data_;
}

// ============================================================================
// SAData::timestamp
// ============================================================================
INLINE_IMPL const struct timespec &SAData::timestamp (void) const
{
  return this->timestamp_;
}

// ============================================================================
// SAHistory::x
// ============================================================================
INLINE_IMPL const SAData::Buffer & SAHistory::x (void)
  throw (Tango::DevFailed)
{
  bpm::MutexLock guard(this->lock_);
  return this->x_.ordered_data();
}

// ============================================================================
// SAHistory::z
// ============================================================================
INLINE_IMPL const SAData::Buffer & SAHistory::z (void)
  throw (Tango::DevFailed)
{
  bpm::MutexLock guard(this->lock_);
  return this->z_.ordered_data();
}

// ============================================================================
// SAHistory::sum
// ============================================================================
INLINE_IMPL const SAData::Buffer & SAHistory::sum (void)
  throw (Tango::DevFailed)
{
  if (! SAHistory::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "SAHistory::sum");
                      
  bpm::MutexLock guard(this->lock_);
  return this->s_.ordered_data();
}

// ============================================================================
// SAHistory::push
// ============================================================================
INLINE_IMPL void SAHistory::push (const SAData & _sad) 
  throw (Tango::DevFailed)
{
  bpm::MutexLock guard(this->lock_);
  
  this->x_.push(_sad.x());
  this->z_.push(_sad.z());
  
  if (SAHistory::m_optional_data_enabled)
    this->s_.push(_sad.sum());
  
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  this->int_x_.push(_sad.int_x());
  this->int_z_.push(_sad.int_z());

  if (SAHistory::m_optional_data_enabled)
    this->int_s_.push(_sad.int_sum());
#endif

  if (this->actual_num_samples_ < this->x_.data().depth())
    this->actual_num_samples_++;
}

// ============================================================================
// SAHistory::actual_num_samples
// ============================================================================
INLINE_IMPL size_t SAHistory::actual_num_samples (void) const
{
  return this->actual_num_samples_;
}

// ============================================================================
// SAHistory::freeze
// ============================================================================
INLINE_IMPL void SAHistory::freeze (void) 
{
  bpm::MutexLock guard(this->lock_);

  //- std::cout << "SAHistory::freeze <-" << std::endl;

  this->x_.freeze();
  this->z_.freeze();
  this->s_.freeze();

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  this->int_x_.freeze();
  this->int_z_.freeze();
  this->int_s_.freeze();
#endif

  this->frozen_ = true;

  //- std::cout << "SAHistory::freeze ->" << std::endl;
}

// ============================================================================
// SAHistory::unfreeze
// ============================================================================
INLINE_IMPL void SAHistory::unfreeze (void) 
{
  bpm::MutexLock guard(this->lock_);

  //- std::cout << "SAHistory::unfreeze <-" << std::endl;

  this->unfreeze_i();

  //- std::cout << "SAHistory::unfreeze ->" << std::endl;
}

// ============================================================================
// SAHistory::unfreeze_i
// ============================================================================
INLINE_IMPL void SAHistory::unfreeze_i (void) 
{
  this->x_.unfreeze();
  this->z_.unfreeze();
  this->s_.unfreeze();

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  this->int_x_.unfreeze();
  this->int_z_.unfreeze();
  this->int_s_.unfreeze();
#endif

  this->frozen_ = false;
}

// ============================================================================
// SAHistory::x_mean
// ============================================================================
INLINE_IMPL const FPDataType & SAHistory::x_mean (void) const
{
  return this->x_mean_;
}

// ============================================================================
// SAHistory::z_mean
// ============================================================================
INLINE_IMPL const FPDataType & SAHistory::z_mean (void) const
{
  return this->z_mean_;
}

// ============================================================================
// SAHistory::sum
// ============================================================================
INLINE_IMPL const FPDataType & SAHistory::sum_mean (void) const
  throw (Tango::DevFailed)
{
  if (! SAHistory::m_optional_data_enabled)
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
                      "no data available - feature disabled",
                      "SAHistory::sum_mean");
                      
  return this->s_mean_;
}

// ============================================================================
// SAHistory::x_rms
// ============================================================================
INLINE_IMPL const FPDataType & SAHistory::x_rms (void) const
{
  return this->x_rms_;
}

// ============================================================================
// SAHistory::z_rms
// ============================================================================
INLINE_IMPL const FPDataType & SAHistory::z_rms (void) const
{
  return this->z_rms_;
}

// ===========================================================================
// SAHistory::x_peak
// ============================================================================
INLINE_IMPL const FPDataType & SAHistory::x_peak (void) const
{
  return this->x_peak_;
}

// ============================================================================
// SAHistory::z_peak
// ============================================================================
INLINE_IMPL const FPDataType & SAHistory::z_peak (void) const
{
  return this->z_peak_;
}

// ============================================================================
// SAHistory::void
// ============================================================================
INLINE_IMPL void SAHistory::reset (void)
{
  bpm::MutexLock guard(this->lock_);
  
  this->actual_num_samples_ = 0;

  this->frozen_ = 0;

  this->x_mean_ = _NAN_;
  this->z_mean_ = _NAN_;
  this->x_rms_  = _NAN_;
  this->z_rms_  = _NAN_;
  this->x_peak_ = _NAN_;
  this->z_peak_ = _NAN_;
  this->s_mean_ = _NAN_;
  
  this->x_.clear();
  this->x_.fill(_NAN_);
  
  this->z_.clear();
  this->z_.fill(_NAN_);
  
  this->s_.clear();
  this->s_.fill(_NAN_);

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  this->int_x_.clear();
  this->int_z_.clear();
  this->int_s_.clear();
#endif
}

// ============================================================================
// ADCData::duplicate
// ============================================================================
INLINE_IMPL ADCData * ADCData::duplicate (void)
{
  return reinterpret_cast < ADCData * >(SharedObject::duplicate ());
}

// ============================================================================
// ADCData::release
// ============================================================================
INLINE_IMPL void ADCData::release (void)
{
  SharedObject::release ();
}

// ============================================================================
// ADCData::a
// ============================================================================
INLINE_IMPL const ADCData::Buffer& ADCData::a (void) const
{
  return this->a_;
}

// ============================================================================
// ADCData::b
// ============================================================================
INLINE_IMPL const ADCData::Buffer& ADCData::b (void) const
{
  return this->b_;
}

// ============================================================================
// ADCData::c
// ============================================================================
INLINE_IMPL const ADCData::Buffer& ADCData::c (void) const
{
  return this->c_;
}

// ============================================================================
// ADCData::d
// ============================================================================
INLINE_IMPL const ADCData::Buffer& ADCData::d (void) const
{
  return this->d_;
}

// ============================================================================
// ADCData::timestamp
// ============================================================================
INLINE_IMPL const struct timespec & ADCData::timestamp (void) const
{
  return this->timestamp_;
}

// ============================================================================
// ADCData::current_buffers_depth
// ============================================================================
INLINE_IMPL size_t ADCData::current_buffers_depth (void) const
{
  return this->a_.depth();
}

// ============================================================================
// ADCData::actual_num_samples
// ============================================================================
INLINE_IMPL void ADCData::actual_num_samples (size_t _num_samples)
{
  this->a_.force_length(_num_samples);
  this->b_.force_length(_num_samples);
  this->c_.force_length(_num_samples);
  this->d_.force_length(_num_samples);
}

} // namespace bpm
