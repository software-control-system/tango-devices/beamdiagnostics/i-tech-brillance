//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

namespace bpm
{

#if defined(_USE_FLOAT_FP_DATA_) 

// ============================================================================
// fixed_to_single                          ** written by M.Abbott - DIAMOND **
// ============================================================================
// Converts fixed point integer to single fp applying the conversion:
//            *result = input * scaling * 2**scaling_shift
//
// For optimal results scaling and scaling_shift should be precomputed so that:
//                      2**31 <= scaling < 2**32
// 
// Scaling factor and shift for conversion are computed so that:
//                  2**31 <= int_scaling < 2**32
//             scaling = int_scaling * 2**scaling_shift 
//  
// Here is the code:
//             
// float ln2 = logf(scaling) / logf(2.0);
// int bits = floorf(ln2) + 1;
// unsigned int int_scaling = (unsigned int) (scaling * powf(2.0, 32 - bits));   
// int scaling_shift = bits - 32;  
// ============================================================================
INLINE_IMPL void ArmOptimization::fixed_to_fp (i32 input, 
                                               float *result, 
                                               u32 scaling, 
                                               i32 scaling_shift)
{
  u32 * iresult = (u32 *)result;
  if (input == 0)
    // Special case for 0
    *iresult = 0;
  else
  {
    // First extract the sign
    u32 sign = input & 0x80000000;
    if (sign != 0)
        input = -input;

    // Normalise the input to maintain largest possible dynamic range.
    // After this we have:
    //      2**31 <= fraction < 2**32
    //      input = sign * fraction * 2**-shift_in
    u32 shift_in = ::CLZ((u32)input);
    u32 fraction = input << shift_in;

    // Rescale the fraction.  The following is optimal, assuming that the
    // scaling factor has been computed to be in the range:
    //      2**31 <= scaling < 2**32
    //
    // If so then the new fraction satisfies
    //      2**30 <= fraction' < 2**32
    //      input = (fraction' / scaling) * 2**(32-shift_in)
    fraction = ::MulUU(fraction, scaling);
    
    // Alas, one more normalisation step required: may need to fix up
    // fraction by one further bit!  This is as good a point as any to
    // finally discard the top fraction bit (but we'll pretend it's still
    // there in the analysis below).
    u32 fixup = ::CLZ(fraction) + 1;
    fraction <<= fixup;
    shift_in += fixup;

    // Finally ready to assemble the final result.  The exponent (which
    // we don't bother to check, because our dynamic range isn't very
    // large) is computed so that
    //      result = sign * fraction * 2**(exponent - BIAS - 32)
    // As it is, we want
    //      result = input * scaling * 2**scaling_shift
    //             = fraction' * 2**(scaling_shift + 32 - shift_in) ,
    // in other words we want
    //      exponent - BIAS - 32 = scaling_shift + 32 - shift_in
    u32 exponent = EXPONENT_BIAS + 64 + scaling_shift - shift_in;
    
    *iresult = sign | (exponent << 23) | (fraction >> 9);
  }
}

#else // _USE_FLOAT_FP_DATA_ 

// ============================================================================
// fixed_to_double                          ** written by M.Abbott - DIAMOND **
// ============================================================================
INLINE_IMPL void ArmOptimization::fixed_to_fp (i32 input, 
                                               double *result, 
                                               u64 scaling, 
                                               i32 scaling_shift)
{
  //- special case: <input> == 0
  if (input == 0)
  {
    *((u64 *)result) = 0;
    return;
  }
  //- capture sign of <input>
  u32 sign = input & 0x80000000;
  if (sign != 0)
      input = -input;
  //- shift the fraction so that it's spread over 32-bits 
  //- |-> get number of leading zeros in the binary signature of <input>...
  u32 shift_in = CLZ((u32)input); 
  //- |-> do the shift
  u32 fraction_u32 = input << shift_in;
  //- scale the fraction and put the result into a 64-bits integer
  u64_pun fraction_u64;
  fraction_u64.l = (u64)fraction_u32 * scaling;
  //- apply IEEE-754 normalization (significant hidden bit)
  u32 fixup = CLZ(fraction_u64.i[1]) + 1;
  fraction_u64.l <<= fixup;
  //- don't forget to add this normalization step to the global shift
  shift_in += fixup;
  //- compute the expoent
  u64 exponent = EXPONENT_BIAS + 64 + scaling_shift - shift_in;
  //- assemble all components
  //  |-> shift the significant to its place (sign + exponent = 12 bits)
  fraction_u64.l >>= 12;
  //  |-> thanks to u64_pun type, it becomes easy to compose the fp64
  u32 * iresult = (u32 *)result;
  iresult[0] = sign | (exponent << 20) | fraction_u64.i[1];
  iresult[1] = fraction_u64.i[0];
}

#endif // _USE_FLOAT_FP_DATA_ 

// ============================================================================
// sqrt                              see http://www.embedded.com/98/9802fe2.htm
// ============================================================================
INLINE_IMPL u32 ArmOptimization::sqrt (u32 a)
{
  u32 rem = 0;
  u32 root = 0;
  for (i32 i = 0; i < 16; i++)
  {
    root <<= 1;
    rem = ((rem << 2) + (a >> 30));
    a <<= 2;
    root++;
    if (root <= rem)
    {
      rem -= root;
      root++;
    }
    else
      root--;
  }
  return root >> 1;
}

} // namespace bpm

#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_
