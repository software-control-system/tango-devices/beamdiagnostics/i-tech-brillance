static const char *ClassId    = "$Id: $";
static const char *CvsPath    = "$Source: $";
static const char *SvnPath    = "$HeadURL: $";
static const char *RcsId = "$Header: /cvsroot/tango-ds/BeamDiag/bpm_libera/src/LiberaClass.cpp,v 1.4.2.30.2.1 2008/05/25 13:21:42 nleclercq Exp $";
static const char *TagName = "$Name:  $";
static const char *FileName = "$Source: /cvsroot/tango-ds/BeamDiag/bpm_libera/src/LiberaClass.cpp,v $";
static const char *HttpServer = "http://controle/DeviceServer/doc/";
static const char *RCSfile = "$RCSfile: LiberaClass.cpp,v $";
//+=============================================================================
//
// file :        LiberaClass.cpp
//
// description : C++ source for the LiberaClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the Libera once per process.
//
// project :     TANGO Device Server
//
// $Author: nleclercq $
//
// $Revision: 1.4.2.30.2.1 $ 
// 
// $Log: LiberaClass.cpp,v $
// Revision 1.4.2.30.2.1  2008/05/25 13:21:42  nleclercq
// Enhanced embedded device support
// Reorganized src tree
// Complete rewrite of threading support (code extracted from NL & JM YAT lib)
// Add DD buffer freezing support for embedded device
// Added BPMSensors class (CPU & Memory uage)
// Misc. minor changes
//
// Revision 1.4.2.30  2008/03/21 15:04:46  nleclercq
// Fixed a pb in PM data handling
// Added a UserData vector attribute
// Removed individual user data atrributes
//
// Revision 1.4.2.29  2008/02/29 08:48:31  nleclercq
// Upated to cpsi 1.60
// Added FOFB attributes
//
// Revision 1.4.2.28  2008/02/28 16:20:52  nleclercq
// Upated to cpsi 1.60
// Added FOFB attributes
//
// Revision 1.4.2.27  2008/02/28 15:07:43  nleclercq
// Minor change
//
// Revision 1.4.2.26  2007/06/27 15:05:33  nleclercq
// no message
//
// Revision 1.4.2.25  2007/06/04 15:27:48  nleclercq
// Changed Read/Write FA data (long)
// Changed default PM array size (no data)
//
// Revision 1.4.2.24  2007/04/19 14:56:43  nleclercq
// Fixed bug in Read/Write FA data
// Added more interlock support
//
// Revision 1.4.2.23  2007/03/28 11:17:53  nleclercq
// Added cspi-1.42 support (cpsi_seek)
// Fixed a bug in ADC buffer size reading
//
// Revision 1.4.2.22  2007/02/27 15:13:28  nleclercq
// Changed internal message handling
// Added ReloadSystemProperties
//
// Revision 1.4.2.21  2007/02/12 17:33:07  nleclercq
// Modified the "embdded" specific code
//
// Revision 1.4.2.20  2007/01/25 13:41:41  nleclercq
// Added fixed some small bugs
// Added support FP data type selection at compile time.
//
// Revision 1.4.2.19  2007/01/23 09:51:34  nleclercq
// Added support for FA block read/write
//
// Revision 1.4.2.18  2007/01/17 15:28:08  nleclercq
// Fixed some bugs - commit for pre-release 1.40
//
// Revision 1.4.2.17  2007/01/16 14:46:27  nleclercq
// Added (almost full) cspi-1.40 support
//
// Revision 1.4.2.16  2007/01/12 10:15:12  nleclercq
// First commit for cspi-1.40 support
//
// Revision 1.4.2.15  2006/11/30 14:43:52  nleclercq
// Added support for cspi 1.22
// Added support for ADC data source
// Added set time (system and machine times)
// Removed BBA offset component for FPGA pos. computation
//
// Revision 1.4.2.14  2006/09/21 16:10:35  nleclercq
// Minor changes
// Fixed some attribute default properties
//
// Revision 1.4.2.13  2006/09/18 13:07:42  nleclercq
// Added suport for cspi 1.21
//
// Revision 1.4.2.12  2006/06/19 14:24:40  nleclercq
// Sync with SOLEIL prod version
//
// Revision 1.5  2006/06/13 12:33:14  nleclercq
// Added support for CSPI 1.03
//
// Revision 1.4.2.10  2006/06/07 08:00:33  nleclercq
// Sync with SOLEIL prod. version
//
// Revision 1.4.2.8  2006/04/28 14:17:56  nleclercq
// Changed DDenabled and SAEnabled to READ_WRITE
// Renamed attr PMNotifed to PMNotified
// No more exception raised when no data available - invalid attr values are returned instead (contain NAN)
//
// Revision 1.4.2.7  2006/04/25 08:45:34  nleclercq
// Fixed switches, attenuators and decimation factor problems
//
// Revision 1.4.2.6  2006/03/27 16:52:52  nleclercq
// Added switches attr, fixed a bug in SA history management.
//
// Revision 1.4.2.5  2006/03/23 09:42:29  nleclercq
// Several minor bugs fixes
//
// Revision 1.4.2.4  2006/03/20 15:33:28  nleclercq
// Added cspi 1.02 support
// Added PM support
//
// Revision 1.4.2.3  2006/03/20 15:01:47  nleclercq
// Restored all src files
//
// Revision 1.4  2005/10/20 08:27:08  nleclercq
// Fixed bug in the doc. Added new features
//
// Revision 1.3  2005/09/26 16:42:37  nleclercq
// Added attributes to the device interface (Libera cache status)
//
// Revision 1.2  2005/08/02 16:02:19  nleclercq
// Sync with code deploy at SOLEILa.u.
//
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//      This file is generated by POGO
//  (Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>

#include <Libera.h>
#include <LiberaClass.h>


namespace Libera_ns
{
//+----------------------------------------------------------------------------
//
// method : 		SetRefIncoherenceCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SetRefIncoherenceCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SetRefIncoherenceCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->set_ref_incoherence());
	return new CORBA::Any();
}


//+----------------------------------------------------------------------------
//
// method : 		ReloadSystemPropertiesCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *ReloadSystemPropertiesCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "ReloadSystemPropertiesCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->reload_system_properties());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		SaveDSCParametersCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SaveDSCParametersCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SaveDSCParametersCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->save_dscparameters());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		WriteFADataCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *WriteFADataCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "WriteFADataCmd::execute(): arrived" << endl;

	const Tango::DevVarLongArray	*argin;
	extract(in_any, argin);

	((static_cast<Libera *>(device))->write_fadata(argin));
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		ReadFADataCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *ReadFADataCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "ReadFADataCmd::execute(): arrived" << endl;

	const Tango::DevVarLongArray	*argin;
	extract(in_any, argin);

	return insert((static_cast<Libera *>(device))->read_fadata(argin));
}

//+----------------------------------------------------------------------------
//
// method : 		SetTimeOnNextTriggerCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SetTimeOnNextTriggerCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SetTimeOnNextTriggerCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->set_time_on_next_trigger());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		DisableADCCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *DisableADCCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "DisableADCCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->disable_adc());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		EnableADCCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *EnableADCCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "EnableADCCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->enable_adc());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		SetInterlockConfigurationCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SetInterlockConfigurationCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SetInterlockConfigurationCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->set_interlock_configuration());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		ResetPMNotificationCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *ResetPMNotificationCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "ResetPMNotificationCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->reset_pmnotification());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		ResetInterlockNotificationCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *ResetInterlockNotificationCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "ResetInterlockNotificationCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->reset_interlock_notification());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		EnableSACmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *EnableSACmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "EnableSACmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->enable_sa());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		EnableDDCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *EnableDDCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "EnableDDCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->enable_dd());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		DisableDDBufferFreezingCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *DisableDDBufferFreezingCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "DisableDDBufferFreezingCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->disable_ddbuffer_freezing());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		EnableDDBufferFreezingCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *EnableDDBufferFreezingCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "EnableDDBufferFreezingCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->enable_ddbuffer_freezing());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		UnfreezeDDBufferCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *UnfreezeDDBufferCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "UnfreezeDDBufferCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->unfreeze_ddbuffer());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		GetParametersCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *GetParametersCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "GetParametersCmd::execute(): arrived" << endl;

	return insert((static_cast<Libera *>(device))->get_parameters());
}


//+----------------------------------------------------------------------------
//
// method : 		DisableSACmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *DisableSACmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "DisableSACmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->disable_sa());
	return new CORBA::Any();
}


//+----------------------------------------------------------------------------
//
// method : 		DisableDDCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *DisableDDCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "DisableDDCmd::execute(): arrived" << endl;

	((static_cast<Libera *>(device))->disable_dd());
	return new CORBA::Any();
}

//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
LiberaClass *LiberaClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method :     LiberaClass::LiberaClass(string &s)
//
// description :  constructor for the LiberaClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
LiberaClass::LiberaClass(string &s):Tango::DeviceClass(s)
{
  cout2 << "Entering LiberaClass constructor" << endl;
	get_class_property();

	set_default_property();
	write_class_property();
  write_class_property ();

  cout2 << "Leaving LiberaClass constructor" << endl;
}
//+----------------------------------------------------------------------------
//
// method :     LiberaClass::~LiberaClass()
//
// description :  destructor for the LiberaClass
//
//-----------------------------------------------------------------------------
LiberaClass::~LiberaClass()
{
  _instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		LiberaClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
LiberaClass *LiberaClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new LiberaClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

LiberaClass *LiberaClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		LiberaClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void LiberaClass::command_factory()
{
	command_list.push_back(new GetParametersCmd("GetParameters",
		Tango::DEV_VOID, Tango::DEVVAR_DOUBLESTRINGARRAY,
		"N/A",
		"The BPM gain and offsets",
		Tango::OPERATOR));
	command_list.push_back(new UnfreezeDDBufferCmd("UnfreezeDDBuffer",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new EnableDDBufferFreezingCmd("EnableDDBufferFreezing",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new DisableDDBufferFreezingCmd("DisableDDBufferFreezing",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new EnableDDCmd("EnableDD",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new DisableDDCmd("DisableDD",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new EnableSACmd("EnableSA",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new DisableSACmd("DisableSA",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new ResetPMNotificationCmd("ResetPMNotification",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new ResetInterlockNotificationCmd("ResetInterlockNotification",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new SetInterlockConfigurationCmd("SetInterlockConfiguration",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new EnableADCCmd("EnableADC",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new DisableADCCmd("DisableADC",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new SetTimeOnNextTriggerCmd("SetTimeOnNextTrigger",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new ReadFADataCmd("ReadFAData",
		Tango::DEVVAR_LONGARRAY, Tango::DEVVAR_LONGARRAY,
		"The reading parameters: [0]:offset in FA data block, [1]:size of elems, [2]:num of elems",
		"The data [as an array of bytes]",
		Tango::OPERATOR));
	command_list.push_back(new WriteFADataCmd("WriteFAData",
		Tango::DEVVAR_LONGARRAY, Tango::DEV_VOID,
		"The writting parameters: [0]:offset in FA data block, bytes[1]:size of elems, [2]:num of elems, [3, ...]: actual to data to be written",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new SaveDSCParametersCmd("SaveDSCParameters",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new ReloadSystemPropertiesCmd("ReloadSystemProperties",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"n/a",
		"n/a",
		Tango::OPERATOR));
	command_list.push_back(new SetRefIncoherenceCmd("SetRefIncoherence",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		LiberaClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum LiberaClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		LiberaClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum LiberaClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		LiberaClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum LiberaClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		LiberaClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void LiberaClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new Libera(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: LiberaClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void LiberaClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : LiberaModel
	LiberaModelAttrib	*libera_model = new LiberaModelAttrib();
	Tango::UserDefaultAttrProp	libera_model_prop;
	libera_model_prop.set_label("Libera Model");
	libera_model_prop.set_unit("[0:e-, 1:br, 2:ph]");
	libera_model_prop.set_format("%d");
	libera_model_prop.set_description("The Libera Model: 0:Electron, 1:Brillance, 2:Photon");
	libera_model->set_default_properties(libera_model_prop);
	att_list.push_back(libera_model);

	//	Attribute : DDEnabled
	DDEnabledAttrib	*ddenabled = new DDEnabledAttrib();
	Tango::UserDefaultAttrProp	ddenabled_prop;
	ddenabled_prop.set_label("DD Enabled");
	ddenabled_prop.set_unit("n/a");
	ddenabled_prop.set_standard_unit("n/a");
	ddenabled_prop.set_display_unit("n/a");
	ddenabled_prop.set_description("DD data source activation flag");
	ddenabled->set_default_properties(ddenabled_prop);
	ddenabled->set_memorized();
	ddenabled->set_memorized_init(true);
	att_list.push_back(ddenabled);

	//	Attribute : DDBufferSize
	DDBufferSizeAttrib	*ddbuffer_size = new DDBufferSizeAttrib();
	Tango::UserDefaultAttrProp	ddbuffer_size_prop;
	ddbuffer_size_prop.set_label("DD Buffer Size");
	ddbuffer_size_prop.set_unit("turns");
	ddbuffer_size_prop.set_format("%5d");
	ddbuffer_size_prop.set_max_value("65535");
	ddbuffer_size_prop.set_min_value("2");
	ddbuffer_size_prop.set_description("The number of samples to be read on DD data source.\nInfluences the size of the associated attributes [such as XPosDD for instance].");
	ddbuffer_size->set_default_properties(ddbuffer_size_prop);
	ddbuffer_size->set_memorized();
	ddbuffer_size->set_memorized_init(true);
	att_list.push_back(ddbuffer_size);

	//	Attribute : DDDecimationFactor
	DDDecimationFactorAttrib	*dddecimation_factor = new DDDecimationFactorAttrib();
	Tango::UserDefaultAttrProp	dddecimation_factor_prop;
	dddecimation_factor_prop.set_label("DD Decim. Factor");
	dddecimation_factor_prop.set_unit("samples");
	dddecimation_factor_prop.set_format("%3d");
	dddecimation_factor_prop.set_max_value("256");
	dddecimation_factor_prop.set_min_value("1");
	dddecimation_factor_prop.set_description("The DD decimation factor");
	dddecimation_factor->set_default_properties(dddecimation_factor_prop);
	dddecimation_factor->set_memorized();
	dddecimation_factor->set_memorized_init(true);
	att_list.push_back(dddecimation_factor);

	//	Attribute : DDTriggerOffset
	DDTriggerOffsetAttrib	*ddtrigger_offset = new DDTriggerOffsetAttrib();
	Tango::UserDefaultAttrProp	ddtrigger_offset_prop;
	ddtrigger_offset_prop.set_label("DD Trigger Offset");
	ddtrigger_offset_prop.set_unit("turns");
	ddtrigger_offset_prop.set_format("%6d");
	ddtrigger_offset_prop.set_description("DD data offset in num. of turns");
	ddtrigger_offset->set_default_properties(ddtrigger_offset_prop);
	ddtrigger_offset->set_memorized();
	ddtrigger_offset->set_memorized_init(true);
	att_list.push_back(ddtrigger_offset);

	//	Attribute : DDBufferFreezingEnabled
	DDBufferFreezingEnabledAttrib	*ddbuffer_freezing_enabled = new DDBufferFreezingEnabledAttrib();
	Tango::UserDefaultAttrProp	ddbuffer_freezing_enabled_prop;
	ddbuffer_freezing_enabled_prop.set_label("DD Buffer Freezing Enabled");
	ddbuffer_freezing_enabled_prop.set_unit("n/a");
	ddbuffer_freezing_enabled_prop.set_standard_unit("n/a");
	ddbuffer_freezing_enabled_prop.set_display_unit("n/a");
	ddbuffer_freezing_enabled_prop.set_description("DD buffer freezing activation flag");
	ddbuffer_freezing_enabled->set_default_properties(ddbuffer_freezing_enabled_prop);
	att_list.push_back(ddbuffer_freezing_enabled);

	//	Attribute : DDBufferFrozen
	DDBufferFrozenAttrib	*ddbuffer_frozen = new DDBufferFrozenAttrib();
	Tango::UserDefaultAttrProp	ddbuffer_frozen_prop;
	ddbuffer_frozen_prop.set_label("DD Buffer Frozen");
	ddbuffer_frozen_prop.set_unit("n/a");
	ddbuffer_frozen_prop.set_standard_unit("n/a");
	ddbuffer_frozen_prop.set_display_unit("n/a");
	ddbuffer_frozen_prop.set_description("DD buffer status");
	ddbuffer_frozen->set_default_properties(ddbuffer_frozen_prop);
	att_list.push_back(ddbuffer_frozen);

	//	Attribute : DDTriggerCounter
	DDTriggerCounterAttrib	*ddtrigger_counter = new DDTriggerCounterAttrib();
	Tango::UserDefaultAttrProp	ddtrigger_counter_prop;
	ddtrigger_counter_prop.set_label("Trig.Counter");
	ddtrigger_counter_prop.set_unit("a.u.");
	ddtrigger_counter_prop.set_standard_unit("a.u.");
	ddtrigger_counter_prop.set_display_unit("a.u.");
	ddtrigger_counter_prop.set_format("%8d");
	ddtrigger_counter_prop.set_description("Number of trigger notifications received since last device <init> ");
	ddtrigger_counter->set_default_properties(ddtrigger_counter_prop);
	att_list.push_back(ddtrigger_counter);

	//	Attribute : ExternalTriggerEnabled
	ExternalTriggerEnabledAttrib	*external_trigger_enabled = new ExternalTriggerEnabledAttrib();
	Tango::UserDefaultAttrProp	external_trigger_enabled_prop;
	external_trigger_enabled_prop.set_label("Ext. Trig. Enabled");
	external_trigger_enabled_prop.set_unit("n/a");
	external_trigger_enabled_prop.set_standard_unit("n/a");
	external_trigger_enabled_prop.set_display_unit("n/a");
	external_trigger_enabled_prop.set_description("External trigger activation flag");
	external_trigger_enabled->set_default_properties(external_trigger_enabled_prop);
	att_list.push_back(external_trigger_enabled);

	//	Attribute : ExternalTriggerDelay
	ExternalTriggerDelayAttrib	*external_trigger_delay = new ExternalTriggerDelayAttrib();
	Tango::UserDefaultAttrProp	external_trigger_delay_prop;
	external_trigger_delay_prop.set_label("External Trigger Delay");
	external_trigger_delay_prop.set_unit("ADC samples");
	external_trigger_delay_prop.set_format("%6d");
	external_trigger_delay_prop.set_min_value("0");
	external_trigger_delay_prop.set_description("The external trigger signal can be internally hardware delayed. \nThe delay is set in steps of ADC samples of about 9ns.");
	external_trigger_delay->set_default_properties(external_trigger_delay_prop);
	external_trigger_delay->set_memorized();
	external_trigger_delay->set_memorized_init(true);
	att_list.push_back(external_trigger_delay);

	//	Attribute : SAEnabled
	SAEnabledAttrib	*saenabled = new SAEnabledAttrib();
	Tango::UserDefaultAttrProp	saenabled_prop;
	saenabled_prop.set_label("SA Enabled");
	saenabled_prop.set_unit("n/a");
	saenabled_prop.set_standard_unit("n/a");
	saenabled_prop.set_display_unit("n/a");
	saenabled_prop.set_description("SA data source activation flag");
	saenabled->set_default_properties(saenabled_prop);
	saenabled->set_memorized();
	saenabled->set_memorized_init(true);
	att_list.push_back(saenabled);

	//	Attribute : VaSA
	VaSAAttrib	*va_sa = new VaSAAttrib();
	Tango::UserDefaultAttrProp	va_sa_prop;
	va_sa_prop.set_label("SA Va");
	va_sa_prop.set_unit("a.u.");
	va_sa_prop.set_standard_unit("a.u.");
	va_sa_prop.set_display_unit("a.u.");
	va_sa_prop.set_format("%10.0f");
	va_sa_prop.set_description("Slow Acquisition: Va");
	va_sa->set_default_properties(va_sa_prop);
	att_list.push_back(va_sa);

	//	Attribute : VbSA
	VbSAAttrib	*vb_sa = new VbSAAttrib();
	Tango::UserDefaultAttrProp	vb_sa_prop;
	vb_sa_prop.set_label("SA Vb");
	vb_sa_prop.set_unit("a.u.");
	vb_sa_prop.set_standard_unit("a.u.");
	vb_sa_prop.set_display_unit("a.u.");
	vb_sa_prop.set_format("%10.0f");
	vb_sa_prop.set_description("Slow Acquisition: Vb");
	vb_sa->set_default_properties(vb_sa_prop);
	att_list.push_back(vb_sa);

	//	Attribute : VcSA
	VcSAAttrib	*vc_sa = new VcSAAttrib();
	Tango::UserDefaultAttrProp	vc_sa_prop;
	vc_sa_prop.set_label("SA Vc");
	vc_sa_prop.set_unit("a.u.");
	vc_sa_prop.set_standard_unit("a.u.");
	vc_sa_prop.set_display_unit("a.u.");
	vc_sa_prop.set_format("%10.0f");
	vc_sa_prop.set_description("Slow Acquisition: Vc");
	vc_sa->set_default_properties(vc_sa_prop);
	att_list.push_back(vc_sa);

	//	Attribute : VdSA
	VdSAAttrib	*vd_sa = new VdSAAttrib();
	Tango::UserDefaultAttrProp	vd_sa_prop;
	vd_sa_prop.set_label("SA Vd");
	vd_sa_prop.set_unit("a.u.");
	vd_sa_prop.set_standard_unit("a.u.");
	vd_sa_prop.set_display_unit("a.u.");
	vd_sa_prop.set_format("%10.0f");
	vd_sa_prop.set_description("Slow Acquisition: Vd");
	vd_sa->set_default_properties(vd_sa_prop);
	att_list.push_back(vd_sa);

	//	Attribute : XPosSA
	XPosSAAttrib	*xpos_sa = new XPosSAAttrib();
	Tango::UserDefaultAttrProp	xpos_sa_prop;
	xpos_sa_prop.set_label("X.Pos.SA");
	xpos_sa_prop.set_unit("mm");
	xpos_sa_prop.set_format("%8.2f");
	xpos_sa_prop.set_description("Slow Acquisition: X");
	xpos_sa->set_default_properties(xpos_sa_prop);
	att_list.push_back(xpos_sa);

	//	Attribute : ZPosSA
	ZPosSAAttrib	*zpos_sa = new ZPosSAAttrib();
	Tango::UserDefaultAttrProp	zpos_sa_prop;
	zpos_sa_prop.set_label("Z.Pos.SA");
	zpos_sa_prop.set_unit("mm");
	zpos_sa_prop.set_format("%8.2f");
	zpos_sa_prop.set_description("Slow Acquisition: Z");
	zpos_sa->set_default_properties(zpos_sa_prop);
	att_list.push_back(zpos_sa);

	//	Attribute : SumSA
	SumSAAttrib	*sum_sa = new SumSAAttrib();
	Tango::UserDefaultAttrProp	sum_sa_prop;
	sum_sa_prop.set_label("Sum SA");
	sum_sa_prop.set_unit("a.u.");
	sum_sa_prop.set_format("%10.0f");
	sum_sa_prop.set_description("Slow Acquisition: Sum");
	sum_sa->set_default_properties(sum_sa_prop);
	att_list.push_back(sum_sa);

	//	Attribute : QuadSA
	QuadSAAttrib	*quad_sa = new QuadSAAttrib();
	Tango::UserDefaultAttrProp	quad_sa_prop;
	quad_sa_prop.set_label("Quad SA");
	quad_sa_prop.set_unit("a.u.");
	quad_sa_prop.set_format("%8.4f");
	quad_sa_prop.set_description("Slow Acquisition: Quad");
	quad_sa->set_default_properties(quad_sa_prop);
	att_list.push_back(quad_sa);

	//	Attribute : CxSA
	CxSAAttrib	*cx_sa = new CxSAAttrib();
	Tango::UserDefaultAttrProp	cx_sa_prop;
	cx_sa_prop.set_label("FOFB X Correction");
	cx_sa_prop.set_unit("a.u.");
	cx_sa_prop.set_format("%8d");
	cx_sa_prop.set_description("FOFB X correction sent to the power supply");
	cx_sa->set_default_properties(cx_sa_prop);
	att_list.push_back(cx_sa);

	//	Attribute : CzSA
	CzSAAttrib	*cz_sa = new CzSAAttrib();
	Tango::UserDefaultAttrProp	cz_sa_prop;
	cz_sa_prop.set_label("FOFB Z Correction");
	cz_sa_prop.set_unit("a.u.");
	cz_sa_prop.set_format("%8d");
	cz_sa_prop.set_description("FOFB Z correction sent to the power supply");
	cz_sa->set_default_properties(cz_sa_prop);
	att_list.push_back(cz_sa);

	//	Attribute : SAStatNumSamples
	SAStatNumSamplesAttrib	*sastat_num_samples = new SAStatNumSamplesAttrib();
	Tango::UserDefaultAttrProp	sastat_num_samples_prop;
	sastat_num_samples_prop.set_label("SA Stats.Num.Samples.");
	sastat_num_samples_prop.set_unit("samples");
	sastat_num_samples_prop.set_format("%5d");
	sastat_num_samples_prop.set_description("The number of sample in SA history used to compute the SA statistics\n(Mean, RMS, Peak pos). The most recent samples will be used.\nThe valid range is [2, SAHistoryLength property value].\n");
	sastat_num_samples->set_default_properties(sastat_num_samples_prop);
	att_list.push_back(sastat_num_samples);

	//	Attribute : XMeanPosSA
	XMeanPosSAAttrib	*xmean_pos_sa = new XMeanPosSAAttrib();
	Tango::UserDefaultAttrProp	xmean_pos_sa_prop;
	xmean_pos_sa_prop.set_label("SA X Mean Pos.");
	xmean_pos_sa_prop.set_unit("mm");
	xmean_pos_sa_prop.set_format("%8.4f");
	xmean_pos_sa_prop.set_description("Slow Acquisition:  X Mean Pos.");
	xmean_pos_sa->set_default_properties(xmean_pos_sa_prop);
	att_list.push_back(xmean_pos_sa);

	//	Attribute : ZMeanPosSA
	ZMeanPosSAAttrib	*zmean_pos_sa = new ZMeanPosSAAttrib();
	Tango::UserDefaultAttrProp	zmean_pos_sa_prop;
	zmean_pos_sa_prop.set_label("SA Z Mean Pos.");
	zmean_pos_sa_prop.set_unit("mm");
	zmean_pos_sa_prop.set_format("%8.4f");
	zmean_pos_sa_prop.set_description("Slow Acquisition:  Z Mean Pos.");
	zmean_pos_sa->set_default_properties(zmean_pos_sa_prop);
	att_list.push_back(zmean_pos_sa);

	//	Attribute : XRMSPosSA
	XRMSPosSAAttrib	*xrmspos_sa = new XRMSPosSAAttrib();
	Tango::UserDefaultAttrProp	xrmspos_sa_prop;
	xrmspos_sa_prop.set_label("SA X RMS Pos.");
	xrmspos_sa_prop.set_unit("um");
	xrmspos_sa_prop.set_format("%8.2f");
	xrmspos_sa_prop.set_description("Slow Acquisition:  X RMS Pos.");
	xrmspos_sa->set_default_properties(xrmspos_sa_prop);
	att_list.push_back(xrmspos_sa);

	//	Attribute : ZRMSPosSA
	ZRMSPosSAAttrib	*zrmspos_sa = new ZRMSPosSAAttrib();
	Tango::UserDefaultAttrProp	zrmspos_sa_prop;
	zrmspos_sa_prop.set_label("SA Z RMS Pos.");
	zrmspos_sa_prop.set_unit("um");
	zrmspos_sa_prop.set_format("%8.2f");
	zrmspos_sa_prop.set_description("Slow Acquisition: Z RMS Pos.");
	zrmspos_sa->set_default_properties(zrmspos_sa_prop);
	att_list.push_back(zrmspos_sa);

	//	Attribute : XPeakPosSA
	XPeakPosSAAttrib	*xpeak_pos_sa = new XPeakPosSAAttrib();
	Tango::UserDefaultAttrProp	xpeak_pos_sa_prop;
	xpeak_pos_sa_prop.set_label("X.Peak.Pos.SA");
	xpeak_pos_sa_prop.set_unit("um");
	xpeak_pos_sa_prop.set_format("%8.2f");
	xpeak_pos_sa_prop.set_description("Slow Acquisition: X pos peak to peak amplitude");
	xpeak_pos_sa->set_default_properties(xpeak_pos_sa_prop);
	att_list.push_back(xpeak_pos_sa);

	//	Attribute : ZPeakPosSA
	ZPeakPosSAAttrib	*zpeak_pos_sa = new ZPeakPosSAAttrib();
	Tango::UserDefaultAttrProp	zpeak_pos_sa_prop;
	zpeak_pos_sa_prop.set_label("Z.Peak.Pos.SA");
	zpeak_pos_sa_prop.set_unit("um");
	zpeak_pos_sa_prop.set_format("%8.2f");
	zpeak_pos_sa_prop.set_description("Slow Acquisition: X pos peak to peak amplitude");
	zpeak_pos_sa->set_default_properties(zpeak_pos_sa_prop);
	att_list.push_back(zpeak_pos_sa);

	//	Attribute : SumMeanSA
	SumMeanSAAttrib	*sum_mean_sa = new SumMeanSAAttrib();
	Tango::UserDefaultAttrProp	sum_mean_sa_prop;
	sum_mean_sa_prop.set_label("SA Sum Mean");
	sum_mean_sa_prop.set_unit("a.u.");
	sum_mean_sa_prop.set_standard_unit("a.u.");
	sum_mean_sa_prop.set_display_unit("a.u.");
	sum_mean_sa_prop.set_format("%8.1f");
	sum_mean_sa_prop.set_description("SA: mean of sum");
	sum_mean_sa->set_default_properties(sum_mean_sa_prop);
	att_list.push_back(sum_mean_sa);

	//	Attribute : ADCEnabled
	ADCEnabledAttrib	*adcenabled = new ADCEnabledAttrib();
	Tango::UserDefaultAttrProp	adcenabled_prop;
	adcenabled_prop.set_label("ADC Enabled");
	adcenabled_prop.set_unit("n/a");
	adcenabled_prop.set_standard_unit("n/a");
	adcenabled_prop.set_display_unit("n/a");
	adcenabled_prop.set_description("ADC data source activation flag");
	adcenabled->set_default_properties(adcenabled_prop);
	adcenabled->set_memorized();
	adcenabled->set_memorized_init(true);
	att_list.push_back(adcenabled);

	//	Attribute : ADCBufferSize
	ADCBufferSizeAttrib	*adcbuffer_size = new ADCBufferSizeAttrib();
	Tango::UserDefaultAttrProp	adcbuffer_size_prop;
	adcbuffer_size_prop.set_label("ADC Buffer Size");
	adcbuffer_size_prop.set_unit("samples");
	adcbuffer_size_prop.set_format("%5d");
	adcbuffer_size_prop.set_max_value("65535");
	adcbuffer_size_prop.set_min_value("8");
	adcbuffer_size_prop.set_description("The number of samples to be read on ADC data source.\nInfluences the size of the associated attributes [such as ADCChannelA for instance].");
	adcbuffer_size->set_default_properties(adcbuffer_size_prop);
	adcbuffer_size->set_memorized();
	adcbuffer_size->set_memorized_init(true);
	att_list.push_back(adcbuffer_size);

	//	Attribute : PMOffset
	PMOffsetAttrib	*pmoffset = new PMOffsetAttrib();
	Tango::UserDefaultAttrProp	pmoffset_prop;
	pmoffset_prop.set_label("Post Mortem Offset");
	pmoffset_prop.set_unit("samples");
	pmoffset_prop.set_format("%3d");
	pmoffset_prop.set_max_value("10000");
	pmoffset_prop.set_min_value("-10000");
	pmoffset_prop.set_description("Internal delay of the post mortem trigger. \nCan be set in the range of +/- 10 ksamples.");
	pmoffset->set_default_properties(pmoffset_prop);
	pmoffset->set_memorized();
	pmoffset->set_memorized_init(true);
	att_list.push_back(pmoffset);

	//	Attribute : PMNotified
	PMNotifiedAttrib	*pmnotified = new PMNotifiedAttrib();
	Tango::UserDefaultAttrProp	pmnotified_prop;
	pmnotified_prop.set_label("Post Moterm Notified");
	pmnotified_prop.set_unit("n/a");
	pmnotified_prop.set_description("Post Moterm notification flag");
	pmnotified->set_default_properties(pmnotified_prop);
	att_list.push_back(pmnotified);

	//	Attribute : PMNotificationCounter
	PMNotificationCounterAttrib	*pmnotification_counter = new PMNotificationCounterAttrib();
	Tango::UserDefaultAttrProp	pmnotification_counter_prop;
	pmnotification_counter_prop.set_label("PM Notif. Counter");
	pmnotification_counter_prop.set_format("%8d");
	pmnotification_counter_prop.set_description("Number a PM event recieved since last Init");
	pmnotification_counter->set_default_properties(pmnotification_counter_prop);
	att_list.push_back(pmnotification_counter);

	//	Attribute : InterlockXNotified
	InterlockXNotifiedAttrib	*interlock_xnotified = new InterlockXNotifiedAttrib();
	att_list.push_back(interlock_xnotified);

	//	Attribute : InterlockZNotified
	InterlockZNotifiedAttrib	*interlock_znotified = new InterlockZNotifiedAttrib();
	att_list.push_back(interlock_znotified);

	//	Attribute : InterlockAttnNotified
	InterlockAttnNotifiedAttrib	*interlock_attn_notified = new InterlockAttnNotifiedAttrib();
	att_list.push_back(interlock_attn_notified);

	//	Attribute : InterlockADCPreFilterNotified
	InterlockADCPreFilterNotifiedAttrib	*interlock_adcpre_filter_notified = new InterlockADCPreFilterNotifiedAttrib();
	att_list.push_back(interlock_adcpre_filter_notified);

	//	Attribute : InterlockADCPostFilterNotified
	InterlockADCPostFilterNotifiedAttrib	*interlock_adcpost_filter_notified = new InterlockADCPostFilterNotifiedAttrib();
	att_list.push_back(interlock_adcpost_filter_notified);

	//	Attribute : XLow
	XLowAttrib	*xlow = new XLowAttrib();
	Tango::UserDefaultAttrProp	xlow_prop;
	xlow_prop.set_label("X Low Int. Thres.");
	xlow_prop.set_unit("mm");
	xlow_prop.set_format("%8.4f");
	xlow_prop.set_description("Lower limit of the X position interlock threshold in mm");
	xlow->set_default_properties(xlow_prop);
	att_list.push_back(xlow);

	//	Attribute : XHigh
	XHighAttrib	*xhigh = new XHighAttrib();
	Tango::UserDefaultAttrProp	xhigh_prop;
	xhigh_prop.set_label("X High Int. Thres.");
	xhigh_prop.set_unit("mm");
	xhigh_prop.set_format("%8.4f");
	xhigh_prop.set_description("Upper limit of the X position interlock threshold in mm");
	xhigh->set_default_properties(xhigh_prop);
	att_list.push_back(xhigh);

	//	Attribute : ZLow
	ZLowAttrib	*zlow = new ZLowAttrib();
	Tango::UserDefaultAttrProp	zlow_prop;
	zlow_prop.set_label("Z Low Int. Thres.");
	zlow_prop.set_unit("mm");
	zlow_prop.set_format("%8.4f");
	zlow_prop.set_description("Lower limit of the Z position interlock threshold in mm");
	zlow->set_default_properties(zlow_prop);
	att_list.push_back(zlow);

	//	Attribute : ZHigh
	ZHighAttrib	*zhigh = new ZHighAttrib();
	Tango::UserDefaultAttrProp	zhigh_prop;
	zhigh_prop.set_label("Z High Int. Thres.");
	zhigh_prop.set_unit("mm");
	zhigh_prop.set_format("%8.4f");
	zhigh_prop.set_description("Upper limit of the Z position interlock threshold in mm");
	zhigh->set_default_properties(zhigh_prop);
	att_list.push_back(zhigh);

	//	Attribute : AutoSwitchingEnabled
	AutoSwitchingEnabledAttrib	*auto_switching_enabled = new AutoSwitchingEnabledAttrib();
	Tango::UserDefaultAttrProp	auto_switching_enabled_prop;
	auto_switching_enabled_prop.set_label("Auto switching enabled");
	auto_switching_enabled_prop.set_unit("n/a");
	auto_switching_enabled_prop.set_standard_unit("n/a");
	auto_switching_enabled_prop.set_display_unit("n/a");
	auto_switching_enabled_prop.set_description("Auto switching status (true=enabled, false=disabled)");
	auto_switching_enabled->set_default_properties(auto_switching_enabled_prop);
	att_list.push_back(auto_switching_enabled);

	//	Attribute : Switches
	SwitchesAttrib	*switches = new SwitchesAttrib();
	Tango::UserDefaultAttrProp	switches_prop;
	switches_prop.set_label("Switches");
	switches_prop.set_unit("a.u.");
	switches_prop.set_format("%3d");
	switches_prop.set_max_value("255");
	switches_prop.set_min_value("0");
	switches_prop.set_description("Switches selection. Must be in [0, 15] or 255 for auto-switching.");
	switches->set_default_properties(switches_prop);
	switches->set_memorized();
	switches->set_memorized_init(true);
	att_list.push_back(switches);

	//	Attribute : ExternalSwitching
	ExternalSwitchingAttrib	*external_switching = new ExternalSwitchingAttrib();
	Tango::UserDefaultAttrProp	external_switching_prop;
	external_switching_prop.set_label("External Switching");
	external_switching_prop.set_description("Determines whether the ADC source switching  will be triggered by the \ninternally (false) or be the externally (true) by the machine clock.");
	external_switching->set_default_properties(external_switching_prop);
	external_switching->set_memorized();
	external_switching->set_memorized_init(true);
	att_list.push_back(external_switching);

	//	Attribute : SwitchingDelay
	SwitchingDelayAttrib	*switching_delay = new SwitchingDelayAttrib();
	Tango::UserDefaultAttrProp	switching_delay_prop;
	switching_delay_prop.set_label("Switching Delay");
	switching_delay_prop.set_unit("a.u.");
	switching_delay_prop.set_display_unit("%6d");
	switching_delay_prop.set_min_value("0");
	switching_delay_prop.set_description("Delay for the ADC source switching.");
	switching_delay->set_default_properties(switching_delay_prop);
	switching_delay->set_memorized();
	switching_delay->set_memorized_init(true);
	att_list.push_back(switching_delay);

	//	Attribute : OffsetTune
	OffsetTuneAttrib	*offset_tune = new OffsetTuneAttrib();
	Tango::UserDefaultAttrProp	offset_tune_prop;
	offset_tune_prop.set_label("OffsetTune");
	offset_tune_prop.set_unit("x 40Hz");
	offset_tune_prop.set_format("%3d");
	offset_tune_prop.set_max_value("500");
	offset_tune_prop.set_min_value("-500");
	offset_tune_prop.set_description("Determines the pll offset to tune the BPM. \nThe unit is ~40Hz.");
	offset_tune->set_default_properties(offset_tune_prop);
	offset_tune->set_memorized();
	offset_tune->set_memorized_init(true);
	att_list.push_back(offset_tune);

	//	Attribute : CompensateTune
	CompensateTuneAttrib	*compensate_tune = new CompensateTuneAttrib();
	Tango::UserDefaultAttrProp	compensate_tune_prop;
	compensate_tune_prop.set_label("Compensate Tune");
	compensate_tune_prop.set_description("Determines whether single or double offset tune will be employed. \nDefault value is true, which means that the the double offset tune is employed.");
	compensate_tune->set_default_properties(compensate_tune_prop);
	compensate_tune->set_memorized();
	compensate_tune->set_memorized_init(true);
	att_list.push_back(compensate_tune);

	//	Attribute : DSCMode
	DSCModeAttrib	*dscmode = new DSCModeAttrib();
	Tango::UserDefaultAttrProp	dscmode_prop;
	dscmode_prop.set_label("DSC Mode");
	dscmode_prop.set_unit("[0:OFF, 1:UNITY, 2:AUTO]");
	dscmode_prop.set_format("%1d");
	dscmode_prop.set_max_value("2");
	dscmode_prop.set_min_value("0");
	dscmode_prop.set_description("Digital Signal Conditioning mode\n0:OFF - 1:UNITY - 2:AUTO");
	dscmode->set_default_properties(dscmode_prop);
	att_list.push_back(dscmode);

	//	Attribute : AGCEnabled
	AGCEnabledAttrib	*agcenabled = new AGCEnabledAttrib();
	Tango::UserDefaultAttrProp	agcenabled_prop;
	agcenabled_prop.set_label("AGC");
	agcenabled_prop.set_description("Enables/disables the Automatic Gain Control");
	agcenabled->set_default_properties(agcenabled_prop);
	agcenabled->set_memorized();
	agcenabled->set_memorized_init(true);
	att_list.push_back(agcenabled);

	//	Attribute : Gain
	GainAttrib	*gain = new GainAttrib();
	Tango::UserDefaultAttrProp	gain_prop;
	gain_prop.set_label("Gain");
	gain_prop.set_unit("dBm");
	gain_prop.set_standard_unit("dBm");
	gain_prop.set_display_unit("dBm");
	gain_prop.set_format("%2.0f");
	gain_prop.set_max_value("0");
	gain_prop.set_min_value("-60");
	gain_prop.set_description("The Libera input gain. \nCan't be change while the AGC is active");
	gain->set_default_properties(gain_prop);
	att_list.push_back(gain);

	//	Attribute : HasMAFSupport
	HasMAFSupportAttrib	*has_mafsupport = new HasMAFSupportAttrib();
	Tango::UserDefaultAttrProp	has_mafsupport_prop;
	has_mafsupport_prop.set_label("Moving Average Filter Support");
	has_mafsupport_prop.set_description("<true> if FGPA design with MAF support installed on Libera, <false> otherwise");
	has_mafsupport->set_default_properties(has_mafsupport_prop);
	att_list.push_back(has_mafsupport);

	//	Attribute : MAFLength
	MAFLengthAttrib	*maflength = new MAFLengthAttrib();
	Tango::UserDefaultAttrProp	maflength_prop;
	maflength_prop.set_label("Moving Average Filter Length");
	maflength_prop.set_unit("ADC samples");
	maflength_prop.set_format("%6d");
	maflength_prop.set_min_value("1");
	maflength_prop.set_description("MAF Delay and MAF Length are two\nparameters, added to adjustable\nDDC design. They are used to\ndetermine the position and the length\nof the acquisition window according\nto the partial fill of the accelerator.");
	maflength->set_default_properties(maflength_prop);
	maflength->set_memorized();
	maflength->set_memorized_init(true);
	att_list.push_back(maflength);

	//	Attribute : MAFDelay
	MAFDelayAttrib	*mafdelay = new MAFDelayAttrib();
	Tango::UserDefaultAttrProp	mafdelay_prop;
	mafdelay_prop.set_label("Moving Average Filter Delay");
	mafdelay_prop.set_unit("ADC samples");
	mafdelay_prop.set_format("%6d");
	mafdelay_prop.set_min_value("0");
	mafdelay_prop.set_description("MAF Delay and MAF Length are two\nparameters, added to adjustable\nDDC design. They are used to\ndetermine the position and the length\nof the acquisition window according\nto the partial fill of the accelerator.\n");
	mafdelay->set_default_properties(mafdelay_prop);
	mafdelay->set_memorized();
	mafdelay->set_memorized_init(true);
	att_list.push_back(mafdelay);

	//	Attribute : MachineTime
	MachineTimeAttrib	*machine_time = new MachineTimeAttrib();
	Tango::UserDefaultAttrProp	machine_time_prop;
	machine_time_prop.set_label("Machine Time");
	machine_time_prop.set_unit("a.u.");
	machine_time_prop.set_format("%9d");
	machine_time_prop.set_min_value("0");
	machine_time_prop.set_description("Machine Time value to be applied on the Libera when the SetTimeOnNextTrigger command is executed");
	machine_time->set_default_properties(machine_time_prop);
	att_list.push_back(machine_time);

	//	Attribute : TimePhase
	TimePhaseAttrib	*time_phase = new TimePhaseAttrib();
	Tango::UserDefaultAttrProp	time_phase_prop;
	time_phase_prop.set_label("Machine Time Phase");
	time_phase_prop.set_unit("a.u.");
	time_phase_prop.set_format("%6d");
	time_phase_prop.set_description("The Machine Time Phase");
	time_phase->set_default_properties(time_phase_prop);
	time_phase->set_memorized();
	time_phase->set_memorized_init(true);
	att_list.push_back(time_phase);

	//	Attribute : SystemTime
	SystemTimeAttrib	*system_time = new SystemTimeAttrib();
	Tango::UserDefaultAttrProp	system_time_prop;
	system_time_prop.set_label("System Time");
	system_time_prop.set_unit("secs since 1/1/1970");
	system_time_prop.set_format("%10d");
	system_time_prop.set_min_value("2000000000");
	system_time_prop.set_description("System Time value to be applied on the Libera when the SetTimeOnNextTrigger command is executed\nUnit is num of secs since 1/1/1970 (Unix system time reference)");
	system_time->set_default_properties(system_time_prop);
	att_list.push_back(system_time);

	//	Attribute : SCPLLStatus
	SCPLLStatusAttrib	*scpllstatus = new SCPLLStatusAttrib();
	Tango::UserDefaultAttrProp	scpllstatus_prop;
	scpllstatus_prop.set_label("SC PLL Locked");
	scpllstatus_prop.set_format("%8d");
	scpllstatus_prop.set_description("The SC PLL lock status");
	scpllstatus->set_default_properties(scpllstatus_prop);
	att_list.push_back(scpllstatus);

	//	Attribute : MCPLLStatus
	MCPLLStatusAttrib	*mcpllstatus = new MCPLLStatusAttrib();
	Tango::UserDefaultAttrProp	mcpllstatus_prop;
	mcpllstatus_prop.set_label("MC PLL Locked");
	mcpllstatus_prop.set_format("%8d");
	mcpllstatus_prop.set_description("The MC PLL lock status");
	mcpllstatus->set_default_properties(mcpllstatus_prop);
	att_list.push_back(mcpllstatus);

	//	Attribute : HWTemperature
	HWTemperatureAttrib	*hwtemperature = new HWTemperatureAttrib();
	Tango::UserDefaultAttrProp	hwtemperature_prop;
	hwtemperature_prop.set_label("HW Temp.");
	hwtemperature_prop.set_unit("deg.C");
	hwtemperature_prop.set_format("%8d");
	hwtemperature_prop.set_max_alarm("50");
	hwtemperature_prop.set_description("The current Libera hardware temperature");
	hwtemperature->set_default_properties(hwtemperature_prop);
	att_list.push_back(hwtemperature);

	//	Attribute : Fan1Speed
	Fan1SpeedAttrib	*fan1_speed = new Fan1SpeedAttrib();
	Tango::UserDefaultAttrProp	fan1_speed_prop;
	fan1_speed_prop.set_label("Fan.1");
	fan1_speed_prop.set_unit("rpm");
	fan1_speed_prop.set_format("%8d");
	fan1_speed_prop.set_min_alarm("1000");
	fan1_speed_prop.set_description("The current rotation speed of the first hardware cooling fan");
	fan1_speed->set_default_properties(fan1_speed_prop);
	att_list.push_back(fan1_speed);

	//	Attribute : Fan2Speed
	Fan2SpeedAttrib	*fan2_speed = new Fan2SpeedAttrib();
	Tango::UserDefaultAttrProp	fan2_speed_prop;
	fan2_speed_prop.set_label("Fan.2");
	fan2_speed_prop.set_unit("rpm");
	fan2_speed_prop.set_format("%8d");
	fan2_speed_prop.set_min_alarm("1000");
	fan2_speed_prop.set_description("The current rotation speed of the second hardware cooling fan");
	fan2_speed->set_default_properties(fan2_speed_prop);
	att_list.push_back(fan2_speed);

	//	Attribute : Incoherence
	IncoherenceAttrib	*incoherence = new IncoherenceAttrib();
	Tango::UserDefaultAttrProp	incoherence_prop;
	incoherence_prop.set_label("Incoherence");
	incoherence_prop.set_unit("a.u.");
	incoherence_prop.set_format("%6.4f");
	incoherence_prop.set_description("Result of the incoherence calculation. Am alarm will be set \non the attribute when an incoherence was detected.");
	incoherence->set_default_properties(incoherence_prop);
	att_list.push_back(incoherence);

	//	Attribute : RefIncoherence
	RefIncoherenceAttrib	*ref_incoherence = new RefIncoherenceAttrib();
	Tango::UserDefaultAttrProp	ref_incoherence_prop;
	ref_incoherence_prop.set_label("Ref. Incoherence");
	ref_incoherence_prop.set_unit("a.u.");
	ref_incoherence_prop.set_format("%6.4f");
	ref_incoherence_prop.set_description("The reference incoherence value registered with the command \nSetReferenceIncoherence. The reference is used to calculate the\nalarm with the MaxIncoherenceDrift.");
	ref_incoherence->set_default_properties(ref_incoherence_prop);
	att_list.push_back(ref_incoherence);

	//	Attribute : MaxIncoherence
	MaxIncoherenceAttrib	*max_incoherence = new MaxIncoherenceAttrib();
	Tango::UserDefaultAttrProp	max_incoherence_prop;
	max_incoherence_prop.set_label("Max. incoherence");
	max_incoherence_prop.set_unit("a.u.");
	max_incoherence_prop.set_format("%6.4f");
	max_incoherence_prop.set_description("Maximum incoherence value. Used to create an alarm on\nthe Incoherence attribute.");
	max_incoherence->set_default_properties(max_incoherence_prop);
	max_incoherence->set_memorized();
	max_incoherence->set_memorized_init(true);
	att_list.push_back(max_incoherence);

	//	Attribute : MaxIncoherenceDrift
	MaxIncoherenceDriftAttrib	*max_incoherence_drift = new MaxIncoherenceDriftAttrib();
	Tango::UserDefaultAttrProp	max_incoherence_drift_prop;
	max_incoherence_drift_prop.set_label("Max. Incoherence Drift ");
	max_incoherence_drift_prop.set_unit("a.u.");
	max_incoherence_drift_prop.set_format("%6.4f");
	max_incoherence_drift_prop.set_description("Maximum incoherence drift value. Used to create an alarm on\nthe Incoherence attribute.");
	max_incoherence_drift->set_default_properties(max_incoherence_drift_prop);
	max_incoherence_drift->set_memorized();
	max_incoherence_drift->set_memorized_init(true);
	att_list.push_back(max_incoherence_drift);

	//	Attribute : UpTime
	UpTimeAttrib	*up_time = new UpTimeAttrib();
	Tango::UserDefaultAttrProp	up_time_prop;
	up_time_prop.set_label("Uptime");
	up_time_prop.set_unit("secs");
	up_time_prop.set_format("%8d");
	up_time_prop.set_description("Number of seconds since system boot on the host running this TANGO device");
	up_time->set_default_properties(up_time_prop);
	att_list.push_back(up_time);

	//	Attribute : CpuUsage
	CpuUsageAttrib	*cpu_usage = new CpuUsageAttrib();
	Tango::UserDefaultAttrProp	cpu_usage_prop;
	cpu_usage_prop.set_label("CPU Usage");
	cpu_usage_prop.set_unit("%");
	cpu_usage_prop.set_format("%8d");
	cpu_usage_prop.set_description("CPU usage on the host running this TANGO device");
	cpu_usage->set_default_properties(cpu_usage_prop);
	att_list.push_back(cpu_usage);

	//	Attribute : FreeMemory
	FreeMemoryAttrib	*free_memory = new FreeMemoryAttrib();
	Tango::UserDefaultAttrProp	free_memory_prop;
	free_memory_prop.set_label("Free Mem.");
	free_memory_prop.set_unit("bytes");
	free_memory_prop.set_format("%8d");
	free_memory_prop.set_description("Amount of free memory on the host running this TANGO device");
	free_memory->set_default_properties(free_memory_prop);
	att_list.push_back(free_memory);

	//	Attribute : RamFsUsage
	RamFsUsageAttrib	*ram_fs_usage = new RamFsUsageAttrib();
	Tango::UserDefaultAttrProp	ram_fs_usage_prop;
	ram_fs_usage_prop.set_label("Ram-fs Usage");
	ram_fs_usage_prop.set_unit("bytes");
	ram_fs_usage_prop.set_format("%8d");
	ram_fs_usage_prop.set_description("Amount of ram-fs allocated bytes on the host running this TANGO device ");
	ram_fs_usage->set_default_properties(ram_fs_usage_prop);
	att_list.push_back(ram_fs_usage);

	//	Attribute : UseLiberaSAData
	UseLiberaSADataAttrib	*use_libera_sadata = new UseLiberaSADataAttrib();
	Tango::UserDefaultAttrProp	use_libera_sadata_prop;
	use_libera_sadata_prop.set_label("Use Libera SA Data");
	use_libera_sadata_prop.set_description("If set to true, the X & Z SA postions are retreived from the Libera FPGA.\nOtherwise, they are computed by the Tango device using the button values.\n ");
	use_libera_sadata->set_default_properties(use_libera_sadata_prop);
	use_libera_sadata->set_disp_level(Tango::EXPERT);
	use_libera_sadata->set_memorized();
	use_libera_sadata->set_memorized_init(true);
	att_list.push_back(use_libera_sadata);

	//	Attribute : XPosDD
	XPosDDAttrib	*xpos_dd = new XPosDDAttrib();
	Tango::UserDefaultAttrProp	xpos_dd_prop;
	xpos_dd_prop.set_label("DD X Pos.");
	xpos_dd_prop.set_unit("mm");
	xpos_dd_prop.set_standard_unit("mm");
	xpos_dd_prop.set_display_unit("mm");
	xpos_dd_prop.set_format("%8.4f");
	xpos_dd_prop.set_description("Turn by turn data: X Pos.");
	xpos_dd->set_default_properties(xpos_dd_prop);
	att_list.push_back(xpos_dd);

	//	Attribute : ZPosDD
	ZPosDDAttrib	*zpos_dd = new ZPosDDAttrib();
	Tango::UserDefaultAttrProp	zpos_dd_prop;
	zpos_dd_prop.set_label("DD Z Pos.");
	zpos_dd_prop.set_unit("mm");
	zpos_dd_prop.set_standard_unit("mm");
	zpos_dd_prop.set_display_unit("mm");
	zpos_dd_prop.set_format("%8.4f");
	zpos_dd_prop.set_description("Turn by turn data: Z Pos.");
	zpos_dd->set_default_properties(zpos_dd_prop);
	att_list.push_back(zpos_dd);

	//	Attribute : QuadDD
	QuadDDAttrib	*quad_dd = new QuadDDAttrib();
	Tango::UserDefaultAttrProp	quad_dd_prop;
	quad_dd_prop.set_label("DD Quad");
	quad_dd_prop.set_unit("a.u.");
	quad_dd_prop.set_standard_unit("a.u.");
	quad_dd_prop.set_display_unit("a.u.");
	quad_dd_prop.set_format("%8.4f");
	quad_dd_prop.set_description("Turn by turn data: Quad");
	quad_dd->set_default_properties(quad_dd_prop);
	att_list.push_back(quad_dd);

	//	Attribute : SumDD
	SumDDAttrib	*sum_dd = new SumDDAttrib();
	Tango::UserDefaultAttrProp	sum_dd_prop;
	sum_dd_prop.set_label("DD Sum");
	sum_dd_prop.set_unit("mm");
	sum_dd_prop.set_standard_unit("mm");
	sum_dd_prop.set_display_unit("mm");
	sum_dd_prop.set_format("%10.0f");
	sum_dd_prop.set_description("Turn by turn data: Sum");
	sum_dd->set_default_properties(sum_dd_prop);
	att_list.push_back(sum_dd);

	//	Attribute : VaDD
	VaDDAttrib	*va_dd = new VaDDAttrib();
	Tango::UserDefaultAttrProp	va_dd_prop;
	va_dd_prop.set_label("DD Va");
	va_dd_prop.set_unit("a.u.");
	va_dd_prop.set_standard_unit("a.u.");
	va_dd_prop.set_display_unit("a.u.");
	va_dd_prop.set_format("%10.0f");
	va_dd_prop.set_description("Turn by turn data: Va");
	va_dd->set_default_properties(va_dd_prop);
	att_list.push_back(va_dd);

	//	Attribute : VbDD
	VbDDAttrib	*vb_dd = new VbDDAttrib();
	Tango::UserDefaultAttrProp	vb_dd_prop;
	vb_dd_prop.set_label("DD Vb");
	vb_dd_prop.set_unit("a.u.");
	vb_dd_prop.set_standard_unit("a.u.");
	vb_dd_prop.set_display_unit("a.u.");
	vb_dd_prop.set_format("%10.0f");
	vb_dd_prop.set_description("Turn by turn data: Vb");
	vb_dd->set_default_properties(vb_dd_prop);
	att_list.push_back(vb_dd);

	//	Attribute : VcDD
	VcDDAttrib	*vc_dd = new VcDDAttrib();
	Tango::UserDefaultAttrProp	vc_dd_prop;
	vc_dd_prop.set_label("DD Vc");
	vc_dd_prop.set_unit("a.u.");
	vc_dd_prop.set_standard_unit("a.u.");
	vc_dd_prop.set_display_unit("a.u.");
	vc_dd_prop.set_format("%10.0f");
	vc_dd_prop.set_description("Turn by turn data: Vc");
	vc_dd->set_default_properties(vc_dd_prop);
	att_list.push_back(vc_dd);

	//	Attribute : VdDD
	VdDDAttrib	*vd_dd = new VdDDAttrib();
	Tango::UserDefaultAttrProp	vd_dd_prop;
	vd_dd_prop.set_label("DD Vd");
	vd_dd_prop.set_unit("a.u.");
	vd_dd_prop.set_standard_unit("a.u.");
	vd_dd_prop.set_display_unit("a.u.");
	vd_dd_prop.set_format("%10.0f");
	vd_dd_prop.set_description("Turn by turn data: Vd");
	vd_dd->set_default_properties(vd_dd_prop);
	att_list.push_back(vd_dd);

	//	Attribute : XPosSAHistory
	XPosSAHistoryAttrib	*xpos_sahistory = new XPosSAHistoryAttrib();
	Tango::UserDefaultAttrProp	xpos_sahistory_prop;
	xpos_sahistory_prop.set_label("SA X Pos. History");
	xpos_sahistory_prop.set_unit("mm");
	xpos_sahistory_prop.set_standard_unit("mm");
	xpos_sahistory_prop.set_display_unit("mm");
	xpos_sahistory_prop.set_format("%8.4f");
	xpos_sahistory_prop.set_description("Slow Acquisition: SA X Pos. History");
	xpos_sahistory->set_default_properties(xpos_sahistory_prop);
	att_list.push_back(xpos_sahistory);

	//	Attribute : ZPosSAHistory
	ZPosSAHistoryAttrib	*zpos_sahistory = new ZPosSAHistoryAttrib();
	Tango::UserDefaultAttrProp	zpos_sahistory_prop;
	zpos_sahistory_prop.set_label("SA Z Pos. History");
	zpos_sahistory_prop.set_unit("mm");
	zpos_sahistory_prop.set_standard_unit("mm");
	zpos_sahistory_prop.set_display_unit("mm");
	zpos_sahistory_prop.set_format("%8.4f");
	zpos_sahistory_prop.set_description("Slow Acquisition: SA Z Pos. History");
	zpos_sahistory->set_default_properties(zpos_sahistory_prop);
	att_list.push_back(zpos_sahistory);

	//	Attribute : SumSAHistory
	SumSAHistoryAttrib	*sum_sahistory = new SumSAHistoryAttrib();
	Tango::UserDefaultAttrProp	sum_sahistory_prop;
	sum_sahistory_prop.set_label("SA Sum. History");
	sum_sahistory_prop.set_unit("a.u.");
	sum_sahistory_prop.set_standard_unit("a.u.");
	sum_sahistory_prop.set_display_unit("a.u.");
	sum_sahistory_prop.set_format("%8.4f");
	sum_sahistory_prop.set_description("Slow Acquisition: SA Sum Pos. History");
	sum_sahistory->set_default_properties(sum_sahistory_prop);
	att_list.push_back(sum_sahistory);

	//	Attribute : XPosPM
	XPosPMAttrib	*xpos_pm = new XPosPMAttrib();
	Tango::UserDefaultAttrProp	xpos_pm_prop;
	xpos_pm_prop.set_label("X Post Mortem Data");
	xpos_pm_prop.set_unit("mm");
	xpos_pm_prop.set_standard_unit("mm");
	xpos_pm_prop.set_display_unit("mm");
	xpos_pm_prop.set_format("%8.4f");
	xpos_pm_prop.set_description("Post Mortem : X pos.");
	xpos_pm->set_default_properties(xpos_pm_prop);
	att_list.push_back(xpos_pm);

	//	Attribute : ZPosPM
	ZPosPMAttrib	*zpos_pm = new ZPosPMAttrib();
	Tango::UserDefaultAttrProp	zpos_pm_prop;
	zpos_pm_prop.set_label("Z Post Mortem Data");
	zpos_pm_prop.set_unit("mm");
	zpos_pm_prop.set_standard_unit("mm");
	zpos_pm_prop.set_display_unit("mm");
	zpos_pm_prop.set_format("%8.4f");
	zpos_pm_prop.set_description("Post Mortem : Z pos.");
	zpos_pm->set_default_properties(zpos_pm_prop);
	att_list.push_back(zpos_pm);

	//	Attribute : QuadPM
	QuadPMAttrib	*quad_pm = new QuadPMAttrib();
	Tango::UserDefaultAttrProp	quad_pm_prop;
	quad_pm_prop.set_label("Quad Post Mortem Data");
	quad_pm_prop.set_unit("a.u");
	quad_pm_prop.set_standard_unit("a.u");
	quad_pm_prop.set_display_unit("a.u");
	quad_pm_prop.set_format("%8.4f");
	quad_pm_prop.set_description("Post Mortem : Quad");
	quad_pm->set_default_properties(quad_pm_prop);
	att_list.push_back(quad_pm);

	//	Attribute : SumPM
	SumPMAttrib	*sum_pm = new SumPMAttrib();
	Tango::UserDefaultAttrProp	sum_pm_prop;
	sum_pm_prop.set_label("Sum Post Mortem Data");
	sum_pm_prop.set_unit("a.u");
	sum_pm_prop.set_standard_unit("a.u");
	sum_pm_prop.set_display_unit("a.u");
	sum_pm_prop.set_format("%10.0f");
	sum_pm_prop.set_description("Post Mortem : Sum");
	sum_pm->set_default_properties(sum_pm_prop);
	att_list.push_back(sum_pm);

	//	Attribute : VaPM
	VaPMAttrib	*va_pm = new VaPMAttrib();
	Tango::UserDefaultAttrProp	va_pm_prop;
	va_pm_prop.set_label("Va Post Mortem Data");
	va_pm_prop.set_unit("a.u.");
	va_pm_prop.set_standard_unit("a.u.");
	va_pm_prop.set_display_unit("a.u.");
	va_pm_prop.set_format("%10.0f");
	va_pm_prop.set_description("Post Mortem : Va");
	va_pm->set_default_properties(va_pm_prop);
	att_list.push_back(va_pm);

	//	Attribute : VbPM
	VbPMAttrib	*vb_pm = new VbPMAttrib();
	Tango::UserDefaultAttrProp	vb_pm_prop;
	vb_pm_prop.set_label("Vb Post Mortem Data");
	vb_pm_prop.set_unit("a.u.");
	vb_pm_prop.set_standard_unit("a.u.");
	vb_pm_prop.set_display_unit("a.u.");
	vb_pm_prop.set_format("%10.0f");
	vb_pm_prop.set_description("Post Mortem : Vb");
	vb_pm->set_default_properties(vb_pm_prop);
	att_list.push_back(vb_pm);

	//	Attribute : VcPM
	VcPMAttrib	*vc_pm = new VcPMAttrib();
	Tango::UserDefaultAttrProp	vc_pm_prop;
	vc_pm_prop.set_label("Post Mortem : Vc");
	vc_pm_prop.set_unit("a.u.");
	vc_pm_prop.set_standard_unit("a.u.");
	vc_pm_prop.set_display_unit("a.u.");
	vc_pm_prop.set_format("%10.0f");
	vc_pm_prop.set_description("Post Mortem : Vc");
	vc_pm->set_default_properties(vc_pm_prop);
	att_list.push_back(vc_pm);

	//	Attribute : VdPM
	VdPMAttrib	*vd_pm = new VdPMAttrib();
	Tango::UserDefaultAttrProp	vd_pm_prop;
	vd_pm_prop.set_label("Vd Post Mortem Data");
	vd_pm_prop.set_unit("a.u.");
	vd_pm_prop.set_standard_unit("a.u.");
	vd_pm_prop.set_display_unit("a.u.");
	vd_pm_prop.set_format("%10.0f");
	vd_pm_prop.set_description("Post Mortem : Vd");
	vd_pm->set_default_properties(vd_pm_prop);
	att_list.push_back(vd_pm);

	//	Attribute : ADCChannelA
	ADCChannelAAttrib	*adcchannel_a = new ADCChannelAAttrib();
	Tango::UserDefaultAttrProp	adcchannel_a_prop;
	adcchannel_a_prop.set_label("ADC Channel A");
	adcchannel_a_prop.set_unit("a.u");
	adcchannel_a_prop.set_standard_unit("a.u");
	adcchannel_a_prop.set_display_unit("a.u");
	adcchannel_a_prop.set_format("%8d");
	adcchannel_a_prop.set_description("ADC values for pickup A");
	adcchannel_a->set_default_properties(adcchannel_a_prop);
	att_list.push_back(adcchannel_a);

	//	Attribute : ADCChannelB
	ADCChannelBAttrib	*adcchannel_b = new ADCChannelBAttrib();
	Tango::UserDefaultAttrProp	adcchannel_b_prop;
	adcchannel_b_prop.set_label("ADC Channel B");
	adcchannel_b_prop.set_unit("a.u");
	adcchannel_b_prop.set_standard_unit("a.u");
	adcchannel_b_prop.set_display_unit("a.u");
	adcchannel_b_prop.set_format("%8d");
	adcchannel_b_prop.set_description("ADC values for pickup B");
	adcchannel_b->set_default_properties(adcchannel_b_prop);
	att_list.push_back(adcchannel_b);

	//	Attribute : ADCChannelC
	ADCChannelCAttrib	*adcchannel_c = new ADCChannelCAttrib();
	Tango::UserDefaultAttrProp	adcchannel_c_prop;
	adcchannel_c_prop.set_label("ADC Channel C");
	adcchannel_c_prop.set_unit("a.u");
	adcchannel_c_prop.set_standard_unit("a.u");
	adcchannel_c_prop.set_display_unit("a.u");
	adcchannel_c_prop.set_format("%8d");
	adcchannel_c_prop.set_description("ADC values for pickup C");
	adcchannel_c->set_default_properties(adcchannel_c_prop);
	att_list.push_back(adcchannel_c);

	//	Attribute : ADCChannelD
	ADCChannelDAttrib	*adcchannel_d = new ADCChannelDAttrib();
	Tango::UserDefaultAttrProp	adcchannel_d_prop;
	adcchannel_d_prop.set_label("ADC Channel D");
	adcchannel_d_prop.set_unit("a.u");
	adcchannel_d_prop.set_standard_unit("a.u");
	adcchannel_d_prop.set_display_unit("a.u");
	adcchannel_d_prop.set_format("%8d");
	adcchannel_d_prop.set_description("ADC values for pickup D");
	adcchannel_d->set_default_properties(adcchannel_d_prop);
	att_list.push_back(adcchannel_d);

	//	Attribute : IaDD
	IaDDAttrib	*ia_dd = new IaDDAttrib();
	Tango::UserDefaultAttrProp	ia_dd_prop;
	ia_dd_prop.set_label("DD Ia");
	ia_dd_prop.set_unit("a.u.");
	ia_dd_prop.set_format("%10.0f");
	ia_dd_prop.set_description("Turn by turn data: Ia");
	ia_dd->set_default_properties(ia_dd_prop);
	att_list.push_back(ia_dd);

	//	Attribute : IbDD
	IbDDAttrib	*ib_dd = new IbDDAttrib();
	Tango::UserDefaultAttrProp	ib_dd_prop;
	ib_dd_prop.set_label("DD Ib");
	ib_dd_prop.set_unit("a.u.");
	ib_dd_prop.set_format("%10.0f");
	ib_dd_prop.set_description("Turn by turn data: Ib");
	ib_dd->set_default_properties(ib_dd_prop);
	att_list.push_back(ib_dd);

	//	Attribute : IcDD
	IcDDAttrib	*ic_dd = new IcDDAttrib();
	Tango::UserDefaultAttrProp	ic_dd_prop;
	ic_dd_prop.set_label("DD Ic");
	ic_dd_prop.set_unit("a.u.");
	ic_dd_prop.set_format("%10.0f");
	ic_dd_prop.set_description("Turn by turn data: Ic");
	ic_dd->set_default_properties(ic_dd_prop);
	att_list.push_back(ic_dd);

	//	Attribute : IdDD
	IdDDAttrib	*id_dd = new IdDDAttrib();
	Tango::UserDefaultAttrProp	id_dd_prop;
	id_dd_prop.set_label("DD Id");
	id_dd_prop.set_unit("a.u.");
	id_dd_prop.set_format("%10.0f");
	id_dd_prop.set_description("Turn by turn data: Id");
	id_dd->set_default_properties(id_dd_prop);
	att_list.push_back(id_dd);

	//	Attribute : QaDD
	QaDDAttrib	*qa_dd = new QaDDAttrib();
	Tango::UserDefaultAttrProp	qa_dd_prop;
	qa_dd_prop.set_label("DD Qa");
	qa_dd_prop.set_unit("a.u.");
	qa_dd_prop.set_format("%10.0f");
	qa_dd_prop.set_description("Turn by turn data: Qa");
	qa_dd->set_default_properties(qa_dd_prop);
	att_list.push_back(qa_dd);

	//	Attribute : QbDD
	QbDDAttrib	*qb_dd = new QbDDAttrib();
	Tango::UserDefaultAttrProp	qb_dd_prop;
	qb_dd_prop.set_label("DD Qb");
	qb_dd_prop.set_unit("a.u.");
	qb_dd_prop.set_format("%10.0f");
	qb_dd_prop.set_description("Turn by turn data: Qb");
	qb_dd->set_default_properties(qb_dd_prop);
	att_list.push_back(qb_dd);

	//	Attribute : QcDD
	QcDDAttrib	*qc_dd = new QcDDAttrib();
	Tango::UserDefaultAttrProp	qc_dd_prop;
	qc_dd_prop.set_label("DD Qc");
	qc_dd_prop.set_unit("a.u.");
	qc_dd_prop.set_format("%10.0f");
	qc_dd_prop.set_description("Turn by turn data: Qc");
	qc_dd->set_default_properties(qc_dd_prop);
	att_list.push_back(qc_dd);

	//	Attribute : QdDD
	QdDDAttrib	*qd_dd = new QdDDAttrib();
	Tango::UserDefaultAttrProp	qd_dd_prop;
	qd_dd_prop.set_label("DD Qd");
	qd_dd_prop.set_unit("a.u.");
	qd_dd_prop.set_format("%10.0f");
	qd_dd_prop.set_description("Turn by turn data: Qd");
	qd_dd->set_default_properties(qd_dd_prop);
	att_list.push_back(qd_dd);

	//	Attribute : UserData
	UserDataAttrib	*user_data = new UserDataAttrib();
	Tango::UserDefaultAttrProp	user_data_prop;
	user_data_prop.set_description("User defined data");
	user_data->set_default_properties(user_data_prop);
	att_list.push_back(user_data);

	//	Attribute : InterlockConfiguration
	InterlockConfigurationAttrib	*interlock_configuration = new InterlockConfigurationAttrib();
	Tango::UserDefaultAttrProp	interlock_configuration_prop;
	interlock_configuration_prop.set_description("The current interlock configuration. The vector mapping is the follwoing:\n[0] Mode : [0]: disabled, [1]: enabled, [3]: enabled with gain dependency\n[1] X low threshold in mm\n[2] X high threshold in mm\n[3] Z threshold low in mm \n[4] Z high threshold in mm\n[5] Overflow limit (ADC threshold)\n[6] Overflow duration (num of overloaded ADC samples before raising interlock)\n[7] Gain limit in dBm  (no interlock under this threshold) ");
	interlock_configuration->set_default_properties(interlock_configuration_prop);
	att_list.push_back(interlock_configuration);

	//	Attribute : logs
	logsAttrib	*logs = new logsAttrib();
	att_list.push_back(logs);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}

//+----------------------------------------------------------------------------
//
// method : 		LiberaClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void LiberaClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------
	cl_prop.push_back(Tango::DbDatum("InterlockConfiguration"));
	cl_prop.push_back(Tango::DbDatum("EnableDDOptionalData"));
	cl_prop.push_back(Tango::DbDatum("EnableSAOptionalData"));
	cl_prop.push_back(Tango::DbDatum("EnableSAHistoryOptionalData"));
	cl_prop.push_back(Tango::DbDatum("EnableADCOptionalData"));
	cl_prop.push_back(Tango::DbDatum("Institute"));

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;

	//	Try to extract InterlockConfiguration value
	if (cl_prop[++i].is_empty()==false)	cl_prop[i]  >>  interlockConfiguration;
	else
	{
		//	Check default value for InterlockConfiguration
		def_prop = get_default_class_property(cl_prop[i].name);
		if (def_prop.is_empty()==false)
		{
			def_prop  >>  interlockConfiguration;
			cl_prop[i]  <<  interlockConfiguration;
		}
	}

	//	Try to extract EnableDDOptionalData value
	if (cl_prop[++i].is_empty()==false)	cl_prop[i]  >>  enableDDOptionalData;
	else
	{
		//	Check default value for EnableDDOptionalData
		def_prop = get_default_class_property(cl_prop[i].name);
		if (def_prop.is_empty()==false)
		{
			def_prop  >>  enableDDOptionalData;
			cl_prop[i]  <<  enableDDOptionalData;
		}
	}

	//	Try to extract EnableSAOptionalData value
	if (cl_prop[++i].is_empty()==false)	cl_prop[i]  >>  enableSAOptionalData;
	else
	{
		//	Check default value for EnableSAOptionalData
		def_prop = get_default_class_property(cl_prop[i].name);
		if (def_prop.is_empty()==false)
		{
			def_prop  >>  enableSAOptionalData;
			cl_prop[i]  <<  enableSAOptionalData;
		}
	}

	//	Try to extract EnableSAHistoryOptionalData value
	if (cl_prop[++i].is_empty()==false)	cl_prop[i]  >>  enableSAHistoryOptionalData;
	else
	{
		//	Check default value for EnableSAHistoryOptionalData
		def_prop = get_default_class_property(cl_prop[i].name);
		if (def_prop.is_empty()==false)
		{
			def_prop  >>  enableSAHistoryOptionalData;
			cl_prop[i]  <<  enableSAHistoryOptionalData;
		}
	}

	//	Try to extract EnableADCOptionalData value
	if (cl_prop[++i].is_empty()==false)	cl_prop[i]  >>  enableADCOptionalData;
	else
	{
		//	Check default value for EnableADCOptionalData
		def_prop = get_default_class_property(cl_prop[i].name);
		if (def_prop.is_empty()==false)
		{
			def_prop  >>  enableADCOptionalData;
			cl_prop[i]  <<  enableADCOptionalData;
		}
	}

	//	Try to extract Institute value
	if (cl_prop[++i].is_empty()==false)	cl_prop[i]  >>  institute;
	else
	{
		//	Check default value for Institute
		def_prop = get_default_class_property(cl_prop[i].name);
		if (def_prop.is_empty()==false)
		{
			def_prop  >>  institute;
			cl_prop[i]  <<  institute;
		}
	}

	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	LiberaClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void LiberaClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	prop_name = "InterlockConfiguration";
	prop_desc = "The user defined interlock configuration. This is the configuration that should be applied on the Libera in case the device 'finds'\nthe Libera in its default startup configuration when it is itself starting up or executing its Init TANGO command. This configuration\ncan also be applied using the dedicated \"SetInterlockConfiguration\" expert command.\nParameters mapping:\n[0] Interlock : mode - [0]: disabled, [1]: enabled, [3]: enabled with gain dependency\n[1] Interlock : threshold : X low in mm\n[2] Interlock : threshold : X high in mm\n[3] Interlock : threshold : Z low in mm (i.e. Y low in the Libera terminology)\n[4] Interlock : threshold : Z high in mm (i.e. Y high in the Libera terminology)\n[5] Interlock : overflow limit (ADC threshold)\n[6] Interlock : overflow duration (num of overloaded ADC samples before raising intlck)\n[7] Interlock : gain limit in dBm  (intlck not active under this limit) - valid range is [-60, 0]";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		cl_def_prop.push_back(data);
		add_wiz_class_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_class_prop(prop_name, prop_desc);

	prop_name = "EnableDDOptionalData";
	prop_desc = "Enables/Disables  DD optional data (IxDD and QxDD)";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		cl_def_prop.push_back(data);
		add_wiz_class_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_class_prop(prop_name, prop_desc);

	prop_name = "EnableSAOptionalData";
	prop_desc = "Enables/disables SA optional Data (currently not used)";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		cl_def_prop.push_back(data);
		add_wiz_class_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_class_prop(prop_name, prop_desc);

	prop_name = "EnableSAHistoryOptionalData";
	prop_desc = "Enables/disables SA History optional data (sum history)";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		cl_def_prop.push_back(data);
		add_wiz_class_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_class_prop(prop_name, prop_desc);

	prop_name = "EnableADCOptionalData";
	prop_desc = "Enables/disables ADC optional data (currently not used)";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		cl_def_prop.push_back(data);
		add_wiz_class_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_class_prop(prop_name, prop_desc);

	prop_name = "Institute";
	prop_desc = "0: TANGO_INSTITUTE (GENERIC)\n1: ALBA\n2: ESRF\n3: ELETTRA\n4: SOLEIL";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		cl_def_prop.push_back(data);
		add_wiz_class_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_class_prop(prop_name, prop_desc);

	//	Set Default Device Properties
	prop_name = "LiberaIpAddr";
	prop_desc = "The Libera IP address [no default value]";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "LiberaPort";
	prop_desc = "The port on which the generic server handles external requests. Defaults to 23721.";
	prop_def  = "23271";
	vect_data.clear();
	vect_data.push_back("23271");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "DefaultDDBufferSize";
	prop_desc = "Default [or initial] value for attribute DDBufferSize [in samples]. Defaults to 1024.";
	prop_def  = "1024";
	vect_data.clear();
	vect_data.push_back("1024");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "Switches";
	prop_desc = "Switches configuration. The valid range is [0..15]. Defaults to 3.";
	prop_def  = "3";
	vect_data.clear();
	vect_data.push_back("3");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "LiberaMulticastIpAddr";
	prop_desc = "Asynch. notifications (e.g. trigger events) will be send to this addr [no default value]";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "Location";
	prop_desc = "The BPM location [TL1, BOOSTER, TL2 or STORAGE_RING]. No default value.";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableExternalTrigger";
	prop_desc = "Enables (or not) the external trigger source.\nInlfuences the TANGO device behaviour not the Libera itself. Defaults to false.";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "DDTaskActivityPeriod";
	prop_desc = "Specify the watch-dog (1) or data reading period (2) in ms.\nMust be in the rangec [500, 25000] ms. Defaults to 1000.\n(1) : external trigger enabled - (2) : external trigger disabled.";
	prop_def  = "1000";
	vect_data.clear();
	vect_data.push_back("1000");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "SATaskActivityPeriod";
	prop_desc = "Specify the watch-dog (1) or data reading period (2) in ms.\nMust be in the range [100, 25000] ms. Defaults to 100.";
	prop_def  = "100";
	vect_data.clear();
	vect_data.push_back("100");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableDD";
	prop_desc = "Specifies whether or not the DD data source should be enabled at startup. Defaults to false.";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableSA";
	prop_desc = "Specifies whether or not the SA data source should be enabled at startup. Defaults to false.";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "SAHistoryLength";
	prop_desc = "SA history buffer length [in samples]. Defaults to 8196.";
	prop_def  = "512";
	vect_data.clear();
	vect_data.push_back("512");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "DDDecimationFactor";
	prop_desc = "The DD decimation factor.\nAllowed values : 1 (no decimation) or 64 (for the so called booster normal mode)";
	prop_def  = "1";
	vect_data.clear();
	vect_data.push_back("1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableAutoSwitchingIfSAEnabled";
	prop_desc = "When set to TRUE, auto-switching is automattically enabled when the SA data source is itself enabled";
	prop_def  = "true";
	vect_data.clear();
	vect_data.push_back("true");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableDSCIfAutoSwitchingEnabled";
	prop_desc = "When set to TRUE, the Digital Signal Conditioning is automattically enabled when the auto-switching is itself enabled";
	prop_def  = "true";
	vect_data.clear();
	vect_data.push_back("true");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "DefaultSAStatNumSamples";
	prop_desc = "Default number of SA history samples to use form RMS pos. computation.\nDefaults to 10 (last second in the SA history).";
	prop_def  = "256";
	vect_data.clear();
	vect_data.push_back("256");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "DefaultADCBufferSize";
	prop_desc = "Default [or initial] value for attribute ADCBufferSize [in samples]. Defaults to 1024.";
	prop_def  = "1024";
	vect_data.clear();
	vect_data.push_back("1024");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "ADCTaskActivityPeriod";
	prop_desc = "Specifies the data reading period in ms.\nMust be in the range [500, 25000] ms. Defaults to 1000.\n";
	prop_def  = "1000";
	vect_data.clear();
	vect_data.push_back("1000");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableADC";
	prop_desc = "Specifies whether or not the ADC data source should be enabled at startup. Defaults to false.";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "DefaultTimePhaseValue";
	prop_desc = "Default value for the machine time phase. Its valid range is [0, RfSfRatio - 1] where\nRfSfRatio is a machine dependent system property.";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "InterlockConfiguration";
	prop_desc = "The user defined interlock configuration. This is the configuration that should be applied on the Libera in case the device 'finds'\nthe Libera in its default startup configuration when it is itself starting up or executing its Init TANGO command. This configuration\ncan also be applied using the dedicated 'SetInterlockConfiguration' expert command.\nParameters mapping:\n[0] Interlock : mode - [0]: disabled, [1]: enabled, [3]: enabled with gain dependency\n[1] Interlock : threshold : X low in mm\n[2] Interlock : threshold : X high in mm\n[3] Interlock : threshold : Z low in mm (i.e. Y low in the Libera terminology)\n[4] Interlock : threshold : Z high in mm (i.e. Y high in the Libera terminology)\n[5] Interlock : overflow limit (ADC threshold)\n[6] Interlock : overflow duration (num of overloaded ADC samples before raising intlck)\n[7] Interlock : gain limit in dBm  (intlck not active under this limit) - valid range is [-60, 0]";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableDDOptionalData";
	prop_desc = "Enables/Disables  DD optional data (IxDD and QxDD)";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableSAOptionalData";
	prop_desc = "Enables/disables SA optional Data (currently not used)";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableSAHistoryOptionalData";
	prop_desc = "Enables/disables SA History optional data (sum history)";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "EnableADCOptionalData";
	prop_desc = "Enables/disables ADC optional data (currently not used)";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "Institute";
	prop_desc = "0: TANGO_INSTITUTE (GENERIC)\n1: ALBA\n2: ESRF\n3: ELETTRA\n4: SOLEIL";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "MaxDDBufferSizeWhenDecimationEnabled";
	prop_desc = "Max. DD buffer size when decimation enabled on DD data source.\nDefaults to 10000";
	prop_def  = "16384";
	vect_data.clear();
	vect_data.push_back("16384");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "PassBBAOffsetsToFPGA";
	prop_desc = "Controls wether or not the BBA offsets are taken into account when computing the offsets passed to the FPGA process";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "FADataCacheRefreshPeriod";
	prop_desc = "The <FA Data> cache refresh period in msecs.\nDefaults to 500 ms (2Hz).";
	prop_def  = "500";
	vect_data.clear();
	vect_data.push_back("500");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		LiberaClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void LiberaClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("Libera BPM Device Server");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("IT Libera BPM Device Server");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs or svn location
	string	filename(classname);
	filename += "Class.cpp";
	
	// Create a string with the class ID to
	// get the string into the binary
	string	class_id(ClassId);
	
	// check for cvs information
	string	src_path(CvsPath);
	start = src_path.find("/");
	if (start!=string::npos)
	{
		end   = src_path.find(filename);
		if (end>start)
		{
			string	strloc = src_path.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}
	// check for svn information
	else
	{
		string	src_path(SvnPath);
		start = src_path.find("://");
		if (start!=string::npos)
		{
			end = src_path.find(filename);
			if (end>start)
			{
				header = "$HeadURL: ";
				start = header.length();
				string	strloc = src_path.substr(start, (end-start));
				
				Tango::DbDatum	svn_loc("svn_location");
				svn_loc << strloc;
				data.push_back(svn_loc);
			}
		}
	}

	//	Get CVS or SVN revision tag
	
	// CVS tag
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}
	
	// SVN tag
	string	svnpath(SvnPath);
	header = "$HeadURL: ";
	start = header.length();
	
	end   = svnpath.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strloc = svnpath.substr(start, end-start);
		
		string tagstr ("/tags/");
		start = strloc.find(tagstr);
		if ( start!=string::npos )
		{
			start = start + tagstr.length();
			end   = strloc.find(filename);
			string	strtag = strloc.substr(start, end-start-1);
			
			Tango::DbDatum	svn_tag("svn_tag");
			svn_tag << strtag;
			data.push_back(svn_tag);
		}
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_4Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
