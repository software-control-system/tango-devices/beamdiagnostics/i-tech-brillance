//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr 
//------------------------------------------------------------------------------

#ifndef _BPM_H_
#define _BPM_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"
#include "BPMConfig.h"
#include "BPMData.h"
#include "BPMBuffer.h"
#include "BPMSensors.h"
#include "LiberaEvents.h"
#include "threading/Task.h"

// ============================================================================
// DEFINEs
// ============================================================================
#define kNO_DATA_MSG "NO_DATA"

// ============================================================================
// CONSTs
// ============================================================================
#define kDEFAULT_TIMEOUT_MS 1000
//-----------------------------------------------------------------------------
#define kDEFAULT_CON_FLAGS \
  CSPI_CON_MODE | CSPI_CON_HANDLER | CSPI_CON_USERDATA | CSPI_CON_EVENTMASK
//-----------------------------------------------------------------------------
#define kDEFAULT_DD_CON_FLAGS kDEFAULT_CON_FLAGS | CSPI_CON_DEC
//-----------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
//! BPM abstraction class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class BPM  : public Tango::LogAdapter
{
public:
  /**
   * BPM states
   */
  typedef enum
  {
    BPM_RUNNING,
    BPM_FAULT,
    BPM_UNKNOWN
  } BPMState;

  typedef int InterlockFlags;
  
	/**
	 * Interlock reasons
	 */
	enum 
	{
	  /** IL: position X out of limit. */
    INTERLOCK_X =    (1<<0),
    /** IL: position Y out of limit. */
    INTERLOCK_Z =    (1<<1),
    /** IL: Attenuators set higher than predefined value. */
    INTERLOCK_ATTN = (1<<2),
    /** IL: ADC Overflow  (filtered). */
    INTERLOCK_ADCF = (1<<3),
    /** IL: ADC Overflow  (not filtered). */
    INTERLOCK_ADC =  (1<<4)
  };
       
  /**
   * BPM software status (connection)
   */
  typedef struct BPMSwStatus
  {
  	BPMState state;
  	std::string status;
  } BPMSwStatus;

  /**
   * BPM hardware status (Health & PLL info)
   */
  typedef struct BPMHwStatus
  {
    //- 
    short temp;
    short fan_1_speed;
    short fan_2_speed;
    bool sc_ppl_lock_status;
    bool mc_ppl_lock_status;
    InterlockFlags intl_flags; 
    long memory_free;  
    long ram_fs_usage;
    long uptime; 
    long cpu_usage;
    //- ctor
    BPMHwStatus () 
      : temp (0), 
        fan_1_speed (0), 
        fan_2_speed (0), 
        sc_ppl_lock_status(false),
        mc_ppl_lock_status(false),
        intl_flags (0),
        memory_free (0),  
        ram_fs_usage (0), 
        uptime (0), 
        cpu_usage (0)
    {
      //- noop ctor
    }
    //- operator=
    BPMHwStatus& operator= (const BPMHwStatus& _src) 
    {
      ::memcpy(this, &_src, sizeof(BPMHwStatus));
      return *this;
    }  
  } BPMHwStatus;
  
  /**
   * FA env. data struct
   */
  typedef struct FAData
  {
public:
    //- map "cspi_set/getenvparam_fa" parameters
    //-----------------------------------------
    //- offset from the beginning of the FA config. bloc 
    size_t offset; 
    //- pointer to data to be written/read
    char * pbuf;
    //- size of a data element
    size_t size;
    //- number of data elements
    size_t count;
    //-default ctor
    FAData () 
      : offset(0), pbuf(0), size(0), count(0)
    {
      //- noop ctor
    }
    //- ctor
    FAData (size_t _off, void * _pbuf, size_t _size, size_t _count) 
      : offset(_off), pbuf(reinterpret_cast<char*>(_pbuf)), size(_size), count(_count)
    {
      //- noop ctor
    }
    //- ctor
    FAData (size_t _off, size_t _size, size_t _count) 
    {
      this->allocate(_off, _size, _count);
    }
    //- copy ctor
    FAData (const FAData& _src)
      : offset(0), pbuf(0), size(0), count(0)
    {
      *this = _src;
    }
    //- dtor
    ~FAData () 
    {
      delete[] this->pbuf;
    }
    //- operator= (this is a deep copy)
    FAData& operator= (const FAData& _src) 
    {
      if (&_src == this)
        return *this;
      try
      {
        size_t src_size = _src.size * _src.count;
        size_t dest_size = this->size * this->count;
        this->offset = _src.offset;
        this->size = _src.size;
        this->count = _src.count;
        if ((!this->pbuf && _src.pbuf) || (src_size != dest_size) || (!_src.pbuf && this->pbuf))
        {
          if (this->pbuf)
          {
            delete[] this->pbuf;
            this->pbuf = 0;
          }
          if (!_src.pbuf || !src_size) 
          {
            return *this;
          }
          this->pbuf = new char[src_size];
          if (!this->pbuf) 
            throw std::bad_alloc();
        }
        
      }
      catch (const std::bad_alloc&)
      {
        Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                        _CPTC ("memory allocation failed"),
                                        _CPTC ("FAData::operator="));
      }
      ::memcpy(this->pbuf, _src.pbuf, _src.size * _src.count);
      return *this;
    }
    //- (re)allocate the underlying memory space
    void allocate (size_t _off, size_t _size, size_t _count) 
    {
      this->offset = _off;
      this->size  = _size;
      this->count = _count;
      if (this->pbuf)
      {
        delete[] this->pbuf;
        this->pbuf = 0;
      }
      this->pbuf = new char[this->size * this->count];
      ::memset(this->pbuf, 0, this->size * this->count);
    }
  private:
    friend class BPM;
    //- copy from full fa data set (means src.offset == 0)
    void copy_from_full_fa_data_set (FAData& _src) 
    {
      if (&_src == this)
        return;
      try
      {
        if (!this->pbuf)
        {
          this->pbuf = new char[this->size * this->count];
          if (!this->pbuf) 
            throw std::bad_alloc();
        }
      }
      catch (const std::bad_alloc&)
      {
        Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                        _CPTC ("memory allocation failed"),
                                        _CPTC ("FAData::copy"));
      }
      ::memcpy(this->pbuf, _src.pbuf + this->offset, this->size * this->count);
    }  
    //- dump
    void dump ()
    {
      std::cout << "FAData::pbuf.....0x" << std::hex << (long)pbuf << std::dec << std::endl; 
      std::cout << "FAData::offset..." << offset << std::endl;
      std::cout << "FAData::size....." << size << std::endl;
      std::cout << "FAData::count...." << count << std::endl;
    }
  } FAData;
  
  /**
   * Time settings
   */
   typedef struct TimeSettings
   { 
      //- members
      FPDataType mt; 
      FPDataType st; 
      long tp;
      //- ctor
      TimeSettings ()
        : mt (0), st (0), tp (0)
      {
        //- noop 
      }
      //- copy ctor
      TimeSettings (const TimeSettings& ts)
        : mt (ts.mt), st (ts.st), tp (ts.tp)
      {
        //- noop 
      }
      //- dtor
      ~TimeSettings ()
      {
        //- noop 
      }
      //- operator=
      TimeSettings& operator= (const TimeSettings& ts)
      {
        if (&ts == this)
          return *this;
        ::memcpy(this, &ts, sizeof(TimeSettings));
        return *this;
      }
   } TimeSettings;
  
   /**
    * Env. params struct for Libera (re)configuration
    */
   typedef struct EnvParamsSpecifier
   {
     //- actual env. params (values)
     CSPI_ENVPARAMS ep;
     //- env. params flags (what to change selector)
     int ep_flags;
     //- default ctor
     EnvParamsSpecifier ()
     {
       ::memset(this, 0, sizeof(EnvParamsSpecifier));   
     }
     //- explicit ctor
     EnvParamsSpecifier (const CSPI_ENVPARAMS& ep, int flags)
      : ep_flags(flags)
     {
       ::memcpy(&this->ep, &ep, sizeof(CSPI_ENVPARAMS));
     }
     //- copy ctor
     EnvParamsSpecifier (const EnvParamsSpecifier& src)
     {
       *this = src;   
     }
     //- operator=
     EnvParamsSpecifier& operator= (const EnvParamsSpecifier& src)
     {
       if (&src == this)
         return *this;
       ::memcpy(this, &src, sizeof(EnvParamsSpecifier));   
       return *this;
     }
   } EnvParamsSpecifier;
   
  /**
   * Constructor (no op. - see init) 
   *
   * \param host_device The device in which this instance runs (for logging purpose)
   */
  BPM (Tango::DeviceImpl * host_device);

  /**
   * Release resources.
   */
  virtual ~BPM (void);

  /**
   * BPM initialization
   * \param initial_config The initial BPM configuration
   */
  virtual void init (const BPMConfig & initial_config) 
    throw (Tango::DevFailed);

  /**
   * Returns the current BPM configuration.
   * \return The current BPM configuration
   */
  const BPMConfig & configuration (void) const;

  /**
   * Reconfigures the BPM.
   *
   * \param config The new BPM configuration
   */
  void reconfigure (const BPMConfig & config) 
    throw (Tango::DevFailed);

  /**
   * Returns the BPM "software" status.
   */
  void get_sw_status (BPMSwStatus &) const;
  
  /**
   * Returns the BPM "hardware" status.
   */
  void get_hw_status (BPMHwStatus &) const;

  /**
   * Unfreezes the on demand data buffer
   */
  void unfreeze_dd_buffer (void) 
    throw (Tango::DevFailed);

  /**
   * Returns the cache status
   */
  bool is_dd_buffer_freezing_enabled (void) 
    throw (Tango::DevFailed);

  /**
   * Returns whether or not the cache is currently frozen
   */
  bool is_dd_buffer_frozen (void) 
    throw (Tango::DevFailed);

  /**
   * Returns the most recently acquired DD data
   */
  void get_dd_data (DDData & dest, bool force_update = false)
    throw (Tango::DevFailed);
    
  /**
   * Returns the most recently acquired ADC data
   */
  void get_adc_data (ADCData & dest, bool force_update = false)
    throw (Tango::DevFailed);
    
   /**
   * Returns the most recently acquired SA data
   */
  void get_sa_data (SAData & dest, bool force_update = false)
    throw (Tango::DevFailed);

  /**
   * Returns the most recently acquired PM data
   */
  void get_pm_data (DDData & dest, bool force_update = false)
    throw (Tango::DevFailed);
    
  /**
   * Returns the SA history
   */
  SAHistory & get_sa_history (void);

  /**
   * Returns the current trigger counter value
   */
  long trigger_counter (void) const;

  /**
   * Resets the trigger counter
   */
  void reset_trigger_counter (void);

  /**
   * Return the PM status
   */
  bool pm_notified (void) const;

  /**
   * Return the PM event counter
   */
  short pm_event_counter (void) const;
  
  /**
   * Resets the PM notification flag
   */
  void reset_pm_notification (void);
  
  /**
   * Resets the Interlock notification flags
   */
  void reset_interlock_notification (void);
  
  /**
   * Set the Libera interlock config (and only this part of the configuration) 
   */
  void set_interlock_configuration (const BPMConfig & new_config) 
    throw (Tango::DevFailed);
  
  /**
   * Synchronizes the Libera internal clock on next trigger  
   */
  void set_time_on_next_trigger (const TimeSettings& ts) 
    throw (Tango::DevFailed);
   
  /**
   * Reads "count x size" bytes from FA data block 
   */
   void read_fa_data (FAData & fa_data_) 
    throw (Tango::DevFailed);
   
  /**
   * Writes "count x size" bytes to FA data block 
   */
   void write_fa_data(const FAData & _fa_data) 
    throw (Tango::DevFailed);
   
  /**
   * Saves the DSC parameters 
   */
   void save_dsc_parameters (void) 
    throw (Tango::DevFailed);
   
  /**
   * Returns the incoherence 
   */
   bpm::FPDataType get_incoherence (void) 
    throw (Tango::DevFailed);

private:
  /**
   * BPM initialization
   * \param initial_config The initial BPM configuration
   */
  virtual void init_i (const BPMConfig & initial_config) 
    throw (Tango::DevFailed);

   /**
   * Close the connection then release allocated resources
   */
  void close_i (void) 
    throw (Tango::DevFailed);

  /**
   * BPM task init: main task (service)
   */
  void init_srv_task (void)
    throw (Tango::DevFailed);
    
  /**
   * BPM task close: main task (service)
   */
  void close_srv_task (void)
    throw (Tango::DevFailed);

#if ! defined(_EMBEDDED_DEVICE_)
  /**
   * Connection to Libera generic server (impl. retry mechanism)
   */
  void connect_to_generic_server (const std::string& caller)
    throw (Tango::DevFailed);
    
  /**
   * Disconnection from Libera generic server
   */
  void disconnect_from_generic_server (const std::string& caller)
    throw (Tango::DevFailed);
#endif

   /**
   * BPM task init: DD
   */
  void dd_task_connect (void)
       
    throw (Tango::DevFailed);
  /**
   * BPM task close: DD
   */
  void dd_task_disconnect (void)
       
    throw (Tango::DevFailed);
       
  /**
   * BPM task init: SA
   */
  void sa_task_connect (void)
       
    throw (Tango::DevFailed);
  /**
   * BPM task close: SA
   */
  void sa_task_disconnect (void)
       
    throw (Tango::DevFailed);

  /**
   * BPM task init: ADC
   */
  void adc_task_connect (void)
       
    throw (Tango::DevFailed);
  /**
   * BPM task close: ADC
   */
  void adc_task_disconnect (void)
       
    throw (Tango::DevFailed);

  /**
   * Enable CSPI notifications for a given connection
   */
  void enable_notifications (int cspi_events_mask)
    throw (Tango::DevFailed);
    
  /**
   * Disable all CSPI notifications for all connections
   */
  void disable_notifications (void)
    throw (Tango::DevFailed);
  
  /**
   * Enables CSPI post mortem notifications
   */
  void enable_pm_notifications (void) 
    throw (Tango::DevFailed);

  /**
   * Disables CSPI post mortem notifications
   */
  void disable_pm_notifications (void) 
    throw (Tango::DevFailed);

  /**
   * Enables CSPI interlock notifications
   */
  void enable_interlock_notifications (void) 
    throw (Tango::DevFailed);

  /**
   * Disables CSPI interlock notifications
   */
  void disable_interlock_notifications (void) 
    throw (Tango::DevFailed);
    
  /**
   * Enables CSPI ADC notifications
   */
  void enable_adc_notifications (void) 
    throw (Tango::DevFailed);

  /**
   * Disables CSPI ADC notifications
   */
  void disable_adc_notifications (void) 
    throw (Tango::DevFailed);
    
  /**
   * Enables CSPI SA notifications
   */
  void enable_sa_notifications (void) 
    throw (Tango::DevFailed);

  /**
   * Disables CSPI SA notifications
   */
  void disable_sa_notifications (void) 
    throw (Tango::DevFailed);

  /**
   * Enables CSPI DD notifications
   */
  void enable_dd_notifications (void) 
    throw (Tango::DevFailed);

  /**
   * Disables CSPI DD notifications
   */
  void disable_dd_notifications (void) 
    throw (Tango::DevFailed);

  /**
   * Disable DD buffer freezing
   */
  void disable_dd_buffer_freezing (void) 
    throw (Tango::DevFailed);

  /**
   * Read DD data source
   */
  void read_dd (void) 
    throw (Tango::DevFailed);

  /**
   * Read SA data source
   */
  void read_sa (void) 
    throw (Tango::DevFailed);
    
  /**
   * Read ADC data source
   */
  void read_adc (void) 
    throw (Tango::DevFailed);
    
  /**
   * Read PM data source
   */
  void read_pm (void) 
    throw (Tango::DevFailed);

  /**
   * Watch dog 
   */
  void watch_dog (void) 
    throw (Tango::DevFailed);

  /**
   * Returns true if this instance is correctly initialized, returns false 
   * or throws an exception otherwise. 
   */
  bool initialized (void) const;
  
  /**
   * Get the current env parmeters  
   */
  void get_env_parameters (CSPIHENV he, CSPI_ENVPARAMS & ep, size_t mask = kALL_PARAMS, size_t retries = 2)
    throw (Tango::DevFailed);
    
  /**
   * Set the con. part of the configuration from hardware  
   */
  void set_con_parameters (CSPIHCON hc, 
                           CSPI_CONPARAMS * cp, 
                           int flags = kDEFAULT_CON_FLAGS) 
    throw (Tango::DevFailed);

  /**
   * Set the con. part of the configuration from hardware  
   */
  void set_con_parameters (CSPIHCON hc, 
                           CSPI_CONPARAMS_EBPP * cp, 
                           int flags = kDEFAULT_DD_CON_FLAGS) 
    throw (Tango::DevFailed);

  /**
   * Get the con. part of the configuration from hardware  
   */
  void get_con_parameters (CSPIHCON hc, CSPI_CONPARAMS * cp) 
    throw (Tango::DevFailed);

  /**
   * Get the con. part of the configuration from hardware  
   */
  void get_con_parameters (CSPIHCON hc, CSPI_CONPARAMS_EBPP * cp) 
    throw (Tango::DevFailed);

  /**
   * Updates health sensors info  
   */
  void update_libera_health_info ();
  
  /**
   * Updates Libera sensors info  
   */
#if defined(_EMBEDDED_DEVICE_)
  void update_libera_sensors_info ();
#endif

  /**
   * The srv task periodic job: main jobs
   */
  void srv_periodic_job () 
    throw (Tango::DevFailed);

  /**
   * The srv task periodic job: fat data chache update
   */
  void update_fa_data_cache () 
      throw (Tango::DevFailed);

  /**
   * The BPM configuration.
   */
  BPMConfig config_;
  
  /**
   * BPM internal state
   */
  BPMState state_;

  /**
   * DD data buffer (for read op).
   */
  DDRawBuffer dd_raw_buffer_;

  /**
   * Most recent DD data
   */
  DDData * last_dd_data_;
  bool last_dd_data_updated_;
  
  /**
   * Preallocated DDData (optimization)
   */
  DDData * dd_data_1_;
  DDData * dd_data_2_;

  /**
   * ADC data buffer (for read op).
   */
  ADCRawBuffer adc_raw_buffer_;
  
  /**
   * Most recent ADC data
   */
  ADCData * last_adc_data_;
  bool last_adc_data_updated_;

  /**
   * Preallocated ADCData (optimization)
   */
  ADCData * adc_data_1_;
  ADCData * adc_data_2_;
  
  /**
   * Most recent SA data
   */
  SAData * last_sa_data_; 
  bool last_sa_data_updated_;

  /**
   * Preallocated SAData (optimization)
   */
  SAData * sa_data_1_;
  SAData * sa_data_2_;
  
  /**
   * SA data history buffer
   */
  SAHistory sa_history_; 
  
  /**
   * Most recent PM data
   */
  DDData * last_pm_data_;
  bool last_pm_data_updated_;
  
  /**
   * Trigger counter
   */
  long trigger_counter_;

#if ! defined(_EMBEDDED_DEVICE_)
  /**
   * Connection flag
   */
  bool connected_;
#endif

  /**
   * DD cache status
   */
  bool dd_buffer_frozen_ ;

  /**
   * ther service task (handles BPM config, PM, ...)
   */
  bpm::Task * srv_task_;

  /**
   * the DD task
   */
  bpm::Task * dd_task_;

  /**
   * the SA task
   */
  bpm::Task * sa_task_;

  /**
   * the ADC task
   */
  bpm::Task * adc_task_;
  
  /**
   * The host TANGO device
   */
  Tango::DeviceImpl * host_device_;

  /**
   * PM status
   */
  bool pm_notified_;
  short pm_event_counter_;
   
  /**
   * HW status
   */
   BPMHwStatus hw_status_;
   
  /**
   * FA Data for FA I/O operations
   */
   FAData fa_data_rd_;
   FAData fa_data_cp_;
   bpm::Timer fa_data_timer_;
   bpm::Mutex fa_data_lock_;

  /**
   * Sensors
   */
   BPMSensors sensors_;
   
  /**
   * Num of SA samples since last stats processing
   */
	size_t num_sa_samples_since_last_stat_proc_;

  /**
   * Some bools used to avoid double trig. notifications (one for DD & one for ADC)
   */
  bool dd_requires_trigger_notifications_;
  bool adc_requires_trigger_notifications_;

  /**
   * The Libera evt manager
   */
#if defined(_EMBEDDED_DEVICE_)
  LiberaEventsManager * evts_man_;
#endif

#if ! defined(_EMBEDDED_DEVICE_)
  /**
   * Returns the current cspi events mask
   */
  int current_cspi_events_mask (void) const;
#endif

  //--------------------------------------------------------
  //- static members
  //--------------------------------------------------------
  
  /**
   * Async. notification callbacks
   */
  static int libera_event_callback (CSPI_EVENT * evt);
  
  /**
   * "Main" message handler
   */
  static void srv_message_handler (const bpm::Message& _msg);

   /**
   * "DD" message handler
   */
  static void dd_message_handler (const bpm::Message& _msg);

   /**
   * "SA" message handler
   */
  static void sa_message_handler (const bpm::Message& _msg);

   /**
   * "ADC" message handler
   */
  static void adc_message_handler (const bpm::Message& _msg);
  
  /**
   * BPM singleton
   */
  static BPM * singleton;

  /**
   * BPM lock
   */
  static bpm::Mutex lock;

  /**
   * Closed flag
   */
  static bool closed;

  // = Disallow these operations.
  //--------------------------------------------
  BPM & operator= (const BPM &);
  BPM (const BPM &);
};

} // namespace bpm

#if defined (__INLINE_IMPL__)
# include "BPM.i"
#endif // __INLINE_IMPL__

#endif // _BPM_H_
