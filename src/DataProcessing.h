//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _DATA_PROC_H_
#define _DATA_PROC_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"
#include "BPMData.h"
#include "BPMConfig.h"

namespace bpm
{

// ============================================================================
//! DataProcessing abstraction class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class DataProcessing
{
public:
  /**
   * BPM DD data processing 
   */
  static void compute_pos (DDData & dd_data,
                           const DDRawBuffer & dd_raw_data,
                           size_t actual_num_samples,
                           const BPMConfig & bpm_config);
                                    
  /**
   * BPM SA data processing
   */                              
  static void compute_pos (SAData & _sa_data, const BPMConfig & _config);
                                    
private:

  /**
   * BPM DD data processing for block electrodes at 45°  
   */
  static void compute_pos_block_45 (DDData & dd_data,
                                    const DDRawBuffer & dd_raw_data,
                                    size_t actual_num_samples,
                                    const BPMConfig & bpm_config);
                                    
  /**
   * BPM SA data processing for block electrodes at 45° 
   */                              
  static void compute_pos_block_45 (SAData & _sa_data,
                                    const BPMConfig & _config);

  /**
   * BPM DD data processing for block electrodes at 90° 
   */
  static void compute_pos_block_90 (DDData & dd_data,
                                    const DDRawBuffer & dd_raw_data,
                                    size_t actual_num_samples,
                                    const BPMConfig & bpm_config);
                                                                   
  /**
   * BPM SA data processing for block electrodes at 90° 
   */                              
  static void compute_pos_block_90 (SAData & _sa_data,
                                    const BPMConfig & _config);
  /**
   * BPM DD data processing for the ESRF SR algorithm
   */                            
  static void compute_pos_esrf_sr (DDData & _dd_data,
                                   const DDRawBuffer & _dd_raw_buffer, 
                                   size_t actual_num_samples,
                                   const BPMConfig & _config);
                                   
  /**
   * BPM SA data processing for the ESRF SR algorithm
   */                            
  static void compute_pos_esrf_sr (SAData & _sa_data,
                                   const BPMConfig & _config);

  // = Disallow these operations.
  //--------------------------------------------
  DataProcessing (void);
  DataProcessing (const DataProcessing &);
  virtual ~DataProcessing (void);
  DataProcessing & operator= (const DataProcessing &);
};

} // namespace bpm

#endif  // _DATA_PROC_H_
