//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"

#if defined(_EMBEDDED_DEVICE_)

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include "LiberaEvents.h"

namespace bpm {

// ============================================================================
// DEFINEs
// ============================================================================
#define LIBERA_EVENT_FIFO_PATHNAME	"/dev/libera.event"
//-----------------------------------------------------------------------------
#define LIBERA_SERVER_PID_PATHNAME  "/var/run/liberad.pid"
//-----------------------------------------------------------------------------
#define LIBERA_EVENTD_PID_PATHNAME  "/var/run/leventd.pid"
//-----------------------------------------------------------------------------

// ============================================================================
// Static members
// ============================================================================
Tango::DevFailed LiberaEventsManager::error;
 
// ============================================================================
// LiberaEventsManager::LiberaEventsManager (SINGLETON)
// ============================================================================
LiberaEventsManager::LiberaEventsManager (const LiberaEventsManagerConfig& cfg)
  : Thread(cfg.host_device), cfg_(cfg), fifo_fd_(-1), go_on_(true)
{
  //- noop
}

// ============================================================================
// LiberaEventsManager::~LiberaEventsManager
// ============================================================================
LiberaEventsManager::~LiberaEventsManager ()
{
  //- noop
}

// ============================================================================
// LiberaEventsManager::is_eventd_running
// ============================================================================
bool LiberaEventsManager::is_eventd_running (bool kill_if_running)
    throw (Tango::DevFailed)
{
  //- std::cout << "LiberaEventsManager::is_eventd_running <->" << std::endl;
  
  return LiberaEventsManager::deamon_running_i(LIBERA_SERVER_PID_PATHNAME, kill_if_running);
}

// ============================================================================
// LiberaEventsManager::is_liberad_running
// ============================================================================
bool LiberaEventsManager::is_liberad_running (bool kill_if_running)
    throw (Tango::DevFailed)
{
  //- std::cout << "LiberaEventsManager::is_liberad_running <->" << std::endl;
  
  return LiberaEventsManager::deamon_running_i(LIBERA_EVENTD_PID_PATHNAME, kill_if_running);
}

// ============================================================================
// LiberaEventsManager::deamon_running_i
// ============================================================================
bool LiberaEventsManager::deamon_running_i (const char * pid_file, bool kill_if_running)
    throw (Tango::DevFailed)
{
  //- std::cout << "LiberaEventsManager::deamon_running_i <-" << std::endl;

  bool daemon_running =  false;
  
  FILE *fp = ::fopen(pid_file, "r");
  if (! fp)
  {
    if (errno != ENOENT)
    {
      Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                     _CPTC("could not check Libera daemon status [system call failed]"),
                                     _CPTC("LiberaEventsManager::deamon_running_i"));
    }
    //- std::cout << "LiberaEventsManager::is_eventd_running::no Libera daemon pid file found [not running]" << std::endl;
    return daemon_running;
  }
  
  //- std::cout << "LiberaEventsManager::deamon_running_i::found existing daemon pid file" << std::endl;

  int rc = 0;
  char *line = 0;
  size_t size = 0;

  if (::getline(&line, &size, fp) != -1)
  {
    //- get eventd pid
    const pid_t pid = ::atol(line);
    //- select signal to send to the eventd
    const int no_signal = kill_if_running ? 9 : 0;
    if (! kill_if_running)
    {
      //- std::cout << "LiberaEventsManager::deamon_running_i::sending signal 0 to daemon" << std::endl;
      //- just "ping" the eventd
      if (::kill( pid, no_signal) == 0)
        daemon_running = true;
      //- std::cout << "LiberaEventsManager::deamon_running_i::daemon is alive!" << std::endl;
    }
    else
    {
      //- std::cout << "LiberaEventsManager::deamon_running_i::sending SIGINT to daemon" << std::endl;
      //- send SIGINT to the eventd
      if (::kill(pid, no_signal) == 0)
        daemon_running = false;
      //- std::cout << "LiberaEventsManager::deamon_running_i::daemon killed" << std::endl;
    }
  }
  
  //- 
  if (line)
    ::free(line);

  //- close pid file
  ::fclose(fp);

  //- (try to) erase pid file
  if (kill_if_running)
    ::unlink(pid_file);

  //- std::cout << "LiberaEventsManager::deamon_running_i ->" << std::endl;
  
  return daemon_running;
}


// ============================================================================
// LiberaEventsManager::go
// ============================================================================
void LiberaEventsManager::go ()
{
  DEBUG_STREAM << "LiberaEventsManager::go <-" << std::endl;
  this->start_undetached();
  DEBUG_STREAM << "LiberaEventsManager::go ->" << std::endl;
}

// ============================================================================
// LiberaEventsManager::exit
// ============================================================================
void LiberaEventsManager::exit ()
{
  DEBUG_STREAM << "LiberaEventsManager::exit <->" << std::endl;
  this->go_on_ = false;
  Thread::IOArg ioa = 0;
  this->join(&ioa);
  //- don't even try to log something after join!
}

// ============================================================================
// LiberaEventsManager::init_i
// ============================================================================
void LiberaEventsManager::init_i ()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "LiberaEventsManager::init_i <-" << std::endl;

  //- already connected?
  if (this->fifo_fd_ != -1)	
    return;
                                   
	// open Libera event device in RW mode to gain exclusive
	// access to the event fifo. this is also more effective
	// than the check above and makes it kind of redundant!
	this->fifo_fd_ = ::open(LIBERA_EVENT_FIFO_PATHNAME, O_RDWR);
	if (this->fifo_fd_ == -1)	
	{
    Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                   _CPTC("could not open the Libera event FIFO [try to reload Libera driver]"),
                                   _CPTC("LiberaEventsManager::init"));
	}

  //- set initial mask
	this->set_events_mask_i(0);
  this->cfg_.evts_mask = 0;

  
  DEBUG_STREAM << "LiberaEventsManager::init_i ->" << std::endl;
}

// ============================================================================
// LiberaEventsManager::close_i
// ============================================================================
void LiberaEventsManager::close_i ()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "LiberaEventsManager::close_i <-" << std::endl;

  //- are we actually coonected to Libera event fifo?
  if (this->fifo_fd_ == -1)
    return;
    
	//- disable events
  size_t mask = 0;
  this->set_events_mask_i(0);
  
	//- close event fifo
	if (::close(this->fifo_fd_))	
	{
    //- anyway... mark event fifo as closed
    this->fifo_fd_ = -1;
    Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                   _CPTC("could not close the Libera event FIFO"),
                                   _CPTC("LiberaEventsManager::close"));
  }
  
  //- mark event fifo as closed
  this->fifo_fd_ = -1;

  DEBUG_STREAM << "LiberaEventsManager::close_i ->" << std::endl;
}

// ============================================================================
// LiberaEventsManager::set_events_mask
// ============================================================================
void LiberaEventsManager::set_events_mask (int _evts_mask)
    throw (Tango::DevFailed)
{
  bpm::AutoMutex<bpm::Mutex> guard(this->m_lock);
  
  this->set_events_mask_i(_evts_mask);
}

// ============================================================================
// LiberaEventsManager::set_events_mask_i
// ============================================================================
void LiberaEventsManager::set_events_mask_i (int _evts_mask)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "LiberaEventsManager::set_events_mask_i <-" << std::endl;

	DEBUG_ASSERT(this->fifo_fd_ != -1);
    
	if (::ioctl(this->fifo_fd_, LIBERA_EVENT_SET_MASK, &_evts_mask))	
	{
    Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                   _CPTC("could not change Libera event mask"),
                                   _CPTC("LiberaEventsManager::set_event_mask"));
  }

  this->cfg_.evts_mask = _evts_mask;

  DEBUG_STREAM << "LiberaEventsManager::set_events_mask_i ->" << std::endl;
}

// ============================================================================
// LiberaEventsManager::get_events_mask
// ============================================================================
int LiberaEventsManager::get_events_mask () const
{
  return this->cfg_.evts_mask;
}

// ============================================================================
// LiberaEventsManager::enable_events
// ============================================================================
void LiberaEventsManager::enable_events (int _evts_mask)
    throw (Tango::DevFailed)
{
  DEBUG_STREAM << "LiberaEventsManager::enable_events <-" << std::endl;

  bpm::AutoMutex<bpm::Mutex> guard(this->m_lock);

  int evts_mask = this->cfg_.evts_mask;
  
  if (_evts_mask & CSPI_EVENT_OVERFLOW)
  {
    DEBUG_STREAM << "LiberaEventsManager::enable_events::CSPI_EVENT_OVERFLOW enabled" << std::endl;
    evts_mask |= CSPI_EVENT_OVERFLOW;
  }

  if (_evts_mask & CSPI_EVENT_CFG)
  {
    DEBUG_STREAM << "LiberaEventsManager::enable_events::CSPI_EVENT_CFG enabled" << std::endl;
    evts_mask |= CSPI_EVENT_CFG;
  }

  if (_evts_mask & CSPI_EVENT_SA)
  {
    DEBUG_STREAM << "LiberaEventsManager::enable_events::CSPI_EVENT_SA enabled" << std::endl;
    evts_mask |= CSPI_EVENT_SA;
  }

  if (_evts_mask & CSPI_EVENT_INTERLOCK)
  {
    DEBUG_STREAM << "LiberaEventsManager::enable_events::CSPI_EVENT_INTERLOCK enabled" << std::endl;
    evts_mask |= CSPI_EVENT_INTERLOCK;
  }

  if (_evts_mask & CSPI_EVENT_PM)
  {
    DEBUG_STREAM << "LiberaEventsManager::enable_events::CSPI_EVENT_PM enabled" << std::endl;
    evts_mask |= CSPI_EVENT_PM;
  }

  if (_evts_mask & CSPI_EVENT_FA)
  {
    DEBUG_STREAM << "LiberaEventsManager::enable_events::CSPI_EVENT_FA enabled" << std::endl;
    evts_mask |= CSPI_EVENT_FA;
  }

  if (_evts_mask & CSPI_EVENT_TRIGGET)
  {
    DEBUG_STREAM << "LiberaEventsManager::enable_events::CSPI_EVENT_TRIGGET enabled" << std::endl;
    evts_mask |= CSPI_EVENT_TRIGGET;
  }

  if (_evts_mask & CSPI_EVENT_TRIGSET)
  {
    DEBUG_STREAM << "LiberaEventsManager::enable_events::CSPI_EVENT_TRIGSET enabled" << std::endl;
    evts_mask |= CSPI_EVENT_TRIGSET;
  }
  
  this->set_events_mask_i(evts_mask);

  this->cfg_.evts_mask = evts_mask;

  DEBUG_STREAM << "LiberaEventsManager::enable_events ->" << std::endl;
}

// ============================================================================
// LiberaEventsManager::disable_events
// ============================================================================
void LiberaEventsManager::disable_events (int _evts_mask)
    throw (Tango::DevFailed)
{
  DEBUG_STREAM << "LiberaEventsManager::disable_events <-" << std::endl;

  bpm::AutoMutex<bpm::Mutex> guard(this->m_lock);

  int evts_mask = this->cfg_.evts_mask;

  if (_evts_mask & CSPI_EVENT_OVERFLOW)
  {
    DEBUG_STREAM << "LiberaEventsManager::disable_events::CSPI_EVENT_OVERFLOW disabled" << std::endl;
    evts_mask &= ~CSPI_EVENT_OVERFLOW;
  }

  if (_evts_mask & CSPI_EVENT_CFG)
  {
    DEBUG_STREAM << "LiberaEventsManager::disable_events::CSPI_EVENT_CFG disabled" << std::endl;
    evts_mask &= ~CSPI_EVENT_CFG;
  }

  if (_evts_mask & CSPI_EVENT_SA)
  {
    DEBUG_STREAM << "LiberaEventsManager::disable_events::CSPI_EVENT_SA disabled" << std::endl;
    evts_mask &= ~CSPI_EVENT_SA;
  }
  
  if (_evts_mask & CSPI_EVENT_INTERLOCK)
  {
    DEBUG_STREAM << "LiberaEventsManager::disable_events::CSPI_EVENT_INTERLOCK disabled" << std::endl;
    evts_mask &= ~CSPI_EVENT_INTERLOCK;
  }

  if (_evts_mask & CSPI_EVENT_PM)
  {
    DEBUG_STREAM << "LiberaEventsManager::disable_events::CSPI_EVENT_PM disabled" << std::endl;
    evts_mask &= ~CSPI_EVENT_PM;
  }

  if (_evts_mask & CSPI_EVENT_FA)
  {
    DEBUG_STREAM << "LiberaEventsManager::disable_events::CSPI_EVENT_FA disabled" << std::endl;
    evts_mask &= ~CSPI_EVENT_FA;
  }

  if (_evts_mask & CSPI_EVENT_TRIGGET)
  {
    DEBUG_STREAM << "LiberaEventsManager::disable_events::CSPI_EVENT_TRIGGET disabled" << std::endl;
    evts_mask &= ~CSPI_EVENT_TRIGGET;
  }
  
  if (_evts_mask & CSPI_EVENT_TRIGSET)
  {
    DEBUG_STREAM << "LiberaEventsManager::disable_events::CSPI_EVENT_TRIGSET disabled" << std::endl;
    evts_mask &= ~LIBERA_EVENT_TRIGSET;
  }
  
  this->set_events_mask_i(evts_mask);

  this->cfg_.evts_mask = evts_mask;

  DEBUG_STREAM << "LiberaEventsManager::disable_events ->" << std::endl;
}

// ============================================================================
// LiberaEventsManager::run
// ============================================================================
Thread::IOArg LiberaEventsManager::run_undetached (Thread::IOArg _arg)
{
	fd_set rfds;
	size_t nleft = 0;

  DEBUG_STREAM << "LiberaEventsManager::run_undetached <-" << std::endl;
                       
  try
  {
    //- initialization
    this->init_i();
  }
  catch (const Tango::DevFailed& df)
  {
    //- store error
    LiberaEventsManager::error = df;
    //- notify user
    this->notify_error_i();
    try
    {
      //- cleanup
      bpm::AutoMutex<bpm::Mutex> guard(this->m_lock);
      this->close_i();
    }
    catch (...)
    {
      //- ignore error
    }
    //- exit thread
    return 0;
  }

	//- while no quit request...
  while (this->go_on_) 
	{
    try
    {
      //- reset
		  FD_ZERO(&rfds);
		  
		  //- add request fifo descriptor to the set of descriptors to watch
		  FD_SET(this->fifo_fd_, &rfds);
		  
      //- setup select timeout
      struct timeval tv;
      tv.tv_sec = 1;
      tv.tv_usec = 0;

		  //- watch the descriptor set for available input
		  int rc = ::select(this->fifo_fd_ + 1, &rfds, 0, 0, &tv);
		  if (rc == -1) 
		  {
        //- error or interrupted by signal
			  if (errno == EINTR && this->go_on_)
			    continue;
		  }
      else if (! rc)
      {
        //- timeout
			  if (! this->go_on_)
          break;
        else
          continue;
      }

      //- if fifo has pending event
		  ssize_t nread = -1;
		  if (FD_ISSET(this->fifo_fd_, &rfds)) 
		  {
        //- read pending event
			  libera_event_t evt;
			  nread = this->read_fifo_i(&evt, sizeof(evt), &nleft);
			  if (nread == -1 && errno != EINTR)
	      {
          Tango::Except::throw_exception(_CPTC("INTERNAL_ERROR"),
                                         _CPTC("could not read Libera event FIFO [try to reload Libera driver]"),
                                         _CPTC("LiberaEventsManager::run_undetached"));
        }
        //- handle event
			  if (! nleft)
			    handle_event_i(evt);
		  }
    }
    catch (const Tango::DevFailed& df)
    {
      //- store error
      LiberaEventsManager::error = df;
      //- notify user
      this->notify_error_i();
      //- exit while loop
      break;
    }
	}

  try
  {
    //- cleanup
    bpm::AutoMutex<bpm::Mutex> guard(this->m_lock);
    DEBUG_STREAM << "LiberaEventsManager::run_undetached::closing connection to Libera events FIFO" << std::endl;
    this->close_i();
    DEBUG_STREAM << "LiberaEventsManager::run_undetached::connection to events FIFO closed" << std::endl;
  }
  catch (const Tango::DevFailed& df)
  {
    //- store error but do not overwrite initial error
    if (LiberaEventsManager::error.errors.length() == 0)
      LiberaEventsManager::error = df;
    //- doesn't make sense to notify user!
  }

  DEBUG_STREAM << "LiberaEventsManager::run_undetached ->" << std::endl;

  return 0;
}

// ============================================================================
// LiberaEventsManager::read_fifo_i
// ============================================================================
int LiberaEventsManager::read_fifo_i (void * buf, const size_t ntotal, size_t * nleft)
    throw (Tango::DevFailed)
{
	DEBUG_ASSERT(buf);
	DEBUG_ASSERT(nleft );
	DEBUG_ASSERT(*nleft <= ntotal);

	//- reset nleft if we just completed reading ntotal bytes.
	if (*nleft == 0) 
	  *nleft = ntotal;
	  
	const ssize_t nread = ::read(this->fifo_fd_, (char*)buf + (ntotal - *nleft), *nleft);

  //- remove consumed bytes
	if (nread > 0) 
	  *nleft -= nread;
	  
	return nread;
}

// ============================================================================
// LiberaEventsManager::handle_event_i
// ============================================================================
void LiberaEventsManager::handle_event_i (const libera_event_t &_evt)
{
/*
  switch (_evt.id)
  {
    case CSPI_EVENT_USER:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_USER"
                   << std::endl;
      break;
    case CSPI_EVENT_OVERFLOW:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_OVERFLOW"
                   << std::endl;
      break;
    case CSPI_EVENT_CFG:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_CFG"
                   << std::endl;
      break;
    case CSPI_EVENT_SA:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_SA"
                   << std::endl;
      break;
    case CSPI_EVENT_INTERLOCK:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_INTERLOCK"
                   << std::endl;
      break;
    case CSPI_EVENT_PM:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_PM"
                   << std::endl;
      break;
    case CSPI_EVENT_FA:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_FA"
                   << std::endl;
      break;
    case CSPI_EVENT_TRIGGET:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_TRIGGET"
                   << std::endl;
      break;
    case CSPI_EVENT_TRIGSET:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling CSPI_EVENT_TRIGSET"
                   << std::endl;
      break;
    default:
      DEBUG_STREAM << "LiberaEventsManager::handle_event_i::handling UNKNOWN evt [id: "
                   << _evt.id
                   << "]"
                   << std::endl;
      break;
  }
*/  

	//- this will block until PM data buffered
	if (_evt.id == CSPI_EVENT_PM) 
    ::ioctl(this->fifo_fd_, LIBERA_EVENT_ACQ_PM);

  //- forward evt to user 
  if (this->cfg_.evt_handler)
  {
    //- setup evt
    CSPI_EVENT cpsi_evt;
    cpsi_evt.hdr.id = _evt.id;
    cpsi_evt.hdr.param = _evt.param;
    cpsi_evt.user_data = this->cfg_.user_data; 
    try
	  {
      //- call event handler
      this->cfg_.evt_handler(&cpsi_evt);
	  }
	  catch (...)
	  {
	    //- ignore error - can't do anything here!
	  }
  }
}

// ============================================================================
// LiberaEventsManager::notify_error_i
// ============================================================================
void LiberaEventsManager::notify_error_i ()
{
  //- forward evt to user 
  if (this->cfg_.evt_handler)
  {
    //- setup evt
    CSPI_EVENT cpsi_evt;
    cpsi_evt.hdr.id = CSPI_EVENTS_MANAGER_ERROR;
    cpsi_evt.hdr.param = 0;
    cpsi_evt.user_data = this->cfg_.user_data;
    //- call event handler
    try
    {
      this->cfg_.evt_handler(&cpsi_evt);
    }
    catch (...)
    {
      //- ignore error - can't do anything here!
    }
  }
}
  
} //- namespace bpm

#endif // _EMBEDDED_DEVICE

