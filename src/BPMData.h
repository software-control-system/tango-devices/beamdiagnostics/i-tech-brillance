//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_DATA_H_
#define _BPM_DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/SharedObject.h"
#include "BPMBuffer.h"

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
# include "ArmOptimization.h"
#endif

#define kNUM_USER_DATA 12

namespace bpm
{
// ============================================================================
//! DDRawData
// ============================================================================
typedef bpm::Buffer<CSPI_DD_RAWATOM> DDRawBuffer;
typedef bpm::Buffer<CSPI_ADC_ATOM> ADCRawBuffer;

// ============================================================================
//! DDData abstraction class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class DDData : private bpm::SharedObject
{
  friend class BPM;
  friend class DataProcessing;

public:

  typedef bpm::Buffer<bpm::FPDataType> Buffer;
  
  /**
   * Ctor
   */
  DDData ();
  
  /**
   * Duplicate (shallow copy) this shared object 
   */
  DDData * duplicate (void);

  /**
   * Release this shared object 
   */
  void release (void);

  /**
   * The data buffers accessors
   */

  //- mandatory data acessors (always available)
  //----------------------------------------------------
  const DDData::Buffer& x (void) const;
  const DDData::Buffer& z (void) const;
  const DDData::Buffer& q (void) const;
  
  const DDData::Buffer& va (void) const;
  const DDData::Buffer& vb (void) const;
  const DDData::Buffer& vc (void) const;
  const DDData::Buffer& vd (void) const;
  
  const DDData::Buffer& sum (void) const;

  //- optional data acessors (may be disabled)
  //----------------------------------------------------
  //- the following methods may throw an exception if
  //- requested data is not available (feature disabled)
  
  const DDData::Buffer& sin_va (void) const 
      throw (Tango::DevFailed);
      
  const DDData::Buffer& cos_va (void) const 
      throw (Tango::DevFailed);
      
  const DDData::Buffer& sin_vb (void) const 
      throw (Tango::DevFailed);
      
  const DDData::Buffer& cos_vb (void) const 
      throw (Tango::DevFailed);
      
  const DDData::Buffer& sin_vc (void) const 
      throw (Tango::DevFailed);
        
  const DDData::Buffer& cos_vc (void) const 
      throw (Tango::DevFailed);
      
  const DDData::Buffer& sin_vd (void) const 
      throw (Tango::DevFailed);
      
  const DDData::Buffer& cos_vd (void) const 
      throw (Tango::DevFailed);
  
  /**
   * The data timestamp
   */
  const struct timespec & timestamp (void) const;
  
  /**
   * is optional data enabled?
   */
  inline static bool optional_data_enabled (void)
  {
    return DDData::m_optional_data_enabled;
  }
  
private:

  virtual ~DDData ();

  DDData & operator= (const DDData &);

  DDData (const DDData &);

  void allocate (size_t num_samples, bool init_to_nan = false) 
      throw (Tango::DevFailed);

  void actual_num_samples (size_t num_samples);

  size_t current_buffers_depth () const;

  //- common data
  //-----------------------
  DDData::Buffer x_;
  DDData::Buffer z_;
  DDData::Buffer q_;
  DDData::Buffer sum_;
  DDData::Buffer va_;
  DDData::Buffer vb_;
  DDData::Buffer vc_;
  DDData::Buffer vd_;
  
  //- optional data
  //-----------------------
  DDData::Buffer sin_va_;
  DDData::Buffer cos_va_;
  DDData::Buffer sin_vb_;
  DDData::Buffer cos_vb_;
  DDData::Buffer sin_vc_;
  DDData::Buffer cos_vc_;
  DDData::Buffer sin_vd_;
  DDData::Buffer cos_vd_;
  
  struct timespec timestamp_;

  static bool m_optional_data_enabled;
};

// ============================================================================
//! SAData abstraction class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class SAData : private bpm::SharedObject
{
  friend class BPM;
  friend class DataProcessing;

public:

  typedef bpm::Buffer<bpm::FPDataType> Buffer;

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  typedef bpm::Buffer<i32> I32Buffer;
  typedef bpm::Buffer<i64> I64Buffer;
#endif

  typedef short UserData[kNUM_USER_DATA];
  
  /**
   * Ctor
   */
  SAData ();

  /**
   * Duplicate (shallow copy) this shared object 
   */
  SAData * duplicate (void);

  /**
   * Release this shared object 
   */
  void release (void);

  /**
   * The fp data accessors
   */
  const bpm::FPDataType& x (void) const;
  const bpm::FPDataType& z (void) const;
  const bpm::FPDataType& q (void) const;

  const bpm::FPDataType& va (void) const;
  const bpm::FPDataType& vb (void) const;
  const bpm::FPDataType& vc (void) const;
  const bpm::FPDataType& vd (void) const;

  const bpm::FPDataType& sum (void) const;

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  /**
   * The int data accessors
   */
  i32 int_x (void) const;
  i32 int_z (void) const;
  i32 int_sum (void) const;
#endif

	const long& cx (void) const;
	const long& cz (void) const;

	const UserData & user_data (void) const;
  
  /**
   * The data timestamp
   */
  const struct timespec & timestamp (void) const;

  /**
   * is optional data enabled?
   */
  inline static bool optional_data_enabled (void)
  {
    return SAData::m_optional_data_enabled;
  }
  
private:
  SAData (const CSPI_SA_ATOM &);
  SAData (const SAData &);
  SAData & operator= (const SAData &);
  SAData & operator= (const CSPI_SA_ATOM &);
  
  virtual ~SAData ();

  bpm::FPDataType x_;
  bpm::FPDataType z_;
  bpm::FPDataType q_;

  bpm::FPDataType sum_;

  bpm::FPDataType va_;
  bpm::FPDataType vb_;
  bpm::FPDataType vc_;
  bpm::FPDataType vd_;
  
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  i32 int_va_;
  i32 int_vb_;
  i32 int_vc_;
  i32 int_vd_;
  i32 int_x_;
  i32 int_z_;
  i32 int_sum_;
#endif

  long cx_;
  long cz_;

	UserData user_data_;   

  struct timespec timestamp_;

  static bool m_optional_data_enabled;
};

// ============================================================================
//! class: SAHistory
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class SAHistory
{
  friend class BPM;
  
public:
  //- define what is a circular buffer of SA data (fp)
  typedef CircularBuffer<bpm::FPDataType> SACircularBuffer;
  
  //- define what is an history iterator
  typedef SACircularBuffer::PastIterator HistoryIterator;
  
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  //- define what is a circular buffer of SA data (for i32) 
  typedef CircularBuffer<i32> SAI32CircularBuffer; 

  //- define what is an history iterator (for i32)
  typedef SAI32CircularBuffer::PastIterator I32HistoryIterator;
#endif

  /**
   * Freeze history
   */ 
  void freeze (void); 
  
  /**
   * Unfreeze history
   */ 
  void unfreeze (void);

  /**
   * Reset history content
   */
  void reset ();
  
  /**
   * The X data buffer accessor
   */
  const SAData::Buffer & x (void)
    throw (Tango::DevFailed);
    
  /**
   * Iterate on the last n history points 
   */
  inline HistoryIterator x_iterator (size_t n) const
  {
    return x_.past_iterator(n);
  }  

  /**
   * The Z data buffer accessor
   */
  const SAData::Buffer & z (void)
    throw (Tango::DevFailed);
    
  /**
   * Iterate on the last n history points 
   */
  inline HistoryIterator z_iterator (size_t n) const
  {
    return z_.past_iterator(n);
  }

  /**
   * The sum ordered data buffer accessor (optional data)
   */
  const SAData::Buffer & sum (void)
    throw (Tango::DevFailed);
    
  /**
   * Iterate on the last n history points  (optional data)
   */
  inline HistoryIterator sum_iterator (size_t n) const
    throw (Tango::DevFailed)
  {
    return s_.past_iterator(n);
  }
  
  /**
   * X and Z Mean Pos. values
   */
  const bpm::FPDataType& x_mean (void) const;
  const bpm::FPDataType& z_mean (void) const;
  
  /**
   * X and Z RMS Pos. values
   */
  const bpm::FPDataType& x_rms (void) const;
  const bpm::FPDataType& z_rms (void) const;

  /**
   * X and Z Peak Pos. values
   */
  const bpm::FPDataType& x_peak (void) const;
  const bpm::FPDataType& z_peak (void) const;

  /**
   *  Mean Sum (optional data)
   */
  const bpm::FPDataType& sum_mean (void) const
    throw (Tango::DevFailed);
  
  /**
   * computes X and Z pos statistics from last <n> samples in history
   */
  void compute_statistics (size_t n);

  /**
   * returns the actual number of samples currently stored in the Buffer
   */
  size_t actual_num_samples (void) const;

  /**
   * are optional data enabled?
   */
  inline static bool optional_data_enabled (void)
  {
    return SAHistory::m_optional_data_enabled;
  }
  
private:
  SAHistory (void);
  SAHistory (size_t depth);

  virtual ~SAHistory (void);
  
  void depth (size_t depth) 
    throw (Tango::DevFailed);
    
  void push (const SAData & sa) 
    throw (Tango::DevFailed);
  
  void unfreeze_i (void); 

  //- mandatory data
  SACircularBuffer x_;
  SACircularBuffer z_;
  //- optional sum history
  SACircularBuffer s_;

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  //- mandatory data
  SAI32CircularBuffer int_x_;
  SAI32CircularBuffer int_z_;
  //- optional sum history
  SAI32CircularBuffer int_s_;
#endif
 
  bpm::Mutex lock_;
  
  bool frozen_;

  bpm::FPDataType x_mean_;
  bpm::FPDataType z_mean_;  
  bpm::FPDataType x_rms_;
  bpm::FPDataType z_rms_;
  bpm::FPDataType x_peak_;
  bpm::FPDataType z_peak_;
  bpm::FPDataType s_mean_;
  
  size_t actual_num_samples_;
  
  //- Undefined methods and ops
  SAHistory & operator= (const SAHistory &);
  SAHistory (const SAHistory &);

  static bool m_optional_data_enabled;
};

// ============================================================================
//! ADCData abstraction class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class ADCData : private bpm::SharedObject
{
  friend class BPM;

public:

  typedef bpm::Buffer<short> Buffer;
  
  /**
   * Ctor
   */
  ADCData ();
  
  /**
   * Duplicate (shallow copy) this shared object 
   */
  ADCData * duplicate (void);

  /**
   * Release this shared object 
   */
  void release (void);

  /**
   * The data buffers accessors
   */
  const ADCData::Buffer& a (void) const;
  const ADCData::Buffer& b (void) const;
  const ADCData::Buffer& c (void) const;
  const ADCData::Buffer& d (void) const;

  /**
   * The data timestamp
   */
  const struct timespec & timestamp (void) const;
  
  /**
   * is optional data enabled?
   */
  inline static bool optional_data_enabled (void)
  {
    return ADCData::m_optional_data_enabled;
  }
  
private:

  virtual ~ADCData ();

  ADCData & operator= (const ADCData &);

  ADCData (const ADCData &);

  void allocate (size_t num_samples, bool init_to_zero = false) 
      throw (Tango::DevFailed);

  void actual_num_samples (size_t num_samples);
  
  size_t current_buffers_depth () const;
      
  ADCData::Buffer a_;
  ADCData::Buffer b_;
  ADCData::Buffer c_;
  ADCData::Buffer d_;
  
  struct timespec timestamp_;

  static bool m_optional_data_enabled;
};

} // namespace bpm

#if defined (__INLINE_IMPL__)
# include "BPMData.i"
#endif // __INLINE_IMPL__

#endif // _BPM_DATA_H_
