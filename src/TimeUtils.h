//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_TIME_UTILS_H_
#define _BPM_TIME_UTILS_H_

// ============================================================================
// TIME MACROS
// ============================================================================
#include <ctime>
#include <sys/time.h>
#include <math.h>

namespace bpm {

typedef struct _timeval
{
  int tv_sec;
  int tv_usec;
  time_t tv_utc;
  _timeval ()
    : tv_sec(0), tv_usec(0), tv_utc(0) {}
} _timeval;

} // namespace

#define _TIMESTAMP _timeval

#define _TIMESPEC timespec

#define _GET_TIME(T) \
  do \
  { \
    struct timeval _now; \
    ::gettimeofday(&_now, 0); \
    T.tv_sec = _now.tv_sec; \
    T.tv_usec = _now.tv_usec; \
    ::time(&T.tv_utc); \
  } while (0)

#define _MAX_DATE_LEN 256

#define _TIMESTAMP_TO_DATE(T,S) \
  do \
  { \
    struct tm * tmv = ::localtime(&T.tv_utc); \
    char b[_MAX_DATE_LEN]; \
    ::memset(b, 0, _MAX_DATE_LEN); \
    ::strftime(b, _MAX_DATE_LEN, "%a, %d %b %Y %H:%M:%S", tmv); \
    S = std::string(b); \
  } while (0)
  
#define	_RESET_TIMESTAMP(T) \
  do \
  { \
    T.tv_sec = 0; \
    T.tv_usec = 0; \
    T.tv_utc = 0; \
  } while (0)
  
#define	_COPY_TIMESTAMP(S, D) \
  do \
  { \
    D.tv_sec = S.tv_sec; \
    D.tv_usec = S.tv_usec; \
    D.tv_utc = S.tv_utc; \
  } while (0)
  
  
#define	_ELAPSED_SEC(B, A) \
  static_cast<double>((A.tv_sec - B.tv_sec) + (1.E-6 * (A.tv_usec - B.tv_usec)))

#define	_ELAPSED_MSEC(B, A) _ELAPSED_SEC(B, A) * 1.E3

#define	_ELAPSED_USEC(B, A) _ELAPSED_SEC(B, A) * 1.E6

#define _IS_VALID_TIMESTAMP(T) T.tv_sec != 0 || T.tv_usec != 0

#define	_TMO_EXPIRED(B, A, TMO) _ELAPSED_SEC (B, A) > TMO


namespace bpm
{

typedef _TIMESTAMP Timestamp;
typedef _TIMESPEC  Timespec;

// ============================================================================
//  A timer object measures elapsed time.
//  It is recommended that implementations measure wall clock rather than CPU
//  time since the intended use is performance measurement on systems where
//  total elapsed time is more important than just process or CPU time.
//  Warnings: The maximum measurable elapsed time may well be only 596.5+ hours
//  due to implementation limitations.  The accuracy of timings depends on the
//  accuracy of timing information provided by the underlying platform, and
//  this varies a great deal from platform to platform.
// ============================================================================
class Timer
{
public:
  Timer () 
  { 
    this->restart();
  } 
  
  void restart() 
  {
    ::gettimeofday(&m_start_time, NULL); 
  }

  //- return elapsed time in seconds
  double elapsed_sec () const             
  { 
    struct timeval now;
    ::gettimeofday(&now, NULL);
    return (now.tv_sec - m_start_time.tv_sec) + 1e-6 * (now.tv_usec - m_start_time.tv_usec);
  }

  //- return elapsed time in milliseconds
  double elapsed_msec () const             
  { 
    struct timeval now;
    ::gettimeofday(&now, NULL);
    return 1e3 * (now.tv_sec - m_start_time.tv_sec) + 1e-3 * (now.tv_usec - m_start_time.tv_usec);
  }

  //- return elapsed time in microseconds
  double elapsed_usec () const             
  { 
    struct timeval now;
    ::gettimeofday(&now, NULL);
    return 1e6 * (now.tv_sec - m_start_time.tv_sec) + (now.tv_usec - m_start_time.tv_usec);
  }

private:
  struct timeval m_start_time;
};

} // bpm

#endif // -_BPM_TIME_UTILS_H_
