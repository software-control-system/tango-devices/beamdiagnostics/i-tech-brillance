//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <limits>
#include <string>
#include "BPMConfig.h"
 
#if !defined (__INLINE_IMPL__)
# include "BPMConfig.i"
#endif // __INLINE_IMPL__

// ============================================================================
// CONSTs
// ============================================================================
#define kDEFAULT_DD_DEC_FACTOR  1
#define kDEFAULT_DD_TRIG_OFFSET 0
//-----------------------------------------------------------------------------
#define kDEFAULT_LO_LIMIT kLIBERA_STARTUP_CONFIG_ILK_XLO 
#define kDEFAULT_HI_LIMIT kLIBERA_STARTUP_CONFIG_ILK_XHI
//-----------------------------------------------------------------------------

namespace bpm
{

//- interlock shortcuts
enum {
  kINTERLOCK_CONFIG_ILK_MODE_IDX = 0,
  kINTERLOCK_CONFIG_ILK_XLO_IDX,
  kINTERLOCK_CONFIG_ILK_XHI_IDX,
  kINTERLOCK_CONFIG_ILK_ZLO_IDX,
  kINTERLOCK_CONFIG_ILK_ZHI_IDX,
  kINTERLOCK_CONFIG_ILK_OVFLL_IDX,
  kINTERLOCK_CONFIG_ILK_OVFLD_IDX,
  kINTERLOCK_CONFIG_ILK_GAINL_IDX
};

// ============================================================================
// BPMConfig::BPMConfig
// ============================================================================
BPMConfig::BPMConfig (Tango::DeviceImpl * _d)
  : host_device_ (_d)
{
#if ! defined(_EMBEDDED_DEVICE_)
  this->ip_addr_ = "unspecified";
  this->mcast_ip_addr_ = "unspecified";
  this->gs_port_ = GS_DEFAULT_PORT;
  this->mcast_port_ = GS_DEFAULT_MCAST_PORT;
#endif

  //- by default, we do not BBA offsets into account when computing 
  //- the offsets passed to the FPGA process
  this->pass_bba_offsets_to_fpga_ = false;

  //- by default, dd buffer freezing is disabled
  this->dd_buffer_freezing_enabled_ = false;
  
  //- by default, external trigger is disabled
  this->external_trigger_enabled_ = false;

  //- reset both "actual" and "requested" env. params
  ::memset (&this->actual_ep_, 0, sizeof (CSPI_ENVPARAMS));
  ::memset (&this->requested_ep_, 0, sizeof (CSPI_ENVPARAMS));

  //- init connection params for DD
  ::memset (&this->cp_dd_, 0, sizeof (CSPI_CONPARAMS_EBPP));
  this->cp_dd_.mode = CSPI_MODE_DD;
  this->cp_dd_.dec = kDEFAULT_DD_DEC_FACTOR;
  this->dd_raw_buffer_depth_ = NB_VALUE_02;
  this->max_dd_buffer_depth_for_dec_on_ = DD_NSAMPLES_MAX_VALUE_DECIM_ENABLED;
  this->dd_trigger_offset_ = kDEFAULT_DD_TRIG_OFFSET;
  
  //- init connection params for SA
  ::memset (&this->cp_sa_, 0, sizeof (CSPI_CONPARAMS_EBPP));
  this->cp_sa_.mode = CSPI_MODE_SA;
  this->cp_sa_.nonblock = 1;
  this->sa_data_history_buffer_depth_ = kDEFAULT_SA_HISTORY_DEPTH;
  this->sa_stats_num_samples_ = kDEFAULT_SA_STAT_SAMPLES;

  //- init connection params for PM
  ::memset (&this->cp_srv_, 0, sizeof (CSPI_CONPARAMS));
  this->cp_srv_.mode = CSPI_MODE_PM;

  //- init connection params for ADC
  ::memset (&this->cp_adc_, 0, sizeof (CSPI_CONPARAMS));
  this->cp_adc_.mode = CSPI_MODE_ADC;
  this->adc_raw_buffer_depth_ = NB_VALUE_02;
  
  //- init offsets and parameters for beam pos computation
  this->Kx = 1.0;
  this->Kz = 1.0;

  this->Xoffset_local = 0.0;
  this->Zoffset_local = 0.0;

  this->Xoffset_fpga = 0.0;
  this->Zoffset_fpga = 0.0;

  this->Qoffset_local = 0.0;
  this->Qoffset_fpga = 0.0;

  this->VaGainCorrection = 1.0; 
  this->VbGainCorrection = 1.0;
  this->VcGainCorrection = 1.0;
  this->VdGainCorrection = 1.0;

#if defined(__arm__) && defined(_USE_ARM_OPTIMIZATION_)
  this->int_Kx = 1;
  this->int_Kz = 1;
  this->int_Xoffset_local = 0;
  this->int_Zoffset_local = 0;
  this->int_Qoffset_local = 0;
  this->int_VaGainCorrection = 1;
  this->int_VbGainCorrection = 1;
  this->int_VcGainCorrection = 1;
  this->int_VdGainCorrection = 1;
#endif

  //- bpm location
  this->location_ = BPM_LOC_UNKNOWN;

  //- block geometry
  this->block_geometry_ = BLOCK_GEOMETRY_45;

  //- data sources flags
  this->sa_enabled_ = false;
  this->pm_enabled_ = false;
  this->adc_enabled_ = false;
  this->dd_enabled_ = false;     
  
  //- thread periods
  this->dd_thread_activity_period_ = kDEFAULT_THREAD_ACTIVITY_PERIOD_MS;
  this->sa_thread_activity_period_ = kDEFAULT_THREAD_ACTIVITY_PERIOD_MS;
  this->adc_thread_activity_period_ = kDEFAULT_THREAD_ACTIVITY_PERIOD_MS;
  this->fa_cache_refresh_period_ = kDEFAULT_FA_CACHE_REFRESH_PERIOD_MS;

  this->enable_auto_switching_on_sa_activation_ = true;
  this->enable_dsc_on_auto_switching_activation_ = true;

  this->local_sa_pos_computation_enabled_ = true;
  
  this->dd_optional_data_enabled_ = false;
  this->sa_optional_data_enabled_ = false;
  this->adc_optional_data_enabled_ = false;
  this->sa_history_optional_data_enabled_ = false;

  this->institute_ = kTANGO_INSTITUTE;
  this->pos_algorithm_ = kSTD_POS_ALGO;
}

// ============================================================================
// BPMConfig::BPMConfig
// ============================================================================
BPMConfig::BPMConfig (const BPMConfig & _src)
{
  //- delegate copy to operator=
  *this = _src;
}

// ============================================================================
// BPMConfig::~BPMConfig
// ============================================================================
BPMConfig::~BPMConfig (void)
{
  //- noop dtor
}

// ============================================================================
// BPMConfig::operator= 
// ============================================================================
BPMConfig & BPMConfig::operator= (const BPMConfig & _src)
{
  //- avoid self copy
  if (&_src == this)
    return *this;

#if ! defined(_EMBEDDED_DEVICE_)
  this->ip_addr_ = _src.ip_addr_;
  this->mcast_ip_addr_ = _src.mcast_ip_addr_;
  this->gs_port_ = _src.gs_port_;
  this->mcast_port_ = _src.mcast_port_;
#endif

  this->dd_buffer_freezing_enabled_ = _src.dd_buffer_freezing_enabled_;
    
  this->external_trigger_enabled_ = _src.external_trigger_enabled_;

  //- copy all CSPI bytes...
  ::memcpy (&this->requested_ep_, &_src.requested_ep_, sizeof (CSPI_ENVPARAMS));
  ::memcpy (&this->actual_ep_, &_src.actual_ep_, sizeof (CSPI_ENVPARAMS));
  ::memcpy (&this->cp_dd_, &_src.cp_dd_, sizeof (CSPI_CONPARAMS_EBPP));
  ::memcpy (&this->cp_sa_, &_src.cp_sa_, sizeof (CSPI_CONPARAMS_EBPP));
  ::memcpy (&this->cp_srv_, &_src.cp_srv_, sizeof (CSPI_CONPARAMS));
  ::memcpy (&this->cp_adc_, &_src.cp_adc_, sizeof (CSPI_CONPARAMS));

  this->offsets_ = _src.offsets_;

  this->dd_raw_buffer_depth_ = _src.dd_raw_buffer_depth_;
  
  this->max_dd_buffer_depth_for_dec_on_ = _src.max_dd_buffer_depth_for_dec_on_;
  
  this->adc_raw_buffer_depth_ = _src.adc_raw_buffer_depth_;
  
  this->dd_trigger_offset_ = _src.dd_trigger_offset_;

  this->Kx = _src.Kx;
  this->Kz = _src.Kz;

  this->Xoffset_local = _src.Xoffset_local;
  this->Zoffset_local = _src.Zoffset_local;

  this->Xoffset_fpga = _src.Xoffset_fpga;
  this->Zoffset_fpga = _src.Zoffset_fpga;

  this->Qoffset_local = _src.Qoffset_local;
  this->Qoffset_fpga = _src.Qoffset_fpga;

  this->VaGainCorrection = _src.VaGainCorrection;
  this->VbGainCorrection = _src.VbGainCorrection;
  this->VcGainCorrection = _src.VcGainCorrection;
  this->VdGainCorrection = _src.VdGainCorrection;

#if defined(__arm__) && defined(_USE_ARM_OPTIMIZATION_)
  this->int_Kx = _src.int_Kx;
  this->int_Kz = _src.int_Kz;
  this->int_Xoffset_local = _src.int_Xoffset_local;
  this->int_Zoffset_local = _src.int_Zoffset_local;
  this->int_Qoffset_local = _src.int_Qoffset_local;
  this->int_VaGainCorrection = _src.int_VaGainCorrection;
  this->int_VbGainCorrection = _src.int_VbGainCorrection;
  this->int_VcGainCorrection = _src.int_VcGainCorrection;
  this->int_VdGainCorrection = _src.int_VdGainCorrection;
#endif

  this->location_ = _src.location_;

  this->block_geometry_ = _src.block_geometry_;

  this->sa_enabled_ = _src.sa_enabled_;
  this->pm_enabled_ = _src.pm_enabled_;
  this->adc_enabled_ = _src.adc_enabled_;
  this->dd_enabled_ = _src.dd_enabled_;

  this->dd_thread_activity_period_ = _src.dd_thread_activity_period_;
  this->sa_thread_activity_period_ = _src.sa_thread_activity_period_;
  this->adc_thread_activity_period_ = _src.adc_thread_activity_period_;
  this->fa_cache_refresh_period_ = _src.fa_cache_refresh_period_;

  this->sa_data_history_buffer_depth_ = _src.sa_data_history_buffer_depth_;
  this->sa_stats_num_samples_ = _src.sa_stats_num_samples_;

  this->enable_auto_switching_on_sa_activation_ = _src.enable_auto_switching_on_sa_activation_;
  this->enable_dsc_on_auto_switching_activation_ = _src.enable_dsc_on_auto_switching_activation_;

  this->local_sa_pos_computation_enabled_ = _src.local_sa_pos_computation_enabled_;
  
  this->dd_optional_data_enabled_ = _src.dd_optional_data_enabled_;
  this->sa_optional_data_enabled_ = _src.sa_optional_data_enabled_;
  this->adc_optional_data_enabled_ = _src.adc_optional_data_enabled_;
  this->sa_history_optional_data_enabled_ = _src.sa_history_optional_data_enabled_;

  this->institute_ = _src.institute_;
  this->pos_algorithm_ = _src.pos_algorithm_;

  this->host_device_ = _src.host_device_;
  
  this->pass_bba_offsets_to_fpga_ = _src.pass_bba_offsets_to_fpga_;
  
  return *this;
}

// ============================================================================
// BPMConfig::set_offsets
// ============================================================================
void BPMConfig::set_offsets (const BPMOffsets & _src) 
  throw (Tango::DevFailed)
{
  //- copy offsets from _src
  this->offsets_ = _src;
}

// ============================================================================
// BPMConfig::compute_actual_offsets
// ============================================================================
void BPMConfig::compute_actual_offsets (CSPI_ENVPARAMS &ep, bool auto_switching_enabled) 
  throw (Tango::DevFailed)
{
  //- set Kx, Kz for DD
  this->Kx = this->offsets_[BPMOffsets::KX];
  this->Kz = this->offsets_[BPMOffsets::KZ];

  //- compute offsets for both DD and SA
  this->Xoffset_local = this->offsets_[BPMOffsets::X_OFFSET_1]
                      + this->offsets_[BPMOffsets::X_OFFSET_BBA];
                      
                      
  if (! auto_switching_enabled)
  {
    this->Xoffset_local = this->Xoffset_local
                        + this->offsets_[BPMOffsets::X_OFFSET_3]
                        + this->offsets_[BPMOffsets::X_OFFSET_4]
                        + this->offsets_[BPMOffsets::X_OFFSET_5];
  }

  this->Zoffset_local = this->offsets_[BPMOffsets::Z_OFFSET_1] 
                      + this->offsets_[BPMOffsets::Z_OFFSET_BBA];
                      
  if (! auto_switching_enabled)
  {
    this->Zoffset_local = this->Zoffset_local
                        + this->offsets_[BPMOffsets::Z_OFFSET_3]
                        + this->offsets_[BPMOffsets::Z_OFFSET_4]
                        + this->offsets_[BPMOffsets::Z_OFFSET_5];
  }
             
  bpm::FPDataType A, B, C, D, invA, invB, invC, invD;
  
  A = this->offsets_[BPMOffsets::A_BLOCK_GAIN_CORRECTION];
  if (! auto_switching_enabled)
    A *= this->offsets_[BPMOffsets::A_HW_GAIN_CORRECTION];
  invA = 1. / A;
   
  B = this->offsets_[BPMOffsets::B_BLOCK_GAIN_CORRECTION];
  if (! auto_switching_enabled)
    B *= this->offsets_[BPMOffsets::B_HW_GAIN_CORRECTION];
  invB = 1. / B;
    
  C = this->offsets_[BPMOffsets::C_BLOCK_GAIN_CORRECTION];
  if (! auto_switching_enabled)
    C *= this->offsets_[BPMOffsets::C_HW_GAIN_CORRECTION];
  invC = 1. / C;
    
  D = this->offsets_[BPMOffsets::D_BLOCK_GAIN_CORRECTION];
  if (! auto_switching_enabled)
    D *= this->offsets_[BPMOffsets::D_HW_GAIN_CORRECTION];
  invD = 1. / D;

  bpm::FPDataType x_global_electrode_offset = 
    this->Kx * ((invA + invD) - (invB + invC)) / (invA + invB + invC + invD);
  
  bpm::FPDataType z_global_electrode_offset = 
    this->Kz * ((invA + invB) - (invC + invD)) / (invA + invB + invC + invD);
  

  this->Xoffset_fpga = this->offsets_[BPMOffsets::X_OFFSET_1] 
                     + x_global_electrode_offset;
                     
  if (! auto_switching_enabled)
  {
    this->Xoffset_fpga  += this->offsets_[BPMOffsets::X_OFFSET_3]
                        +  this->offsets_[BPMOffsets::X_OFFSET_4]
                        +  this->offsets_[BPMOffsets::X_OFFSET_5];
  }
  
  if (this->pass_bba_offsets_to_fpga_)
  {
    this->Xoffset_fpga += this->offsets_[BPMOffsets::X_OFFSET_BBA];
  }                   

  this->Zoffset_fpga = this->offsets_[BPMOffsets::Z_OFFSET_1] 
                     + z_global_electrode_offset;
                     
  if (! auto_switching_enabled)
  {
    this->Zoffset_fpga  += this->offsets_[BPMOffsets::Z_OFFSET_3]
                        +  this->offsets_[BPMOffsets::Z_OFFSET_4]
                        +  this->offsets_[BPMOffsets::Z_OFFSET_5];
  }

  if (this->pass_bba_offsets_to_fpga_)
  {
    this->Zoffset_fpga += this->offsets_[BPMOffsets::Z_OFFSET_BBA];
  }   
  
  this->Qoffset_local = this->offsets_[BPMOffsets::Q_OFFSET_1];
  if (! auto_switching_enabled)
    this->Qoffset_local += this->offsets_[BPMOffsets::Q_OFFSET_2];

  this->Qoffset_fpga = 0;
    
  //- gain corrections
  this->VaGainCorrection = this->offsets_[BPMOffsets::A_BLOCK_GAIN_CORRECTION];
  if (! auto_switching_enabled)
    this->VaGainCorrection *= this->offsets_[BPMOffsets::A_HW_GAIN_CORRECTION];

  this->VbGainCorrection = this->offsets_[BPMOffsets::B_BLOCK_GAIN_CORRECTION];
  if (! auto_switching_enabled)
    this->VbGainCorrection *= this->offsets_[BPMOffsets::B_HW_GAIN_CORRECTION];

  this->VcGainCorrection = this->offsets_[BPMOffsets::C_BLOCK_GAIN_CORRECTION];
  if (! auto_switching_enabled)
    this->VcGainCorrection *= this->offsets_[BPMOffsets::C_HW_GAIN_CORRECTION];

  this->VdGainCorrection = this->offsets_[BPMOffsets::D_BLOCK_GAIN_CORRECTION];
  if (! auto_switching_enabled)
    this->VdGainCorrection *= this->offsets_[BPMOffsets::D_HW_GAIN_CORRECTION];

  //- we have to pass K and Offset values in nanometer to the Libera (FPGA processing)
  ep.Kx      = static_cast<int>(kMILLI_TO_NANO_METERS * this->Kx);
  ep.Ky      = static_cast<int>(kMILLI_TO_NANO_METERS * this->Kz);
  ep.Xoffset = static_cast<int>(kMILLI_TO_NANO_METERS * this->Xoffset_fpga);
  ep.Yoffset = static_cast<int>(kMILLI_TO_NANO_METERS * this->Zoffset_fpga);
  ep.Qoffset = static_cast<int>(kMILLI_TO_NANO_METERS * this->Qoffset_fpga);

#if defined(__arm__) && defined(_USE_ARM_OPTIMIZATION_)
  //- Kx, Kz and offsets in nanometer
  this->int_Kx = static_cast<bpm::IntOffsetDataType>(kMILLI_TO_NANO_METERS * this->Kx);
  this->int_Kz = static_cast<bpm::IntOffsetDataType>(kMILLI_TO_NANO_METERS * this->Kz);
  this->int_Xoffset_local = static_cast<bpm::IntOffsetDataType>(kMILLI_TO_NANO_METERS * this->Xoffset_local);
  this->int_Zoffset_local = static_cast<bpm::IntOffsetDataType>(kMILLI_TO_NANO_METERS * this->Zoffset_local);
  this->int_Qoffset_local = static_cast<bpm::IntOffsetDataType>(kMILLI_TO_NANO_METERS * this->Qoffset_local);
  //- here we trunc the gain correction factor to 4 digits: a.bcdefghij -> abcdef   
  this->int_VaGainCorrection = static_cast<bpm::IntOffsetDataType>(kGAIN_TRUNC_FACTOR * this->VaGainCorrection);
  this->int_VbGainCorrection = static_cast<bpm::IntOffsetDataType>(kGAIN_TRUNC_FACTOR * this->VbGainCorrection);
  this->int_VcGainCorrection = static_cast<bpm::IntOffsetDataType>(kGAIN_TRUNC_FACTOR * this->VcGainCorrection);
  this->int_VdGainCorrection = static_cast<bpm::IntOffsetDataType>(kGAIN_TRUNC_FACTOR * this->VdGainCorrection);
#endif

  if (this->offsets_[BPMOffsets::GEOMETRY] == kBLOCK_GEOMETRY_45)
  {
    this->block_geometry_ = BLOCK_GEOMETRY_45;
  }
  else if (this->offsets_[BPMOffsets::GEOMETRY] == kBLOCK_GEOMETRY_90)
  {
    this->block_geometry_ = BLOCK_GEOMETRY_90;
  }
  else
  {
    Tango::Except::throw_exception ((const char *) "invalid block geometry specified", 
                                    (const char *) "invalid block geometry specified - check BPM::BlockParameters property", 
                                    (const char *) "BPMConfig::compute_actual_offsets");
  }

#if defined(_DEBUG)
  this->dump_offsets();
#endif
}

// ============================================================================
// BPMConfig::dump_offsets
// ============================================================================
void BPMConfig::dump_offsets (void) const  
{
#if 0 //-TODO: remove this #if
  std::cout << "BPMConfig::offsets::Kx................." << this->Kx << std::endl;
  std::cout << "BPMConfig::offsets::Kz................." << this->Kz << std::endl;
  std::cout << "BPMConfig::offsets::Xoffset_local......" << this->Xoffset_local << std::endl;
  std::cout << "BPMConfig::offsets::Xoffset_fpga......." << this->Xoffset_fpga << std::endl;
  std::cout << "BPMConfig::offsets::Zoffset_local......" << this->Zoffset_local << std::endl;
  std::cout << "BPMConfig::offsets::Zoffset_fpga......." << this->Zoffset_fpga << std::endl;
  std::cout << "BPMConfig::offsets::Qoffset_local......" << this->Qoffset_local << std::endl;
  std::cout << "BPMConfig::offsets::Qoffset_fpga......." << this->Qoffset_fpga << std::endl;
  std::cout << "BPMConfig::offsets::VaGainCorrection..." << this->VaGainCorrection << std::endl;
  std::cout << "BPMConfig::offsets::VbGainCorrection..." << this->VbGainCorrection << std::endl;
  std::cout << "BPMConfig::offsets::VcGainCorrection..." << this->VcGainCorrection << std::endl;
  std::cout << "BPMConfig::offsets::VdGainCorrection..." << this->VdGainCorrection << std::endl;
# if defined(__arm__) && defined(_USE_ARM_OPTIMIZATION_)
  std::cout << "BPMConfig::offsets::int_Kx_local..........." << this->int_Kx << std::endl;
  std::cout << "BPMConfig::offsets::int_Kz_local..........." << this->int_Kz << std::endl;
  std::cout << "BPMConfig::offsets::int_Xoffset_local......" << this->int_Xoffset_local << std::endl;
  std::cout << "BPMConfig::offsets::int_Zoffset_local......" << this->int_Zoffset_local << std::endl;
  std::cout << "BPMConfig::offsets::int_Qoffset_local......" << this->int_Qoffset_local << std::endl;
  std::cout << "BPMConfig::offsets::int_VaGainCorrection..." << this->int_VaGainCorrection << std::endl;
  std::cout << "BPMConfig::offsets::int_VbGainCorrection..." << this->int_VbGainCorrection << std::endl;
  std::cout << "BPMConfig::offsets::int_VcGainCorrection..." << this->int_VcGainCorrection << std::endl;
  std::cout << "BPMConfig::offsets::int_VdGainCorrection..." << this->int_VdGainCorrection << std::endl;
# endif
#endif
}

// ============================================================================
// BPMConfig::dump_env_parameters
// ============================================================================
void BPMConfig::dump_env_parameters (void) const
{
#if 0 //-TODO: remove this #if
  std::cout << std::endl;

  std::cout << " Main env. parameters " << std::endl;
  std::cout << "----------------------" << std::endl;

# if ! defined(_EMBEDDED_DEVICE_)
  std::cout << "- IP@..........................." << this->ip_addr_ << std::endl;
  std::cout << "- GS-port......................." << this->gs_port_ << std::endl;
  std::cout << "- MCast-IP@....................." << this->mcast_ip_addr_ << std::endl;
  std::cout << "- MCast-port...................." << this->mcast_port_ << std::endl;
  std::cout << "- B-Freezing...................." << this->dd_buffer_freezing_enabled_ << std::endl;
# endif

  std::cout << "- Location......................";
  switch (this->location_)
  {
  case BPM_LOC_TL1:
    std::cout << "TL1";
    break;
  case BPM_LOC_BOOSTER:
    std::cout << "BOOSTER";
    break;
  case BPM_LOC_TL2:
    std::cout << "TL2";
    break;
  case BPM_LOC_STORAGE_RING:
    std::cout << "STORAGE RING";
    break;
  default:
    std::cout << "unknown";
    break;
  }
  std::cout << std::endl; 
  std::cout << std::endl; 
    
  std::cout << "   Requested  env. parameters   " << std::endl;
  std::cout << "--------------------------------" << std::endl;
  this->dump_env_parameters(this->requested_ep_);
  
  std::cout << std::endl;
  
  std::cout << "     Actual env. parameters     " << std::endl;
  std::cout << "--------------------------------" << std::endl;
  this->dump_env_parameters(this->actual_ep_);
  
  std::cout << std::endl;
  
  std::cout << "Actual match requested env. parameters : " 
            << ((this->requested_ep_ == this->actual_ep_) ? "YES" : "NO") 
            << std::endl;

  std::cout << std::endl;
#endif
}
 
// ============================================================================
// BPMConfig::dump_env_parameters
// ============================================================================
void BPMConfig::dump_env_parameters (const CSPI_ENVPARAMS &ep) const
{
  std::cout << "- Feature.customer..............0x" << std::hex << ep.feature.customer << std::dec << std::endl;
  std::cout << "- Feature.itech.................0x" << std::hex << ep.feature.itech << std::dec << std::endl;
  std::cout << "- Trigger mode.................." << ep.trig_mode << std::endl;
  std::cout << "- Kx............................" << ep.Kx << std::endl;
  std::cout << "- Kz............................" << ep.Ky << std::endl;
  std::cout << "- Xoffset......................." << ep.Xoffset << std::endl;
  std::cout << "- Zoffset......................." << ep.Yoffset << std::endl;
  std::cout << "- Qoffset......................." << ep.Qoffset << std::endl;
  std::cout << "- Switches......................" << ep.switches << std::endl;
  std::cout << "- Gain.........................." << ep.gain << std::endl;
  std::cout << "- AGC..........................." << ep.agc << std::endl;
  std::cout << "- DSC..........................." << ep.dsc << std::endl;
  std::cout << "- Interlocks.Mode..............." << ep.ilk.mode << std::endl;
  std::cout << "- Interlocks.Xlow..............." << ep.ilk.Xlow << std::endl;
  std::cout << "- Interlocks.Xhigh.............." << ep.ilk.Xhigh << std::endl;
  std::cout << "- Interlocks.Zlow..............." << ep.ilk.Ylow << std::endl;
  std::cout << "- Interlocks.Zhigh.............." << ep.ilk.Yhigh << std::endl;
  std::cout << "- Interlocks.OverflowLimit......" << ep.ilk.overflow_limit << std::endl;
  std::cout << "- Interlocks.OverflowDuration..." << ep.ilk.overflow_dur << std::endl;
  std::cout << "- Interlocks.GainLimit.........." << ep.ilk.gain_limit << std::endl;
  std::cout << "- External Switching ..........." << ep.external_switching << std::endl;
  std::cout << "- Switching Delay..............." << ep.switching_delay << std::endl;  
  std::cout << "- Tune Compensation ............" << ep.pll_status.mt_stat.nco_shift << std::endl;
  std::cout << "- Tune Offset .................." << ep.pll_status.mt_stat.vcxo_offset << std::endl;
  std::cout << "- External Trigger Delay........" << ep.trig_delay << std::endl;
  std::cout << "- Post Mortem Offset............" << ep.PMoffset << std::endl;
  std::cout << "- Has MAF support..............." << LIBERA_IS_MAF(ep.feature.itech) << std::endl;
  if (LIBERA_IS_MAF(ep.feature.itech))
  {
    std::cout << "- MAF Length...................." << ep.ddc_maflength << std::endl;
    std::cout << "- MAF Delay....................." << ep.ddc_mafdelay << std::endl;
  } 
}

// ============================================================================
// BPMConfig::dump_con_parameters
// ============================================================================
void BPMConfig::dump_con_parameters (void) const
{
  std::cout << std::endl;
  //- DD --
  std::cout << "Current DD Connection Parameters:" << std::endl;
  std::cout << "\t-async.notif.handler..." << std::hex << (unsigned int) this->cp_dd_.handler << std::endl;
  std::cout << "\t-user data............." << std::hex << (unsigned int) this->cp_dd_.user_data << std::endl;
  std::cout << "\t-event mask............" << std::hex << (unsigned int) this->cp_dd_.event_mask << std::endl;
  std::cout << "\t-decimation factor....." << std::hex << (unsigned int) this->cp_dd_.dec << std::endl;
  std::cout << std::endl;
  //- SA --
  std::cout << "Current SA Connection Parameters:" << std::endl;
  std::cout << "\t-async.notif.handler..." << std::hex << (unsigned int) this->cp_sa_.handler << std::endl;
  std::cout << "\t-user data............." << std::hex << (unsigned int) this->cp_sa_.user_data << std::endl;
  std::cout << "\t-event mask............" << std::hex << (unsigned int) this->cp_sa_.event_mask << std::endl;
  std::cout << std::endl;
  //- PM --
  std::cout << "Current PM Connection Parameters:" << std::endl;
  std::cout << "\t-async.notif.handler..." << std::hex << (unsigned int) this->cp_srv_.handler << std::endl;
  std::cout << "\t-user data............." << std::hex << (unsigned int) this->cp_srv_.user_data << std::endl;
  std::cout << "\t-event mask............" << std::hex << (unsigned int) this->cp_srv_.event_mask << std::endl;
  std::cout << std::endl;
  //- ADC --
  std::cout << "Current ADC Connection Parameters:" << std::endl;
  std::cout << "\t-async.notif.handler..." << std::hex << (unsigned int) this->cp_adc_.handler << std::endl;
  std::cout << "\t-user data............." << std::hex << (unsigned int) this->cp_adc_.user_data << std::endl;
  std::cout << "\t-event mask............" << std::hex << (unsigned int) this->cp_adc_.event_mask << std::dec << std::endl;
  std::cout << std::endl;
}

// ============================================================================
// BPMConfig::set_location
// ============================================================================
void BPMConfig::set_location (BPMLocation _loc) 
  throw (Tango::DevFailed)
{
  switch (_loc)
  {
    case BPM_LOC_TL1:
    case BPM_LOC_BOOSTER:
    case BPM_LOC_TL2:
    case BPM_LOC_STORAGE_RING:
      break;
    default:
      this->location_ = BPM_LOC_UNKNOWN;
      Tango::Except::throw_exception ((const char *) "invalid BPM location specified", 
                                      (const char *) "invalid BPM location specified", 
                                      (const char *) "BPMConfig::set_location");
      break;
  }

  this->location_ = _loc;
}

// ============================================================================
// BPMConfig::set_switches_mode
// ============================================================================
void BPMConfig::set_switches_mode (short _mode) 
  throw (Tango::DevFailed)
{ 
  if ((_mode < kMIN_SWITCHES_MODE || _mode > kMAX_SWITCHES_MODE) && _mode != kAUTO_SWITCHING_MODE)
  {
    Tango::Except::throw_exception ((const char *) "invalid Libera switch mode",
                                    (const char *) "invalid switches mode specified - valid range is [0...15] or 255 for auto-switching",
                                    (const char *) "BPMConfig::set_switches_mode");
  }
  
  this->requested_ep_.switches = _mode;

  if (this->enable_dsc_on_auto_switching_activation_ && _mode == kAUTO_SWITCHING_MODE)
    this->set_dsc_mode(CSPI_DSC_AUTO);
}

// ============================================================================
// BPMConfig::set_interlock_configuration
// ============================================================================
void BPMConfig::set_interlock_configuration (const std::vector<bpm::FPDataType>& vd) 
  
  throw (Tango::DevFailed)
{
  if (vd.size() != kINTERLOCK_CONFIG_FIELDS)
  {
    Tango::Except::throw_exception ((const char *) "invalid parameters",
                                    (const char *) "InterlockConfiguration property: vector size mismatch - too few or too much parameters passed",
                                    (const char *) "BPMConfig::set_interlock_configuration");
  }
  
  this->requested_ep_.ilk.mode           = static_cast<int>(vd[kINTERLOCK_CONFIG_ILK_MODE_IDX]);
  this->requested_ep_.ilk.Xlow           = static_cast<int>(vd[kINTERLOCK_CONFIG_ILK_XLO_IDX] * kMILLI_TO_NANO_METERS);
  this->requested_ep_.ilk.Xhigh          = static_cast<int>(vd[kINTERLOCK_CONFIG_ILK_XHI_IDX] * kMILLI_TO_NANO_METERS);
  this->requested_ep_.ilk.Ylow           = static_cast<int>(vd[kINTERLOCK_CONFIG_ILK_ZLO_IDX] * kMILLI_TO_NANO_METERS);
  this->requested_ep_.ilk.Yhigh          = static_cast<int>(vd[kINTERLOCK_CONFIG_ILK_ZHI_IDX] * kMILLI_TO_NANO_METERS);
  this->requested_ep_.ilk.overflow_limit = static_cast<int>(vd[kINTERLOCK_CONFIG_ILK_OVFLL_IDX]);
  this->requested_ep_.ilk.overflow_dur   = static_cast<int>(vd[kINTERLOCK_CONFIG_ILK_OVFLD_IDX]);
  this->requested_ep_.ilk.gain_limit     = static_cast<int>(vd[kINTERLOCK_CONFIG_ILK_GAINL_IDX]);
}

// ============================================================================
// BPMConfig::check_env_parameters
// ============================================================================
void BPMConfig::check_env_parameters (const CSPI_ENVPARAMS& ep) const 
  
  throw (Tango::DevFailed)
{
  //------------------------------------------
  //- TODO: rewrite this using the CSPI consts
  //------------------------------------------
  
  if (ep.trig_mode < kMIN_TRG_MODE || ep.trig_mode > kMAX_TRG_MODE)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid trigger mode specified - valid range is [1,2]",
                                    (const char *) "BPMConfig::check_env_parameters");
  }
  
  if (
      (ep.switches < kMIN_SWITCHES_MODE || ep.switches > kMAX_SWITCHES_MODE) 
        &&
      (ep.switches !=  kAUTO_SWITCHING_MODE)
     ) 
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid switches mode specified - valid range is [0..15] or 255",
                                    (const char *) "BPMConfig::check_env_parameters");
  }
 
  if (ep.gain < kMIN_GAIN || ep.gain > kMAX_GAIN)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid gain specified - valid range is [-100, 0] dBm",
                                    (const char *) "BPMConfig::check_env_parameters");
  }
  
  if (ep.agc < kMIN_AGC || ep.agc > kMAX_AGC)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid AGC mode specified - valid range is [0, 1]",
                                    (const char *) "BPMConfig::check_env_parameters");
  }
  
  if (ep.dsc < kMIN_DSC || ep.dsc > kMAX_DSC)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid DSC mode specified - valid range is [0, 2]",
                                    (const char *) "BPMConfig::check_env_parameters");
  }
 
  if (ep.ilk.mode != 0 && ep.ilk.mode != 1 && ep.ilk.mode != 3)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid interlock mode  - valid range is [0,1,3]",
                                    (const char *) "BPMConfig::set_startup_env_parameters");
  }

  if (ep.ilk.gain_limit < kMIN_GAIN || ep.ilk.gain_limit > kMAX_GAIN)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid interlock gain limit specified - valid range is [-100, 0] dBm",
                                    (const char *) "BPMConfig::set_startup_env_parameters");
  }
 
  if (ep.external_switching < kMIN_SWITCHING_EXT || ep.external_switching > kMAX_SWITCHING_EXT)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid external switching flag value - valid range is [0(internal), 1(external)]",
                                    (const char *) "BPMConfig::check_env_parameters");
  } 

  if (ep.switching_delay < kMIN_SWITCHING_DELAY )
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid switching delay - valid range is [>0]",
                                    (const char *) "BPMConfig::check_env_parameters");
  }   
  
  if (ep.mtncoshft < kMIN_TUNE_COMP || ep.mtncoshft > kMAX_TUNE_COMP)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid tune compensation flag value - valid range is [0(no), 1(yes)]",
                                    (const char *) "BPMConfig::check_env_parameters");
  } 

  if (ep.mtvcxoffs < kMIN_TUNE_OFFSET || ep.mtvcxoffs > kMAX_TUNE_OFFSET)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid tune compensation offset - valid range is [-500, 500]",
                                    (const char *) "BPMConfig::check_env_parameters");
  } 
  
  if (ep.trig_delay < kMIN_EXT_TRIGGER_DELAY )
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid external trigger delay - valid range is [>0]",
                                    (const char *) "BPMConfig::check_env_parameters");
  }
  
  if (ep.PMoffset < kMIN_PM_OFFSET || ep.PMoffset > kMAX_PM_OFFSET)
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid post mortem offset - valid range is [-512, 512]",
                                    (const char *) "BPMConfig::check_env_parameters");
  }
  
  /*
  if (ep.ddc_maflength < kMIN_MAF_LENGTH )
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid moving average filter length - valid range is [>1]",
                                    (const char *) "BPMConfig::check_env_parameters");
  }  

  if (ep.ddc_mafdelay < kMIN_MAF_DELAY )
  {
    Tango::Except::throw_exception ((const char *) "invalid env. parameter",
                                    (const char *) "invalid moving averafe filter delay - valid range is [>0]",
                                    (const char *) "BPMConfig::check_env_parameters");
  } 
  */ 
}

// ============================================================================
// BPMConfig::compare
// ============================================================================
int BPMConfig::compare (const BPMConfig& _cfg)
{
  int flags = 0;

  if (this->offsets_ != _cfg.offsets_)
    flags |= GAINS_OR_OFFSETS_CHANGED;
  
  if (this->dd_enabled() != _cfg.dd_enabled())
    flags |= DD_SOURCE_CHANGED;

  if (this->sa_enabled() != _cfg.sa_enabled())
    flags |= SA_SOURCE_CHANGED;

  if (this->adc_enabled() != _cfg.adc_enabled())
    flags |= ADC_SOURCE_CHANGED;
    
  //- if (this->get_dd_raw_buffer_depth() != const_cast<BPMConfig&>(_cfg).get_dd_raw_buffer_depth())
  //-   flags |= DD_RAW_BUFFER_SIZE_CHANGED;
    
  //- if (this->get_adc_raw_buffer_depth() != _cfg.get_adc_raw_buffer_depth())
  //-   flags |= ADC_RAW_BUFFER_SIZE_CHANGED;
    
  //- if (this->get_dd_trigger_offset() != _cfg.get_dd_trigger_offset())
  //-   flags |= DD_TRIGGER_OFFSET_CHANGED;
  
  if (this->get_decimation_factor() != _cfg.get_decimation_factor())
    flags |= DD_DECIMATION_CHANGED;
  
  if (this->get_switches_mode() != _cfg.requested_ep_.switches)
    flags |= SWITCHES_CHANGED; 

  if (this->agc_enabled() != _cfg.requested_ep_.agc)
    flags |= AGC_CHANGED;

  if (this->get_dsc_mode() != _cfg.requested_ep_.dsc)
    flags |= DSC_CHANGED;
    
  if (! this->agc_enabled() && (this->actual_ep_.gain != _cfg.requested_ep_.gain))
    flags |= GAIN_CHANGED;
    
#if ! defined(_EMBEDDED_DEVICE_)
  if (this->dd_buffer_freezing_enabled() != _cfg.dd_buffer_freezing_enabled_)
    flags |= DD_CACHE_STATUS_CHANGED; 
#endif
    
  if (this->external_switching_enabled() != _cfg.requested_ep_.external_switching)
    flags |= EXT_SWITCHING_CHANGED;
    
  if (this->get_switching_delay() != _cfg.requested_ep_.switching_delay)
    flags |= SWITCHING_DELAY_CHANGED;
    
  bool requested_ep_mtncoshft = _cfg.requested_ep_.mtncoshft ? true : false;
  if (this->tune_compensation_enabled() != requested_ep_mtncoshft)
    flags |= TUNE_COMPENSATION_CHANGED;
	
  if (this->get_tune_offset() != _cfg.requested_ep_.mtvcxoffs)
    flags |= TUNE_OFFSET_CHANGED;

  if (this->get_external_trigger_delay() != _cfg.requested_ep_.trig_delay)
    flags |= EXT_TRIGGER_DELAY_CHANGED;
	 
  if (this->get_pm_offset() != _cfg.requested_ep_.PMoffset)
    flags |= PM_OFFSET_CHANGED;	 	 
        
  if (this->get_maf_length() != _cfg.requested_ep_.ddc_maflength)
    flags |= MAF_LENGTH_CHANGED;

  if (this->get_maf_delay() != _cfg.requested_ep_.ddc_mafdelay) 
    flags |= MAF_DELAY_CHANGED;
    
  if (this->get_pos_algorithm() != _cfg.pos_algorithm_)
    flags |= POS_COMP_ALGO_CHANGED;
    
#if 0
  std::cout << "DD_SOURCE_CHANGED............." << ((flags & DD_SOURCE_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "SA_SOURCE_CHANGED............." << ((flags & SA_SOURCE_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "ADC_SOURCE_CHANGED............" << ((flags & ADC_SOURCE_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "DD_RAW_BUFFER_SIZE_CHANGED...." << ((flags & DD_RAW_BUFFER_SIZE_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "ADC_RAW_BUFFER_SIZE_CHANGED..." << ((flags & ADC_RAW_BUFFER_SIZE_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "DD_TRIGGER_OFFSET_CHANGED....." << ((flags & DD_TRIGGER_OFFSET_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "DD_DECIMATION_CHANGED........." << ((flags & DD_DECIMATION_CHANGED) ? "yes" : "no") << std::endl;
#if ! defined(_EMBEDDED_DEVICE_)
  std::cout << "DD_CACHE_STATUS_CHANGED......." << ((flags & DD_CACHE_STATUS_CHANGED) ? "yes" : "no") << std::endl;
#endif
  std::cout << "SWITCHES_CHANGED.............." << ((flags & SWITCHES_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "AGC_CHANGED..................." << ((flags & AGC_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "DSC_CHANGED..................." << ((flags & DSC_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "GAIN_CHANGED.................." << ((flags & GAIN_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "GAINS_OR_OFFSETS_CHANGED......" << ((flags & GAINS_OR_OFFSETS_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "EXT_SWITCHING_CHANGED........." << ((flags & EXT_SWITCHING_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "SWITCHING_DELAY_CHANGED......." << ((flags & SWITCHING_DELAY_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "COMPENSATION_TUNE_CHANGED....." << ((flags & TUNE_COMPENSATION_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "OFFSET_TUNE_CHANGED..........." << ((flags & TUNE_OFFSET_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "EXT_TRIGGER_DELAY_CHANGED....." << ((flags & EXT_TRIGGER_DELAY_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "PM_OFFSET_CHANGED............." << ((flags & PM_OFFSET_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "MAF_LENGTH_CHANGED............" << ((flags & MAF_LENGTH_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "MAF_DELAY_CHANGED............." << ((flags & MAF_DELAY_CHANGED) ? "yes" : "no") << std::endl;
  std::cout << "POS_COMP_ALGO_CHANGED........." << ((flags & POS_COMP_ALGO_CHANGED) ? "yes" : "no") << std::endl;
#endif

  return flags;
}

// ============================================================================
// BPMConfig::actual_to_requested_ep
// ============================================================================
void BPMConfig::actual_to_requested_ep (void)
{
  this->actual_ep_.mtvcxoffs = this->actual_ep_.pll_status.mt_stat.vcxo_offset;
  this->actual_ep_.mtncoshft = this->actual_ep_.pll_status.mt_stat.nco_shift;
  ::memcpy(&this->requested_ep_, &this->actual_ep_, sizeof(CSPI_ENVPARAMS));
} 
             
// ============================================================================
// bool operator!= (const CSPI_ENVPARAMS&, const CSPI_ENVPARAMS&)
// ============================================================================
bool operator!= (const CSPI_ENVPARAMS &env_params_1, const CSPI_ENVPARAMS &env_params_2)
{
  size_t cspi_env_base_size = sizeof(cspi_health_t)
                            + sizeof(cspi_pll_t) 
                            + sizeof(int)
                            + sizeof(cspi_feature_t);
                            
  size_t size_to_cmp = sizeof(CSPI_ENVPARAMS) - cspi_env_base_size;
  
  bool c1 = ::memcmp (&env_params_1.trig_mode, &env_params_2.trig_mode, size_to_cmp) != 0;

  bool c2 = env_params_1.trig_mode != env_params_2.trig_mode;

  return c1 || c2;
}

// ============================================================================
// bool operator== (const CSPI_ENVPARAMS&, const CSPI_ENVPARAMS&)
// ============================================================================
bool operator== (const CSPI_ENVPARAMS &env_params_1, const CSPI_ENVPARAMS &env_params_2)
{
  return ! (env_params_1 != env_params_2);
}

} // namespace bpm
