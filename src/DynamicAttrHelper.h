//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2007-2008 Julien Malik, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _DYNAMIC_ATTR_HELPER_H_
#define _DYNAMIC_ATTR_HELPER_H_

#include "CommonHeader.h"

namespace bpm
{

typedef std::map <std::string, Tango::Attr*> DynAttrRepository;
typedef DynAttrRepository::value_type        DynAttrEntry;
typedef DynAttrRepository::iterator          DynAttrIt;
typedef DynAttrRepository::const_iterator    DynAttrCIt;

// ============================================================================
//
// class: DynamicAttrHelper
//
// ============================================================================
class DynamicAttrHelper
{
public:
  /**
   * Constructor. 
   * @param  _host_device the device handled by the instance
   */
  DynamicAttrHelper();
  
  /**
   * Destructor
   */
  ~DynamicAttrHelper();

  /**
   * host_device
   * @param attr a Tango::Attr* to be registered
   */
  void host_device (Tango::DeviceImpl * host_device);
    
  /**
   * add
   * @param attr a Tango::Attr* to be registered
   */
  void add (Tango::Attr * attr)
		throw (Tango::DevFailed);

  /**
   * remove
   * @param name the attribute name
   */
  void remove (const std::string& name)
		throw (Tango::DevFailed);

  /**
   * remove_all
   *
   * Removes all the dynamic attributes registered
   */
  void remove_all ()
		throw (Tango::DevFailed);

  /**
   * get (Tango::Attr version)
   * @param _name the attribute name
   * @param _a a reference to a Tango::Attr* where the pointer to the desired attribute is stored
   */
  Tango::Attr* get (const std::string& _name)
		throw (Tango::DevFailed)
  {
    DynAttrIt it = this->rep_.find(_name);
	  if (it == this->rep_.end())
	  {
	    THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
	                    "Attribute does not exist",
                      "DynamicAttrHelper::get_attribute");
	  }
	  return (*it).second;
  };

  DynAttrCIt begin() const;
  DynAttrIt  begin();
  
  DynAttrCIt end() const;
  DynAttrIt  end();
  
  size_t size() const;
  bool empty() const;

private:
  Tango::DeviceImpl * host_device_;
  DynAttrRepository   rep_;

  DynamicAttrHelper (const DynamicAttrHelper&);
  DynamicAttrHelper& operator= (const DynamicAttrHelper&);
};

} // namespace

//=============================================================================
// INLINED CODE
//=============================================================================
#if defined (__INLINE_IMPL__)
# include "DynamicAttrHelper.i"
#endif

#endif 
