//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _ARM_OPTIMIZATION_H_
#define _ARM_OPTIMIZATION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"

// ============================================================================
//- check configuration
// ============================================================================
#if ! defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
# error ARM optimized code can only runs on ARM target - please undef _USE_ARM_OPTIMIZATION_  
#endif

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

// ============================================================================
// INCLUDE MA's CODE
// ============================================================================
# include "ma/cordic.h"
# include "ma/numeric.h"

// ============================================================================
// CONVERSION/SCALING MACROS
// ============================================================================
# define INT_TO_FP(i, fp) \
   ArmOptimization::fixed_to_fp(i, \
                                fp, \
                                ArmOptimization::no_scaling_scale, \
                                ArmOptimization::no_scaling_shift)
//-----------------------------------------------------------------------------    
# define NM_TO_MM(i, fp) \
   ArmOptimization::fixed_to_fp(i, \
                                fp, \
                                ArmOptimization::nm_to_mm_scale, \
                                ArmOptimization::nm_to_mm_shift) 
//-----------------------------------------------------------------------------    
# define NM_TO_UM(i, fp) \
   ArmOptimization::fixed_to_fp(i, \
                                fp, \
                                ArmOptimization::nm_to_um_scale, \
                                ArmOptimization::nm_to_um_shift) 
//----------------------------------------------------------------------------- 
# define UNSCALE_BUTTON_GAIN_CORRECTION(i, fp) \
   ArmOptimization::fixed_to_fp(i, \
                                fp, \
                                ArmOptimization::gain_scaling_scale, \
                                ArmOptimization::gain_scaling_shift)
//-----------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// SHORTCUTS
// ============================================================================
typedef int i32;
typedef unsigned int u32;
typedef long long i64;
typedef unsigned long long u64;
typedef union
{
  u64 l;
  u32 i[2];
} u64_pun;

// ============================================================================
// IEEE-754 EXPONENT BIAS FOR SINGLES (FP32) & DOUBLES (FP64)
// ============================================================================
#if defined(_USE_FLOAT_FP_DATA_) 
# define EXPONENT_BIAS 0x7F   // 127 
#else
# define EXPONENT_BIAS 0x3FF  // 1023
#endif

// ============================================================================
//! ArmOptimization class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class ArmOptimization
{
public:
  /**
   * Scaling factors initializations for int to fp conversions
   */ 
  static void init_scaling_factors ();
  
  /**
   * int-32 to fp-{32 or 64}
   */ 
# if defined(_USE_FLOAT_FP_DATA_)
  static void fixed_to_fp (i32 input, float *result, u32 scaling, i32 scaling_shift);
#else
  static void fixed_to_fp (i32 input, double *result, u64 scaling, i32 scaling_shift);
#endif

  /**
   * Scaling factors computation
   */ 
#if defined(_USE_FLOAT_FP_DATA_)
  static void compute_scaling_factor (float scaling, u32& i32_scaling, i32& scaling_shift);
#else
  static void compute_scaling_factor (double scaling, u64& i64_scaling, i32& scaling_shift); 
#endif

  /**
   * A nice integer sqrt implementation (http://www.embedded.com/98/9802fe2.htm) 
   */ 
  static u32 sqrt (u32 a);

  //- scaling factors
#if defined(_USE_FLOAT_FP_DATA_)
  typedef u32 ScalingFactor;
#else
  typedef u64 ScalingFactor;
#endif
  typedef i32 ScalingShift;
  
  static ScalingFactor no_scaling_scale;
  static ScalingShift  no_scaling_shift;
  static ScalingFactor nm_to_mm_scale;
  static ScalingShift  nm_to_mm_shift;
  static ScalingFactor nm_to_um_scale;
  static ScalingShift  nm_to_um_shift;
  static ScalingFactor gain_scaling_scale;
  static ScalingShift  gain_scaling_shift;
  
private:
  // = Disallow these operations.
  //--------------------------------------------
  ArmOptimization (void);
  ArmOptimization (const ArmOptimization &);
  virtual ~ArmOptimization (void);
  ArmOptimization & operator= (const ArmOptimization &);
};

} // namespace bpm

#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

#if defined (__INLINE_IMPL__)
# include "ArmOptimization.i"
#endif // __INLINE_IMPL__

#endif // _ARM_OPTIMIZATION_H_
