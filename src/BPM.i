//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
// BPM::configuration
// ============================================================================
INLINE_IMPL const BPMConfig & BPM::configuration (void) const
{
  //- return current configuration to caller
  //- since the local storage (i.e. config_ member) is updated each time
  //- BPM::configure is called we can simply return it.
  //- no need to request it from hardware
  return this->config_;
}

// ============================================================================
// BPM::initialized
// ============================================================================
INLINE_IMPL bool BPM::initialized (void) const
{
  return this->srv_task_ != 0;
}

#if ! defined(_EMBEDDED_DEVICE_)
// ============================================================================
// BPM::current_cspi_events_mask
// ============================================================================
INLINE_IMPL int BPM::current_cspi_events_mask (void) const
{
  return reinterpret_cast<const CSPI_CONPARAMS&>(this->config_.cp_srv_).event_mask;
}
#endif

// ============================================================================
// BPM::get_state
// ============================================================================
INLINE_IMPL void BPM::get_sw_status (BPMSwStatus& sw_status_) const
{
  sw_status_.state = this->state_;
  
  if (! this->initialized())
  {
#if ! defined(_EMBEDDED_DEVICE_)
    sw_status_.status = std::string("BPM initialization failed [no active connection to the Libera - unreachable or embedded generic server down]"); 
#else
    sw_status_.status = std::string("BPM initialization failed [Libera software error]");
#endif
  }
  else
  {
	  switch (sw_status_.state) 
    {
      case BPM::BPM_RUNNING:
        sw_status_.status = "BPM is up and ready [running]";
        break;
      case BPM::BPM_FAULT:
#if ! defined(_EMBEDDED_DEVICE_)
        sw_status_.status = std::string("CSPI request failed [connection lost or Libera internal error - see device log for details]\n");
        sw_status_.status += std::string("Be sure the Libera is reachable and all embedded services are running then execute the <Init> command on the device");
#else
        sw_status_.status = std::string("CSPI request failed  [Libera internal error - see device log for details]\n");
        sw_status_.status += std::string("Be sure all Libera embedded services are running then execute the <Init> command on the device");
#endif
        break;
      default:
        sw_status_.status = std::string("unknown state");
        break;
    }
  }
}

// ============================================================================
// BPM::pm_notified
// ============================================================================
INLINE_IMPL bool BPM::pm_notified (void) const
{
  return this->pm_notified_;
}

// ============================================================================
// BPM::pm_event_counter
// ============================================================================
INLINE_IMPL short BPM::pm_event_counter (void) const
{
  return this->pm_event_counter_;
}

// ============================================================================
// BPM::trigger_counter
// ============================================================================
INLINE_IMPL long BPM::trigger_counter (void) const
{
  return this->trigger_counter_;
}

// ============================================================================
// BPM::reset_trigger_counter
// ============================================================================
INLINE_IMPL void BPM::reset_trigger_counter (void)
{
  this->trigger_counter_ = 0;
}

// ============================================================================
// BPM::get_sa_history
// ============================================================================
INLINE_IMPL SAHistory & BPM::get_sa_history (void)
{
  return this->sa_history_;
}

// ============================================================================
// BPM::get_hw_status
// ============================================================================
INLINE_IMPL void BPM::get_hw_status (BPMHwStatus& _dest) const
{
  _dest = this->hw_status_;
}

} //- namespace bpm
