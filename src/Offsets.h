//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#ifndef _BPM_OFFSETS_H_
#define _BPM_OFFSETS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BPMLocation.h"

// ============================================================================
// SYSTEM WIDE PROPERTIES
// ============================================================================
#define BPM_ROOT_PROPERTY       "BPM"
#define KX_KZ_PARAMS_PROPERTY   "KxKzParameters"
#define DEVICE_PARAMS_PROPERTY  "DeviceParameters"
#define BLOCK_PARAMS_PROPERTY   "BlockParameters"
#define HW_PARAMS_PROPERTY      "HwParameters"
// ----------------------------------------------------------------------------

namespace bpm
{

// ============================================================================
//! Offests abstraction class.
// ============================================================================
//!
//! detailed description to be defined
//!
// ============================================================================
class BPMOffsets
{
  friend class BPM;

public:

  typedef FPDataType FpOffsetsType;

  //- BPM parameters IDs ----------------------------------
  enum
  {
    //- block parameters
    GEOMETRY,
    Q_OFFSET_1,
    A_BLOCK_GAIN_CORRECTION,
    B_BLOCK_GAIN_CORRECTION,
    C_BLOCK_GAIN_CORRECTION,
    D_BLOCK_GAIN_CORRECTION,
    X_OFFSET_1,
    X_OFFSET_BBA,
    Z_OFFSET_1,
    Z_OFFSET_BBA,
    X_LOW,
    Z_LOW,
    X_HIGH,
    Z_HIGH,
    X_WARN,
    Z_WARN,
    X_ALARM,
    Z_ALARM,
    //- hw parameters
    KX,
    KZ,
    Q_OFFSET_2,
    A_HW_GAIN_CORRECTION,
    B_HW_GAIN_CORRECTION,
    C_HW_GAIN_CORRECTION,
    D_HW_GAIN_CORRECTION,
    X_OFFSET_3,
    RESERVED_1,
    RESERVED_2,
    X_OFFSET_4,
    X_OFFSET_5,
    Z_OFFSET_3,
    RESERVED_3,
    RESERVED_4,
    Z_OFFSET_4,
    Z_OFFSET_5,
    //- last parameter
    LAST_PARAMETER
  };

  /**
   * BPMOffsets default ctor 
   */
  BPMOffsets (void);

  /**
   * BPMOffsets copy ctor
   * \param src The source configuration.
   */
  BPMOffsets (const BPMOffsets & src);

  /**
   * BPMOffsets operator=
   * \param src The source configuration.
   */
  BPMOffsets & operator= (const BPMOffsets & src);

  /**
   * BPMOffsets dtor
   */
  virtual ~ BPMOffsets (void);

  /**
   * BPMOffsets operator==
   * \param src The source configuration.
   */
  bool operator== (const BPMOffsets & src);
  
   /**
   * BPMOffsets operator==
   * \param src The source configuration.
   */
  bool operator!= (const BPMOffsets & src);
  
  /**
   * Read all offsets and parameters from the TANGO database
   */
  void read_from_tango_db (BPMLocation location, Tango::DeviceImpl * device) 
    throw (Tango::DevFailed);

  /**
   * Returns the specified offset (use enum)
   */
  FpOffsetsType operator[] (int idx) const 
    throw (Tango::DevFailed);

  /**
   * Returns the name of the specified offset (use enum)
   */
  const char * parameter_name (int idx) const 
    throw (Tango::DevFailed);

  /**
   * Returns the block identifier
   */
  const std::string & block_identifier (void) const;

  /**
   * Returns Libera hardware identifier
   */
  const std::string & hw_identifier (void) const;

private:
  //- The TANGO device
  Tango::DeviceImpl * device;

  //- Floating point params repository
  std::vector<FpOffsetsType> bpm_params;
  
  //- Block identifier
  std::string block_id;

  //- Libera hardware identifier
  std::string hw_id;
  
  //- BPM::device parameters names
  static const char *bpm_params_names[];
};

} // namespace bpm

#if defined (__INLINE_IMPL__)
# include "Offsets.i"
#endif // __INLINE_IMPL__

#endif // _BPM_OFFSETS_H_
