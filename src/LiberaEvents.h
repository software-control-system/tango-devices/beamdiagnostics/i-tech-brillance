//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

#include "CommonHeader.h"

#if defined(_EMBEDDED_DEVICE_)

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "threading/Thread.h"

namespace bpm
{

// ============================================================================
// SOME VERY SPECIAL EVENTS
// ============================================================================
#define CSPI_EVENTS_MANAGER_ERROR -1

// ============================================================================
// Libera event handler signature
// ============================================================================
typedef int (*LiberaEventHandler) (CSPI_EVENT *);
  
// ============================================================================
//! LiberaEventsManagerConfig class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
struct LiberaEventsManagerConfig
{
  LiberaEventsManagerConfig ()
    : evt_handler(0), user_data(0), evts_mask(0), host_device(0)
  {
    //- noop
  }
  LiberaEventsManagerConfig (const LiberaEventsManagerConfig& src)
  {
    *this = src;
  }
  const LiberaEventsManagerConfig& operator= (const LiberaEventsManagerConfig& src)
  {
    if (&src == this)
      return *this;
    ::memcpy(this, &src, sizeof(LiberaEventsManagerConfig));
    return *this;
  }
  //- Libera event handler (called upon receipt of a Libera event)
  LiberaEventHandler evt_handler;
  //- abstract user data (passed back with each event)
  void * user_data;
  //- events mask
  int evts_mask;
  //- host device (logging)
  Tango::DeviceImpl * host_device;
};

// ============================================================================
//! LiberaEventsManager class.
// ============================================================================
//!
//! detailed description to be written
//!
// ============================================================================
class LiberaEventsManager : public Thread
{
  friend class BPM;
  
protected:
  //- ctor
  LiberaEventsManager (const LiberaEventsManagerConfig & cfg);

  //- dtor
  ~LiberaEventsManager ();

  //- returns true if the generic server is running on the Libera (should not!)
  static bool is_liberad_running (bool kill_if_running = false)
    throw (Tango::DevFailed);

  //- returns true if the eventd is running on the Libera (should not!)
  static bool is_eventd_running (bool kill_if_running = false)
    throw (Tango::DevFailed);

  //- start the underlying thread
  void go ();

  //- stop then join with the underlying thread
  virtual void exit ();
  
  //- change events mask
  void set_events_mask (int evts_mask)
    throw (Tango::DevFailed);
    
  //- returns current events mask
  int get_events_mask () const;
    
  //- enable the specified events
  void enable_events (int evts_mask)
    throw (Tango::DevFailed);

  //- enable the specified events
  void disable_events (int evts_mask)
    throw (Tango::DevFailed);
  
  //- thread entry point
  virtual IOArg run_undetached (IOArg);
  
private:
  //- initializer
  void init_i ()
    throw (Tango::DevFailed);

  //- close
  void close_i ()
    throw (Tango::DevFailed);

  //- change events mask (thread unsafe)
  void set_events_mask_i (int _evts_mask)
    throw (Tango::DevFailed);
    
  //- read Libera event FIFO
  int read_fifo_i (void * buf, const size_t ntotal, size_t * nleft)
    throw (Tango::DevFailed);

  //- handle Libera event
  void handle_event_i (const libera_event_t &_evt);
  
  //- notify error
  void notify_error_i ();

  //- returns true if the specified daemon is running on the Libera
  static bool deamon_running_i (const char * pid_file, bool kill_if_running)
    throw (Tango::DevFailed);

  //- config
  LiberaEventsManagerConfig cfg_;
  
  //- Libera event FIFO descriptor
  int fifo_fd_;

  //- thread ctrl flag
  bool go_on_;

  //- reason why the thread died
  static Tango::DevFailed error;
};

} //- namespace bpm

#endif // _EMBEDDED_DEVICE

