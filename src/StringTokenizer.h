//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

/*
 ***********************************************************************
 * Class: StringTokenizer                                              *
 * By Arash Partow - 2000                                              *
 * URL: http://www.partow.net/programming/stringtokenizer/index.html   *
 *                                                                     *
 * Copyright Notice:                                                   *
 * Free use of this library is permitted under the guidelines and      *
 * in accordance with the most current version of the Common Public    *
 * License.                                                            *
 * http://www.opensource.org/licenses/cpl.php                          *
 *                                                                     *
 ***********************************************************************
*/



#ifndef INCLUDE_STRINGTOKENIZER_H
#define INCLUDE_STRINGTOKENIZER_H


#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>



using namespace std;

namespace bpm {

class StringTokenizer
{

public:

  StringTokenizer (string str, string delim);
  ~StringTokenizer ()
  {}
  ;

  int countTokens ();
  bool hasMoreTokens ();
  string nextToken ();
  int nextIntToken ();
  long nextLongToken ();
  bpm::FPDataType nextFPToken ();
  string nextToken (string delim);
  string remainingString ();
  string filterNextToken (string filterStr);

private:

  string tokenStr;
  string delim;

};

} // namespace

#endif
