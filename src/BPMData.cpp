//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott,
// Diamond Light Source Ltd. See ./ma/README for details.
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include "BPMData.h"
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
# include "ArmOptimization.h"
#endif

#if !defined (__INLINE_IMPL__)
# include "BPMData.i"
#endif // __INLINE_IMPL___

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
# define kINV_NM_TO_MICRON_POW2   1000000ULL
#else
# define kMM_TO_MICRON        1.e3
# define kMM_TO_MICRON_POW2   1.e6
#endif

namespace bpm
{

// ============================================================================
// optional data flags
// ============================================================================
bool DDData::m_optional_data_enabled = false;
bool SAData::m_optional_data_enabled = false;
bool ADCData::m_optional_data_enabled = false;
bool SAHistory::m_optional_data_enabled = false;

// ============================================================================
// DDData::DDData
// ============================================================================
DDData::DDData (void)
  : SharedObject (),
    x_ (0),
    z_ (0),
    q_ (0),
    sum_ (0),
    va_ (0),
    vb_ (0),
    vc_ (0),
    vd_ (0),
    sin_va_ (0),
    cos_va_ (0),
    sin_vb_ (0),
    cos_vb_ (0),
    sin_vc_ (0),
    cos_vc_ (0),
    sin_vd_ (0),
    cos_vd_ (0)
{
  //- noop
}

// ============================================================================
// DDData::allocate
// ============================================================================
void DDData::allocate (size_t num_samples, bool init_to_nan)
  throw (Tango::DevFailed)
{
  try
  {
    this->x_.depth(num_samples);
    if (init_to_nan) this->x_.fill(_NAN_);

    this->z_.depth(num_samples);
    if (init_to_nan) this->z_.fill(_NAN_);

    this->q_.depth(num_samples);
    if (init_to_nan) this->q_.fill(_NAN_);

    this->sum_.depth(num_samples);
    if (init_to_nan) this->sum_.fill(_NAN_);

    this->va_.depth(num_samples);
    if (init_to_nan) this->va_.fill(_NAN_);

    this->vb_.depth(num_samples);
    if (init_to_nan) this->vb_.fill(_NAN_);

    this->vc_.depth(num_samples);
    if (init_to_nan) this->vc_.fill(_NAN_);

    this->vd_.depth(num_samples);
    if (init_to_nan) this->vd_.fill(_NAN_);

    if (DDData::m_optional_data_enabled)
    {
      this->sin_va_.depth(num_samples);
      if (init_to_nan) this->sin_va_.fill(_NAN_);
      this->cos_va_.depth(num_samples);
      if (init_to_nan) this->cos_va_.fill(_NAN_);

      this->sin_vb_.depth(num_samples);
      if (init_to_nan) this->sin_vb_.fill(_NAN_);
      this->cos_vb_.depth(num_samples);
      if (init_to_nan) this->cos_vb_.fill(_NAN_);

      this->sin_vc_.depth(num_samples);
      if (init_to_nan) this->sin_vc_.fill(_NAN_);
      this->cos_vc_.depth(num_samples);
      if (init_to_nan) this->cos_vc_.fill(_NAN_);

      this->sin_vd_.depth(num_samples);
      if (init_to_nan) this->sin_vd_.fill(_NAN_);
      this->cos_vd_.depth(num_samples);
      if (init_to_nan) this->cos_vd_.fill(_NAN_);
    }
  }
  catch (const std::bad_alloc &)
  {
    Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                    _CPTC ("memory allocation failed"),
                                    _CPTC ("DDData::allocate"));
  }
  catch (const Tango::DevFailed& df)
  {
    Tango::Except::re_throw_exception (const_cast<Tango::DevFailed&>(df),
                                       _CPTC ("OUT_OF_MEMORY"),
                                       _CPTC ("memory allocation failed"),
                                       _CPTC ("DDData::allocate"));
  }
  catch (...)
  {
    Tango::Except::throw_exception (_CPTC ("UNKNOWN_ERROR"),
                                    _CPTC ("unknown exception caught"),
                                    _CPTC ("DDData::allocate"));
  }
}

// ============================================================================
// DDData::DDData
// ============================================================================
DDData::~DDData (void)
{
  //- noop dtor
}

// ============================================================================
// DDData::operator=
// ============================================================================
DDData & DDData::operator= (const DDData& src)
{
  if (this == &src)
    return *this;

  if (this->current_buffers_depth() != src.current_buffers_depth())
    this->allocate(src.current_buffers_depth(), false);

  this->x_ = src.x_;
  this->z_ = src.z_;
  this->q_ = src.q_;

  this->sum_ = src.sum_;

  this->va_ = src.va_;
  this->vb_ = src.vb_;
  this->vc_ = src.vc_;
  this->vd_ = src.vd_;

  if (DDData::m_optional_data_enabled)
  {
    this->sin_va_ = src.sin_va_;
    this->cos_va_ = src.cos_va_;

    this->sin_vb_ = src.sin_vb_;
    this->cos_vb_ = src.cos_vb_;

    this->sin_vc_ = src.sin_vc_;
    this->cos_vc_ = src.cos_vc_;

    this->sin_vd_ = src.sin_vd_;
    this->cos_vd_ = src.cos_vd_;
  }

  return *this;
}

// ============================================================================
// SAData::SAData
// ============================================================================
SAData::SAData ()
  : SharedObject (),
    x_ (0),
    z_ (0),
    q_ (0),
    sum_ (0),
    va_ (0),
    vb_ (0),
    vc_ (0),
    vd_ (0),
    cx_ (0),
    cz_ (0)
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  , int_va_ (0),
    int_vb_ (0),
    int_vc_ (0),
    int_vd_ (0),
    int_sum_(0)
#endif
{
  ::memset(this->user_data_, 0, sizeof(UserData));
}

// ============================================================================
// SAData::SAData
// ============================================================================
SAData::SAData (const CSPI_SA_ATOM & _saa)
  : SharedObject (),
    x_ (_saa.X),
    z_ (_saa.Y),
    q_ (_saa.Q),
    sum_ (_saa.Sum),
    va_ (_saa.Va),
    vb_ (_saa.Vb),
    vc_ (_saa.Vc),
    vd_ (_saa.Vd),
    cx_ (_saa.Cx),
    cz_ (_saa.Cy)
#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  , int_va_ (_saa.Va),
    int_vb_ (_saa.Vb),
    int_vc_ (_saa.Vc),
    int_vd_ (_saa.Vd),
    int_sum_ (_saa.Sum)
#endif
{
	for (size_t i = 0, j = 0; i < 6; i++)
	{
		this->user_data_[j++] =
			static_cast<short>(_saa.reserved[i] & 0x0000FFFF);

		this->user_data_[j++] =
			static_cast<short>((_saa.reserved[i] & 0xFFFF0000) / 0x10000);
	}
}


// ============================================================================
// SAData::~SAData
// ============================================================================
SAData::~SAData (void)
{
  //- noop dtor
}

// ============================================================================
// SAHistory::SAHistory
// ============================================================================
SAHistory::SAHistory (void)
  : frozen_ (false),
    x_mean_ (_NAN_),
    z_mean_ (_NAN_),
    x_rms_ (_NAN_),
    z_rms_ (_NAN_),
    x_peak_ (_NAN_),
    z_peak_ (_NAN_),
    s_mean_ (_NAN_),
    actual_num_samples_(0)
{
  //- noop
}

// ============================================================================
// SAHistory::SAHistory
// ============================================================================
SAHistory::SAHistory (size_t _depth)
  : frozen_ (false),
    x_mean_ (_NAN_),
    z_mean_ (_NAN_),
    x_rms_ (_NAN_),
    z_rms_ (_NAN_),
    x_peak_ (_NAN_),
    z_peak_ (_NAN_),
    s_mean_ (_NAN_),
    actual_num_samples_(0)
{
  this->depth (_depth);
}

// ============================================================================
// SAHistory::~SAHistory
// ============================================================================
SAHistory::~SAHistory (void)
{
  //- noop dtor
}

// ============================================================================
// SAHistory::depth
// ============================================================================
void SAHistory::depth (size_t _depth)
  throw (Tango::DevFailed)
{
  bpm::MutexLock guard(this->lock_);

  if (this->frozen_)
    this->unfreeze_i();

  this->x_.depth (_depth);
  this->x_.fill (_NAN_);
  this->x_rms_ = _NAN_;
  this->x_mean_ = _NAN_;
  this->x_peak_ = _NAN_;

  this->z_.depth (_depth);
  this->z_.fill (_NAN_);
  this->z_rms_ = _NAN_;
  this->z_mean_ = _NAN_;
  this->z_peak_ = _NAN_;

  if (SAHistory::m_optional_data_enabled)
  {
    this->s_.depth (_depth);
    this->s_.fill (_NAN_);
    this->s_mean_ = _NAN_;
  }

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)
  this->int_x_.depth (_depth);
  this->int_z_.depth (_depth);
  if (SAHistory::m_optional_data_enabled)
    this->int_s_.depth (_depth);
#endif

  this->actual_num_samples_ = 0;
}

// ============================================================================
// SAHistory::compute_statistics
// ============================================================================
void SAHistory::compute_statistics (size_t _n)
{
  bpm::MutexLock guard(this->lock_);

  if (! this->actual_num_samples_)
    return;

// bpm::Timer t;

#if defined(_EMBEDDED_DEVICE_) && defined(_USE_ARM_OPTIMIZATION_)

  //- optimized alofo for the ARM platform
  //-----------------------------------------------------------
  i64 x_sum  = 0;
  i64 z_sum  = 0;
  i64 s_sum  = 0;
  u64 xx_sum = 0;
  u64 zz_sum = 0;

  const SAData::I32Buffer & x_data = this->int_x_.data();
  i32 x_lo_peak = x_data[0];
  i32 x_hi_peak = x_data[0];

  const SAData::I32Buffer & z_data = this->int_z_.data();
  i32 z_lo_peak = z_data[0];
  i32 z_hi_peak = z_data[0];

  //- get an iterator on the last <max_i> samples in the history
  I32HistoryIterator x_it = this->int_x_.past_iterator(_n);
  I32HistoryIterator z_it = this->int_z_.past_iterator(_n);
  I32HistoryIterator s_it = this->int_s_.past_iterator(SAHistory::m_optional_data_enabled ? _n : 0);

  size_t sample_counter = 0;

  //- x and z have same depth, so checking x.end_of_past() is enough
  //- to control the number of iterations in the while loop
  while (! x_it.end_of_past())
  {
    i32 xd = *x_it;
    i32 zd = *z_it;

    if (x_lo_peak > xd)
      x_lo_peak = xd;
    else if (xd > x_hi_peak)
      x_hi_peak = xd;

    x_sum += xd;
    xx_sum += (i64)xd * xd;

    if (z_lo_peak > zd)
      z_lo_peak = zd;
    else if (zd > z_hi_peak)
      z_hi_peak = zd;

    z_sum += zd;
    zz_sum += (i64)zd * zd;

    x_it++;
    z_it++;

    if (SAHistory::m_optional_data_enabled)
    {
      s_sum += *s_it;
      s_it++;
    }

    ++sample_counter;
  }

  if (sample_counter)
  {
    i32 x_mean = x_sum / (i64)sample_counter;
    NM_TO_MM(x_mean, &this->x_mean_);

    i32 z_mean = z_sum / (i64)sample_counter;
    NM_TO_MM(z_mean, &this->z_mean_);

    if (SAHistory::m_optional_data_enabled)
    {
      i32 s_mean = s_sum / (i64)sample_counter;
      NM_TO_MM(s_mean, &this->s_mean_);
    }

    //TODO: THIS IS PARTIAL OPTIMIZATION - TO BE IMPROVED IN A NEAR FUTURE
    //---------------------------------------------------------------------
    i64 x_rms_pow2 = kINV_NM_TO_MICRON_POW2*(xx_sum/ ((i64)sample_counter ) - (i64)x_mean * x_mean);
    i64 z_rms_pow2 = kINV_NM_TO_MICRON_POW2*(zz_sum/ ((i64)sample_counter ) - (i64)z_mean * z_mean);

    this->x_rms_ = static_cast<bpm::FPDataType>(::sqrt((double)x_rms_pow2));
    this->z_rms_ = static_cast<bpm::FPDataType>(::sqrt((double)z_rms_pow2));
  }
  else
  {
    this->x_mean_ = 0;
    this->z_mean_ = 0;
    this->s_mean_ = 0;
    this->x_rms_  = 0;
    this->z_rms_  = 0;
  }

  NM_TO_UM(x_hi_peak - x_lo_peak, &this->x_peak_);
  NM_TO_UM(z_hi_peak - z_lo_peak, &this->z_peak_);

  //- std::cout << "SAHistory::compute_statistics::optimized::x_mean: " << this->x_mean_ << std::endl;
  //- std::cout << "SAHistory::compute_statistics::optimized::z_mean: " << this->z_mean_ << std::endl;
  //- std::cout << "SAHistory::compute_statistics::optimized::s_mean: " << this->s_mean_ << std::endl;
  //- std::cout << "SAHistory::compute_statistics::optimized::x_peak: " << this->x_peak_ << std::endl;
  //- std::cout << "SAHistory::compute_statistics::optimized::z_peak: " << this->z_peak_ << std::endl;
  //- std::cout << "SAHistory::compute_statistics::optimized::x_rms : " << this->x_rms_  << std::endl;
  //- std::cout << "SAHistory::compute_statistics::optimized::z_rms : " << this->z_rms_  << std::endl;

#else // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

  //- pure floating point algorithm
  //-----------------------------------------------------------
  bpm::FPDataType x_sum  = 0.;
  bpm::FPDataType z_sum  = 0.;
  bpm::FPDataType s_sum  = 0.;
  bpm::FPDataType xx_sum = 0.;
  bpm::FPDataType zz_sum = 0.;

  const SAData::Buffer & x_data = this->x_.data();
  bpm::FPDataType x_lo_peak = x_data[0];
  bpm::FPDataType x_hi_peak = x_data[0];

  const SAData::Buffer & z_data = this->z_.data();
  bpm::FPDataType z_lo_peak = z_data[0];
  bpm::FPDataType z_hi_peak = z_data[0];

  //- get an iterator on the last <max_i> samples in the history
  HistoryIterator x_it = this->x_iterator(_n);
  HistoryIterator z_it = this->z_iterator(_n);
  HistoryIterator s_it = this->sum_iterator(SAHistory::m_optional_data_enabled ? _n : 0);

  size_t sample_counter = 0;

  //- x and z have same depth, so checking x.end_of_past() is enough
  //- to control the number of iterations in the while loop...
  while (! x_it.end_of_past())
  {
    bpm::FPDataType xd = *x_it;
    bpm::FPDataType zd = *z_it;

    if (x_lo_peak > xd)
      x_lo_peak = xd;
    else if (xd > x_hi_peak)
      x_hi_peak = xd;

    x_sum += xd;
    xx_sum += xd * xd;

    if (z_lo_peak > zd)
      z_lo_peak = zd;
    else if (zd > z_hi_peak)
      z_hi_peak = zd;

    z_sum += zd;
    zz_sum += zd * zd;

    x_it++;
    z_it++;

    if (SAHistory::m_optional_data_enabled)
    {
      s_sum += *s_it;
      s_it++;
    }

    ++sample_counter;
  }

  if (sample_counter)
  {
    this->x_mean_ = x_sum / sample_counter;

    this->z_mean_ = z_sum / sample_counter;

    this->s_mean_ = SAHistory::m_optional_data_enabled
                  ? s_sum / sample_counter
                  : 0;

    this->x_rms_ =
    ::sqrt(kMM_TO_MICRON_POW2 *
           (xx_sum/ (static_cast<bpm::FPDataType>(sample_counter) ) - (x_mean_ * x_mean_)));

    this->z_rms_ =
    ::sqrt(kMM_TO_MICRON_POW2 *
           (zz_sum / (static_cast<bpm::FPDataType>(sample_counter)) - (z_mean_ * z_mean_)));
  }
  else
  {
    this->x_mean_ = 0;
    this->z_mean_ = 0;
    this->s_mean_ = 0;
    this->x_rms_  = 0;
    this->z_rms_  = 0;
  }

  this->x_peak_ = kMM_TO_MICRON * (x_hi_peak - x_lo_peak);
  this->z_peak_ = kMM_TO_MICRON * (z_hi_peak - z_lo_peak);

/*
  std::cout << "SAHistory::compute_statistics::---std---::x_mean: " << this->x_mean_ << std::endl;
  std::cout << "SAHistory::compute_statistics::---std---::z_mean: " << this->z_mean_ << std::endl;
  std::cout << "SAHistory::compute_statistics::---std---::s_mean: " << this->s_mean_ << std::endl;
  std::cout << "SAHistory::compute_statistics::---std---::x_peak: " << this->x_peak_ << std::endl;
  std::cout << "SAHistory::compute_statistics::---std---::z_peak: " << this->z_peak_ << std::endl;
  std::cout << "SAHistory::compute_statistics::---std---::x_rms : " << this->x_rms_ << std::endl;
  std::cout << "SAHistory::compute_statistics::---std---::z_rms : " << this->z_rms_ << std::endl;
  std::cout << "SAHistory::compute_statistics::---std---::smpls : " << sample_counter << std::endl;
*/

#endif // _EMBEDDED_DEVICE_ && _USE_ARM_OPTIMIZATION_

//  std::cout << "BPM::SAHistory::compute_statistics::took "
//            << t.elapsed_msec()
//            << " msecs"
//            << std::endl;
}

// ============================================================================
// ADData::ADCData
// ============================================================================
ADCData::ADCData (void)
  : SharedObject (),
    a_ (0),
    b_ (0),
    c_ (0),
    d_ (0)
{
  //- noop
}

// ============================================================================
// ADCData::allocate
// ============================================================================
void ADCData::allocate (size_t num_samples, bool init_to_zero)
  throw (Tango::DevFailed)
{
  try
  {
    this->a_.depth(num_samples);
    if (init_to_zero) this->a_.fill(0);

    this->b_.depth(num_samples);
    if (init_to_zero) this->b_.fill(0);

    this->c_.depth(num_samples);
    if (init_to_zero) this->c_.fill(0);

    this->d_.depth(num_samples);
    if (init_to_zero) this->d_.fill(0);
  }
  catch (const std::bad_alloc &)
  {
    Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                    _CPTC ("memory allocation failed"),
                                    _CPTC ("ADCData::allocate"));
  }
  catch (const Tango::DevFailed& df)
  {
    Tango::Except::re_throw_exception (const_cast<Tango::DevFailed&>(df),
                                       _CPTC ("OUT_OF_MEMORY"),
                                       _CPTC ("memory allocation failed"),
                                       _CPTC ("ADCData::allocate"));
  }
  catch (...)
  {
    Tango::Except::throw_exception (_CPTC ("UNKNOWN_ERROR"),
                                    _CPTC ("unknown exception caught"),
                                    _CPTC ("ADCData::allocate"));
  }
}

// ============================================================================
// ADCData::ADCData
// ============================================================================
ADCData::~ADCData (void)
{
  //- noop dtor
}

// ============================================================================
// DDData::operator=
// ============================================================================
ADCData & ADCData::operator= (const ADCData& src)
{
  if (this == &src)
    return *this;

  if (this->current_buffers_depth() != src.current_buffers_depth())
    this->allocate(src.current_buffers_depth(), false);

  this->a_ = src.a_;
  this->b_ = src.b_;
  this->c_ = src.c_;
  this->d_ = src.d_;

  ::memcpy(&(this->timestamp_), &(src.timestamp_), sizeof(timespec));

  return *this;
}
} //- namespace bpm
