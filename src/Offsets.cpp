//------------------------------------------------------------------------------
// This file is part of the Libera Tango Device
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2008  Nicolas Leclercq, Synchrotron SOLEIL.
//
// Part of the code is copyright (C) 2005-2008 Michael Abbott, 
// Diamond Light Source Ltd. See ./ma/README for details. 
//
// The Libera Tango Device is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// The Libera Tango Device is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Contact:
//      Nicolas Leclercq
//      Synchrotron SOLEIL
//      libera-sofware<AT>esrf<DOT>fr
//------------------------------------------------------------------------------

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CommonHeader.h"
#include "StringTokenizer.h"
#include "Offsets.h"


#if !defined (__INLINE_IMPL__)
# include "Offsets.i"
#endif // __INLINE_IMPL__

// ============================================================================
// RETURN_PARAM_OR_THROW_EX
// ============================================================================
/*
#define RETURN_PARAM_OR_THROW_EXCEPT(_PARAMS_, _IDX_) \
case _IDX_: \
  if (_PARAMS_[_IDX_] != -1) return _PARAMS_[_IDX_]; \
  Tango::Except::throw_exception(_CPTC("SOFTWARE_FAILURE"), \
                                 _CPTC("invalid BPM parameter [undefined]"), \
                                 _CPTC("BPMOffsets::operator[]")); \
  break; \
*/
// ============================================================================
#define RETURN_PARAM(_IDX_) \
  case _IDX_: \
    return this->bpm_params[_IDX_]; \
    break;
// ============================================================================
#define RETURN_PARAM_NAME( _IDX_) \
  case _IDX_: \
    return BPMOffsets::bpm_params_names[_IDX_]; \
    break;
// ============================================================================
#define LOG_PARAM(_TYPE_, _IDX_) \
// std::cout << _TYPE_ 
//           << BPMOffsets::bpm_params_names[_IDX_] 
//           << "::" 
//           << this->bpm_params[_IDX_] 
//           << std::endl;

// ============================================================================
#define SEP ":"
// ----------------------------------------------------------------------------
#define NUM_BLOCK_PARAMS   (Z_ALARM - GEOMETRY + 1)
#define NUM_HW_PARAMS      (Z_OFFSET_5 - KX + 1)
// ----------------------------------------------------------------------------
#define DEVICE_PARAMS_PROPERTY_NUM_TOKENS  3
#define KX_KZ_PARAMS_PROPERTY_NUM_TOKENS   3
#define BLOCK_PARAMS_PROPERTY_NUM_TOKENS   (NUM_BLOCK_PARAMS + 1)
#define HW_PARAMS_PROPERTY_NUM_TOKENS      (NUM_HW_PARAMS - 2 + 1)
// ----------------------------------------------------------------------------

namespace
      bpm
{

// ============================================================================
// BPM parameter names
// ============================================================================
const char *
BPMOffsets::bpm_params_names[] =
  {
    //- hw params
    "GEOMETRY",
    "Q_OFFSET_1",
    "A_OFFSET_E",
    "B_OFFSET_E",
    "C_OFFSET_E",
    "D_OFFSET_E",
    "X_OFFSET_1",
    "X_OFFSET_BBA",
    "Z_OFFSET_1",
    "Z_OFFSET_BBA",
    "X_LOW",
    "Z_LOW",
    "X_HIGH",
    "Z_HIGH",
    "X_WARN",
    "Z_WARN",
    "X_ALARM",
    "Z_ALARM",
    //- hw params
    "KX",
    "KZ",
    "Q_OFFSET_2",
    "A_OFFSET_C",
    "B_OFFSET_C",
    "C_OFFSET_C",
    "D_OFFSET_C",
    "X_OFFSET_3",
    "RESERVED_1",
    "RESERVED_2",
    "X_OFFSET_4",
    "X_OFFSET_5",
    "Z_OFFSET_3",
    "RESERVED_3",
    "RESERVED_4",
    "Z_OFFSET_4",
    "Z_OFFSET_5"
  };

// ============================================================================
// BPMOffsets::BPMOffsets
// ============================================================================
BPMOffsets::BPMOffsets (void)
{
  this->device = 0;
  this->block_id = "unspecified";
  this->hw_id = "unspecified";
  this->bpm_params.resize (LAST_PARAMETER);
  this->bpm_params.assign (this->bpm_params.size (), 0);
}

// ============================================================================
// BPMOffsets::BPMOffsets
// ============================================================================
BPMOffsets::BPMOffsets (const BPMOffsets & _src)
{
  //- delegate copy to operator=
  *this = _src;
}

// ============================================================================
// BPMOffsets::~BPMOffsets
// ============================================================================
BPMOffsets::~BPMOffsets (void)
{
  //- noop dtor
}

// ============================================================================
// BPMOffsets::operator=
// ============================================================================
BPMOffsets & BPMOffsets::operator= (const BPMOffsets & _src)
{
  //- avoid self copy
  if (&_src == this)
    return *this;
  this->device = _src.device;
  this->block_id = _src.block_id;
  this->hw_id = _src.hw_id;
  this->bpm_params = _src.bpm_params;
  return *this;
}

// ============================================================================
// BPMOffsets::operator==
// ============================================================================
bool BPMOffsets::operator== (const BPMOffsets & _src)
{
  return this->bpm_params == _src.bpm_params;
}

// ============================================================================
// BPMOffsets::operator!=
// ============================================================================
bool BPMOffsets::operator!= (const BPMOffsets & _src)
{
  return this->bpm_params != _src.bpm_params;
}

// ============================================================================
// BPMOffsets::read_from_tango_db
// ============================================================================
void
BPMOffsets::read_from_tango_db (BPMLocation _location, Tango::DeviceImpl * _device)
throw (Tango::DevFailed)
{
  //- store device locally
  this->device = _device;

  //- be sure device is valid
  if (this->device == 0)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("unexpected null parameter"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
  }

  //- obtain a reference to the TANGO database
  Tango::Database * db = device->get_db_device ()->get_dbase ();
  if (db == 0)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("unexpected null reference to TANGO database"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
  }

  //----------------------------------------------------------------------
  //- get kx & kz - SOLEIL system property: KxKzParameters
  //----------------------------------------------------------------------
  // this system property is a vector of strings (one entry per device)
  // for a given device the string contains the location id and the kx
  // and kz parameters. The following syntax is used:
  // <location>:kx:kz
  //----------------------------------------------------------------------
  bool found = false;
  Tango::DbData bpm_dbdata;
  try
  {
    //- get value from database
    bpm_dbdata.push_back (Tango::DbDatum (KX_KZ_PARAMS_PROPERTY));
    db->get_property (BPM_ROOT_PROPERTY, bpm_dbdata);
  }
  catch (Tango::DevFailed & df)
  {
    Tango::Except::re_throw_exception (df, 
                                       _CPTC ("SOFTWARE_FAILURE"), 
                                       _CPTC ("TANGO exception caught while trying to read BPM Kx/Kz parameters [system property]"), 
                                       _CPTC ("BPMOffsets::read_from_tango_db"));
  }
  //- set location we search
  std::string searched_location;
  switch (_location)
  {
  case BPM_LOC_TL1:
    searched_location = LOCATION_TL1;
    break;
  case BPM_LOC_BOOSTER:
    searched_location = LOCATION_BOOSTER;
    break;
  case BPM_LOC_TL2:
    searched_location = LOCATION_TL2;
    break;
  case BPM_LOC_STORAGE_RING:
    searched_location = LOCATION_STORAGE_RING;
    break;
  case BPM_LOC_UNKNOWN:
    Tango::Except::throw_exception (_CPTC ("unexpected location"), 
                                    _CPTC ("unknown BPM location"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
    break;
  }
  //- tmp log
  //- std::cout << "BPM::KxKzParameters::searched_location::" << searched_location << std::endl;
  //- extract value as a vector of strings
  std::vector < std::string > bpm_params_prop;
  if ((bpm_dbdata[0] >> bpm_params_prop) == false)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("TANGO exception caught while trying to extract data from BPM Kx/Kz parameters [system property]"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
  }
  //- get properties for this->device
  for (size_t i = 0; i < bpm_params_prop.size (); i++)
  {
    StringTokenizer
    tokens (bpm_params_prop[i], SEP);
    //- tmp log
    //- std::cout << "BPM::KxKzParameters::entry-" << i << "::" << bpm_params_prop[i] << std::endl;
    //- get number of tokens in current entry
    int
    num_tokens = tokens.countTokens ();
    //- tmp log
    //- std::cout << "BPM::KxKzParameters::entry-" << i << " contains " << num_tokens << " tokens" << std::endl;
    //- token-1: device name
    std::string location = tokens.nextToken ();
    if (location == searched_location)
    {
      //- tokens should cointains 3 tokens
      if (num_tokens != KX_KZ_PARAMS_PROPERTY_NUM_TOKENS)
      {
        Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                        _CPTC ("invalid BPM Kx/Kz parameters [system property syntax error]"), 
                                        _CPTC ("BPMOffsets::read_from_tango_db"));
      }
      //- mark as found
      found = true;
      //- token-2: kx
      this->bpm_params[KX] = tokens.nextFPToken ();
      //- tmp log
      //- std::cout << "BPM::KxKzParameters::entry-" << i << "::Kx:" << this->bpm_params[KX] << std::endl;
      //- token-2: kz
      this->bpm_params[KZ] = tokens.nextFPToken ();
      //- tmp log
      //- std::cout << "BPM::KxKzParameters::entry-" << i << "::Kz:" << this->bpm_params[KZ] << std::endl;
      //- found... so abort
      break;
    }
  }
  //- could not found device parameters for this->device
  if (!found)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("no BPM Kx/Kz parameters found for this location"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
  }

  //----------------------------------------------------------------------
  //- get block id - SOLEIL system property: DeviceParameters
  //----------------------------------------------------------------------
  // this system property is a vector of strings (one entry per device)
  // for a given device the string contains the blcok id and the libera
  // serial number. The following syntax is used:
  // <dev-name-prefix/xxx/xxx>:block-id:serial-num
  //----------------------------------------------------------------------
  found = false;
  try
  {
    //- get value from database
    bpm_dbdata.clear ();
    bpm_dbdata.push_back (Tango::DbDatum (DEVICE_PARAMS_PROPERTY));
    db->get_property (BPM_ROOT_PROPERTY, bpm_dbdata);
  }
  catch (Tango::DevFailed & df)
  {
    Tango::Except::re_throw_exception (df, 
                                      _CPTC ("SOFTWARE_FAILURE"), 
                                      _CPTC ("TANGO exception caught while trying to read BPM device parameters [system property]"), 
                                      _CPTC ("BPMOffsets::read_from_tango_db"));
  }
  //- extract value as a vector of strings
  bpm_params_prop.clear ();
  if ((bpm_dbdata[0] >> bpm_params_prop) == false)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("TANGO exception caught while trying to extract data from BPM device parameters [system property]"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
  }
  //- actual device name
  std::string adn = this->device->name ();
  std::transform (adn.begin (), adn.end (), adn.begin (),::tolower);
  //- get properties for this->device
  for (size_t i = 0; i < bpm_params_prop.size (); i++)
  {
    StringTokenizer
    tokens (bpm_params_prop[i], SEP);
    //- tmp log
    //- std::cout << "BPM::DeviceParameters::entry-" << i << "::" << bpm_params_prop[i] << std::endl;
    //- get number of tokens in current entry
    int
    num_tokens = tokens.countTokens ();
    //- tmp log
    //- std::cout << "BPM::DeviceParameters::entry-" << i << " contains " << num_tokens << " tokens" << std::endl;
    //- token-1: device name
    std::string tdn = tokens.nextToken ();
    std::transform (tdn.begin (), tdn.end (), tdn.begin (),::tolower);
    if (tdn == adn)
    {
      //- tokens should cointains LAST_DEVICE_PARAMETER tokens
      if (num_tokens != DEVICE_PARAMS_PROPERTY_NUM_TOKENS)
      {
        Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                        _CPTC ("invalid BPM device parameters [system property syntax error]"), 
                                        _CPTC ("BPMOffsets::read_from_tango_db"));
      }
      //- mark as found
      found = true;
      //- token-2: block id
      this->block_id = tokens.nextToken ();
      //- tmp log
      //- std::cout << "BPM::DeviceParameters::entry-" << i << "::block-id:" << this->block_id << std::endl;
      //- token-3: libera hw id
      this->hw_id = tokens.nextToken ();
      //- tmp log
      //- std::cout << "BPM::DeviceParameters::entry-" << i << "::hw-serial#:" << this->hw_id << std::endl;
      //- found... so abort
      break;
    }
  }
  //- could not found device parameters for this->device
  if (!found)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("no BPM device parameters found in system property"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
  }

  //----------------------------------------------------------------------
  //- get block parameters - SOLEIL system property: BlockParameters
  //----------------------------------------------------------------------
  // this system property is a vector of strings (one entry per device)
  // for a given device the string contains all the block parmeters. The
  // following syntax is used:
  // block-id:p1-name:p1-value:p1-name:p2-value:....
  //----------------------------------------------------------------------
  found = false;
  try
  {
    //- get value from database
    bpm_dbdata.clear ();
    bpm_dbdata.push_back (Tango::DbDatum (BLOCK_PARAMS_PROPERTY));
    db->get_property (BPM_ROOT_PROPERTY, bpm_dbdata);
  }
  catch (Tango::DevFailed & df)
  {
    Tango::Except::re_throw_exception (df, 
                                       _CPTC ("SOFTWARE_FAILURE"), 
                                       _CPTC ("TANGO exception caught while trying to read BPM block parameters [system property]"), 
                                       _CPTC ("BPMOffsets::read_from_tango_db"));
  }
  //- extract value as a vector of strings
  bpm_params_prop.clear ();
  if ((bpm_dbdata[0] >> bpm_params_prop) == false)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("TANGO exception caught while trying to extract data from BPM block parameters [system property]"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
  }
  //- get properties for this->device
  for (size_t i = 0; i < bpm_params_prop.size (); i++)
  {
    StringTokenizer
    tokens (bpm_params_prop[i], SEP);
    //- tmp log
    //- std::cout << "BPM::BlockParameters::entry-" << i << "::" << bpm_params_prop[i] << std::endl;
    //- get number of tokens in current entry
    int
    num_tokens = tokens.countTokens ();
    //- tmp log
    //- std::cout << "BPM::BlockParameters::entry-" << i << " contains " << num_tokens << " tokens" << std::endl;
    //- token-1: block id
    std::string bid = tokens.nextToken ();
    if (bid == this->block_id)
    {
      //- tokens should cointains [LAST_BLOCK_PARAMETER + 1] tokens
      if (num_tokens != BLOCK_PARAMS_PROPERTY_NUM_TOKENS)
      {
        Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                        _CPTC ("invalid BPM block parameters [system property syntax error]"), 
                                        _CPTC ("BPMOffsets::read_from_tango_db"));
      }
      //- mark as found
      found = true;
      //- get block parameter
      size_t
      max = GEOMETRY + NUM_BLOCK_PARAMS;
      for (size_t p = GEOMETRY; p < max; p++)
      {
        //- store parameter in local vector
        this->bpm_params[p] = (BPMOffsets::FpOffsetsType) tokens.nextFPToken();
        //- tmp log
        LOG_PARAM ("BLOCK::", p);
      }
      //- found... so abort
      break;
    }
  }

  //----------------------------------------------------------------------
  //- get hw parameters - SOLEIL system property: BPMHwParameters
  //----------------------------------------------------------------------
  // this system property is a vector of strings (one entry per device)
  // for a given device the string contains all the libera hw parmeters.
  // The following syntax is used:
  // libera-hw-serial-num:p1-name:p1-value:p1-name:p2-value:....
  //----------------------------------------------------------------------
  found = false;
  try
  {
    //- get value from database
    bpm_dbdata.clear ();
    bpm_dbdata.push_back (Tango::DbDatum (HW_PARAMS_PROPERTY));
    db->get_property (BPM_ROOT_PROPERTY, bpm_dbdata);
  }
  catch (Tango::DevFailed & df)
  {
    Tango::Except::re_throw_exception (df, 
                                       _CPTC ("SOFTWARE_FAILURE"), 
                                       _CPTC ("TANGO exception caught while trying to read BPM hardware parameters [system property]"), 
                                       _CPTC ("BPMOffsets::read_from_tango_db"));
  }
  //- extract value as a vector of strings
  bpm_params_prop.clear ();
  if ((bpm_dbdata[0] >> bpm_params_prop) == false)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("TANGO exception caught while trying to extract data from BPM hardware parameters [system property]"), 
                                    _CPTC ("BPMOffsets::read_from_tango_db"));
  }
  //- get properties for this->device
  for (size_t i = 0; i < bpm_params_prop.size (); i++)
  {
    StringTokenizer
    tokens (bpm_params_prop[i], SEP);
    //- tmp log
    //- std::cout << "BPM::HwParameters::entry-" << i << "::" << bpm_params_prop[i] << std::endl;
    //- get number of tokens in current entry
    int
    num_tokens = tokens.countTokens ();
    //- tmp log
    //- std::cout << "BPM::HwParameters::entry-" << i << " contains " << num_tokens << " tokens" << std::endl;
    //- token-1: libera hw id
    std::string hwid = tokens.nextToken ();
    if (hwid == this->hw_id)
    {
      //- tokens should cointains HW_PARAMS_PROPERTY_NUM_TOKENS
      if (num_tokens != HW_PARAMS_PROPERTY_NUM_TOKENS)
      {
        Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                        _CPTC ("invalid BPM hardware parameters [system property syntax error]"), 
                                        _CPTC ("BPMOffsets::read_from_tango_db"));
      }
      //- mark as found
      found = true;
      //- get block parameter
      size_t
      max = Q_OFFSET_2 + NUM_HW_PARAMS - 2;
      for (size_t p = Q_OFFSET_2; p < max; p++)
      {
        //- store parameter in local vector
        this->bpm_params[p] = 
                static_cast<BPMOffsets::FpOffsetsType>(tokens.nextFPToken ());
        //- tmp log
        LOG_PARAM ("HW::", p);
      }
      //- found... so abort
      break;
    }
  }

}

// ============================================================================
// BPMOffsets::operator[]
// ============================================================================
BPMOffsets::FpOffsetsType BPMOffsets::operator[](int idx) const
throw (Tango::DevFailed)
{
  if (idx < GEOMETRY || idx >= LAST_PARAMETER)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("unknown BPM parameter [invalid id specified]"), 
                                    _CPTC ("BPMOffsets::operator[]"));
  }
  return this->bpm_params[idx];
}

// ============================================================================
// BPMOffsets::parameter_name
// ============================================================================
const char *
BPMOffsets::parameter_name (int idx) const
throw (Tango::DevFailed)
{
  if (idx < GEOMETRY || idx >= LAST_PARAMETER)
  {
    Tango::Except::throw_exception (_CPTC ("SOFTWARE_FAILURE"), 
                                    _CPTC ("unknown BPM parameter [invalid id specified]"), 
                                    _CPTC ("BPMOffsets::parameter_name"));
  }
  return BPMOffsets::bpm_params_names[idx];
}

}                               // namespace bpm
