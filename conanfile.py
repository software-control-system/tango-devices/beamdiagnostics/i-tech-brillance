from conan import ConanFile

class BPMLiberaRecipe(ConanFile):
    name = "bpmlibera"
    executable = "ds_BPMLibera"
    version = "1.3.1"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Nicolas Leclercq"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/beamdiagnostics/i-tech-brillance.git"
    description = "BPMLibera Tango Device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("liberaclient/1.0.0@soleil/stable")
        self.requires("liberacspi/1.0.0@soleil/stable")
        self.requires("liberadrivers/1.0.0@soleil/stable")
        self.requires("cpptango/9.2.5@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
